namespace HappyGames.Utilities
{
    using HappyGames.Data;
    using UnityEngine;

    public class CursorManager : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The cursors for the different situations.")]
        protected CursorSet cursors = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The currently used cursor.
        /// </summary>
        public static CursorType CurrentCursor { get; protected set; } = CursorType.Default;

        /// <summary>
        /// The name of the custom cursor.
        /// </summary>
        public static string CustomCursorName { get; protected set; } = UNUSED_CUSTOMCURSOR_NAME;

        /// <summary>
        /// The name to set the <see cref="CustomCursorName"/> when no custom cursor is set.
        /// </summary>
        protected const string UNUSED_CUSTOMCURSOR_NAME = "None";

        protected ProfileSettings settings;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            if (ProfileManager.HasActiveProfile)
            {
                SwitchProfile(null, ProfileManager.GetActiveProfile());
            }
            else
            {
                SwitchProfile(null, new UserProfile(string.Empty));
            }
            ProfileManager.OnProfileActivated += SwitchProfile;
        }

        private void OnDisable()
        {
            ProfileManager.OnProfileActivated -= SwitchProfile;
            if (ProfileManager.HasActiveProfile)
            {
                SwitchProfile(ProfileManager.GetActiveProfile(), null);
            }
        }

        /// <summary>
        /// Changes the saved setting to the new ones.
        /// </summary>
        /// <param name="oldProfile">The deactivated profile.</param>
        /// <param name="newProfile">The activated profile.</param>
        protected void SwitchProfile(UserProfile oldProfile, UserProfile newProfile)
        {
            if (oldProfile != null)
            {
                settings.OnCursorScaleUpdated -= SetScaleFromSettings;
                settings = null;
            }
            if (newProfile != null)
            {
                settings = newProfile.Settings;
                SetScale(settings.CursorSize, false, false);
                CursorSet.CursorData cursor = cursors.GetCursor(CurrentCursor);
                Cursor.SetCursor(cursor.Texture, cursor.Hotspot, CursorMode.ForceSoftware);
                settings.OnCursorScaleUpdated += SetScaleFromSettings;
            }
        }

        /// <summary>
        /// Sets the cursors apperience.
        /// </summary>
        /// <param name="type">The type to use.</param>
        public void SetCursor(CursorType type)
        {
            if (CurrentCursor != type && type != CursorType.Custom)
            {
                if (CurrentCursor == CursorType.Custom)
                {
                    CustomCursorName = UNUSED_CUSTOMCURSOR_NAME;
                }
                CursorSet.CursorData cursor = cursors.GetCursor(type);
                Cursor.SetCursor(cursor.Texture, cursor.Hotspot, CursorMode.ForceSoftware);
                CurrentCursor = type;
            }
        }

        /// <summary>
        /// Sets the cursor to a custom one.
        /// </summary>
        /// <param name="name">The name of the cursor.</param>
        /// <param name="sprite">The sprite to use.</param>
        /// <param name="hotspot">Where on the mouse position to place the sprite.</param>
        public void SetCustomCursor(string name, Texture2D sprite, Vector2 hotspot)
        {
            CustomCursorName = name;
            CurrentCursor = CursorType.Custom;
            Cursor.SetCursor(sprite, hotspot, CursorMode.Auto);
        }
        /// <summary>
        /// Sets the cursor to a custom one.
        /// </summary>
        /// <param name="cursor">The cursor to display.</param>
        public void SetCustomCursor(CustomCursor cursor) => SetCustomCursor(cursor.Name, cursor.Sprite, cursor.Hotspot);

        /// <summary>
        /// Sets the scale of the cursor.
        /// </summary>
        /// <param name="scale">The sacle to apply.</param>
        /// <param name="checkForChange">True if a check for a change is nesseccary.</param>
        /// <param name="updateSettings">True if the settings should be updated.</param>
        protected void SetScale(float scale, bool checkForChange, bool updateSettings)
        {
            if (!checkForChange || settings.CursorSize != scale)
            {
                foreach (CursorSet.CursorData data in cursors)
                {
                    data.SetScale(scale);
                }
                if (updateSettings)
                {
                    settings.UpdateCursorScale(scale);
                }
            }
            CursorSet.CursorData cursor = cursors.GetCursor(CurrentCursor);
            Cursor.SetCursor(cursor.Texture, cursor.Hotspot, CursorMode.ForceSoftware);
        }
        /// <summary>
        /// Sets the scale of the cursor.
        /// </summary>
        /// <param name="scale">The sacle to apply.</param>
        public void SetScale(float scale) => SetScale(scale, true, true);

        /// <summary>
        /// Updates the scale of the cursor without updating the settings.
        /// </summary>
        /// <param name="scale">The scale to apply.</param>
        protected void SetScaleFromSettings(float scale) => SetScale(scale, true, false);

        /// <summary>
        /// Resets the custom cursor.
        /// </summary>
        public void ResetCursor() => SetCursor(CursorType.Default);


        // --- | Classes | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// The type of cursor to display.
        /// </summary>
        public enum CursorType 
        {
            /// <summary>
            /// Default for any other situation.
            /// </summary>
            [Tooltip("Default for any other situation.")]
            Default,
            /// <summary>
            /// Set to a custom cursor.
            /// </summary>
            [Tooltip("Set to a custom cursor.")]
            Custom,
            /// <summary>
            /// Interacting with a none emotion interaction.
            /// </summary>
            [Tooltip("Interacting with a none emotion interaction.")]
            Interact,
            /// <summary>
            /// Navigating in the hubworld.
            /// </summary>
            [Tooltip("Navigating in the hubworld.")]
            Navigate,
            /// <summary>
            /// Emotion interaction without for any emotion.
            /// </summary>
            [Tooltip("Emotion interaction without for any emotion.")]
            Emotion, 
            /// <summary>
            /// Emotion interactions of the angry type.
            /// </summary>
            [Tooltip("Emotion interactions of the angry type.")]
            Angry,
            /// <summary>
            /// Emotion interactions of the happy type.
            /// </summary>
            [Tooltip("Emotion interactions of the happy type.")]
            Happy,
            /// <summary>
            /// Emotion interactions of the sad type.
            /// </summary>
            [Tooltip("Emotion interactions of the sad type.")]
            Sad,
            /// <summary>
            /// Emotion interactions of the surprised type.
            /// </summary>
            [Tooltip("Emotion interactions of the surprised type.")]
            Surprised 
        }
    }
}
