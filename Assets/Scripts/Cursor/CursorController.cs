namespace HappyGames.Utilities
{
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.EventSystems;

    public class CursorController : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The layers for the navmesh.")]
        protected LayerMask navLayers = default;

        [SerializeField, Tooltip("The layers for the interactables.")]
        protected LayerMask interactiveLayers = default;

        [Space, SerializeField, Tooltip("Called when a new cursor should be set.")]
        protected UnityEvent<CursorManager.CursorType> onCursorChange = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected Collider2D currHit, prevHit;
        protected new Camera camera = null;
        /// <summary>
        /// The current camera.
        /// </summary>
        protected Camera Camera
        {
            get
            {
                if (camera == null)
                {
                    camera = Camera.main;
                }
                return camera;
            }
        }
        protected EventSystem eventSystem;
        /// <summary>
        /// The active event system.
        /// </summary>
        protected EventSystem EventSystem => eventSystem ??= EventSystem.current;


        // --- | Inspector | -----------------------------------------------------------------------------------------------

        private void Update()
        {
            if (!EventSystem.IsPointerOverGameObject())
            {
                Vector2 point = Camera.ScreenToWorldPoint(Input.mousePosition);
                currHit = Physics2D.OverlapPoint(point, interactiveLayers);
                if (currHit)
                {
                    if (currHit != prevHit)
                    {
                        Interactable interactable = currHit.GetComponent<Interactable>();
                        if (interactable)
                        {
                            if (interactable.reactTo.Length == 1)
                            {
                                switch (interactable.reactTo[0])
                                {
                                    default: onCursorChange?.Invoke(CursorManager.CursorType.Emotion); break;
                                    case Emotion.EmotionType.Angry: onCursorChange?.Invoke(CursorManager.CursorType.Angry); break;
                                    case Emotion.EmotionType.Happy: onCursorChange?.Invoke(CursorManager.CursorType.Happy); break;
                                    case Emotion.EmotionType.Sad: onCursorChange?.Invoke(CursorManager.CursorType.Sad); break;
                                    case Emotion.EmotionType.Surprised: onCursorChange?.Invoke(CursorManager.CursorType.Surprised); break;
                                }
                            }
                            else
                            {
                                onCursorChange?.Invoke(CursorManager.CursorType.Emotion);
                            }
                        }
                        else
                        {
                            onCursorChange?.Invoke(CursorManager.CursorType.Interact);
                        }
                        prevHit = currHit;
                    }
                    return;
                }
                currHit = Physics2D.OverlapPoint(point, navLayers);
                if (currHit)
                {
                    onCursorChange?.Invoke(CursorManager.CursorType.Navigate);
                    prevHit = null;
                    return;
                }
            }
            onCursorChange?.Invoke(CursorManager.CursorType.Default);
        }
    }
}
