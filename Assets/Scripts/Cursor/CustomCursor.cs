namespace HappyGames.Data
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "CustomCursor", menuName = "Cursor/Custom")]
    public class CustomCursor : ScriptableObject
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The name of the cursor.")]
        private new string name = default;
        /// <summary>
        /// The name of the cursor.
        /// </summary>
        public string Name => name;

        [SerializeField, Tooltip("The sprite to display.")]
        private Texture2D sprite = default;
        /// <summary>
        /// The sprite to display.
        /// </summary>
        public Texture2D Sprite => sprite;

        [SerializeField, Tooltip("The origin of the cursor.")]
        private Vector2 hotspot = default;
        /// <summary>
        /// The origin of the cursor.
        /// </summary>
        public Vector2 Hotspot => hotspot;
    }
}
