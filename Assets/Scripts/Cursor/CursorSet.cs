namespace HappyGames.Data
{
    using HappyGames.Utilities;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(fileName = "CursorSet", menuName = "Cursor/Set")]
    public class CursorSet : ScriptableObject, IEnumerable<CursorSet.CursorData> 
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The default cursor.")]
        protected CursorData standard = default;
        /// <summary>
        /// The default cursor.
        /// </summary>
        public CursorData Dafault => standard;

        [SerializeField, Tooltip("The cursor for interactions.")]
        protected CursorData interact = default;
        /// <summary>
        /// The cursor for interactions.
        /// </summary>
        public CursorData Interact => interact;

        [SerializeField, Tooltip("The cursor for navigating.")]
        protected CursorData navigate = default;
        /// <summary>
        /// The cursor for navigating.
        /// </summary>
        public CursorData Navigate => navigate;

        [SerializeField, Tooltip("The cursor for unspecified emotion interactions.")]
        protected CursorData emotion = default;
        /// <summary>
        /// The cursor for unspecified emotion interactions.
        /// </summary>
        public CursorData Emotion => emotion;

        [SerializeField, Tooltip("The cursor for an angry interactions.")]
        protected CursorData angry = default;
        /// <summary>
        /// The cursor for an angry interactions.
        /// </summary>
        public CursorData Angry => angry;

        [SerializeField, Tooltip("The cursor for a happy interactions.")]
        protected CursorData happy = default;
        /// <summary>
        /// The cursor for a happy interactions.
        /// </summary>
        public CursorData Happy => happy;

        [SerializeField, Tooltip("The cursor for a sad interactions.")]
        protected CursorData sad = default;
        /// <summary>
        /// The cursor for a sad interactions.
        /// </summary>
        public CursorData Sad => sad;

        [SerializeField, Tooltip("The cursor for a surprised interactions.")]
        protected CursorData surprised = default;
        /// <summary>
        /// The cursor for a surprised interactions.
        /// </summary>
        public CursorData Surprised => surprised;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The default scale of the cursor.
        /// </summary>
        protected const float DEFAULT_SCALE = 1f;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns the cursor <see cref="Sprite"/> for the given <see cref="CursorManager.CursorType"/>.
        /// </summary>
        /// <param name="type">The type of the cursor.</param>
        /// <returns>The <see cref="Sprite"/> for the <see cref="CursorManager.CursorType"/>.</returns>
        public CursorData GetCursor(CursorManager.CursorType type)
        {
            switch (type)
            {
                default: return null;
                case CursorManager.CursorType.Default: return standard;
                case CursorManager.CursorType.Interact: return interact;
                case CursorManager.CursorType.Navigate: return navigate;
                case CursorManager.CursorType.Emotion: return emotion;
                case CursorManager.CursorType.Angry: return angry;
                case CursorManager.CursorType.Happy: return happy;
                case CursorManager.CursorType.Sad: return sad;
                case CursorManager.CursorType.Surprised: return surprised;
            }
        }

        /// <inheritdoc/>
        public IEnumerator<CursorData> GetEnumerator()
        {
            yield return standard;
            yield return interact;
            yield return navigate;
            yield return emotion;
            yield return angry;
            yield return happy;
            yield return sad;
            yield return surprised;
        }

        /// <inheritdoc/>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();


        // --- | Classes | -------------------------------------------------------------------------------------------------

        [System.Serializable]
        public class CursorData
        {
            // Inspector --------------------------------------------------------------------------

            [SerializeField, Tooltip("The sprite to display.")]
            protected Texture2D texture = default;
            /// <summary>
            /// The sprite to display.
            /// </summary>
            public Texture2D Texture => scaledTex ??= SetScale(texture, DEFAULT_SCALE);

            [SerializeField, Tooltip("The pivot of the sprites image.")]
            protected Vector2 hotspot = default;
            /// <summary>
            /// The pivot of the sprites image.
            /// </summary>
            public Vector2 Hotspot => hotspot;

            // Variables --------------------------------------------------------------------------

            [System.NonSerialized]
            protected Texture2D scaledTex = null;

            // Methods ----------------------------------------------------------------------------

            /// <summary>
            /// Sets the scle of the texture.
            /// </summary>
            /// <param name="scale">The scale to apply.</param>
            public void SetScale(float scale)
            {
                scaledTex = SetScale(texture, scale);
            }    
            
            /// <summary>
            /// Creates a new scaled version of a <see cref="Texture2D"/>. 
            /// </summary>
            /// <param name="original">The original texture.</param>
            /// <param name="scale">The scale to apply.</param>
            /// <returns>The scaled texture.</returns>
            protected static Texture2D SetScale(Texture2D original, float scale)
            {
                /*Texture2D clone = Instantiate(original);
                if (scale != 1f)
                {
                    clone.Resize(Mathf.RoundToInt(original.width * scale), Mathf.RoundToInt(original.height * scale));
                    clone.Apply();
                }
                return clone;
                */
                int targetWidth = Mathf.RoundToInt(original.width * scale);
                int targetHeight = Mathf.RoundToInt(original.height * scale);
                Texture2D result = new Texture2D(targetWidth, targetHeight, original.format, false);
                Color[] rpixels = result.GetPixels(0);
                float incX = (1.0f / targetWidth);
                float incY = (1.0f / targetHeight);
                for (int px = 0; px < rpixels.Length; px++)
                {
                    rpixels[px] = original.GetPixelBilinear(incX * ((float)px % targetWidth), incY * ((float)Mathf.Floor(px / targetWidth)));
                }
                result.SetPixels(rpixels, 0);
                result.Apply();
                return result;
            }
        }
    }
}
