using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balloon : MonoBehaviour
{
    public float speed = 5;
    private BalloonSpawner spawner;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector3(0, speed*Time.deltaTime, 0));
        if(transform.position.y > spawner.maxXY.position.y)
        {
           // Destroy(this.gameObject);
        }
    }

    public void setSpawner(BalloonSpawner s)
    {
        spawner = s;
    }
}
