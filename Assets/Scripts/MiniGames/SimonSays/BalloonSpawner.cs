using HappyGames.Emotion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalloonSpawner : MonoBehaviour
{
    public Transform minXY, maxXY;
    public Balloon[] balloons;
    private EmotionType nextEmotion;

    public void SpawnBalloon()
    {
        float x = Random.Range(minXY.position.x, maxXY.position.x);
        Debug.Log("balloon spawned type= " +nextEmotion);
        Balloon b = Instantiate(balloons[(int)nextEmotion], new Vector3(x, minXY.position.y, 0), Quaternion.identity);
        b.setSpawner(this);
        Debug.Log(b);
    }

    public void SetEmotion(EmotionType emotion)
    {
        nextEmotion = emotion;
    }
}
