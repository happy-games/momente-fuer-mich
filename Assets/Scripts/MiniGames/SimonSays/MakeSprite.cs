namespace HappyGames.SimonSays
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class MakeSprite : MonoBehaviour
    {
        public Sprite[] spriteArray;

        private List<GameObject> dots;

        [SerializeField, Tooltip("Unlit Material")]
        private Material unlitMaterial;

        [SerializeField, Tooltip("y position of the circles")]
        private int yPosition = 6;

        [SerializeField, Tooltip("scale of the circles")]
        private float scale = 0.8f;

        [SerializeField, Tooltip("space along the x axis between the circles")]
        private float space = 0.8f;

        public void Start()
        {
            dots = new List<GameObject>();
        }

        public void deleteDots()
        {
            foreach(GameObject go in dots)
            {
                Destroy(go);
            }
        }

        public void createDots(int number)
        {
            if(dots.Count > 0)
            {
                deleteDots();
            }
            dots.Clear();

            float y = yPosition;
            for (int i = 0, x=0; i < number; i++, x++)
            {
                if(i % 6 == 0 && i!= 0)
                {
                    x = 0;
                    y -= space*2;
                }
                dots.Add(makeSprite("Sprite" + i, new Vector3((x+space*x)-(number/2*space+space/2), y, 0)));
            }
        }

        public GameObject makeSprite(string name, Vector3 position)
        {
            GameObject go = new GameObject(name);
            SpriteRenderer renderer = go.AddComponent<SpriteRenderer>();
            renderer.material = unlitMaterial;
            renderer.sprite = spriteArray[0];

            Transform t = go.GetComponent<Transform>();
            t.position = position;
            t.localScale = new Vector3(scale, scale, scale);

            return go;
        }

        public void setDot(int number, int spriteNum)
        {
            if (number < dots.Count && spriteNum < spriteArray.Length)
            {
                dots[number].GetComponent<SpriteRenderer>().sprite = spriteArray[spriteNum];
            }
            else
            {
                Debug.LogError(number + " or " + spriteNum + "is not in the correct range.");
            }
            
        }
    }
}