namespace HappyGames.SimonSays
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using HappyGames.Emotion;
    using UnityEngine.Events;
    using UnityEngine.UI;
    using HappyGames.TimeManagement;
    using HappyGames.UI;
    using HappyGames.Data;

    public class SimonSays : MonoBehaviour
    {
        // --------------------------------------------- Variables --------------------------------------------------------
        [SerializeField, Tooltip("Script responsible for emotion selection")]
        private EmotionSelect emotionSelection;

        [SerializeField, Tooltip("Number of Rounds")]
        private int numOfRounds = 4;

        // current round
        private int currRound = 0;

        public int unlockSurprisedRound = 3;

        [SerializeField, Tooltip("Set containing Trophies")]
        private RankSpriteSet balloonTrophies;

        [SerializeField, Tooltip("Manages the scoreDisplay in the GameOverScreen")]
        private HighScoreDisplay scoreManager;

        //String to acces the highscore
        private string HIGHSCORE_KEY = "simonSays";

        [SerializeField, Tooltip("The ingame score display")]
        private Text scoreDisplay;

        public int CurrRound
        {
            get => currRound;
        }

        [SerializeField, Tooltip("Amount of guesses that can be wrong, before game ends")]
        private int wrongEmotionMax = 2;

        public int WrongEmotionMax
        {
            get => wrongEmotionMax;
        }

        // how many emotions have been guessed wrong
        private int wrongEmotionGuesses = 0;

        public int WrongEmotionGuesses
        {
            get => wrongEmotionGuesses;
            set => this.wrongEmotionGuesses = value;
        }

        [SerializeField, Tooltip("The game over screen")]
        private GameOverScreen gameOverScreen;

        // which medals the player can get
        public int firstPlace, secondPlace, thirdPlace;

        // --------------------------------------------- Events ----------------------------------------------------------

        [Space, SerializeField, Tooltip("Called when the scene starts.")]
        private UnityEvent onStart = default;

        [Space, SerializeField, Tooltip("Called when a new round has to start.")]
        private UnityEvent onStartRound = default;

        [Space, SerializeField, Tooltip("Called when the game ended.")]
        private UnityEvent onGameEnded = default;

        [Space, SerializeField, Tooltip("Called to display the button to start the next round.")]
        private UnityEvent onStartNextRound = default;

        [Space, SerializeField, Tooltip("Called when surprised Blob is collected.")]
        private UnityEvent onBlobCollected = default;

        // --------------------------------------------- Methods ---------------------------------------------------------

        // Start is called before the first frame update
        void Start()
        {
            onStart.Invoke();
        }

        // Update is called once per frame
        void Update()
        {
           
        }

        // starts a new round
        public void startRound()
        {
                currRound++;
                scoreDisplay.text = currRound.ToString();
                onStartRound?.Invoke();     
        }

        // checks if another round should be started. Depends on the maximum amount of rounds
        public void anotherRound()
        {
            if (currRound < numOfRounds)
            {
                onStartNextRound?.Invoke();
            }
            else
            {
                onGameEnded?.Invoke();
            }
        }

        // displays which medal the player won
        public void whichMedal()
        {
            int highscore = ProfileManager.GetActiveProfile().GetHighscore(HIGHSCORE_KEY);
            if (highscore < currRound)
            {
                ProfileManager.GetActiveProfile().SetHighscore(HIGHSCORE_KEY, currRound);
            }
            scoreManager?.DisplayScores(currRound, highscore);

            RankType throphy = currRound > firstPlace ? RankType.Gold : currRound > secondPlace ? RankType.Silver : currRound > thirdPlace ? RankType.Bronze : RankType.Wood;
            int recources = ProfileManager.GetActiveProfile().Inventory.ResourceCount;

            BaloonTrophy BaloonTrophy = new BaloonTrophy(throphy);
            BaloonTrophy.Score = currRound;
            int recoucesMade = currRound * 2;
            ProfileManager.GetActiveProfile().Inventory.AddTrophy(BaloonTrophy);
            ProfileManager.GetActiveProfile().Inventory.AddResources(recoucesMade);

            gameOverScreen.Show(balloonTrophies.GetValue(throphy), recources, recources + recoucesMade);
            Debug.Log(throphy);
           
        }

        // invokes the game ended event
        public void gameEnd()
        {
            onGameEnded?.Invoke();
        }

        public void TogglePause()
        {
            // Check if the time is disabled by the script.
            if (Timeline.IsPausedBy(this))
            {
                // Enable the time.
                Timeline.UnpauseGlobal(this);
            }
            else if (!Timeline.IsGlobalyPaused)
            {
                // Disable the time, if it was not disabled by the script or any other component.
                Timeline.PauseGlobal(this);
            }
        }

        public void CheckIfBlobCollected()
        {
            Debug.Log("checkBlob");
            if (currRound == unlockSurprisedRound && !ProfileManager.GetActiveProfile().HasEmotionUnlocked(EmotionType.Surprised))
            {
                ProfileManager.GetActiveProfile().SetStoryPart(3);
                onBlobCollected.Invoke();
                Debug.Log("UNOLOCKED SURPRISED");
            }
        }
    }
}

