using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeCounter : MonoBehaviour
{
    public GameObject[] lifes;
    public int lifeCount = 3;
    

    public void LooseLife()
    {
        lifeCount--;
        lifes[lifeCount].SetActive(false);
    }

}
