namespace HappyGames.SimonSays
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using HappyGames.Emotion;
    using UnityEngine.Events;
    using UnityEngine.UI;
    using HappyGames.Data;
    using System;

    public class SimonSaysRound : MonoBehaviour
    {
        // --------------------------------------------- Variables --------------------------------------------------------
        [SerializeField, Tooltip("Script responsible for emotion selection")]
        private EmotionSelect emotionSelect = default;

        [SerializeField, Tooltip("Script that handles the accepting of the selected emotion")]
        private AcceptEmotion acceptEmotion;

        private List<EmotionType> emotionsList = new List<EmotionType>();

        [SerializeField, Tooltip("Audio cues")]
        private AudioClip fail, sucess;

        [SerializeField, Tooltip("Time (in seconds) one emotion to remember is shown")]
        private float timeBetween = 3f;

        [SerializeField, Tooltip("Time (in seconds) of showing nothing between two emotion to remember")]
        private float waitEmotionShowing = 0.5f;

        [SerializeField, Tooltip("Time (in seconds) the wrong emotion selected message is shown")]
        private float waitWrongEmotionText = 2.0f;

        [SerializeField, Tooltip("How many Emotions are shown in the first run")]
        private int startEmotionAmmount = 3;

        [SerializeField, Tooltip("How many runs are in a round")]
        private int numOfRuns = 3;

        [SerializeField, Tooltip("Script managing the rounds of Simon Says")]
        private SimonSays simonSaysManager = default;

        // the number of the emotion the player is curently trying to guess
        private int numberPlayerEmotions = 0;

        // as not all emotion type emotions are used, define which emotions are used in the minigame
        private EmotionType[] emotionsUsed;


        [SerializeField, Tooltip("LifeCounter that tracks the lifes")]
        private LifeCounter lifeCounter;

        [SerializeField, Tooltip("Script for creating the sprites displaying if guess was right or wrong")]
        private MakeSprite statusDisplay = default;

        // --------------------------------------------- Events ----------------------------------------------------------
        [SerializeField, Tooltip("Called when the emotiontype that is shown to the player changes.")]
        private EmotionTypeEvent onShowingChange = default;

        [Space, SerializeField, Tooltip("Called when showing the emotions starts.")]
        private UnityEvent onShowingStart = default;

        [Space, SerializeField, Tooltip("Called when showing the emotions ends.")]
        private UnityEvent onShowingEnd = default;

        [Space, SerializeField, Tooltip("Called when its the players turn.")]
        private UnityEvent onPlayerTurn = default;

        [Space, SerializeField, Tooltip("Called when the game ended.")]
        private UnityEvent onTooManyWrongGuesses = default;

        [Space, SerializeField, Tooltip("Called when the round ended.")]
        private UnityEvent onRoundEnded = default;

        private void Awake()
        {
            List<EmotionType> l = new List<EmotionType>();
            foreach (EmotionType emo in ProfileManager.GetActiveProfile().UnlockedEmotions)
            {
                if (emo != EmotionType.Neutral)
                {
                    l.Add(emo);
                }
            }
            emotionsUsed = l.ToArray();
        }
        // --------------------------------------------- Methods ---------------------------------------------------------

        // Called when a new round is started
        public void beginRound()
        {
            numberPlayerEmotions = 0;

            // every round the number of emotions shown in the first run is increased by one
            startEmotionAmmount = simonSaysManager.CurrRound == 1 ? startEmotionAmmount : startEmotionAmmount+1;

            emotionsList = new List<EmotionType>();
            setRandomEmotions(startEmotionAmmount);
            //_________ for debug purpose
            foreach (EmotionType e in emotionsList)
            {
                Debug.Log(e);
            }
            //________
            StartCoroutine(showEmotions());
        }

        // called, when the player selects an emotion. 
        // checks if the selected emotion is the correct one.
        public void SelectEmotion()
        {
            Debug.Log(emotionSelect.Selection);

            // wrong emotion selected
            if(emotionsList[numberPlayerEmotions] == emotionSelect.Selection || (emotionsList[numberPlayerEmotions] == 0 && emotionSelect.Selection == EmotionType.Happy))
            {

                GetComponent<AudioSource>().clip = sucess;
                GetComponent<AudioSource>().Play();
                statusDisplay.setDot(numberPlayerEmotions, 1);
                numberPlayerEmotions++;
                if (numberPlayerEmotions >= emotionsList.Count)
                {
                    emotionSelect?.StopSelection();

                    // round ended
                    // -1 because if startEmotionAmmount is 3 and there are 3 runs to play in one round (numOfRuns=3) 
                    // the maximum number of emotions to show is 5 (1. run 3, 2. run 4, 3. run 5)
                    if (emotionsList.Count >= startEmotionAmmount + numOfRuns - 1)
                    {
                        onRoundEnded?.Invoke();
                    }
                    else // next run
                    {
                        AddRandomEmotion();
                        numberPlayerEmotions = 0;
                        foreach (EmotionType e in emotionsList)
                        {
                            Debug.Log(e);
                        }

                        StartCoroutine(deleteDotsAndDisplayEmotions(0.5f));
                    }
                }
                
                
            }
            else // right emotion selected
            {
                lifeCounter.LooseLife();
                GetComponent<AudioSource>().clip = fail;
                GetComponent<AudioSource>().Play();
                if (lifeCounter.lifeCount <= 0)
                {
                    emotionSelect?.StopSelection();
                    onTooManyWrongGuesses?.Invoke();
                }
                else
                {
                    StartCoroutine(showDotShortly(waitWrongEmotionText));
                }
            }
        }

        // sets the list of emotions randomly.
        // ammount => number of emotions to add randomly to the emotions list
        private void setRandomEmotions(int ammount)
        {
            for(int i = 0; i< ammount; i++)
            {
                if (i == ammount-1 && simonSaysManager.CurrRound == simonSaysManager.unlockSurprisedRound && ProfileManager.GetActiveProfile().StoryPart == 2)
                {
                    emotionsList.Add(EmotionType.Neutral);
                }
                else
                {
                    AddRandomEmotion();
                }
            }
        }

        // adds a random emotion to the emotion list
        private void AddRandomEmotion()
        {
            emotionsList.Add(emotionsUsed[UnityEngine.Random.Range(0, emotionsUsed.Length)]);
        }

        // Displayes the emotions to be remembered by the player
        private IEnumerator showEmotions()
        {

            yield return new WaitForSeconds(1f);

            foreach (EmotionType e in emotionsList)
            {
                onShowingChange?.Invoke(e);
                onShowingStart?.Invoke();
                yield return new WaitForSeconds(timeBetween);
                onShowingEnd?.Invoke();
                yield return new WaitForSeconds(waitEmotionShowing);
            }

            onPlayerTurn?.Invoke();
        }

        // displays a message for x seconds in a given textfield
        // message => the message to display
        // delay => time the message is displayed
        // text => the textfield, the message is displayed in
        private IEnumerator showMessageShortly(string message, float delay, Text text)
        {
            text.text = message;
            text.enabled = true;
            yield return new WaitForSeconds(delay);
            text.enabled = false;
        }

        // displays the current dot as "false" for 'delay' seconds
        private IEnumerator showDotShortly(float delay)
        {
            statusDisplay.setDot(numberPlayerEmotions, 2);
            acceptEmotion.Deactivate();
            yield return new WaitForSeconds(delay);
            acceptEmotion.Activate();
            statusDisplay.setDot(numberPlayerEmotions, 0);
        }

        // creates the dots showing what has already been guessed correctly
        private void createStatusDots()
        {
            statusDisplay.createDots(emotionsList.Count);
        }

        private IEnumerator deleteDotsAndDisplayEmotions(float delay)
        {
            yield return new WaitForSeconds(delay);
            statusDisplay.deleteDots();
            StartCoroutine(showEmotions());
        }
    }
}
