using HappyGames.Emotion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AcceptEmotion : MonoBehaviour
{
    public GameObject overlay, text;
    public EmotionSelect selection;
    public UnityEvent OnAccept;
    public EmotionImageSwap img;
    public EmotionSpriteColorSwap sprite;
    bool active = false;
    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            if (Input.GetMouseButtonDown(0))
            {
                OnAccept.Invoke();
            }
        } 

       
    }
    public void Activate()
    {
        active = true;
        overlay.SetActive(true);
        text.SetActive(true);
        img.Display(EmotionType.Neutral);
        sprite.Display(EmotionType.Neutral);

    }

    public void Deactivate()
    {
        active = false;
        overlay.SetActive(false);
        text.SetActive(false);
    }
}
