namespace HappyGames.Minigames.JumpingGame
{
    using UnityEngine;

    public class ParalaxObject : MovingObject
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The paralax values.")]
        protected Vector2 paralax = Vector2.one;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override void MoveTo(Vector2 position)
        {
            Vector2 delta = position - (Vector2)transform.position;
            base.MoveTo((Vector2)transform.position + delta * paralax);
        }
    }
}
