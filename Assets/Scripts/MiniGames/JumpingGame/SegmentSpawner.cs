namespace HappyGames.Minigames.JumpingGame
{
    using HappyGames.Data;
    using HappyGames.Minigames.Utilities;
    using HappyGames.Utilities;
    using System.Collections.Generic;
    using UnityEngine;

    public class SegmentSpawner : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The area the segment should be part of.")]
        protected GameArea area = null;

        [SerializeField, Min(0f), Tooltip("The height when to start showing the slime.")]
        protected float slimeHeight = 100f;

        [SerializeField, Tooltip("The segments that can be spawned.")]
        protected WeightedPrefabCollection<MapSegment> segmentPrefabs = new WeightedPrefabCollection<MapSegment>();

        [Space, SerializeField, Tooltip("Called when a segment is spawned.")]
        protected MapSegmentEvent onSpawn = default;

        [SerializeField, Tooltip("Called when a map segment gets destroyed.")]
        protected MapSegmentEvent onDestroy = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The container object to spawn the segments in.
        /// </summary>
        protected Transform Container => transform;

        protected List<MapSegment> spawnedSegments = new List<MapSegment>();
        protected float currentHeight = 0f;
        protected bool needsSlime = false;
        protected bool slimeIsVisible = false;
        protected const int STORY_POINT = 1;
        protected bool extraPlatforms = false;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            SwitchProfile(null, ProfileManager.GetActiveProfile());
            ProfileManager.OnProfileActivated += SwitchProfile;
        }

        private void OnDisable()
        {
            ProfileManager.OnProfileActivated -= SwitchProfile;
            SwitchProfile(ProfileManager.GetActiveProfile(), null);
        }

        private void Start()
        {
            SpawnSegment(new Vector2(area.Center.x, area.Min.y));
        }

        /// <summary>
        /// Spawns a segment at a position.
        /// </summary>
        /// <param name="positon">The position where to spawn the segment at.</param>
        public void SpawnSegment(Vector2 positon)
        {
            MapSegment prefab = segmentPrefabs.GetElement(currentHeight);
            MapSegment segment = prefab.Clone(area, positon + Vector2.up * prefab.SegmentHeight, Container);
            if (segment.HasSlime && currentHeight > slimeHeight && needsSlime && !slimeIsVisible)
            {
                segment.ShowSlime();
                slimeIsVisible = true;
                segment.OnAreaExit += () => slimeIsVisible = false;
            }
            currentHeight += prefab.SegmentHeight;
            spawnedSegments.Add(segment);
            if (extraPlatforms == true)            {                segment.EnableExtraPlaltforms();            }
            segment.OnAreaEnter += delegate
            {
                SpawnSegment(segment.transform.position);
            };
            segment.OnAreaExit += delegate
            {
                onDestroy?.Invoke(segment);
                spawnedSegments.Remove(segment);
                Destroy(segment.gameObject);
            };
            onSpawn?.Invoke(segment);
        }

        /// <summary>
        /// Enables the extra platforms in the segments.
        /// </summary>
        public void EnableExtraPlatforms()
        {
            extraPlatforms = true;
            foreach (MapSegment segment in spawnedSegments)
            {
                segment.EnableExtraPlaltforms();
            }
        }

        /// <summary>
        /// Disables the extra platforms in the segments.
        /// </summary>
        public void DisableExtraPlatforms()
        {
            foreach (MapSegment segment in spawnedSegments)
            {
                segment.DisableExtraPlatforms();
            }
            extraPlatforms = false;
        }

        /// <summary>
        /// Switches the used profile.
        /// </summary>
        /// <param name="oldProfile">The previously used profile.</param>
        /// <param name="newProfile">The profile now in use.</param>
        private void SwitchProfile(UserProfile oldProfile, UserProfile newProfile)
        {
            if (oldProfile != null)
            {
                oldProfile.OnStoryPartChanged -= CheckStory;
            }
            if (newProfile != null)
            {
                CheckStory(newProfile.StoryPart);
                newProfile.OnStoryPartChanged += CheckStory;
            }
        }

        /// <summary>
        /// Checks if the bolb needs to be spawned.
        /// </summary>
        /// <param name="emotion">The emotion unlocked.</param>
        private void CheckStory(int part) => needsSlime = part == (STORY_POINT - 1);


        // --- | Classes | -------------------------------------------------------------------------------------------------

        [System.Serializable]
        protected class SegmentWithChance
        {
            // Inspector --------------------------------------------------------------------------

            [SerializeField, Tooltip("The segment prefab to place in the map.")]
            protected MapSegment prefab = null;
            /// <summary>
            /// The segment prefab to place in the map.
            /// </summary>
            public MapSegment Prefab => prefab;

            [SerializeField, Tooltip("The chances to get the segment.")]
            protected AnimationCurve chance = new AnimationCurve(new Keyframe(), new Keyframe(1f, 1f));
            /// <summary>
            /// The chances to get the segment.
            /// </summary>
            public AnimationCurve Chance => chance;

            [SerializeField, Tooltip("True if this segment has always a chance to be spawned after the min height was reached.")]
            protected bool infinitHeightLimit = false;

            // Variables -------------------------------------------------------------------------

            /// <summary>
            /// The minimum height the segment has a chance to be spawned.
            /// </summary>
            public float MinHeight => chance[0].time;

            /// <summary>
            /// The maximum height the segmet has a chance to be spawned.
            /// </summary>
            public float MaxHeight => infinitHeightLimit ? float.PositiveInfinity : chance[chance.length - 1].time;

            // Classes ----------------------------------------------------------------------------

            /// <summary>
            /// A class that provides a method to compare min values of <see cref="SegmentWithChance"/>s.
            /// </summary>
            public class MinHeightComperison : IComparer<SegmentWithChance>
            {
                public int Compare(SegmentWithChance x, SegmentWithChance y)
                {
                    if (x.MinHeight == y.MinHeight)
                    {
                        return 1;
                    }
                    return x.MinHeight.CompareTo(y.MinHeight);
                }
            }

            /// <summary>
            /// A class that provides a method to compare max values of <see cref="SegmentWithChance"/>s.
            /// </summary>
            public class MaxHeightComperison : IComparer<SegmentWithChance>
            {
                public int Compare(SegmentWithChance x, SegmentWithChance y)
                {
                    if (x.MaxHeight == y.MaxHeight)
                    {
                        return 1;
                    }
                    return x.MaxHeight.CompareTo(y.MaxHeight);
                }
            }
        }
    }
}
