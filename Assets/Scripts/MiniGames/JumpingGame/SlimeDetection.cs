namespace HappyGames.Minigames.JumpingGame
{
    using HappyGames.Data;
    using UnityEngine;
    using UnityEngine.Events;

    public class SlimeDetection : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The emotion to unlock when collecting.")]
        protected Emotion.EmotionType emotion = Emotion.EmotionType.Sad;

        [SerializeField, Tooltip("Called when the blob is collected")]
        protected UnityEvent onCollect = default;

        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The tag to look for on collision.
        /// </summary>
        protected const string PLAYER_TAG = "Player";

        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == PLAYER_TAG)
            {
                UserProfile profile = ProfileManager.GetActiveProfile();
                profile.SetStoryPart(1);
                BlobIconActivator.Instance.Activate();
                gameObject.SetActive(false);
                onCollect?.Invoke();
            }
        }
    }
}
