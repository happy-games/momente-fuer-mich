namespace HappyGames.Minigames.JumpingGame
{
    using HappyGames.Minigames.Utilities;
    using UnityEngine;

    public class HappyAbility : EmotionAbility
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The player whos physics to modify.")]
        protected JumpingPhysics player = null;

        [SerializeField, Min(0f), Tooltip("The boost to apply.")]
        protected float boostStrength = 15f;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override void UseAbility()
        {
            base.UseAbility();
            player.FreezVelocity(JumpingPhysics.ForceDirection.Vertical, boostStrength);
        }

        /// <inheritdoc/>
        public override void StopAbility()
        {
            player.UnfreezVelocity();
            base.StopAbility();
        }
    }
}
