using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HappyGames.Minigames.JumpingGame;

public class JumpingAnimationManager : MonoBehaviour
{
    [SerializeField]
    private Animator jumpingAnimator;

    [SerializeField]
    private GameObject player;

    private JumpingPhysics playerRB;

    // Start is called before the first frame update
    void Start()
    {
        playerRB = player.GetComponent<JumpingPhysics>();
    }

    // Update is called once per frame
    void Update()
    {
        jumpingAnimator.SetFloat("Velocity", playerRB.Velocity.y); 
    }
}
