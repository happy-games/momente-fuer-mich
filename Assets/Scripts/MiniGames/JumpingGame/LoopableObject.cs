namespace HappyGames.Minigames.JumpingGame
{
    using UnityEngine;
    using HappyGames.Minigames.Utilities;

    /// <summary>
    /// Loops an objcet inside a <see cref="GameArea"/>.
    /// </summary>
    public class LoopableObject : MovingObject
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The are to loop the object inside.")]
        protected GameArea area = null;

        [SerializeField, Tooltip("True ")]
        protected bool horizontalLoop = true;

        [SerializeField, Tooltip("True ")]
        protected bool verticalLoop = false;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override void MoveTo(Vector2 position)
        {
            base.MoveTo(LoopPosition(position));
        }

        /// <summary>
        /// Clamps the position inside the loop area.
        /// </summary>
        /// <param name="position">The position to clamp.</param>
        /// <returns>The clamped position.</returns>
        public Vector2 LoopPosition(Vector2 position)
        {
            return new Vector2
            (
                horizontalLoop? Mathf.Repeat(position.x - area.Max.x, area.Size.x) + area.Min.x : position.x, 
                verticalLoop ? Mathf.Repeat(position.y - area.Max.y, area.Size.y) + area.Min.y : position.y
            );
        }

    }
}
