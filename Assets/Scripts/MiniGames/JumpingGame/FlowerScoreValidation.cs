namespace HappyGames.Minigames.JumpingGame
{
    using HappyGames.Data;
    using HappyGames.Emotion;
    using HappyGames.UI;
    using UnityEngine;
    using UnityEngine.Events;

    public class FlowerScoreValidation : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The resources you get depending on the score.")]
        protected ScoreResource[] resources = default;

        [SerializeField, Tooltip("The script storing the score.")]
        private JumpingScore score = null;

        [SerializeField, Tooltip("The game over screen displaying the score.")]
        private GameOverScreen gameOverScreen = null;

        [SerializeField, Tooltip("The fields to display the score.")]
        private HighScoreDisplay highScoreDisplay = null;

        [SerializeField, Tooltip("The sprites for the trophies.")]
        private EmotionSpriteSet trophySprites = null;

        [Space, SerializeField, Tooltip("Called when the emotion selected is not valid.")]
        private UnityEvent<EmotionType> onInvalidEmotion = default;

        [SerializeField, Tooltip("Called when the selected emotion was valid for selection.")]
        private UnityEvent<EmotionType> onValidEmotion = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The name of the highscore.
        /// </summary>
        public const string HIGHSCORE_KEY = "Emotionjump";

        protected EmotionType selectedEmotion;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Applies the score to the profile.
        /// </summary>
        public void Validate()
        {
            ScoreResource resources = GetReward(score.Score);
            UserProfile profile = ProfileManager.GetActiveProfile();
            int oldResourceCount = profile.Inventory.ResourceCount;
            profile.Inventory.AddResources(resources.resources);
            profile.Inventory.AddTrophy(new FlowerTrophy(selectedEmotion, score.Score));
            Debug.Log($"old: { oldResourceCount }, added: { resources.resources }, new: { profile.Inventory.ResourceCount }");
            gameOverScreen?.Show(trophySprites.GetValue(selectedEmotion), oldResourceCount, profile.Inventory.ResourceCount);
            int highscore = profile.GetHighscore(HIGHSCORE_KEY);
            if (highscore < score.Score)
            {
                profile.SetHighscore(HIGHSCORE_KEY, score.Score);
            }
            highScoreDisplay?.DisplayScores(score.Score, highscore);
        }

        /// <summary>
        /// Sets the emotion.
        /// </summary>
        /// <param name="emotion">The emotion selected.</param>
        public void SetEmotion(EmotionType emotion)
        {
            if (emotion != default)
            {
                selectedEmotion = emotion;
                onValidEmotion?.Invoke(emotion);
            }
            else
            {
                selectedEmotion = EmotionType.Happy;
                onValidEmotion?.Invoke(emotion);
                //onInvalidEmotion?.Invoke(emotion);
            }
        }

        /// <summary>
        /// Calculates the reward for a given score.
        /// </summary>
        /// <returns>The resources rewarded for the score.</returns>
        private ScoreResource GetReward(int score)
        {
            ScoreResource current = resources[0];
            foreach (ScoreResource resource in resources)
            {
                if (current.score > score || current.score <= resource.score && resource.score <= score)
                {
                    current = resource;
                }
            }
            return current;
        }


        // --- | Classes | -------------------------------------------------------------------------------------------------

        [System.Serializable]
        protected struct ScoreResource
        {
            public int score;
            public int resources;

            public ScoreResource(int score, int resources)
            {
                this.score = score;
                this.resources = resources;
            }
        }
    }
}
