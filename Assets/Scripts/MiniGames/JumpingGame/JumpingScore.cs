namespace HappyGames.Minigames.JumpingGame
{
    using HappyGames.Data;
    using HappyGames.Emotion;
    using HappyGames.Events;
    using UnityEngine;

    public class JumpingScore : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Min(0.1f), Tooltip("The distance between the platforms.")]
        protected float platformDistance = 2f;

        [SerializeField, Tooltip("Called when the players score updates.")]
        protected IntEvent onScoreUpdate = default;

        [SerializeField, Min(0f), Tooltip("The amount of resources for every scored point.")]
        protected float scoreToResourceRealtion = 0.5f;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected float height = 0f;
        protected int lateScore = 0;

        /// <summary>
        /// The score of the player.
        /// </summary>
        public int Score => Mathf.FloorToInt(height / platformDistance);


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Updates the distance traveled by the player.
        /// </summary>
        /// <param name="nextPosition">The next position of the player.</param>
        public void UpdateDistance(Vector2 nextPosition)
        {
            if (nextPosition.y > 0f)
            {
                height += nextPosition.y;
                if (lateScore < Score)
                {
                    onScoreUpdate?.Invoke(Score);
                    lateScore = Score;
                }
            }
        }

        /// <summary>
        /// Adds a flower to the inventory.
        /// </summary>
        /// <param name="emotion">The emotion for the flower.</param>
        public void AddResourceWithScore(EmotionType emotion)
        {
            ProfileManager.GetActiveProfile().Inventory.AddResources(Mathf.CeilToInt(Score * scoreToResourceRealtion));
            ProfileManager.GetActiveProfile().Inventory.AddTrophy(new FlowerTrophy(emotion, Score));
        }
    }
}
