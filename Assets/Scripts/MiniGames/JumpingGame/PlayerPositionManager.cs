namespace HappyGames.Minigames.JumpingGame
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Handels the positioning of all map elements.
    /// </summary>
    public class PlayerPositionManager : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The player to position.")]
        protected MovingObject player;

        [SerializeField, Tooltip("The map to position.")]
        protected List<MovingObject> mapElements;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected bool isMovingObjects = false;
        protected Queue<MovingObject> addedObjects = new Queue<MovingObject>();
        protected Queue<MovingObject> removedObjects = new Queue<MovingObject>();


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Updates the players position in the map.
        /// </summary>
        /// <param name="position">The new position for the player.</param>
        public void UpdatePositon(Vector2 position)
        {
            if (position.y > 0f)
            {
                player.MoveTo(new Vector2(position.x, 0f));
                MoveMapElements(Vector3.down * position.y);
            }
            else
            {
                player.MoveTo(position);
            }
        }

        /// <summary>
        /// Moves all map registered map elements.
        /// </summary>
        /// <param name="direction">The direction to move the elements.</param>
        private void MoveMapElements(Vector2 direction)
        {
            isMovingObjects = true;
            foreach (MovingObject mapElement in mapElements)
            {
                if (mapElement)
                {
                    mapElement.Move(direction);
                }
            }
            isMovingObjects = false;
            UpdateRegistedMapElements();
        }

        /// <summary>
        /// Adds a object to the managed objects.
        /// </summary>
        /// <param name="element">The element to add.</param>
        public void AddMapElement(MovingObject element)
        {
            if (isMovingObjects)
            {
                addedObjects.Enqueue(element);
            }
            else
            {
                mapElements.Add(element);
            }
        }

        /// <summary>
        /// Removes a object from the managed objects.
        /// </summary>
        /// <param name="element">The element to remove.</param>
        public void RemoveMapElement(MovingObject element)
        {
            if (isMovingObjects)
            {
                removedObjects.Enqueue(element);
            }
            else
            {
                mapElements.Remove(element);
            }
        }

        /// <summary>
        /// Updates the list with the registed map elements.
        /// </summary>
        private void UpdateRegistedMapElements()
        {
            while (removedObjects.Count > 0)
            {
                mapElements.Remove(removedObjects.Dequeue());
            }
            while (addedObjects.Count > 0)
            {
                mapElements.Add(addedObjects.Dequeue());
            }
        }
    }
}
