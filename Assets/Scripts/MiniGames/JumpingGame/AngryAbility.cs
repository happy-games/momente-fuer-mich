namespace HappyGames.Minigames.JumpingGame
{
    using HappyGames.Minigames.Utilities;
    using UnityEngine;

    public class AngryAbility : EmotionAbility
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tag, Tooltip("The tag to identify obstacles.")]
        private string obstacleTag = default;

        [SerializeField, Tooltip("The origin of the barrier.")]
        private Vector2 origin = default;

        [SerializeField, Min(0f), Tooltip("The radius of the barrier.")]
        private float radius = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private bool isActive = false;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position + (Vector3)origin, radius);
        }

        private void Update()
        {
            if (isActive)
            {
                foreach (Collider2D collider in Physics2D.OverlapCircleAll((Vector2)transform.position + origin, radius))
                {
                    if (collider.tag == obstacleTag)
                    {
                        collider.GetComponent<SpriteRenderer>().enabled = false;
                        collider.GetComponent<Collider2D>().enabled = false;
                        collider.GetComponent<ParticleSystem>().Play();
                    }
                }
            }
        }

        /// <inheritdoc/>
        public override void UseAbility()
        {
            isActive = true;
            base.UseAbility();
        }

        /// <inheritdoc/>
        public override void StopAbility()
        {
            base.StopAbility();
            isActive = false;
        }
    }
}