namespace HappyGames.Minigames.JumpingGame
{
    using HappyGames.Events;
    using HappyGames.Minigames.Utilities;
    using HappyGames.TimeManagement;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    /// Manages an objects physics for the jumping minigame.
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D))]
    public class JumpingPhysics : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The gravity effecting the object.")]
        protected Vector2 gravity = Vector2.down * 15f;
        /// <summary>
        /// The gravity effecting the object.
        /// </summary>
        public Vector2 Gravity => gravity;

        [SerializeField, Tooltip("The timeline to use to calculate the physics.")]
        protected Timeline timeline = null;

        [SerializeField, Tooltip("The area where collisions will be detected.")]
        protected GameArea area = null;

        [SerializeField, Tag, Tooltip("The tags to collide with.")]
        protected List<string> obstacleTags = default;

        [SerializeField, Tag, Tooltip("The objects to jump through.")]
        protected string jumpThroughTag = "Untagged";


        [Space, SerializeField, Tooltip("Called when the object collides with the floor.")]
        protected UnityEvent onGroundCollision = default;
        /// <summary>
        /// Called when the object collides with the floor.
        /// </summary>
        public event UnityAction OnGroundCollsion
        {
            add => onGroundCollision.AddListener(value);
            remove => onGroundCollision.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called every time a new position was calculated.")]
        protected Vector2Event onPositionUpdate = default;
        /// <summary>
        /// Called every time a new position was calculated.
        /// </summary>
        public event UnityAction<Vector2> OnPositionUpdate
        {
            add => onPositionUpdate.AddListener(value);
            remove => onPositionUpdate.RemoveListener(value);
        }


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected Rigidbody2D body;

        /// <summary>
        /// The objects velocity.
        /// </summary>
        public Vector2 Velocity { get; protected set; }

        /// <summary>
        /// True if the object is touching the ground.
        /// </summary>
        public bool IsGrounded { get; protected set; }

        /// <summary>
        /// True if the velocity is frozen horizontaly.
        /// </summary>
        public bool IsVelocityFrozenHorizontal { get; protected set; }

        /// <summary>
        /// True if the velocity is frozen verticaly.
        /// </summary>
        public bool IsVelocityFrozenVertical { get; protected set; }

        /// <summary>
        /// True if all collisions should be ignored.
        /// </summary>
        public bool IgnoreCollisions { get; set; } = false;

        /// <summary>
        /// The maximum amount of collisions that can be handled by the script.
        /// </summary>
        private const int MAX_COLLISION_COUNT = 4;
        private RaycastHit2D[] hitBuffer = new RaycastHit2D[MAX_COLLISION_COUNT];
        private ContactFilter2D hitFilter;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        protected virtual void Awake()
        {
            body = GetComponent<Rigidbody2D>();
            hitFilter.useTriggers = false;
            hitFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        }

        protected virtual void FixedUpdate()
        {
            // Add gravity.
            if (!IsVelocityFrozenVertical)
            {
                Velocity += Gravity * timeline.FixedDeltaTime;
            }
            // Reset the grounded state.
            IsGrounded = false;

            // Get the movement step for the fixed frame.
            Vector2 step = Velocity * timeline.FixedDeltaTime;
            if (!IgnoreCollisions)
            {
                // Check for collisions in the path of the current movement.
                int hitCount = body.Cast(step, hitFilter, hitBuffer, step.magnitude);
                // Iterate over all detected collisions.
                for (int i = 0; i < hitCount; i++)
                {
                    // Check if the colliding object is an obstace.
                    if (!obstacleTags.Contains(hitBuffer[i].transform.tag))
                    {
                        continue;
                    }
                    // Check if the collision is inside the area.
                    if (!area || hitBuffer[i].point.y >= area.Min.y)
                    {
                        bool wasModified = false;
                        // Check for horizontal collision.
                        if (hitBuffer[i].transform.tag != jumpThroughTag && hitBuffer[i].normal.x > 0f != Velocity.x > 0f)
                        {
                            SetVelocity(0f, ForceDirection.Horizontal);
                            step.Set(hitBuffer[i].centroid.x - body.position.x, step.y);
                            wasModified = true;
                        }
                        // Check if the obstacle is jump through.
                        if (hitBuffer[i].transform.tag != jumpThroughTag || (body.position.y >= hitBuffer[i].collider.bounds.max.y && Velocity.y < 0))
                        {
                            // Check for vertical collision.
                            if (hitBuffer[i].normal.y > 0f != Velocity.y > 0f)
                            {
                                // Reset the vertical velocity.
                                SetVelocity(0f, ForceDirection.Vertical);
                                step.Set(step.x, hitBuffer[i].centroid.y - body.position.y);
                            }
                            wasModified = true;
                        }
                        if (wasModified)
                        {
                            // Set the object as grounded.
                            IsGrounded = hitBuffer[i].normal.y > 0f && Velocity.y <= 0f;
                            if (IsGrounded)
                            {
                                // Invoke the ground collision event.
                                onGroundCollision?.Invoke();
                            }
                            SendMessage("OnRayPhysicsEnter2D", hitBuffer[i].transform.gameObject, SendMessageOptions.DontRequireReceiver);
                            hitBuffer[i].transform.SendMessage("OnRayPhysicsEnter2D", gameObject, SendMessageOptions.DontRequireReceiver);
                        }
                    }
                }
            }
            // Update object position if no collision was detected.
            onPositionUpdate?.Invoke(body.position + step);
        }

        /// <summary>
        /// Sets the velocity of the object.
        /// </summary>
        /// <param name="velocity">The new velocity to use.</param>
        public void SetVelocity(Vector2 velocity) => Velocity = velocity;
        /// <summary>
        /// Sets the velocity of the object.
        /// </summary>
        /// <param name="velocity">The new velocity.</param>
        /// <param name="direction">The value that should be changed.</param>
        public void SetVelocity(float velocity, ForceDirection direction)
        {
            if (direction == ForceDirection.Horizontal && IsVelocityFrozenHorizontal || direction == ForceDirection.Vertical && IsVelocityFrozenVertical)
            {
                return;
            }
            switch (direction)
            {
                case ForceDirection.Horizontal: Velocity = new Vector2(velocity, Velocity.y); break;
                case ForceDirection.Vertical: Velocity = new Vector2(Velocity.x, velocity); break;
            }
        }
        /// <summary>
        /// Resets the velocity.
        /// </summary>
        public void ResetVelocity() => SetVelocity(Vector2.zero);

        /// <summary>
        /// Adds force to the velocity.
        /// </summary>
        /// <param name="force">the force to add.</param>
        public void AddForce(Vector2 force) => Velocity += force;

        /// <summary>
        /// Sets the frozen state of the velocity.
        /// </summary>
        /// <param name="direction">The direction to change.</param>
        /// <param name="state">True if the direction should be frozen.</param>
        protected void FreezVelocity(ForceDirection direction, bool state)
        {
            switch (direction)
            {
                case ForceDirection.Horizontal: IsVelocityFrozenHorizontal = state; break;
                case ForceDirection.Vertical: IsVelocityFrozenVertical = state; break;
            }
        }
        /// <summary>
        /// Frezes the velocity.
        /// </summary>
        public void FreezVelocity()
        {
            FreezVelocity(ForceDirection.Horizontal, true);
            FreezVelocity(ForceDirection.Vertical, true);
        }
        /// <summary>
        /// Frezes the velocity.
        /// </summary>
        /// <param name="direction">The direction to freez.</param>
        public void FreezVelocity(ForceDirection direction) => FreezVelocity(direction, true);
        /// <summary>
        /// Frezes the velocity.
        /// </summary>
        /// <param name="direction">The direction to freez.</param>
        /// <param name="velocity">The value to freez.</param>
        public void FreezVelocity(ForceDirection direction, float velocity)
        {
            SetVelocity(velocity, direction);
            FreezVelocity(direction, true);
        }
        /// <summary>
        /// Frezes the velocity.
        /// </summary>
        /// <param name="velocity">The value to freez.</param>
        public void FreezVelocity(Vector2 velocity)
        {
            SetVelocity(velocity);
            FreezVelocity();
        }

        /// <summary>
        /// Unfreezes the velocity.
        /// </summary>
        public void UnfreezVelocity()
        {
            FreezVelocity(ForceDirection.Horizontal, false);
            FreezVelocity(ForceDirection.Vertical, false);
        }
        /// <summary>
        /// Unfreezes the velocity.
        /// </summary>
        /// <param name="direction">The direction to unfreez.</param>
        public void UnfreezVelocity(ForceDirection direction) => FreezVelocity(direction, false);

        /// <summary>
        /// Adds a tag to the objects to collide with.
        /// </summary>
        /// <param name="tag">The tag to add.</param>
        public void AddObstacleTag(string tag) => obstacleTags.Add(tag);

        /// <summary>
        /// Removes a tag from the objects to collide with.
        /// </summary>
        /// <param name="tag">The tag to remove.</param>
        public void RemoveObstacleTag(string tag) => obstacleTags.Remove(tag);


        // --- | Enums | ---------------------------------------------------------------------------------------------------

        /// <summary>
        /// The direction the force should be applied.
        /// </summary>
        public enum ForceDirection { Horizontal, Vertical }
    }
}
