namespace HappyGames.Minigames.JumpingGame
{
    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    /// Provides a method to set an objects position.
    /// </summary>
    public class MovingObject : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Another movement restraint to perform before this one.")]
        protected MovingObject nextMove = null;

        [SerializeField, Tooltip("Called when the position was updated.")]
        protected UnityEvent<Vector2> onPositionUpdated = default;

        [SerializeField, Tooltip("Called when the object was moved.")]
        protected UnityEvent<Vector2> onObjectMoved = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Updates the position of the object.
        /// </summary>
        /// <param name="position">The position to move the object to.</param>
        public virtual void MoveTo(Vector2 position)
        {
            if (nextMove)
            {
                nextMove.MoveTo(position);
            }
            else
            {
                Vector2 delta = position - (Vector2)transform.position;
                transform.position = position;
                onPositionUpdated?.Invoke(position);
                onObjectMoved?.Invoke(delta);
            }
        }

        /// <summary>
        /// Moves the object in a direction.
        /// </summary>
        /// <param name="direction">The direction to move the object.</param>
        public void Move(Vector2 direction) => MoveTo((Vector2)transform.position + direction);
    }
}
