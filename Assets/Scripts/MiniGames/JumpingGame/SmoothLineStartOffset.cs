namespace HappyGames.Minigames.JumpingGame
{
    using HappyGames.Utilities;
    using UnityEngine;

    public class SmoothLineStartOffset : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The lines to update.")]
        protected SmoothLineUpdater lines = default;

        [SerializeField, Tooltip("The origin of the paralax.")]
        protected float origin = 0f;

        [SerializeField, Tooltip("The paralax effect applied.")]
        protected float paralax = 0.1f;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Start()
        {
            foreach (SmoothLine line in lines.Lines)
            {
                line.SetEndY(line.End.y + (origin - line.transform.position.y) * paralax);
            }
        }
    }
}
