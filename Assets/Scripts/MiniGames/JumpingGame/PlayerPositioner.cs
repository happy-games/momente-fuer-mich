namespace HappyGames.Minigames.JumpingGame
{
    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    /// Handles the repositioning of the player.
    /// </summary>
    public sealed class PlayerPositioner : LoopableObject
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Min(0f), Tooltip("The height of the player.")]
        private float playerHeight = 1f;

        [Space, SerializeField, Tooltip("Called when the player leaves the area.")]
        private UnityEvent onAreaLeft = default;


        // --- | Variable | ------------------------------------------------------------------------------------------------

        private bool hasLeft = false;


        // --- | Methods | -------------------------------------------------------------------------------------------------

#if (UNITY_EDITOR)

        private void OnDrawGizmosSelected()
        {
            Vector2 top = (Vector2)transform.position + Vector2.up * playerHeight;

            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(transform.position, top);
            Gizmos.DrawLine((Vector2)transform.position + Vector2.left * 0.25f, (Vector2)transform.position + Vector2.right * 0.25f);
            Gizmos.DrawLine(top + Vector2.left * 0.25f, top + Vector2.right * 0.25f);
        }

#endif

        /// <inheritdoc/>
        public override void MoveTo(Vector2 position)
        {
            base.MoveTo(position);
            if (!hasLeft && position.y + playerHeight < area.Min.y)
            {
                hasLeft = true;
                onAreaLeft?.Invoke();
            }
        }
    }
}
