namespace HappyGames.Minigames.JumpingGame
{
    using HappyGames.TimeManagement;
    using UnityEngine;

    /// <summary>
    /// Manages the movement of the player in the jumping game.
    /// </summary>
    [RequireComponent(typeof(JumpingPhysics))]
    public class JumpingMovement : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The speed the object moves horizontaly.")]
        protected float movementSpeed = 5f;

        [SerializeField, Tooltip("The speed gain per second.")]
        protected float acceleration = 1f;

        [SerializeField, Min(0f), Tooltip("The force applied to the object when landing on a platform.")]
        protected float jumpStength = 25f;

        [SerializeField, Tooltip("The timeline to use to calculate the movement.")]
        protected Timeline timeline = null;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected JumpingPhysics physics;
        protected float currentSpeed = 0f;

        protected const string TIME_LOOP = "Physics";


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            physics = GetComponent<JumpingPhysics>();
        }

        private void OnDisable()
        {
            currentSpeed = 0f;
            physics.SetVelocity(0f, JumpingPhysics.ForceDirection.Horizontal);
        }

        /// <summary>
        /// Lets the object jump.
        /// </summary>
        public void Jump()
        {
            if (enabled)
            {
                physics.SetVelocity(jumpStength, JumpingPhysics.ForceDirection.Vertical);
            }
        }

        /// <summary>
        /// Moves the object horizontaly.
        /// </summary>
        /// <param name="value">The movement to perform. Value between -1 and 1.</param>
        public void Move(float value)
        {
            if (enabled)
            {
                // Make sure the value to move never exceeds positive or negative 1.
                value = Mathf.Clamp(value, -1f, 1f);
                // Calculate the new speed.
                currentSpeed = Mathf.MoveTowards(currentSpeed, value * movementSpeed, acceleration * timeline.DeltaTime);
                // Apply the movement.
                physics.SetVelocity(currentSpeed, JumpingPhysics.ForceDirection.Horizontal);
            }
        }


        /// <summary>
        /// Sets the Jump Strength of the Object.
        /// </summary>
        /// <param name="newJumpStrength"> The Jump Strength that it will get set too (normal is 25f) </param>
        public void SetJumpStrength (float newJumpStrength)
        {
            jumpStength = newJumpStrength;
        }
    }
}
