namespace HappyGames.Minigames.JumpingGame
{
    using HappyGames.Minigames.Utilities;
    using UnityEngine;
    using UnityEngine.Events;

    public class MouseMovement : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The target to calculate the direction from.")]
        private Transform target;

        [SerializeField, Tooltip("The area to move inside.")]
        private GameArea gameArea = null;

        [SerializeField]
        private KeyCode movementKey = KeyCode.None;

        [SerializeField, Min(0f), Tooltip("Decides how fast the character goes.")]
        protected float adjustmentSpeed = 0.25f;

        [Space, SerializeField]
        private UnityEvent<float> onDirectionInput = default;

        [SerializeField]
        private UnityEvent onMovementStart = default;

        [SerializeField]
        private UnityEvent onMovementEnd = default;

        [SerializeField]
        private UnityEvent<Vector2> onTargetPositionUpdate = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private new Camera camera;
        protected Camera Camera => camera == null ? camera = Camera.main : camera;

        private bool isActive = false;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Start()
        {
            if (movementKey != KeyCode.None)
            {
                isActive = true;
                onMovementStart?.Invoke();
            }
        }

        private void Update()
        {
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = Camera.nearClipPlane;
            float direction = 0f;
            Vector2 targetPosition = Camera.ScreenToWorldPoint(mousePos);
            if (movementKey == KeyCode.None || Input.GetKey(movementKey))
            {
                direction = (targetPosition - (Vector2)target.position).x * adjustmentSpeed;
                onTargetPositionUpdate?.Invoke(targetPosition);
                if (!isActive)
                {
                    isActive = true;
                    onMovementStart?.Invoke();
                }
            }
            else if (isActive)
            {
                isActive = false;
                onMovementEnd?.Invoke();
            }

            // Vector2 targetPosition = gameArea.LoopPoint(Camera.ScreenToWorldPoint(mousePos));
            // if (movementKey == KeyCode.None || Input.GetKey(movementKey))
            // {
            //     direction = targetPosition.x - target.position.x;
            //     float[] directions = new float[]
            //     {
            //         gameArea.Max.x - target.position.x + targetPosition.x - gameArea.Min.x,
            //         gameArea.Min.x - target.position.x + targetPosition.x - gameArea.Max.x
            //     };
            //     for (int i = 0; i < directions.Length; i++)
            //     {
            //         if (Mathf.Abs(direction) > Mathf.Abs(directions[i]))
            //         {
            //             direction = directions[i];
            //         }
            //     }
            //     onTargetPositionUpdate?.Invoke(targetPosition);
            //     if (!isActive)
            //     {
            //         isActive = true;
            //         onMovementStart?.Invoke();
            //     }
            // }
            // else if (isActive)
            // {
            //     isActive = false;
            //     onMovementEnd?.Invoke();
            // }
            onDirectionInput?.Invoke(direction);
        }
    }
}
