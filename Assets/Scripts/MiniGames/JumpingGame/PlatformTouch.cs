namespace HappyGames.Minigames.JumpingGame
{
    using HappyGames.TimeManagement;
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Events;

    public class PlatformTouch : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The vertical motion of the platform.")]
        private AnimationCurve motion = new AnimationCurve(new Keyframe(), new Keyframe(0.1f, -0.5f), new Keyframe(0.4f, 0f));

        [SerializeField, Tooltip("The timeline to use to move the platform.")]
        private Timeline timeline = null;

        [SerializeField, Tooltip("The object to move.")]
        private MovingObject target = null;

        [Space, SerializeField, Tooltip("Called when the position was updated.")]
        private UnityEvent onPositionUpdated = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private Vector2 startPosition;
        private Coroutine move;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Start()
        {
            startPosition = transform.localPosition;
        }

        private void OnRayPhysicsEnter2D()
        {
            MovePlatform();
        }

        public void MovePlatform()
        {
            if (move != null)
            {
                StopCoroutine(move);
            }
            move = StartCoroutine(DoMovePlatform());
        }

        private IEnumerator DoMovePlatform()
        {
            float time = 0f;
            float maxTime = motion[motion.length - 1].time;
            while (time < maxTime)
            {
                time += timeline.DeltaTime;
                target.MoveTo((Vector2)transform.parent.position + startPosition + Vector2.up * motion.Evaluate(time));
                onPositionUpdated?.Invoke();
                yield return null;
            }
            target.MoveTo((Vector2)transform.parent.position + startPosition);
            onPositionUpdated?.Invoke();
            move = null;
        }
    }
}
