namespace HappyGames.Minigames.JumpingGame
{
    using HappyGames.Minigames.Utilities;
    using UnityEngine;
    using UnityEngine.Events;

    public class MapSegment : MovingObject
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The are where the segment should be visible.")]
        protected GameArea area = null;

        [SerializeField, Min(0f), Tooltip("The height of the segment.")]
        protected float segmentHeight = 0f;
        /// <summary>
        /// The height of the segment.
        /// </summary>
        public float SegmentHeight => segmentHeight;

        [SerializeField, Tooltip("The platforms to enable during the plant growth powerup.")]
        protected GameObject[] extraPlatforms = default;

        [SerializeField, Tooltip("The slime in the segment.")]
        protected GameObject slime = null;

        [Space, SerializeField, Tooltip("Called when the segment enters the area.")]
        protected UnityEvent onAreaEnter = default;
        /// <summary>
        /// Called when the segment enters the area.
        /// </summary>
        public event UnityAction OnAreaEnter
        {
            add => onAreaEnter.AddListener(value);
            remove => onAreaEnter.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called when the segment exits the area.")]
        protected UnityEvent onAreaExit = default;
        /// <summary>
        /// Called when the segment exits the area.
        /// </summary>
        public event UnityAction OnAreaExit
        {
            add => onAreaExit.AddListener(value);
            remove => onAreaExit.RemoveListener(value);
        }


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// True if there is a slime in the segment.
        /// </summary>
        public bool HasSlime => slime;
        private bool isInside = false;


        // --- | Methods | -------------------------------------------------------------------------------------------------

#if (UNITY_EDITOR)

        private void OnDrawGizmosSelected()
        {
            if (area)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawRay((Vector2)transform.position + Vector2.left * area.Size.x * 0.5f, Vector2.down * segmentHeight);
                Gizmos.DrawRay((Vector2)transform.position + Vector2.left * area.Size.x * 0.5f, Vector2.right * area.Size.x);
                Gizmos.DrawRay((Vector2)transform.position + Vector2.right * area.Size.x * 0.5f, Vector2.down * segmentHeight);
                Gizmos.DrawRay((Vector2)transform.position + Vector2.left * area.Size.x * 0.5f + Vector2.down * segmentHeight, Vector2.right * area.Size.x);
            }
        }

#endif

        private void Awake()
        {
            if (slime)
            {
                slime.SetActive(false);
            }
        }

        private void Start()
        {
            if (transform.position.y < area.Min.y)
            {
                isInside = false;
                onAreaExit?.Invoke();
            }
            else if (transform.position.y <= area.Max.y)
            {
                isInside = true;
                onAreaEnter?.Invoke();
            }
        }

        /// <inheritdoc/>
        public override void MoveTo(Vector2 position)
        {
            base.MoveTo(position);
            if (position.y < area.Min.y)
            {
                if (isInside)
                {
                    isInside = false;
                    onAreaExit?.Invoke();
                }
            }
            else if (!isInside && position.y <= area.Max.y)
            {
                isInside = true;
                onAreaEnter?.Invoke();
            }
        }

        /// <summary>
        /// Enables the extra platforms.
        /// </summary>
        public void EnableExtraPlaltforms()
        {
            foreach (GameObject platform in extraPlatforms)
            {
                platform.SetActive(true);
            }
        }

        /// <summary>
        /// Disables the extra platforms.
        /// </summary>
        public void DisableExtraPlatforms()
        {
            foreach (GameObject platform in extraPlatforms)
            {
                platform.SetActive(false);
            }
        }

        /// <summary>
        /// Shows the slime in the segment.
        /// </summary>
        public void ShowSlime()
        {
            if (slime)
            {
                slime.SetActive(true);
            }
        }

        /// <summary>
        /// Clones the segment.
        /// </summary>
        /// <param name="area">The area the segment is part of.</param>
        /// <param name="position">The position of the segment.</param>
        /// <param name="parent">The parent of the segment.</param>
        /// <returns>The cloned segment.</returns>
        public MapSegment Clone(GameArea area, Vector2 position, Transform parent)
        {
            MapSegment clone = Instantiate(this, position, Quaternion.identity, parent);
            clone.area = area;
            return clone;
        }
    }

    // --- | Classes | -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// An <see cref="UnityEvent"/> with a <see cref="MapSegment"/> as attribute.
    /// </summary>
    [System.Serializable]
    public class MapSegmentEvent : UnityEvent<MapSegment> { }
}
