namespace HappyGames.Minigames.Fruitbasket
{
    using UnityEngine;
    using HappyGames.Minigames.Utilities;
    using UnityEngine.Events;

    public class Fireball : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Min(1), Tooltip("The damage the fireball should deal.")]
        protected int damge = 1;

        [SerializeField, Tooltip("Called when the fireball hits something.")]
        protected UnityEvent onHit = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnCollisionEnter(Collision collision)
        {
            CharacterHealth health = collision.collider.GetComponent<CharacterHealth>();
            if (health)
            {
                health.TakeDamage(damge);
            }
            onHit?.Invoke();
            Destroy(gameObject);
        }
    }
}
