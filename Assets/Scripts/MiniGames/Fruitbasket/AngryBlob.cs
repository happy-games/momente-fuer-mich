namespace HappyGames.Minigames.Fruitbasket
{
    using HappyGames.Data;
    using UnityEngine;

    public class AngryBlob : MonoBehaviour
    {
        [SerializeField]
        private Fruit fruit = default;

        private void Awake()
        {
            fruit.OnCollected += EnableEmotion;
        }

        private void EnableEmotion()
        {
            //ProfileManager.GetActiveProfile().UnlockEmotion(Emotion.EmotionType.Angry);
            ProfileManager.GetActiveProfile().SetStoryPart(2);
            BlobIconActivator.Instance.Activate();
        }
    }
}
