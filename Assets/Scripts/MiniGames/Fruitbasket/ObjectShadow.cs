namespace HappyGames.Minigames.Fruitbasket
{
    using UnityEngine;
    using UnityEngine.Events;

    public class ObjectShadow : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The object casting the shadow.")]
        protected Rigidbody target = null;

        [SerializeField, Tooltip("The offset to the targets center.")]
        protected float shadowOffset = 0f;

        [Space, SerializeField, Tooltip("Called when the shadow is in range.")]
        protected UnityEvent onShadowEnable = default;

        [SerializeField, Tooltip("Called when the shadow is out of range.")]
        protected UnityEvent onShadowDisable = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private const float MAX_SHADOW_DISTANCE = 8f;
        private bool hasShadow = true;
        private Vector3 eulers;
        private Vector3 scale;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            eulers = transform.localEulerAngles;
            scale = transform.localScale;
        }

        private void FixedUpdate()
        {
            if (EnableShadow(target.SweepTest(Vector2.down, out RaycastHit hit, MAX_SHADOW_DISTANCE)))
            {
                transform.position = target.position + Vector3.down * (hit.distance + shadowOffset);
                transform.localScale = scale * (1f - hit.distance / MAX_SHADOW_DISTANCE);
                transform.eulerAngles = eulers;
            }
        }

        /// <summary>
        /// Enables or disables the shadow.
        /// </summary>
        /// <param name="hasShadow">True if the shadow is visible.</param>
        /// <returns>True if the shadow is visible.</returns>
        private bool EnableShadow(bool hasShadow)
        {
            if (this.hasShadow != hasShadow)
            {
                this.hasShadow = hasShadow;
                if (hasShadow)
                {
                    onShadowEnable?.Invoke();
                }
                else
                {
                    onShadowDisable?.Invoke();
                }
            }
            return hasShadow;
        }
    }
}
