namespace HappyGames.Minigames.Fruitbasket
{
    using UnityEngine;

    public class ForceAbility : Utilities.EmotionSingleUseAbility
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Min(0f), Tooltip("The range of the force field.")]
        protected float range = 20f;

        [SerializeField, Min(0f), Tooltip("The strength of the force field.")]
        protected float force = 10f;

        [SerializeField, Tooltip("The offset of the origin.")]
        protected Vector2 origin = Vector2.zero;

        [SerializeField, Tooltip("The layers to effect.")]
        protected LayerMask effectedLayers = 0;


        // --- | Methods | -------------------------------------------------------------------------------------------------

#if (UNITY_EDITOR)

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = new Color(0f, 0.5f, 1f);
            Gizmos.DrawWireSphere(transform.position + (Vector3)origin, range);
        }

#endif

        /// <inheritdoc/>
        public override void UseAbility()
        {
            Vector3 positition = transform.position + (Vector3)origin;
            foreach (Collider collider in Physics.OverlapSphere(positition, range, effectedLayers))
            {
                if (collider.attachedRigidbody)
                {
                    Vector2 direction = collider.transform.position - positition;
                    collider.attachedRigidbody.velocity += (Vector3)direction.normalized * force * Mathf.Clamp01(1f - direction.sqrMagnitude / (range * range));
                }
            }
            base.UseAbility();
        }
    }
}
