namespace HappyGames.Minigames.Fruitbasket
{
    using HappyGames.TimeManagement;
    using UnityEngine;
    using HappyGames.Minigames.Utilities;

    [RequireComponent(typeof(Rigidbody))]
    public class PlayerPhysics : MonoBehaviour
    {
        //  --- | Inspector | ----------------------------------------------------------------------------------------------

        [SerializeField, Min(0f), Tooltip("The speed the player is moving in units per second.")]
        protected float speed = 3f;

        [SerializeField, Min(0f), Tooltip("The speed gain per second.")]
        protected float acceleration = 1f;

        [SerializeField, Tooltip("The area to move inside.")]
        protected GameArea gameArea = null;

        [SerializeField, Tooltip("The timeline to use to calculate the movement.")]
        protected Timeline timeline = null;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected float targetSpeed = 0f;
        /// <summary>
        /// The horizontal velocity of the player.
        /// </summary>
        public float Velocity { get; protected set; } = 0f;

        protected Rigidbody body;

        protected float modifiedSpeed = 0f;


        // --- | Methods | -------------------------------------------------------------------------------------------------
        // MonoBehaviour --------------------------------------------------------------------------

        protected virtual void Awake()
        {
            body = GetComponent<Rigidbody>();
            ResetSpeedModifier();
        }

        protected virtual void FixedUpdate()
        {
            // Update the velocity.
            Velocity = Mathf.MoveTowards(Velocity, targetSpeed, acceleration * timeline.FixedDeltaTime);

            // Update the position.
            if (gameArea.ClampPoint(transform.position + Vector3.right * Velocity * timeline.FixedDeltaTime, Vector2.zero, out Vector2 position))
            {
                Velocity = 0f;
            }
            body.MovePosition(position);
        }

        // Movement -------------------------------------------------------------------------------

        /// <summary>
        /// Updates the direction the character is moving.
        /// </summary>
        /// <param name="direction">The direction to move.</param>
        public void Move(float direction)
        {
            // Clamp the value.
            direction = Mathf.Clamp(direction, -1f, 1f);
            // Set the target speed.
            targetSpeed = modifiedSpeed * direction;
        }

        // Speed ----------------------------------------------------------------------------------

        /// <summary>
        /// Modifies the speed by multiplying the given value.
        /// </summary>
        /// <param name="modifier">The value to muliply the speed with.</param>
        public void ModifySpeedRelative(float modifier)
        {
            modifiedSpeed = speed * modifier;
        }

        /// <summary>
        /// Modifies the speed by adding speed.
        /// </summary>
        /// <param name="modifier">The speed to add.</param>
        public void ModifySpeedAdditive(float modifier)
        {
            modifiedSpeed = speed + modifier;
        }

        /// <summary>
        /// Modifies the speed by overriding it.
        /// </summary>
        /// <param name="modifier"></param>
        public void ModifySpeedAbsolute(float modifier)
        {
            modifiedSpeed = modifier;
        }

        /// <summary>
        /// Resets the speed to the default value, reverts all speed modifications.
        /// </summary>
        public void ResetSpeedModifier()
        {
            modifiedSpeed = speed;
        }
    }
}
