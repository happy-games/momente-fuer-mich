namespace HappyGames.Minigames.Fruitbasket
{
    using HappyGames.Data;
    using HappyGames.UI;
    using UnityEngine;

    public class FruitScoreValidation : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The resources you get depending on the score.")]
        protected ScoreResource[] resources = default;

        [SerializeField, Tooltip("The script storing the score.")]
        private FruitScore score = null;

        [SerializeField, Tooltip("The game over screen displaying the score.")]
        private GameOverScreen gameOverScreen = null;

        [SerializeField, Tooltip("The fields to display the score.")]
        private HighScoreDisplay highScoreDisplay = null;

        [SerializeField, Tooltip("The sprites for the trophies.")]
        private Sprite[] trophySprites = null;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The name of the highscore.
        /// </summary>
        public const string HIGHSCORE_KEY = "Fruitbasket";


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Applies the score to the profile.
        /// </summary>
        public void Validate()
        {
            ScoreResource resources = GetReward(score.Score);
            UserProfile profile = ProfileManager.GetActiveProfile();
            int oldResourceCount = profile.Inventory.ResourceCount;
            profile.Inventory.AddResources(resources.resources);
            profile.Inventory.AddTrophy(new FruitBasketTrophy((RankType)resources.fruitBasketSize, score.Score));
            gameOverScreen?.Show(trophySprites[resources.fruitBasketSize - 1], oldResourceCount, profile.Inventory.ResourceCount);
            int highscore = profile.GetHighscore(HIGHSCORE_KEY);
            if (highscore < score.Score)
            {
                profile.SetHighscore(HIGHSCORE_KEY, score.Score);
            }
            highScoreDisplay?.DisplayScores(score.Score, highscore);
        }

        /// <summary>
        /// Calculates the reward for a given score.
        /// </summary>
        /// <returns>The resources rewarded for the score.</returns>
        private ScoreResource GetReward(int score)
        {
            ScoreResource current = resources[0];
            foreach (ScoreResource resource in resources)
            {
                if (current.score > score || current.score <= resource.score && resource.score <= score)
                {
                    current = resource;
                }
            }
            return current;
        }


        // --- | Classes | -------------------------------------------------------------------------------------------------

        [System.Serializable]
        protected struct ScoreResource
        {
            public int score;
            public int resources;
            public int fruitBasketSize;

            public ScoreResource(int score, int resources, int fruitBasketSize)
            {
                this.score = score;
                this.resources = resources;
                this.fruitBasketSize = fruitBasketSize;
            }
        }
    }
}
