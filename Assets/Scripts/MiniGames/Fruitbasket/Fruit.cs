namespace HappyGames.Minigames.Fruitbasket
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Events;

    public class Fruit : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Min(1), Tooltip("The points the fruit is worth.")]
        protected int points = 1;
        /// <summary>
        /// The points the fruit is worth.
        /// </summary>
        public int Points => points;

        [SerializeField, Tooltip("The layers disabling the fruit on collision.")]
        protected LayerMask obstacleLayers = 0;

        [SerializeField, Min(0f), Tooltip("The time it takes for the fruit to despawn after colliding.")]
        protected float despawnDuration = 2f;

        [Space, SerializeField, Tooltip("Called when the despawn time gets updated.")]
        protected UnityEvent<float, float> onDespawnUpdate = default;

        [SerializeField, Tooltip("Called when the object is despawned.")]
        protected UnityEvent onDespawnEnd = default;
        /// <summary>
        /// Called when the object is despawned.
        /// </summary>
        public event UnityAction OnDespawnEnd
        {
            add => onDespawnEnd.AddListener(value);
            remove => onDespawnEnd.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called when the fruit was collected.")]
        private UnityEvent onCollected = default;
        /// <summary>
        /// Called when the fruit was collected.
        /// </summary>
        public event UnityAction OnCollected
        {
            add => onCollected.AddListener(value);
            remove => onCollected.RemoveListener(value);
        }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnCollisionEnter(Collision collision)
        {
            if (enabled && obstacleLayers.Contains(collision.gameObject.layer))
            {
                StartCoroutine(DoDespawn(despawnDuration));
            }
        }

        /// <summary>
        /// Despawns the fruit.
        /// </summary>
        /// <param name="duration">The time it takes the fruit to despawn.</param>
        protected IEnumerator DoDespawn(float duration)
        {
            float time = 0f;
            enabled = false;
            while (time < duration)
            {
                onDespawnUpdate?.Invoke(time, duration);
                yield return null;
                time += Time.deltaTime;
            }
            onDespawnUpdate?.Invoke(duration, duration);
            onDespawnEnd?.Invoke();
            Destroy(gameObject);
        }

        /// <summary>
        /// Collects the fruit.
        /// </summary>
        public void Collect()
        {
            onCollected?.Invoke();
            Destroy(gameObject);
        }
    }
}
