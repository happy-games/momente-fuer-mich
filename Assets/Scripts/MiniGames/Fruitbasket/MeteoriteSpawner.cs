namespace HappyGames.Minigames.Fruitbasket
{
    using HappyGames.Minigames.Utilities;
    using HappyGames.TimeManagement;
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Events;

    public class MeteoriteSpawner : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [Header("References")] // -----------------------------------------------------------------
        [SerializeField, Tooltip("The prefab for the meteorites.")]
        protected Rigidbody meteoritePrefab = default;

        [SerializeField, Tooltip("The timeline to update the spawn delay.")]
        protected Timeline timeline = default;

        [SerializeField, Tooltip("The area the meteorites can spawn in.")]
        protected GameArea area = default;

        [SerializeField, Tooltip("The score to change the meteorite spawn rate.")]
        protected FruitScore score = default;

        [Header("Spawn Delay")] // ----------------------------------------------------------------
        [SerializeField, Tooltip("The minimum time it takes to spawn a meteorite, relative to the current score.")]
        protected AnimationCurve spawnMinDelay = new AnimationCurve(new Keyframe(0f, 2f), new Keyframe(5000f, 0.2f));

        [SerializeField, Tooltip("The maximum time it takes to spawn a meteorite, relative to the current score.")]
        protected AnimationCurve spawnMaxDelay = new AnimationCurve(new Keyframe(0f, 5f), new Keyframe(5000f, 2f));

        [Header("Spawn Speed")] // ----------------------------------------------------------------
        [SerializeField, Tooltip("The minumum speed of the meteorite when been spawned relative to the current score.")]
        protected AnimationCurve meteoriteMinSpeed = new AnimationCurve(new Keyframe(0f, 7f), new Keyframe(5000f, 15f));

        [SerializeField, Tooltip("The maximum speed of the meteorite when been spawned relative to the current score.")]
        protected AnimationCurve meteoriteMaxSpeed = new AnimationCurve(new Keyframe(0f, 10f), new Keyframe(5000f, 20f));

        [Header("Events")] // ---------------------------------------------------------------------
        [SerializeField, Tooltip("Called when a meteorite was spawned.")]
        protected UnityEvent<Rigidbody> onSpawn = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The offset in the z directon.
        /// </summary>
        protected const float Z_OFFSET = 0.25f;

        /// <summary>
        /// The container holding the meteorites.
        /// </summary>
        protected Transform Container => transform;

        private Vector3 spawnPosition;
        private Vector2 minDirection;
        private Vector2 maxDirection;
        protected float nextSpawn;
        protected Coroutine spawnUpdate = null;

        /// <summary>
        /// True if the spawner is enabled and active.
        /// </summary>
        public bool IsSpawning => spawnUpdate != null;


        // --- | Methods | -------------------------------------------------------------------------------------------------

#if (UNITY_EDITOR)

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(spawnPosition, minDirection);
            Gizmos.DrawRay(spawnPosition, maxDirection);
        }

#endif

        /// <summary>
        /// Enables the spawning of meteorites.
        /// </summary>
        public void StartSpawning()
        {
            if (!IsSpawning)
            {
                SpawnWithDelayByScore();
            }
        }

        /// <summary>
        /// Stops the spawning of meteorites.
        /// </summary>
        public void StopSpawning()
        {
            if (IsSpawning)
            {
                StopCoroutine(spawnUpdate);
                spawnUpdate = null;
            }
        }

        /// <summary>
        /// Spawns a meteorite with the delay between the min-, max-spawn delays.
        /// </summary>
        protected void SpawnWithDelayByScore()
        {
            if (!IsSpawning)
            {
                spawnUpdate = StartCoroutine(DoSpawn(Random.Range(spawnMinDelay.Evaluate(score.Score), spawnMaxDelay.Evaluate(score.Score))));
            }
        }

        /// <summary>
        /// Spawns a meteorite after a given delay.
        /// </summary>
        /// <param name="delay">The time to wait before spawning.</param>
        protected IEnumerator DoSpawn(float delay)
        {
            float time = 0f;
            while (time < delay)
            {
                time += timeline.DeltaTime;
                yield return null;
            }
            spawnUpdate = null;
            SpawnMeteorite();
            SpawnWithDelayByScore();
        }

        /// <summary>
        /// Spawns a meteorite.
        /// </summary>
        [ContextMenu("Debug/Spawn")]
        protected void SpawnMeteorite()
        {
            spawnPosition = new Vector3(area.Min.x + Random.Range(0f, area.Size.x), area.Max.y, Random.Range(-Z_OFFSET, Z_OFFSET));
            minDirection = area.Min - (Vector2)spawnPosition;
            maxDirection = new Vector2(area.Max.x, area.Min.y) - (Vector2)spawnPosition;
            float minAngle = Vector2.Angle(maxDirection, Vector2.left) - 90f;
            float maxAngle = Vector2.Angle(minDirection, Vector2.left) - 90f;
            float angle = Random.Range(minAngle, maxAngle);
            Rigidbody meteorite = Instantiate(meteoritePrefab, spawnPosition, Quaternion.AngleAxis(angle, Vector3.forward), Container);
            meteorite.velocity = -meteorite.transform.up * Random.Range(meteoriteMinSpeed.Evaluate(score.Score), meteoriteMaxSpeed.Evaluate(score.Score));
            onSpawn?.Invoke(meteorite);
        }
    }
}
