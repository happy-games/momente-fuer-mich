namespace HappyGames.Minigames.Fruitbasket
{
    using HappyGames.Data;
    using UnityEngine;
    using UnityEngine.Events;

    public class FruitScore : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tag, Tooltip("The name of the tag to look out for charges.")]
        protected string chargeTag = string.Empty;

        [Space, SerializeField, Tooltip("Called when a fruit was collected.")]
        protected UnityEvent<Fruit> onFruitCollected = default;

        [SerializeField, Tooltip("Called when the players score changes.")]
        protected UnityEvent<int> onScoreUpdated = default;

        [SerializeField, Tooltip("Called when an emotion charge was collected.")]
        protected UnityEvent onChargeCollected = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The score of the player.
        /// </summary>
        public int Score { get; protected set; } = 0;

        /// <summary>
        /// True if the script is activly counting the score.
        /// </summary>
        public bool IsActive { get; protected set; } = true;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnTriggerEnter(Collider other)
        {
            Fruit fruit = other.GetComponent<Fruit>();
            if (fruit && fruit.enabled)
            {
                onFruitCollected?.Invoke(fruit);
                Score += fruit.Points;
                onScoreUpdated?.Invoke(Score);
                if (fruit.gameObject.tag == chargeTag)
                {
                    onChargeCollected?.Invoke();
                }
                fruit.Collect();
            }
        }

        /// <summary>
        /// Stops the score counting.
        /// </summary>
        public void StopCount()
        {
            IsActive = false;
        }
    }
}
