namespace HappyGames.Minigames.Fruitbasket
{
    using UnityEngine;
    using UnityEngine.Events;

    public class FruitTypeFilter : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The filters to use.")]
        protected FruitTypeSelector[] filters = default;

        [Space, SerializeField, Tooltip("Called when one of the filters was used.")]
        protected UnityEvent<Fruit> onFitlered = default;

        [SerializeField, Tooltip("Called when none of the filters was used.")]
        protected UnityEvent<Fruit> onNotFilterd = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Checks all registed filters for the fruits type.
        /// </summary>
        /// <param name="fruit">The frui to filter.</param>
        public void CheckFruit(Fruit fruit)
        {
            bool wasFilterd = false;

            foreach (FruitTypeSelector filter in filters)
            {
                wasFilterd = filter.Filter(fruit) ? true : wasFilterd;
            }

            if (wasFilterd)
            {
                onFitlered?.Invoke(fruit);
            }
            else
            {
                onNotFilterd?.Invoke(fruit);
            }
        }


        // --- | Classes | -------------------------------------------------------------------------------------------------

        [System.Serializable]
        protected class FruitTypeSelector
        {
            // Inspector --------------------------------------------------------------------------
            [SerializeField, LockInPlaymode, Tooltip("An object(prefab) of the fruits to filter.")]
            private Fruit[] filteredFruits = default;

            [SerializeField, Tooltip("Called when the a fruit of one of the filtered types was detected.")]
            private UnityEvent<Fruit> onDetected = default;
            [SerializeField, Tooltip("Called when the fruit beeing filterd was not of the filterd types.")]
            private UnityEvent<Fruit> onNotDetected = default;


            // Variables --------------------------------------------------------------------------

            private System.Type[] filterdTypes = null;


            // Methods ----------------------------------------------------------------------------

            /// <summary>
            /// Checks if the fruit has a type of the filtered ones.
            /// </summary>
            /// <param name="fruit">The fruit whos type to check.</param>
            /// <returns>True if the fruit is part of the filter.</returns>
            public bool Filter(Fruit fruit)
            {
                if (filterdTypes == null)
                {
                    AssignFilterdTypes();
                }
                System.Type fruitType = fruit.GetType();
                foreach (System.Type filter in filterdTypes)
                {
                    Debug.Log($"fruit: { fruitType }, filter: { filter }, sublass: { fruitType.IsSubclassOf(filter) }");
                    if (fruitType == filter || fruitType.IsSubclassOf(filter))
                    {
                        onDetected?.Invoke(fruit);
                        return true;
                    }
                }
                onNotDetected?.Invoke(fruit);
                return false;
            }

            /// <summary>
            /// Fills the <see cref="filterdTypes"/> array with the types of the <see cref="filteredFruits"/>.
            /// </summary>
            private void AssignFilterdTypes()
            {
                filterdTypes = new System.Type[filteredFruits.Length];
                for (int i = 0; i < filterdTypes.Length; i++)
                {
                    filterdTypes[i] = filteredFruits[i].GetType();
                }
            }
        }
    }
}
