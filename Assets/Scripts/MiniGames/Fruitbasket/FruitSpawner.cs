namespace HappyGames.Minigames.Fruitbasket
{
    using HappyGames.Data;
    using HappyGames.Minigames.Utilities;
    using HappyGames.Utilities;
    using UnityEngine;

    public class FruitSpawner : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The area determening where to spawn the fruit.")]
        protected GameArea gameArea = null;

        [SerializeField, Tooltip("The time between spawns relative to the score.")]
        protected AnimationCurve spawnRate = new AnimationCurve(new Keyframe(), new Keyframe(1f, 1f));

        [SerializeField, Tooltip("The amount of object spawned per wave.")]
        protected AnimationCurve waveSize = new AnimationCurve(new Keyframe(1f, 1f), new Keyframe(1f, 1f));

        [SerializeField, Min(0), Tooltip("The amount of points the objects can spawn.")]
        protected int spawnPointCount = 10;

        [SerializeField, Tooltip("The prefabs to spawn.")]
        protected WeightedPrefabCollection<GameObject> prefabs = default;

        [SerializeField, Tooltip("The angry blob to spawn.")]
        protected Fruit angryBlob = default;

        [SerializeField, Min(0), Tooltip("The score that must be reached to spawn the angry blob.")]
        protected int angrySpawnScore = 500;



        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected float currentScore = 0f;
        protected float time = 0f;
        protected float delay = 0f;
        protected int[] spawnDelay;
        /// <summary>
        /// The container holding the spawned fruit.
        /// </summary>
        protected Transform Container => transform;
        protected WaveModification waveModification = WaveModification.None;
        protected float waveModifier = 0f;
        /// <summary>
        /// True if the spawner is active.
        /// </summary>
        public bool IsActive { get; protected set; }
        protected Fruit spawnedAngryBlob = null;
        protected UserProfile profile;
        protected bool needsEmotion = false;
        /// <summary>
        /// The emotion of the collectable blob.
        /// </summary>
        private const int STORY_PART = 2;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnDrawGizmosSelected()
        {
            if (gameArea && spawnPointCount > 0)
            {
                Gizmos.color = Color.blue;
                for (int i = 0; i < spawnPointCount; i++)
                {
                    Gizmos.DrawRay(gameArea.Max + Vector2.left * gameArea.Size.x / (spawnPointCount) * (i + 0.5f), Vector2.up * 0.25f);
                }
            }
        }

        private void OnEnable()
        {
            ChangeProfile(null, ProfileManager.GetActiveProfile());
            ProfileManager.OnProfileActivated += ChangeProfile;
        }

        protected void OnDisable()
        {
            ProfileManager.OnProfileActivated -= ChangeProfile;
            ChangeProfile(profile, null);
        }

        private void Start()
        {
            delay = 1f / spawnRate.Evaluate(currentScore);
            spawnDelay = new int[spawnPointCount];
        }

        private void Update()
        {
            if (IsActive)
            {
                while (time >= delay)
                {
                    time -= delay;
                    int count = GetWaveSize();
                    for (int i = 0; i < count; i++)
                    {
                        SpawnFruit(i == 0);
                    }
                }
                time += Time.deltaTime;
            }
        }

        /// <summary>
        /// Spawns a fruit at a random position in the area.
        /// </summary>
        private void SpawnFruit(bool updateCount)
        {
            if (needsEmotion && currentScore >= angrySpawnScore && !spawnedAngryBlob)
            {
                spawnedAngryBlob = Instantiate(angryBlob, GetRandomSpawnPoint(updateCount), Quaternion.Euler(0f, 0f, Random.Range(0f, 360f)), Container);
                spawnedAngryBlob.OnDespawnEnd += BlobDespawnListener;
            }
            else
            {
                Instantiate(prefabs.GetElement(currentScore), GetRandomSpawnPoint(updateCount), Quaternion.Euler(0f, 0f, Random.Range(0f, 360f)), Container);
            }
        }

        /// <summary>
        /// Updates the score determening the prefabs to spawn.
        /// </summary>
        /// <param name="score">The new score.</param>
        public void UpdateScore(int score)
        {
            currentScore = score;
            delay = 1f / spawnRate.Evaluate(currentScore);
        }

        private Vector3 GetRandomSpawnPoint(bool updateCount)
        {
            Vector3 point = new Vector3
            (
                Random.Range(gameArea.Min.x + 0.5f, gameArea.Max.x - 0.5f),
                gameArea.Max.y,
                Random.Range(-0.5f, 0.5f)
            );
            if (spawnPointCount > 0)
            {
                int totalDelay = 0;
                for (int i = 0; i < spawnPointCount; i++)
                {
                    if (updateCount)
                    {
                        spawnDelay[i]++;
                    }
                    totalDelay += spawnDelay[i];
                }

                int selection = Random.Range(0, totalDelay);
                int index = 0;
                for (int i = 0; i < spawnPointCount; i++)
                {
                    if (totalDelay > selection)
                    {
                        index = i;
                    }
                    totalDelay -= spawnDelay[i];
                }
                spawnDelay[index] = 0;
                point = new Vector3(gameArea.Max.x - gameArea.Size.x / (spawnPointCount) * (float)(index + 0.5f), gameArea.Max.y, Random.Range(-0.5f, 0.5f));
            }
            return point;
        }

        /// <summary>
        /// Calculates the modified wave size.
        /// </summary>
        /// <returns>The wave size to use for spawning.</returns>
        protected int GetWaveSize()
        {
            int baseValue = Random.Range(1, Mathf.CeilToInt(waveSize.Evaluate(currentScore)));
            switch (waveModification)
            {
                default: return baseValue;
                case WaveModification.Absolute: return (int)waveModifier;
                case WaveModification.Additive: return baseValue + (int)waveModifier;
                case WaveModification.Relative: return Mathf.CeilToInt(baseValue * waveModifier);
            }
        }

        /// <summary>
        /// Overrides the wave size of the spawned fruit.
        /// </summary>
        /// <param name="modifier">The size to set the wave at.</param>
        public void ModifyWaveAbsolute(int modifier)
        {
            waveModification = WaveModification.Absolute;
            waveModifier = modifier;
        }

        /// <summary>
        /// Adds a value to the wave size.
        /// </summary>
        /// <param name="modifier">The value to add.</param>
        public void ModifyWaveAdditive(int modifier)
        {
            waveModification = WaveModification.Additive;
            waveModifier = modifier;
        }

        /// <summary>
        /// Multiplys the wave size with the given value.
        /// </summary>
        /// <param name="modifier">The value to modify with.</param>
        public void ModifyWaveRelative(float modifier)
        {
            waveModification = WaveModification.Relative;
            waveModifier = modifier;
        }

        /// <summary>
        /// Resets all wave modifications.
        /// </summary>
        public void ResetWaveModification()
        {
            waveModification = WaveModification.None;
        }

        /// <summary>
        /// Enables the spawner to spawn fruit.
        /// </summary>
        public void StartSpawning()
        {
            IsActive = true;
        }

        /// <summary>
        /// Disables the spawning of fruit.
        /// </summary>
        public void StopSpawning()
        {
            IsActive = false;
        }

        /// <summary>
        /// Changes the used profile.
        /// </summary>
        /// <param name="oldProfile">The previously used profile.</param>
        /// <param name="newProfile">The profile now in use.</param>
        protected void ChangeProfile(UserProfile oldProfile, UserProfile newProfile)
        {
            if (profile != null)
            {
                profile.OnStoryPartChanged -= CheckStory;
            }
            profile = newProfile;
            if (profile != null)
            {
                CheckStory(profile.StoryPart);
                profile.OnStoryPartChanged += CheckStory;
            }
            else
            {
                needsEmotion = false;
            }
        }

        /// <summary>
        /// Handles the unlocked emotions.
        /// </summary>
        /// <param name="emotion">The emotion unlocked.</param>
        protected void CheckStory(int part)
        {
            needsEmotion = part == STORY_PART - 1;
        }

        /// <summary>
        /// Listens for the blobs despawning and resets the <see cref="spawnedAngryBlob"/> variable.
        /// </summary>
        private void BlobDespawnListener()
        {
            spawnedAngryBlob.OnDespawnEnd -= BlobDespawnListener;
            spawnedAngryBlob = null;
        }

        protected enum WaveModification { None, Absolute, Additive, Relative }
    }
}
