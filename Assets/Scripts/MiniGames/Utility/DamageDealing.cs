namespace HappyGames.Minigames.JumpingGame
{
    using HappyGames.Minigames.Utilities;
    using UnityEngine;
    using UnityEngine.Events;

    public class DamageDealing : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Min(1), Tooltip("The damage the object deals.")]
        protected int damge = 1;

        [Space, SerializeField, Tooltip("Called when the object deals damage.")]
        protected UnityEvent<CharacterHealth> onDamageDelt = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnRayPhysicsEnter2D(GameObject other)
        {
            CharacterHealth health = other.GetComponent<CharacterHealth>();
            if (health)
            {
                health.TakeDamage(damge);
                onDamageDelt?.Invoke(health);
            }
        }
    }
}
