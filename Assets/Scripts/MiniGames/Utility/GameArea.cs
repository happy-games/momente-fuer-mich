namespace HappyGames.Minigames.Utilities
{
    using UnityEngine;

    /// <summary>
    /// An area to loop objects inside.
    /// </summary>
    public class GameArea : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The center of the area.")]
        protected Vector2 offset = Vector2.zero;

        [SerializeField, Tooltip("The size of the area.")]
        protected Vector2 size = Vector2.one;
        /// <summary>
        /// The size of the area.
        /// </summary>
        public Vector2 Size => size;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The center of the area.
        /// </summary>
        public Vector2 Center => (Vector2)transform.position + offset;

        /// <summary>
        /// The min positions of the area.
        /// </summary>
        public Vector2 Min => Center - size * 0.5f;

        /// <summary>
        /// The max positions of the area.
        /// </summary>
        public Vector2 Max => Center + size * 0.5f;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Checks if a point is inside the area.
        /// </summary>
        /// <param name="point">The point to check.</param>
        /// <param name="offset">The offset to apply to the area bounds.</param>
        /// <returns>True if the point is inside the area.</returns>
        public bool InsideCheck(Vector2 point, Vector2 offset) => 
            !(Min.x + offset.x > point.x || Min.y + offset.y > point.y || Max.x - offset.x < point.x || Max.y - offset.y < point.y);

        /// <summary>
        /// Clamps a point inside the area.
        /// </summary>
        /// <param name="point">The point to clamp.</param>
        /// <returns>The clamped point.</returns>
        public Vector2 ClampPoint(Vector2 point) => ClampPoint(point, Vector2.zero);
        /// <summary>
        /// Clamps a point inside the area.
        /// </summary>
        /// <param name="point">The point to clamp.</param>
        /// <param name="offset">An offset for the areas bounds.</param>
        /// <returns>The clamped point.</returns>
        public Vector2 ClampPoint(Vector2 point, Vector2 offset) => 
            new Vector2(Mathf.Clamp(point.x, Min.x + offset.x, Max.x - offset.x), Mathf.Clamp(point.y, Min.y + offset.y, Max.y - offset.y));
        /// <summary>
        /// Clamps a point inside the area.
        /// </summary>
        /// <param name="point">The point to clamp.</param>
        /// <param name="offset">An offset for the areas bounds.</param>
        /// <param name="clamp">The clamped point.</param>
        /// <returns>True if the value was clamped.</returns>
        public bool ClampPoint(Vector2 point, Vector2 offset, out Vector2 clamp)
        {
            if (InsideCheck(point, offset))
            {
                clamp = point;
                return false;
            }
            clamp = ClampPoint(point, offset);
            return true;
        }

        /// <summary>
        /// Loops a point inside the area.
        /// </summary>
        /// <param name="point">The point to loop.s</param>
        /// <returns>The position of the looped point.</returns>
        public Vector2 LoopPoint(Vector2 point) => LoopPoint(point, Vector2.zero);
        /// <summary>
        /// Loops a point inside the area.
        /// </summary>
        /// <param name="point">The point to loop.s</param>
        /// <param name="offset">An offset for the areas bounds.</param>
        /// <returns>The position of the looped point.</returns>
        public Vector2 LoopPoint(Vector2 point, Vector2 offset) 
            => new Vector2
            (
                Mathf.Repeat(point.x - Min.x - offset.x, size.x - offset.x * 2f) + Min.x + offset.x, 
                Mathf.Repeat(point.y - Min.y - offset.y, size.y - offset.y * 2f) + Min.y + offset.y
            );
        /// <summary>
        /// Loops a point inside the area.
        /// </summary>
        /// <param name="point">The point to loop.s</param>
        /// <param name="offset">An offset for the areas bounds.</param>
        /// <param name="loop">The looped point.</param>
        /// <returns>True if the value was looped.</returns>
        public bool LoopPoint(Vector2 point, Vector2 offset, out Vector2 loop)
        {
            if (InsideCheck(point, offset))
            {
                loop = point;
                return false;
            }
            loop = LoopPoint(point, offset);
            return true;
        }

#if (UNITY_EDITOR)

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawRay(Max, Vector2.left * size.x);
            Gizmos.DrawRay(Max, Vector2.down * size.y);
            Gizmos.DrawRay(Min, Vector2.right * size.x);
            Gizmos.DrawRay(Min, Vector2.up * size.y);
        }

#endif
    }
}
