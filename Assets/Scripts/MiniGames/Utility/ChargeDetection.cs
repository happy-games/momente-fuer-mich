namespace HappyGames.Minigames.Utilities
{
    using UnityEngine;
    using UnityEngine.Events;

    public class ChargeDetection : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tag, Tooltip("The tag to look out for.")]
        private string collectableTag = string.Empty;

        [Space, SerializeField, Tooltip("Called when a charge was collected.")]
        private UnityEvent onChargeCollected = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnTriggerEnter(Collider other) => Collect(other.gameObject);
        private void OnTriggerEnter2D(Collider2D collision) => Collect(collision.gameObject);

        /// <summary>
        /// Collects the collectable.
        /// </summary>
        /// <param name="collectable">The collectable to collect.</param>
        private void Collect(GameObject collectable)
        {
            if (collectable.tag == collectableTag)
            {
                Destroy(collectable);
                onChargeCollected?.Invoke();
            }
        }
    }
}
