namespace HappyGames.Minigames.Utilities
{
    using HappyGames.Events;
    using UnityEngine;
    using UnityEngine.Events;

    public class CharacterHealth : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Min(1), Tooltip("The staring health of the character.")]
        protected int maxHealth = 1;

        [Space, SerializeField, Tooltip("Called when the character takes damage.")]
        protected UnityEvent<int> onDamageTaken = default;

        [SerializeField, Tooltip("Called when the character gets hit, but is ignoring damage.")]
        protected UnityEvent onIgnoredDamage = default;

        [SerializeField, Tooltip("Called when the characters health updates.")]
        protected IntRangeEvent onHealthUpdate = default;

        [SerializeField, Tooltip("Called when the character dies.")]
        protected UnityEvent onDeath = default;

        [Space, SerializeField, Tooltip("Called when the character gets enabled to take damage.")]
        protected UnityEvent onDamageEnable = default;

        [SerializeField, Tooltip("Called when the character gets disabled to take damage.")]
        protected UnityEvent onDamageDisable = default;


        // --- | Varaiables | ----------------------------------------------------------------------------------------------

        /// <summary>
        /// True if dammage is ignored.
        /// </summary>
        public bool IgnoreDamage { get; protected set; } = false;

        /// <summary>
        /// The characterrs health.
        /// </summary>
        public int Health { get; protected set; }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            Health = maxHealth;
            onHealthUpdate?.Invoke(Health, maxHealth);
        }

        /// <summary>
        /// Deals damage to the character.
        /// </summary>
        /// <param name="amount">The amount of damage to deal.</param>
        public void TakeDamage(int amount)
        {
            if (!IgnoreDamage)
            {
                int deltDamage = Health;
                if (Health < amount)
                {
                    Health = 0;
                }
                else
                {
                    Health -= amount;
                }
                deltDamage -= Health;
                onDamageTaken?.Invoke(deltDamage);
                onHealthUpdate?.Invoke(Health, maxHealth);
                if (Health <= 0)
                {
                    Die();
                }
            }
        }

        /// <summary>
        /// Kills the palyer.
        /// </summary>
        public void Kill()
        {
            if (Health != 0)
            {
                Health = 0;
                onHealthUpdate?.Invoke(Health, maxHealth);
                Die();
            }
        }

        /// <summary>
        /// Kills the character.
        /// </summary>
        private void Die()
        {
            onDeath?.Invoke();
        }

        /// <summary>
        /// Enables the character to take damage.
        /// </summary>
        public void EnableDamgeTaking()
        {
            if (IgnoreDamage)
            {
                IgnoreDamage = false;
                onDamageEnable?.Invoke();
            }
        }

        /// <summary>
        /// Disables the character to take damage.
        /// </summary>
        public void DisableDamageTaking()
        {
            if (!IgnoreDamage)
            {
                IgnoreDamage = true;
                onDamageDisable?.Invoke();
            }
        }
    }
}
