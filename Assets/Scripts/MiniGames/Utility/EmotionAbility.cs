namespace HappyGames.Minigames.Utilities
{
    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    /// Manages an ability.
    /// </summary>
    public class EmotionAbility : EmotionAbilityBase
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The duration of the ability in seconds for every charge.")]
        protected float durationPerCharge = 1f;
        /// <summary>
        /// The duration of the ability in seconds for every charge.
        /// </summary>
        public float DurationPerCharge => durationPerCharge;

        [Space, SerializeField, Tooltip("Called when the ability starts.")]
        protected UnityEvent onAbilityStart = default;

        [SerializeField, Tooltip("Called when the ability ends.")]
        protected UnityEvent onAbilityEnd = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Starts the abilitys effect.
        /// </summary>
        public virtual void UseAbility() => onAbilityStart?.Invoke();

        /// <summary>
        /// Stops the abilitys effect.
        /// </summary>
        public virtual void StopAbility() => onAbilityEnd?.Invoke();
    }
}
