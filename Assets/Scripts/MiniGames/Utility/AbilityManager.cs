namespace HappyGames.Minigames.Utilities
{
    using HappyGames.Emotion;
    using HappyGames.Events;
    using HappyGames.TimeManagement;
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    /// Manages the players ablities.
    /// </summary>
    public class AbilityManager : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [Header("Charge")] // ---------------------------------------------------------------------
        [SerializeField, Tooltip("The amount of collectables to get for a charge.")]
        protected int maxCharge = 5;

        [SerializeField, Tooltip("True if collecting pickups during ailities is allowed.")]
        protected bool collectPickupsDuringAbility = true;

        [Space, SerializeField, Tooltip("Called when the charge updates.")]
        protected IntRangeEvent onChargeUpdate = default;

        [SerializeField, Tooltip("Called when the maximum charge is reached.")]
        protected UnityEvent onCharged = default;

        [SerializeField, Tooltip("Called when the charge was used.")]
        protected UnityEvent onChargeUsed = default;

        [Header("Abilities")] // ------------------------------------------------------------------
        [SerializeField, Tooltip("The timeline to update the abilities.")]
        protected Timeline timeline = null;

        [SerializeField, Tooltip("The abilities for all emotions.")]
        protected AblilityCollection abilities = default;

        [Space, SerializeField, Tooltip("Called when the duration of the spell changes.")]
        protected FloarRangeEvent onAbilityDurationUpdate = default;

        [SerializeField, Tooltip("Called when an ability is used.")]
        protected UnityEvent onAbilityStart = default;

        [SerializeField, Tooltip("Called when an used ability is over.")]
        protected UnityEvent onAblilityEnd = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The current charge amount.
        /// </summary>
        public int Charge { get; protected set; } = 0;

        /// <summary>
        /// True if the maximum charge is reached.
        /// </summary>
        public bool IsCharged => Charge >= maxCharge;

        protected float abilityDuration = 0f;
        protected Coroutine abilityUpdate = null;
        protected EmotionAbility usedAbility = null;
        /// <summary>
        /// True if an ability us currently used.
        /// </summary>
        public bool IsUsingAbility => abilityUpdate != null;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Start()
        {
            onChargeUpdate?.Invoke(0, maxCharge);
        }

        /// <summary>
        /// Adds to the charge.
        /// </summary>
        public void AddCharge()
        {
            if (!IsCharged && (!IsUsingAbility || collectPickupsDuringAbility))
            {
                Charge++;
                if (!IsUsingAbility)
                {
                    onChargeUpdate?.Invoke(Charge, maxCharge);
                    if (IsCharged)
                    {
                        onCharged?.Invoke();
                    }
                }
            }
        }

        /// <summary>
        /// Uses the charge.
        /// </summary>
        /// <param name="emotion">The emotion to use.</param>
        public void UseAbility(EmotionType emotion)
        {
            if (IsCharged && !IsUsingAbility)
            {
                EmotionAbilityBase ability = abilities.GetValue(emotion);
                if (ability is EmotionAbility)
                {
                    onChargeUsed?.Invoke();
                    abilityUpdate = StartCoroutine(DoUpdateAbility((EmotionAbility)ability));
                }
                else if (ability is EmotionSingleUseAbility)
                {
                    onChargeUsed?.Invoke();
                    ((EmotionSingleUseAbility)ability).UseAbility();
                    Charge = 0;
                    onChargeUpdate?.Invoke(0, maxCharge);
                }
            }
        }
        /// <summary>
        /// Updates the ability.
        /// </summary>
        /// <param name="ability">The ability to update.</param>
        protected IEnumerator DoUpdateAbility(EmotionAbility ability)
        {
            usedAbility = ability;
            float time = 0f;
            onAbilityStart?.Invoke();
            onAbilityDurationUpdate?.Invoke(Charge * ability.DurationPerCharge - time, ability.DurationPerCharge * maxCharge);
            ability.UseAbility();
            while (Charge > 0)
            {
                while (time < ability.DurationPerCharge)
                {
                    yield return null;
                    time += timeline.DeltaTime;
                    onAbilityDurationUpdate?.Invoke(Charge * ability.DurationPerCharge - time, ability.DurationPerCharge * maxCharge);
                }
                time -= ability.DurationPerCharge;
                Charge--;
            }
            usedAbility = null;
            ability.StopAbility();
            abilityUpdate = null;
            onAblilityEnd?.Invoke();
        }

        public void CancelAbility()
        {
            if (usedAbility)
            {
                usedAbility.StopAbility();
            }
        }


        // --- | Classes | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// A collection of abilities for all emotions.
        /// </summary>
        [System.Serializable]
        protected class AblilityCollection : EmotionCollection<EmotionAbilityBase> { }
    }
}
