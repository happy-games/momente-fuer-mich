namespace HappyGames.Minigames.Utilities
{
    using UnityEngine;
    using UnityEngine.Events;

    public class EmotionSingleUseAbility : EmotionAbilityBase
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called when the ability was used.")]
        protected UnityEvent onUse = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Uses the ability.
        /// </summary>
        public virtual void UseAbility() => onUse?.Invoke();
    }
}
