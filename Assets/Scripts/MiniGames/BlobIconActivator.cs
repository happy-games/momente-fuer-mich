using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlobIconActivator : MonoBehaviour
{
    public static bool collected = false;
    public static BlobIconActivator Instance;
    public GameObject Icon1, Icon2;
    private void Awake()
    {
        Instance = this;
        if (collected)
        {
            Activate();
        }
    }

    public void Activate()
    {
        Icon1.SetActive(true);
        Icon2.SetActive(true);
        collected = true;

    }

    public void NotCollectedAnymore()
    {
        collected = false;
    }
}
