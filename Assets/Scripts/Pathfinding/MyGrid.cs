﻿using System;
using UnityEngine;

//Erzeugt ein Grid in den Koordinaten des von IsoMatrix bestimmten Koordinatensystems (gibt und nimmt koordinaten im isometrischen System)
public class MyGrid<MyGridObject>
{
    //definition eines events und dessen argumente und argumentatribute (x und y)
    public event EventHandler<OnGridValueChangedEventArgs> OnGridValueChanged;
    public class OnGridValueChangedEventArgs : EventArgs
    {
        public int x;
        public int y;
    }

    private int height, width;          //anzahl der gridkästchen
    private MyGridObject[,] gridArray;  //Grid welches die objekte beinhaltet
    private float cellSize;             //größe eines Kästchens
    private Vector3 origin;             //0 punkt des grids in Unity coordinaten
    private TextMesh[,] debugTextArray; //Array beinhaltet alle objekte in textform --debug (obj.toString())
    public bool debug = false;           //aktiviert debug ausgabe

    //Constructor braucht zu den normalen parametern eine Methode mit der er alle Gridobjekte initialisiert (diese hat parameter grid x und y)
    public MyGrid(int width, int height, float cellSize, Vector3 origin, Func<MyGrid<MyGridObject>, int, int, MyGridObject> createGridObject)
    {
        this.width = width;
        this.height = height;
        this.cellSize = cellSize;
        this.origin = origin;

        gridArray = new MyGridObject[width, height];
        debugTextArray = new TextMesh[width, height];

        for (int x = 0; x < gridArray.GetLength(0); x++) //initialisierung aller gridobjekte
        {
            for (int y = 0; y < gridArray.GetLength(1); y++)
            {
                gridArray[x, y] = createGridObject(this,x,y);
            }
        }

        //DEBUG grid wird gezeichnet und beschriftet 
        if (debug)
        {
            for (int i = 0; i < gridArray.GetLength(0); i++)
            {
                for (int j = 0; j < gridArray.GetLength(1); j++)
                {
                    //debugTextArray[i, j] = UtilsClass.CreateWorldText(gridArray[i, j]?.ToString(), null, GetWorldPosition(i, j) + new Vector3(0,cellSize/2), 20, Color.white, TextAnchor.MiddleCenter);
                    Debug.DrawLine(GetWorldPosition(i, j), GetWorldPosition(i, j + 1), Color.white, 100f);
                    Debug.DrawLine(GetWorldPosition(i, j), GetWorldPosition(i + 1, j), Color.white, 100f);
                }
            }
            Debug.DrawLine(GetWorldPosition(0, height), GetWorldPosition(width, height), Color.white, 100f);
            Debug.DrawLine(GetWorldPosition(width, 0), GetWorldPosition(width, height), Color.white, 100f);

            //triggert mit dem event eine Änderung des texts im debugarray falls sich ein gridobject geändert hat
            OnGridValueChanged += (object sender, OnGridValueChangedEventArgs eventArgs) =>
            {
                debugTextArray[eventArgs.x, eventArgs.y].text = gridArray[eventArgs.x, eventArgs.y].ToString();
            };
        }

        Debug.Log("Grid is initialized and goes from  0  to "+ width * cellSize +" and 0 to " + height * cellSize);
    }

    //gibt die position eines Gridkästchens als coordinaten zurück
    public Vector3 GetWorldPosition(int x, int y)
    {
        return IsoMatrix.Iso(new Vector3(x, y) * cellSize + origin);
    }

    //gibt das passende Gridkästchen zu einer coordinate zurück
    public void GetXY(Vector3 isoWorldPosition, out int x, out int y)
    {
        Vector3 worldPosition = IsoMatrix.InvIso(isoWorldPosition);
        x = Mathf.FloorToInt((worldPosition - origin).x / cellSize);
        y = Mathf.FloorToInt((worldPosition - origin).y / cellSize);
    }

    //triggert das event welches die debug anzeige des kästchens updatet
    public void TriggerObjectChanged(int x, int y)
    {
        if (OnGridValueChanged != null) OnGridValueChanged(this, new OnGridValueChangedEventArgs { x = x, y = y });
    }

    //setzt das Gridobject an der grid position gleich dem gegebenen gridobjekt
    public void SetGridObject(int x, int y, MyGridObject value)
    {
        if (x >= 0 && x < width && y >= 0 && y < height)
        {
            gridArray[x, y] = value;
            debugTextArray[x, y].text = gridArray[x, y].ToString();
            //triggert event zum debug update
            if (OnGridValueChanged != null) OnGridValueChanged(this, new OnGridValueChangedEventArgs { x = x, y = y });
        }
    }

    //setzt das Gridobject an der gegebenen position gleich dem gegebenen gridobjekt
    public void SetGridObject(Vector3 isoWorldPosition, MyGridObject value)
    {
        int x, y;
        GetXY(isoWorldPosition, out x, out y);
        SetGridObject(x, y, value);
    }

    //gibt das Gridobject in einem gewissen kästchen zurück
    public MyGridObject GetGridObject(int x, int y)
    {
        if (x >= 0 && x < width && y >= 0 && y < height)
        {
            return gridArray[x, y];

        } else
        {
            return default(MyGridObject);
        }
    }

    //gibt das Gridobject an einer gewissen coordinate zurück 
    public MyGridObject GetGridObject(Vector3 worldPosition)
    {
        int x, y;
        GetXY(worldPosition, out x, out y);

        return GetGridObject(x, y);
    }

    public int GetWidth()
    {
        return width;
    }
    public int GetHeight()
    {
        return height;
    }
    public float GetYCellsize()
    {
        
        return  cellSize;
    }

    public float GetXCellsize()
    {
        return cellSize;
    }
}
