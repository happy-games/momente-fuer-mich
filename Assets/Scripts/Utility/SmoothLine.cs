namespace HappyGames.Utilities
{
    using UnityEngine;

    [RequireComponent(typeof(LineRenderer))]
    public class SmoothLine : MonoBehaviour, ISmoothLine
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The x coordinate of the branch start.")]
        private float xStart = 0f;
        [SerializeField, Tooltip("The y coordinate of the branch start.")]
        private float yStart = 0f;
        /// <inheritdoc/>
        public Vector2 Start => new Vector2(xStart, yStart);
        [SerializeField, Range(0f, 1f), Tooltip("The offset at the start.")]
        private float startOffset = 0f;
        /// <inheritdoc/>
        public float StartOffset => startOffset;

        [SerializeField, Tooltip("The x coordinate of the branch end.")]
        private float xEnd = 0f;
        [SerializeField, Tooltip("The y coordinate of the branch end.")]
        private float yEnd = 0f;
        /// <inheritdoc/>
        public Vector2 End => new Vector2(xEnd, yEnd);
        [SerializeField, Range(0f, 1f), Tooltip("The offset at the end.")]
        private float endOffset = 0f;
        /// <inheritdoc/>
        public float EndOffset => endOffset;

        [SerializeField, Range(0f, 1f), Tooltip("The range between the offsets.")]
        private float offsetRange = 1f;

        [SerializeField, Tooltip("True if the start x coordinate is local.")]
        private bool useLocalXStart = true;
        /// <inheritdoc/>
        /// <inheritdoc/>
        public bool UseLocalXStart => useLocalXStart;

        [SerializeField, Tooltip("True if the start x coordinate is local.")]
        private bool useLocalYStart = true;
        /// <inheritdoc/>
        public bool UseLocalYStart => useLocalYStart;

        [SerializeField, Tooltip("True if the start x coordinate is local.")]
        private bool useLocalXEnd = false;
        /// <summary>
        /// True if the end x coordinate is local.
        /// </summary>
        public bool UseLocalXEnd => useLocalXEnd;

        [SerializeField, Tooltip("True if the start x coordinate is local.")]
        private bool useLocalYEnd = false;
        /// <inheritdoc/>
        public bool UseLocalYEnd => useLocalYEnd;

        [SerializeField, Tooltip("The line renderer to draw the branch.")]
        private LineRenderer line = null;
        /// <summary>
        /// The line renderer to draw the branch.
        /// </summary>
        public LineRenderer LineRenderer
        {
            get
            {
                if (!line)
                {
                    line = GetComponent<LineRenderer>();
                }
                return line;
            }
        }
        [SerializeField, Tooltip("True if the branch is horizontal.")]
        private bool isHorizontal = true;
        /// <inheritdoc/>
        public bool IsHorizontal => isHorizontal;

        [SerializeField, Tooltip("The curve of the branch.")]
        private AnimationCurve curve = new AnimationCurve(new Keyframe(), new Keyframe(1f, 1f));
        /// <inheritdoc/>
        public AnimationCurve Curve => curve;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        [SerializeField]
        private Vector2 localStartPoint = Vector2.zero;
        /// <inheritdoc/>
        public Vector2 LocalStartPoint => localStartPoint;
        [SerializeField]
        private Vector2 localEndPoint = Vector2.zero;
        /// <inheritdoc/>
        public Vector2 LocalEndPoint => localEndPoint;
        [SerializeField]
        private Vector2 delta = Vector2.zero;
        /// <inheritdoc/>
        public Vector2 PointDelta => delta;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        public static Vector2 GetLocalPoint(ISmoothLine line, float progress)
        {
            return line.LocalStartPoint + (line.IsHorizontal ? new Vector2(progress, line.Curve.Evaluate(progress)) : new Vector2(line.Curve.Evaluate(progress), progress)) * line.PointDelta;
        }
        public Vector2 GetLocalPoint(float progress) => GetLocalPoint(this, progress);

        public static Vector2 GetGloablPoint(ISmoothLine line, float progress)
        {
            return (Vector2)line.transform.position + GetLocalPoint(line, progress) * line.transform.lossyScale;
        }
        public Vector2 GetGloablPoint(float progress) => GetGloablPoint(this, progress);

        private void CalculatePoints()
        {
            for (int i = 0; i < line.positionCount; i++)
            {
                line.SetPosition(i, line.useWorldSpace ? GetGloablPoint(startOffset + i / (float)(line.positionCount - 1 * offsetRange)) : GetLocalPoint(startOffset + i / (float)(line.positionCount - 1) * offsetRange));
            }
        }

        private void UpdateRenderer()
        {
            CalculatePoints();
        }

        private void CalculateDelta() => delta = localEndPoint - localStartPoint;

        public void UpdateAll(Vector2 start, Vector2 end, bool sx, bool sy, bool ex, bool ey, float so, float eo, AnimationCurve curve, bool isHorizontal)
        {
            this.isHorizontal = isHorizontal;
            this.curve = curve;
            SetOffsets(so, eo, false);
            SetStartPoint(start.x, start.y, sx, sy);
            SetEndPoint(end.x, end.y, ex, ey);
            CalculateDelta();
            UpdateRenderer();
        }

        // Start Point ----------------------------------------------------------------------------

        private void CalculateStartPoint()
        {
            localStartPoint = CalculateStartPoint(this);
        }
        public static Vector2 CalculateStartPoint(ISmoothLine line)
        {
            return new Vector2
            (
                line.UseLocalXStart ? line.Start.x : (line.Start.x - line.transform.position.x) / line.transform.lossyScale.x,
                line.UseLocalYStart ? line.Start.y : (line.Start.y - line.transform.position.y) / line.transform.lossyScale.y
            );
        }

        private void SetStartPoint(float xPosition, float yPosition, bool xLocal, bool yLocal)
        {
            xStart = xPosition;
            yStart = yPosition;
            useLocalXStart = xLocal;
            useLocalYStart = yLocal;
            CalculateStartPoint();
        }
        public void SetStartPoint(Vector2 point, bool localX, bool localY)
        {
            SetStartPoint(point.x, point.y, localX, localY);
            CalculateDelta();
            UpdateRenderer();
        }
        public void SetStartPoint(Vector2 point) => SetStartPoint(point, useLocalXStart, useLocalYStart);

        public void SetStartX(float value, bool local)
        {
            SetStartPoint(value, yStart, local, useLocalYStart);
            CalculateDelta();
            UpdateRenderer();
        }
        public void SetStartX(float value) => SetStartX(value, useLocalXStart);

        public void SetStartY(float value, bool local)
        {
            SetStartPoint(xStart, value, useLocalXStart, local);
            CalculateDelta();
            UpdateRenderer();
        }
        public void SetStartY(float value) => SetStartY(value, useLocalYStart);

        // End Point ------------------------------------------------------------------------------

        private void CalculateEndPoint()
        {
            localEndPoint = CalculateEndPoint(this);
        }
        public static Vector2 CalculateEndPoint(ISmoothLine line)
        {
            return new Vector2
            (
                line.UseLocalXEnd ? line.End.x : (line.End.x - line.transform.position.x) / line.transform.lossyScale.x,
                line.UseLocalYEnd ? line.End.y : (line.End.y - line.transform.position.y) / line.transform.lossyScale.y
            );
        }

        private void SetEndPoint(float xPosition, float yPosition, bool xLocal, bool yLocal)
        {
            xEnd = xPosition;
            yEnd = yPosition;
            useLocalXEnd = xLocal;
            useLocalYEnd = yLocal;
            CalculateEndPoint();
        }
        public void SetEndPoint(Vector2 point, bool localX, bool localY)
        {
            SetEndPoint(point.x, point.y, localX, localY);
            CalculateDelta();
            UpdateRenderer();
        }
        public void SetEndPoint(Vector2 point) => SetEndPoint(point, useLocalXEnd, useLocalYEnd);

        public void SetEndX(float value, bool local)
        {
            SetEndPoint(value, yEnd, local, useLocalYEnd);
            CalculateDelta();
            UpdateRenderer();
        }
        public void SetEndX(float value) => SetEndX(value, useLocalXEnd);

        public void SetEndY(float value, bool local)
        {
            SetEndPoint(xEnd, value, useLocalXEnd, local);
            CalculateDelta();
            UpdateRenderer();
        }
        public void SetEndY(float value) => SetEndY(value, useLocalYEnd);

        // Points ---------------------------------------------------------------------------------

        public void SetPoints(Vector2 start, bool localStartX, bool localStartY, Vector2 end, bool localEndX, bool localEndY)
        {
            SetStartPoint(start.x, start.y, localStartX, localStartY);
            SetEndPoint(end.x, end.y, localEndX, localEndY);
            CalculateDelta();
            UpdateRenderer();
        }
        public void SetPoints(Vector2 start, Vector2 end) => SetPoints(start, useLocalXStart, useLocalYStart, end, useLocalXEnd, useLocalYEnd);

        // Offset ---------------------------------------------------------------------------------

        private void SetOffsets(float start, float end, bool rerender)
        {
            startOffset = start;
            endOffset = end;
            offsetRange = 1f - startOffset - endOffset;
            if (offsetRange > 1f || start < 0f || end < 0f)
            {
                throw new System.ArgumentOutOfRangeException("The start and end offset can't be greather than 1.");
            }
            if (rerender)
            {
                UpdateRenderer();
            }
        }
        public void SetOffsets(float start, float end) => SetOffsets(start, end, true);
        public void SetStartOffset(float value) => SetOffsets(value, endOffset);
        public void SetEndOffset(float value) => SetOffsets(startOffset, value);

        // Update ---------------------------------------------------------------------------------

        public void UpdatePoints()
        {
            bool change = false;
            if (line.useWorldSpace == useLocalXStart || line.useWorldSpace == useLocalYStart)
            {
                CalculateStartPoint();
                change = true;
            }
            if (line.useWorldSpace == useLocalXEnd || line.useWorldSpace == useLocalYEnd)
            {
                CalculateEndPoint();
                change = true;
            }
            if (change)
            {
                CalculateDelta();
                UpdateRenderer();
            }
        }
    }
}
