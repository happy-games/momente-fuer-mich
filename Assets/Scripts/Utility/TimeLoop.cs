namespace HappyGames.Utilities
{
    using HappyGames.TimeManagement;
    using UnityEngine;
    using UnityEngine.Events;

    public class TimeLoop : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Min(0f), Tooltip("The duration of the loop.")]
        protected float duration = 1f;

        [SerializeField, Tooltip("The timeline to use.")]
        protected Timeline timeline = null;

        [Space, SerializeField, Tooltip("Called when the loop gets updated.")]
        protected UnityEvent<float, float> onUpdate = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected float time = 0f;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        public void Awake()
        {
            if (!timeline)
            {
                timeline = new Timeline();
            }
        }

        private void Update()
        {
            time += timeline.DeltaTime;
            time %= duration;
            onUpdate?.Invoke(time, duration);
        }
    }
}
