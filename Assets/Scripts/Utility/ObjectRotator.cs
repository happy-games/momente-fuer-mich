namespace HappyGames.Utilities
{
    using UnityEngine;

    public class ObjectRotator : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The axis to spin around.")]
        protected Vector3 axis = Vector3.forward;

        [SerializeField, Tooltip("The amount of degrees to rotate per second.")]
        protected float speed = 1f;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Update()
        {
            transform.Rotate(axis, speed * Time.deltaTime);
        }
    }
}