namespace HappyGames.Utilities
{
    using UnityEngine;

    public class TransformModifier : MonoBehaviour
    {
        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Sets the scale of the object.
        /// </summary>
        /// <param name="scale">The scale to apply.</param>
        public void SetScael(Vector3 scale) => transform.localScale = scale;
        /// <summary>
        /// Sets the scale of the object.
        /// </summary>
        /// <param name="scale">The scale to apply.</param>
        public void SetScael(Vector2 scale) => SetScael(Vector3.forward + (Vector3)scale);
        /// <summary>
        /// Sets the scale of the object.
        /// </summary>
        /// <param name="scale">The scale to apply.</param>
        public void SetScael(float scale) => SetScael(Vector2.one * scale);
    }
}
