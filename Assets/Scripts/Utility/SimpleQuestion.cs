namespace HappyGames.Utilities
{
    using HappyGames.Data;
    using UnityEngine;
    using UnityEngine.Events;

    public class SimpleQuestion : MonoBehaviour, ISimpleQuestion
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, TextArea, Tooltip("The message to display.")]
        protected string message_ger = string.Empty, message_engl = string.Empty;

        /// <inheritdoc/>
        public string Message_ger => message_ger;
        public string Message_engl => message_engl;

        [Space, SerializeField, Tooltip("The label of the accept button.")]
        protected string acceptLabel_ger = "Ja", acceptLabel_engl = "Yes";
        /// <inheritdoc/>
        public string AcceptLabel_ger => acceptLabel_ger;
        public string AcceptLabel_engl => acceptLabel_engl;

        [SerializeField, Tooltip("The icon to display on the accept button.")]
        protected Sprite acceptIcon = default;
        /// <summary>
        /// The icon to display on the accept button.
        /// </summary>
        public Sprite AcceptIcon => acceptIcon;

        [SerializeField, Tooltip("The action to perform when accepting.")]
        protected UnityEvent onAccept = default;
        /// <inheritdoc/>
        public UnityAction AcceptResonse => onAccept.Invoke;

        [Space, SerializeField, Tooltip("The label of the deny button.")]
        protected string denyLabel_ger = "Nein", denyLabel_engl = "No";
        /// <inheritdoc/>
        public string DenyLabel_ger => denyLabel_ger;
        public string DenyLabel_engl => denyLabel_engl;

        [SerializeField, Tooltip("The icon to display on the deny button.")]
        protected Sprite denyIcon = default;
        /// <summary>
        /// The icon to display on the deny button.
        /// </summary>
        public Sprite DenyIcon => denyIcon;

        [SerializeField, Tooltip("The action to perform when denying.")]
        protected UnityEvent onDeny = default;
        /// <inheritdoc/>
        public UnityAction DenyResponse => onDeny.Invoke;

        [Space, SerializeField, Tooltip("The action to perform when the question was asked.")]
        protected UnityEvent onStated = default;
        /// <inheritdoc/>
        public UnityAction StatedResponse => onStated.Invoke;

        [SerializeField, Tooltip("The action to perform when the question was answered.")]
        protected UnityEvent onAnswered = default;
        /// <inheritdoc/>
        public UnityAction AnsweredResponse => onAnswered.Invoke;

        private void Start()
        {
            
        }
        

    }
}
