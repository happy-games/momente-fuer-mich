namespace HappyGames.Utilities
{
    using HappyGames.Events;
    using UnityEngine;

    public class StringConverter : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called when a value is converted.")]
        protected StringEvent onConverted = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Converts an <see cref="int"/> to a <see cref="string"/>.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        public void Convert(int value)
        {
            onConverted?.Invoke(value.ToString());
        }
    }
}
