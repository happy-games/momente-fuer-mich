namespace HappyGames.Utilities
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    [System.Serializable]
    public class WeightedPrefabCollection<T>
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The elements to choose from.")]
        private List<WeightedPrefab<T>> elements = new List<WeightedPrefab<T>>();

        [SerializeField, Tooltip("True if the X-axis of the penalty is in other selections since own selection.")]
        private bool penaltyWithSelectionCount = true;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected SortedSet<WeightedPrefab<T>> elementsInRange;
        protected Dictionary<WeightedPrefab<T>, float> penalties;
        protected int tailIndex = 0;
        protected int headIndex = 0;
        protected float current = float.MinValue;
        protected int selectionCount = 0;
        protected float chanceRange = 0f;
        protected float CurrentPenalty => penaltyWithSelectionCount ? selectionCount : Time.time;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Assign the values.
        /// </summary>
        protected void SetUp()
        {
            elementsInRange = new SortedSet<WeightedPrefab<T>>(new WeightedPrefab<T>.MaxHeightComperison());
            penalties = new Dictionary<WeightedPrefab<T>, float>();
            elements.Sort(new WeightedPrefab<T>.MinHeightComperison());
        }

        /// <summary>
        /// Returns an element from the selection.
        /// </summary>
        /// <param name="score">The current score.</param>
        /// <returns>The selected element.</returns>
        public T GetElement(float score)
        {
            if (elementsInRange == null)
            {
                SetUp();
            }
            WeightedPrefab<T> selection = elements[0];
            UpdateElementsInRange(score);
            UpdateChance(score);
            float randomChance = Random.Range(0f, chanceRange);
            foreach (var element in elementsInRange)
            {
                if (randomChance < 0f)
                {
                    break;
                }
                selection = element;
                randomChance -= element.Chance.Evaluate(score) * CalculateChanceModifier(element);
            }
            UpdatePenalty(selection);
            return selection.Prefab;
        }

        /// <summary>
        /// Update the elements that are in range.
        /// </summary>
        /// <param name="score">The current score.</param>
        protected void UpdateElementsInRange(float score)
        {
            if (score > current)
            {
                // Loop until all elements with a lower min score than the current one have been added.
                for (; tailIndex < elements.Count && elements[tailIndex].MinScore <= score; tailIndex++)
                {
                    elementsInRange.Add(elements[tailIndex]);
                }
                // Remove all elements where the max score exceeds the current one.
                for (; headIndex < elements.Count && elements[headIndex].MaxScore < score; headIndex++)
                {
                    elementsInRange.Remove(elements[headIndex]);
                }
            }
            else if (score < current)
            {
                // Loop until all elements with a higher max score then the current one have been added.
                for (; tailIndex >= 0 && elements[tailIndex].MaxScore >= score; tailIndex--)
                {
                    elementsInRange.Add(elements[tailIndex]);
                }
                // Remove all elements where the min score subceeds the current one.
                for (; headIndex >= 0 && elements[headIndex].MaxScore > score; headIndex--)
                {
                    elementsInRange.Remove(elements[headIndex]);
                }
            }
            current = score;
        }

        /// <summary>
        /// Updates the values with penalty.
        /// </summary>
        /// <param name="selection">The newly selected object.</param>
        protected void UpdatePenalty(WeightedPrefab<T> selection)
        {
            UpdatePenalty();
            AddPenalty(selection);
        }
        /// <summary>
        /// Updates the values with penalty.
        /// </summary>
        protected void UpdatePenalty()
        {
            WeightedPrefab<T>[] elementsWithPenaltyKeys = penalties.Keys.ToArray();
            foreach (WeightedPrefab<T> element in elementsWithPenaltyKeys)
            {
                if (CurrentPenalty - penalties[element] > element.Penalty[element.Penalty.length - 1].time)
                {
                    penalties.Remove(element);
                }
            }
        }

        /// <summary>
        /// Adds a penalty to an element.
        /// </summary>
        /// <param name="selection">The element to add a penalty for.</param>
        protected void AddPenalty(WeightedPrefab<T> selection)
        {
            selectionCount++;
            penalties[selection] = CurrentPenalty;
        }

        /// <summary>
        /// Updates the chance range.
        /// </summary>
        /// <param name="score">The current score.</param>
        protected void UpdateChance(float score)
        {
            chanceRange = 0f;
            foreach (var element in elementsInRange)
            {
                chanceRange += element.Chance.Evaluate(score) * CalculateChanceModifier(element);
            }
        }

        /// <summary>
        /// Calculates the chance modifier 
        /// </summary>
        /// <param name="element">The element to calculate the modifier for.</param>
        /// <returns>The value to modify the chance with.</returns>
        private float CalculateChanceModifier(WeightedPrefab<T> element) => penalties.ContainsKey(element) ? 1f - element.Penalty.Evaluate(CurrentPenalty - penalties[element]) : 1f;
    }

    [System.Serializable]
    public sealed class WeightedPrefab<T>
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------
        
        [SerializeField, Tooltip("The prefab.")]
        private T prefab = default;
        /// <summary>
        /// The prefab.
        /// </summary>
        public T Prefab => prefab;

        [SerializeField, Tooltip("The chances for this prefab to be selected.")]
        private AnimationCurve chance = default;
        /// <summary>
        /// The chances for this prefab to be selected.
        /// </summary>
        public AnimationCurve Chance => chance;

        [SerializeField, Tooltip("The penalty to apply.")]
        private AnimationCurve penalty = new AnimationCurve(new Keyframe(), new Keyframe(1f, 1f));
        /// <summary>
        /// The penalty to apply.
        /// </summary>
        public AnimationCurve Penalty => penalty;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The minimum value required for this prefab to be selected.
        /// </summary>
        public float MinScore => chance[0].time;

        /// <summary>
        /// The maximum value allowed for this prefab to be selected.
        /// </summary>
        public float MaxScore => chance[chance.length - 1].value <= 0f ? chance[chance.length - 1].time : float.PositiveInfinity;


        // --- | Classes | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// A class that provides a method to compare min values of <see cref="WeightedPrefab"/>s.
        /// </summary>
        public class MinHeightComperison : IComparer<WeightedPrefab<T>>
        {
            public int Compare(WeightedPrefab<T> x, WeightedPrefab<T> y)
            {
                if (x.MinScore == y.MinScore)
                {
                    return 1;
                }
                return x.MinScore.CompareTo(y.MinScore);
            }
        }

        /// <summary>
        /// A class that provides a method to compare max values of <see cref="WeightedPrefab"/>s.
        /// </summary>
        public class MaxHeightComperison : IComparer<WeightedPrefab<T>>
        {
            public int Compare(WeightedPrefab<T> x, WeightedPrefab<T> y)
            {
                if (x.MaxScore == y.MaxScore)
                {
                    return 1;
                }
                return x.MaxScore.CompareTo(y.MaxScore);
            }
        }
    }
}
