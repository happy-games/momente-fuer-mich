namespace HappyGames.Utilities
{
    using System.Collections.Generic;
    using UnityEngine;

    public class SmoothLineUpdater : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The lines to update.")]
        protected SmoothLine[] lines = default;
        /// <summary>
        /// The lines to update.
        /// </summary>
        public IEnumerable<SmoothLine> Lines => lines;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Sets the start point of the line.
        /// </summary>
        /// <param name="position">The position of the start point.</param>
        public void SetStarts(Vector2 position)
        {
            foreach (SmoothLine line in lines)
            {
                line.SetStartPoint(position);
            }
        }

        /// <summary>
        /// Moves the start point of the lines.
        /// </summary>
        /// <param name="direction">The direction to move the point.</param>
        public void MoveStarts(Vector2 direction)
        {
            foreach (SmoothLine line in lines)
            {
                line.SetStartPoint(line.End + direction);
            }
        }

        /// <summary>
        /// Moves the start point of the lines in the opposite direction.
        /// </summary>
        /// <param name="direction">The direction to move the line away.</param>
        public void InvMoveStarts(Vector2 direction) => MoveStarts(-direction);

        /// <summary>
        /// Sets the end points of the lines.
        /// </summary>
        /// <param name="position">The position of the end point.</param>
        public void SetEnds(Vector2 position)
        {
            foreach (SmoothLine line in lines)
            {
                line.SetEndPoint(position);
            }
        }

        /// <summary>
        /// Moves the end point of the lines.
        /// </summary>
        /// <param name="direction">The direction to move the line.</param>
        public void MoveEnds(Vector2 direction)
        {
            foreach (SmoothLine line in lines)
            {
                line.SetEndPoint(line.End + direction);
            }
        }

        /// <summary>
        /// Moves the end point of the lines in the opposite direction.
        /// </summary>
        /// <param name="direction">The direction to move the line away.</param>
        public void InvMoveEnds(Vector2 direction) => MoveEnds(-direction);
    }
}