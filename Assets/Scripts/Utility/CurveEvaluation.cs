namespace HappyGames.Utilities
{
    using UnityEngine;
    using UnityEngine.Events;

    public class CurveEvaluation : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The curve to evaluate.")]
        protected AnimationCurve curve = new AnimationCurve(new Keyframe(), new Keyframe(1f, 1f));

        [Space, SerializeField, Tooltip("Called when a value is evaluated.")]
        protected UnityEvent<float> onEvaluation = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Evaluates the value and calls the event.
        /// </summary>
        /// <param name="value">The value to evaluate.</param>
        public void Evaluate(float value) => onEvaluation?.Invoke(curve.Evaluate(value));
        /// <summary>
        /// Evaluates the value and calls the event.
        /// </summary>
        /// <param name="current">The current value.</param>
        /// <param name="max">The max value.</param>
        public void Evaluate(float current, float max) => Evaluate(current / max);
    }
}
