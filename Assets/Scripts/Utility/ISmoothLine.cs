namespace HappyGames.Utilities
{
using UnityEngine;

    public interface ISmoothLine
    {
        /// <summary>
        /// The start coordinates.
        /// </summary>
        public Vector2 Start { get; }

        /// <summary>
        /// The end coordinates.
        /// </summary>
        public Vector2 End { get; }

        /// <summary>
        /// True if the start x coordinate is local.
        /// </summary>
        public bool UseLocalXStart { get; }

        /// <summary>
        /// True if the start y coordinate is local.
        /// </summary>
        public bool UseLocalYStart { get; }

        /// <summary>
        /// True if the end x coordinate is local.
        /// </summary>
        public bool UseLocalXEnd { get; }

        /// <summary>
        /// True if the end y coordinate is local.
        /// </summary>
        public bool UseLocalYEnd { get; }

        /// <summary>
        /// True if the branch is horizontal.
        /// </summary>
        public bool IsHorizontal { get; }

        /// <summary>
        /// The start point in the local space.
        /// </summary>
        public Vector2 LocalStartPoint { get; }

        /// <summary>
        /// The offset of the start point.
        /// </summary>
        public float StartOffset {get;}

        /// <summary>
        /// The end point in the local space.
        /// </summary>
        public Vector2 LocalEndPoint { get; }

        /// <summary>
        /// The offset of the end point.
        /// </summary>
        public float EndOffset { get; }

        /// <summary>
        /// The vector from the start to the end point.
        /// </summary>
        public Vector2 PointDelta { get; }

        /// <summary>
        /// The <see cref="Transform"/> component on the line.
        /// </summary>
        public Transform transform { get; }

        /// <summary>
        /// The curve of the branch.
        /// </summary>
        public AnimationCurve Curve { get; }
    }
}
