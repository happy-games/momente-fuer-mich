namespace HappyGames.Utilities
{
    using HappyGames.TimeManagement;
    using UnityEngine;
    using UnityEngine.Events;

    public class ConstantVelocity : MonoBehaviour
    {
        [SerializeField, Tooltip("The velocity.")]
        private Vector2 velocity = Vector2.zero;

        [SerializeField, Tooltip("The time scale to use.")]
        private Timeline timeline = null;

        [Space, SerializeField, Tooltip("Called every frame with the velocity.")]
        private UnityEvent<Vector2> onUpdate = default;


        private void Update()
        {
            onUpdate?.Invoke(velocity * (timeline ? timeline.DeltaTime : Time.deltaTime));
        }
    }
}
