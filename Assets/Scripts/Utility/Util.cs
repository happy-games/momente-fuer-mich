namespace HappyGames
{
    using System;
    using UnityEngine;

    /// <summary>
    /// <see cref="HappyGames"/> utility class for generally usful methods.
    /// </summary>
    public static class Util
    {
        /// <summary>
        /// Creates a copy of a <see cref="Component"/>.
        /// </summary>
        /// <param name="original">The component to copy.</param>
        /// <param name="destination">The object to add the component to.</param>
        /// <returns>The copieded component.</returns>
        public static Component CopyComponent(Component original, GameObject destination)
        {
            Type type = original.GetType();
            Component copy = destination.AddComponent(type);
            // Copied fields can be restricted with BindingFlags
            System.Reflection.FieldInfo[] fields = type.GetFields();
            foreach (System.Reflection.FieldInfo field in fields)
            {
                field.SetValue(copy, field.GetValue(original));
            }
            return copy;
        }
        /// <summary>
        /// Creates a copy of a <see cref="Component"/>.
        /// </summary>
        /// <typeparam name="T">The type of the component to copy.</typeparam>
        /// <param name="original">The component to copy.</param>
        /// <param name="destination">The object to add the component to.</param>
        /// <returns>The copieded component.</returns>
        public static T CopyComponent<T>(T original, GameObject destination) where T : Component => CopyComponent(original, destination);
    }

    /// <summary>
    /// A class extending the functionaliy of <see cref="Array"/>s.
    /// </summary>
    public static class ArrayExtention
    {
        /// <summary>
        /// Returns true if any statement is true.
        /// </summary>
        /// <param name="array">The array of statrments.</param>
        /// <returns>True if any entry is true.</returns>
        public static bool AnyTrue(this bool[] array)
        {
            foreach (bool b in array)
            {
                if (b)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns true if any statement is false.
        /// </summary>
        /// <param name="array">The array of statrments.</param>
        /// <returns>True if any entry is false.</returns>
        public static bool AnyFalse(this bool[] array)
        {
            foreach (bool b in array)
            {
                if (!b)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns true if all statements are true.
        /// </summary>
        /// <param name="array">The array of statrments.</param>
        /// <returns>True if all entries are true.</returns>
        public static bool AllTrue(this bool[] array) => !AnyFalse(array);

        /// <summary>
        /// Returns true if all statements are false.
        /// </summary>
        /// <param name="array">The array of statrments.</param>
        /// <returns>True if all entries are false.</returns>
        public static bool AllFalse(this bool[] array) => !AnyTrue(array);

        /// <summary>
        /// Performs a check on all elements in the array.
        /// </summary>
        /// <typeparam name="T">The thype of the array.</typeparam>
        /// <param name="array">The array to check.</param>
        /// <param name="andCompersion">True if all checks must meet the state to be succsessful. False if only one needs to.</param>
        /// <param name="state">True if the check needs to succseed, false if it needs to fail.</param>
        /// <param name="check">The check to perform.</param>
        /// <returns>True if the requirements were met.</returns>
        public static bool Check<T>(this T[] array, bool andCompersion, bool state, Func<T, bool> check)
        {
            foreach (T value in array)
            {
                if ((check(value) == state) != andCompersion)
                {
                    return !andCompersion;
                }
            }
            return andCompersion;
        }

        /// <summary>
        /// Returns the index of the biggest value.
        /// </summary>
        /// <typeparam name="T">The type of the array.</typeparam>
        /// <param name="array">The array whos biggest index to return.</param>
        /// <param name="index">The index of the max value.</param>
        /// <returns>The index of the biggest value in the array.</returns>
        public static T Max<T>(this T[] array, out int index) where T : IComparable<T>
        {
            index = 0;
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i].CompareTo(array[index]) > 0)
                {
                    index = i;
                }
            }
            return array[index];
        }
        /// <summary>
        /// Returns the biggest value from the array.
        /// /// </summary>
        /// <typeparam name="T">The type of the array.</typeparam>
        /// <param name="array">The array whos max value to return.</param>
        /// <returns>The biggest value in the array.</returns>
        public static T Max<T>(this T[] array) where T : IComparable<T> => Max(array, out int index);

        /// <summary>
        /// Returns the index of the smallest value.
        /// </summary>
        /// <typeparam name="T">The type of the array.</typeparam>
        /// <param name="array">The array whos smalles index to return.</param>
        /// <param name="index">The index of the min value.</param>
        /// <returns>The index of the smallest value in the array.</returns>
        public static T Min<T>(this T[] array, out int index) where T : IComparable<T>
        {
            index = 0;
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i].CompareTo(array[index]) > 0)
                {
                    index = i;
                }
            }
            return array[index];
        }
        /// <summary>
        /// Returns the smallest value from the array.
        /// /// </summary>
        /// <typeparam name="T">The type of the array.</typeparam>
        /// <param name="array">The array whos min value to return.</param>
        /// <returns>The smalles value in the array.</returns>
        public static T Min<T>(this T[] array) where T : IComparable<T> => Min(array, out int index);

        /// <summary>
        /// Returns a random element in the array.
        /// </summary>
        /// <typeparam name="T">The type of the array.</typeparam>
        /// <param name="array">The array to look in.</param>
        /// <returns>The random value.</returns>
        public static T Random<T>(this T[] array)
        {
            if (array == null || array.Length == 0)
            {
                return default(T);
            }
            return array[UnityEngine.Random.Range(0, array.Length)];
        }
        /// <summary>
        /// Returns a random element with the exception of one.
        /// </summary>
        /// <typeparam name="T">The type of the array.</typeparam>
        /// <param name="array">The array to look in.</param>
        /// <param name="exception">The index of the element to avoid.</param>
        /// <param name="index">The index of the found elemment.</param>
        /// <returns>Rhe random value from the array.</returns>
        public static T Random<T>(this T[] array, int exception, out int index)
        {
            if (array.Length <= 1)
            {
                index = 0;
                return Random(array);
            }
            if (array == null || array.Length == 0)
            {
                index = -1;
                return default(T);
            }
            if (exception < 0)
            {
                exception = array.Length;
            }
            int randomIndex = UnityEngine.Random.Range(0, array.Length - 1);
            if (randomIndex >= exception)
            {
                randomIndex++;
            }
            index = randomIndex;
            return array[randomIndex];
        }

        /// <summary>
        /// Checks if the array contains a value.
        /// </summary>
        /// <typeparam name="T">The type of the array.</typeparam>
        /// <param name="array">The array where to serch for the value.</param>
        /// <param name="requirements">Changes if all or any value must be contained in the array.</param>
        /// <param name="values">The value to look for in the array.</param>
        /// <returns>True if the value is in the array.</returns>
        public static bool Contains<T>(this T[] array, ConparisonRequirements requirements = ConparisonRequirements.All, params T[] values) where T : IEquatable<T>
        {
            foreach (T entry in array)
            {
                foreach (T value in values)
                {
                    if ((entry == null ? value == null : entry.Equals(value)) != (requirements == ConparisonRequirements.All))
                    {
                        return !(requirements == ConparisonRequirements.All);
                    }
                }
            }
            return (requirements == ConparisonRequirements.All);
        }

        /// <summary>
        /// Counts the elements in the array.
        /// </summary>
        /// <typeparam name="T">The type of the array.</typeparam>
        /// <param name="array">The array to look at.</param>
        /// <param name="value">The value to count.</param>
        /// <returns>The amount of times the value is in the array.</returns>
        public static int Count<T>(this T[] array, T value) where T : IEquatable<T>
        {
            int count = 0;
            foreach (T index in array)
            {
                if (value == null ? index == null : value.Equals(index))
                {
                    count++;
                }
            }
            return count;
        }

        public enum ConparisonRequirements { All, Any }
    }

    /// <summary>
    /// A class extending the functionaliy of <see cref="LayerMask"/>s.
    /// </summary>
    public static class LayerMaskExtention
    {
        /// <summary>
        /// Checks if a layer is part of a <see cref="LayerMask"/>.
        /// </summary>
        /// <param name="mask">The <see cref="LayerMask"/> to check.</param>
        /// <param name="layer">The layer to look for.</param>
        /// <returns>True if the layer is part of the <see cref="LayerMask"/>.</returns>
        public static bool Contains(this LayerMask mask, int layer) => mask == (mask | (1 << layer));
    }
}