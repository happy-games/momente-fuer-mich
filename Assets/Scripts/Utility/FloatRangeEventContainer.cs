namespace HappyGames.Utilities
{
    using UnityEngine;
    using UnityEngine.Events;

    public class FloatRangeEventContainer : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called when the values changed.")]
        protected UnityEvent<float, float> onRangeUpdate = default;
        /// <summary>
        /// Called when the values changed.
        /// </summary>
        public event UnityAction<float, float> OnRangeUpdate
        {
            add => onRangeUpdate.AddListener(value);
            remove => onRangeUpdate.RemoveListener(value);
        }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Invokes all registed listeners.
        /// </summary>
        /// <param name="current">The current value.</param>
        /// <param name="max">The max value.</param>
        public void Invoke(float current, float max) => onRangeUpdate?.Invoke(current, max);
    }
}
