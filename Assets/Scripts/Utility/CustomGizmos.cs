namespace HappyGames.Utilities
{
    using UnityEngine;

    public static class CustomGizmos 
    {
        public static void DrawPoint2D(Vector2 position, Vector2 size)
        {
            Vector2[] directions = new Vector2[]
            {
                Vector2.up * size.y * 0.5f,
                Vector2.right * size.x * 0.5f,
                Vector2.down * size.y * 0.5f,
                Vector2.left * size.x * 0.5f
            };

            Gizmos.DrawLine(position + directions[0], position + directions[2]);
            Gizmos.DrawLine(position + directions[1], position + directions[3]);
            Gizmos.DrawLine(position + directions[0] * 0.5f, position + directions[1] * 0.5f);
            Gizmos.DrawLine(position + directions[1] * 0.5f, position + directions[2] * 0.5f);
            Gizmos.DrawLine(position + directions[2] * 0.5f, position + directions[3] * 0.5f);
            Gizmos.DrawLine(position + directions[3] * 0.5f, position + directions[0] * 0.5f);
        }
    }
}
