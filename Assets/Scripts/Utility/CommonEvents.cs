namespace HappyGames.Events
{
    /**
     * This scipt hold UnityEvents with common attributes, 
     * that cannot be placed under the attributes class.
     * 
     * Please place UnityEvents with a custom attribute in the 
     * script of the attribute, under its definition.
     * 
     * Example:
     * 
     * public class MyClass
     * {
     * ...
     * }
     * 
     * [System.Serializable]
     * public class MyClassEvent : UnityEvent<MyClass> { }
     */

    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    /// An <see cref="UnityEvent"/> with a <see cref="bool"/> as attribute.
    /// </summary>
    [System.Serializable]
    public class BoolEvent : UnityEvent<bool> { }

    /// <summary>
    /// An <see cref="UnityEvent"/> with a <see cref="float"/> as attribute.
    /// </summary>
    [System.Serializable]
    public class FloatEvent : UnityEvent<float> { }

    /// <summary>
    /// An <see cref="UnityEvent"/> with an <see cref="int"/> as attribute.
    /// </summary>
    [System.Serializable]
    public class IntEvent : UnityEvent<int> { }
    /// <summary>
    /// An <see cref="UnityEvent"/> with a <see cref="string"/> as attribute.
    /// </summary>
    [System.Serializable]
    public class StringEvent : UnityEvent<string> { }

    /// <summary>
    /// An <see cref="UnityEvent"/> with a <see cref="Vector2"/> as attribute.
    /// </summary>
    [System.Serializable]
    public class Vector2Event : UnityEvent<Vector2> { }

    /// <summary>
    /// An <see cref="UnityEvent"/> with a <see cref="GameObject"/> as attribute.
    /// </summary>
    [System.Serializable]
    public class GameObjectEvent : UnityEvent<GameObject> { }

    /// <summary>
    /// An <see cref="UnityEvent"/> with a <see cref="Transform"/> as attribute.
    /// </summary>
    [System.Serializable]
    public class TransformEvent : UnityEvent<Transform> { }

    /// <summary>
    /// An <see cref="UnityEvent"/> with two <see cref="float"/>s as attribute.
    /// </summary>
    [System.Serializable]
    public class FloarRangeEvent : UnityEvent<float, float> { }

    /// <summary>
    /// An <see cref="UnityEvent"/> with two <see cref="int"/>s as attribute.
    /// </summary>
    [System.Serializable]
    public class IntRangeEvent : UnityEvent<int, int> { }
}
