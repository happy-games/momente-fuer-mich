namespace HappyGames.Utilities
{
    using UnityEngine;
    using UnityEngine.Events;

    public interface ISimpleQuestion
    {
        /// <summary>
        /// The message to display.
        /// </summary>
        public string Message_ger { get; }
        public string Message_engl { get; }

        /// <summary>
        /// The message to display on the accept button.
        /// </summary>
        string AcceptLabel_ger { get; }
        string AcceptLabel_engl { get; }

    /// <summary>
    /// The messate to display on the deny button.
    /// </summary>
        string DenyLabel_ger { get; }
        string DenyLabel_engl { get; }

        /// <summary>
        /// The icon to display on the accept button.
        /// </summary>
        Sprite AcceptIcon { get; }

        /// <summary>
        /// The icon to display on the deny button.
        /// </summary>
        Sprite DenyIcon { get; }

        /// <summary>
        /// The method to call when the question is was stated.
        /// </summary>
        UnityAction StatedResponse { get; }

        /// <summary>
        /// The method to call when the question was answerd.
        /// </summary>
        UnityAction AnsweredResponse { get; }

        /// <summary>
        /// The method to call when accepting.
        /// </summary>
        UnityAction AcceptResonse { get; }

        /// <summary>
        /// The method to call when denying.
        /// </summary>
        UnityAction DenyResponse { get; }
    }
}
