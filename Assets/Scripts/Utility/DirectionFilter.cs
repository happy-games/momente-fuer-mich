namespace HappyGames.Utilities
{
    using UnityEngine;
    using UnityEngine.Events;

    public class DirectionFilter : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called when the filter gets enabled.")]
        private UnityEvent onEnabled = default;

        [SerializeField, Tooltip("Called when the filter gets disabled.")]
        private UnityEvent onDisabled = default;

        [Space, SerializeField, Tooltip("Called when a neutral input was given.")]
        private UnityEvent onNeutral = default;

        [SerializeField, Tooltip("Called when the input is in the left direction.")]
        private UnityEvent onLeft = default;

        [SerializeField, Tooltip("Called when the input is in the right direction.")]
        private UnityEvent onRight = default;

        // --- | Variables | -----------------------------------------------------------------------------------------------

        private float current = 0f;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            onEnabled?.Invoke();
            if (current == 0f)
            {
                onNeutral?.Invoke();
            }
            else if (current < 0f)
            {
                onLeft?.Invoke();
            }
            else
            {
                onRight?.Invoke();
            }
        }

        private void OnDisable() => onDisabled?.Invoke();

        /// <summary>
        /// Filters the value and calls the correct event.
        /// </summary>
        /// <param name="input">The input to fitler.</param>
        public void Filter(float input)
        {
            if (enabled)
            {
                if (input == 0f)
                {
                    if (current != 0f)
                    {
                        onNeutral?.Invoke();
                    }
                }
                else if (input < 0f)
                {
                    if (current >= 0f)
                    {
                        onLeft?.Invoke();
                    }
                }
                else
                {
                    if (current <= 0f)
                    {
                        onRight?.Invoke();
                    }
                }
                current = input;
            }
        }
    }
}
