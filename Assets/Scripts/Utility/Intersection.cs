namespace HappyGames.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public class IntersectionCalculation
    {
        public static IEnumerable<Vector2> Intersection(Circle circle, Line line)
        {
            var intersection = LineIntersection(circle, line);
            return line.IsSegment
                ? intersection.Where(p => CompareTo(p, line.P1) >= 0 && CompareTo(p, line.P2) <= 0)
                : intersection;

            static IEnumerable<Vector2> LineIntersection(Circle circle, Line line)
            {
                float x, y, A, B, C, D;
                var (m, c) = (line.Slope, line.YIntercept);
                var (p, q, r) = (circle.X, circle.Y, circle.Radius);

                if (line.IsVertical)
                {
                    x = line.P1.x;
                    B = -2 * q;
                    C = p * p + q * q - r * r + x * x - 2 * p * x;
                    D = B * B - 4 * C;
                    if (D == 0) yield return new Vector2(x, -q);
                    else if (D > 0)
                    {
                        D = Mathf.Sqrt(D);
                        yield return new Vector2(x, (-B - D) / 2);
                        yield return new Vector2(x, (-B + D) / 2);
                    }
                }
                else
                {
                    A = m * m + 1;
                    B = 2 * (m * c - m * q - p);
                    C = p * p + q * q - r * r + c * c - 2 * c * q;
                    D = B * B - 4 * A * C;
                    if (D == 0)
                    {
                        x = -B / (2 * A);
                        y = m * x + c;
                        yield return new Vector2(x, y);
                    }
                    else if (D > 0)
                    {
                        D = Mathf.Sqrt(D);
                        x = (-B - D) / (2 * A);
                        y = m * x + c;
                        yield return new Vector2(x, y);
                        x = (-B + D) / (2 * A);
                        y = m * x + c;
                        yield return new Vector2(x, y);
                    }
                }
            }

        }

        public readonly struct Line
        {
            public Line(Vector2 p1, Vector2 p2, bool isSegment = false)
            {
                (P1, P2) = CompareTo(p2, p1) < 0 ? (p2, p1) : (p1, p2);
                IsSegment = isSegment;
                if (p1.x == p2.x) (Slope, YIntercept) = (float.PositiveInfinity, float.NaN);
                else
                {
                    Slope = (P2.y - P1.y) / (P2.x - P1.x);
                    YIntercept = P2.y - Slope * P2.x;
                }
            }

            public static implicit operator Line((Vector2 p1, Vector2 p2) l) => new Line(l.p1, l.p2);
            public static implicit operator Line((Vector2 p1, Vector2 p2, bool isSegment) l) => new Line(l.p1, l.p2, l.isSegment);

            public Vector2 P1 { get; }
            public Vector2 P2 { get; }
            public float Slope { get; }
            public float YIntercept { get; }
            public bool IsSegment { get; }
            public bool IsVertical => P1.x == P2.x;

            public override string ToString() => $"[{P1}, {P2}]";
        }

        public readonly struct Circle
        {
            public Circle(Vector2 center, float radius) => (Center, Radius) = (center, radius);

            public static implicit operator Circle((Vector2 center, float radius) c) => new Circle(c.center, c.radius);

            public Vector2 Center { get; }
            public float Radius { get; }
            public float X => Center.x;
            public float Y => Center.y;

            public override string ToString() => $"{{ C:{Center}, R:{Radius} }}";
        }

        private static int CompareTo(Vector2 v1, Vector2 v2)
        {
            int c = v1.x.CompareTo(v2.x);
            if (c != 0) return c;
            return v1.y.CompareTo(v2.y);
        }
    }
}
