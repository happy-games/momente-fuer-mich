namespace HappyGames.Emotion
{
    using UnityEngine;

    /// <summary>
    /// Provides strings for the different emotions.
    /// </summary>
    [CreateAssetMenu(fileName = "EmotionStringSet", menuName = "Set/Emotion/String")]
    public class EmotionStringSet : EmotionSet<string> { }
}
