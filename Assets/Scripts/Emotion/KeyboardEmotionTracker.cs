namespace HappyGames.Emotion
{
    using HappyGames.Data;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Detects the selected emotion using the keyboard.
    /// </summary>
    public sealed partial class EmotionManager : MonoBehaviour
    {
#if !UNITY_WEBGL || UNITY_EDITOR


        // --- | Methods | -------------------------------------------------------------------------------------------------

        void TrackingStart() { }
        void TrackingStop() { }

        private EmotionType UpdateEmotion()
        {
            foreach (EmotionType emotion in selectableEmotions)
            {
                if (Input.GetKey((KeyCode)((int)KeyCode.Alpha0 + (int)emotion)))
                {
                    return emotion;
                }
            }
            return EmotionType.Neutral;
        }
#endif
    }
}
