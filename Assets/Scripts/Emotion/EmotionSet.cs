namespace HappyGames.Emotion
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Provides values for all emtoions.
    /// </summary>
    /// <typeparam name="T">The type of the value.</typeparam>
    public abstract class EmotionSet<T> : ScriptableObject, IEnumerable<KeyValuePair<EmotionType, T>>
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The value for the neutral emotion.")]
        protected T neutral = default;
        /// <summary>
        /// The value for the neutral emotion.
        /// </summary>
        public T Neutral => neutral;

        [SerializeField, Tooltip("The value for the angry emotion.")]
        protected T angry = default;
        /// <summary>
        /// The value for the angry emotion.
        /// </summary>
        public T Angry => angry;

        [SerializeField, Tooltip("The value for the happy emotion.")]
        protected T happy = default;
        /// <summary>
        /// The value for the happy emotion.
        /// </summary>
        public T Happy => happy;

        [SerializeField, Tooltip("The value for the sad emotion.")]
        protected T sad = default;
        /// <summary>
        /// The value for the sad emotion.
        /// </summary>
        public T Sad => sad;

        [SerializeField, Tooltip("The value for the surprised emotion.")]
        protected T surprised = default;
        /// <summary>
        /// The value for the surprised emotion.
        /// </summary>
        public T Surprised => surprised;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns the value for the emotion.
        /// </summary>
        /// <param name="emotion">The emotion whos value to return.</param>
        /// <returns>The value for the emotion.</returns>
        public T GetValue(EmotionType emotion)
        {
            switch (emotion)
            {
                default: return default(T);
                case EmotionType.Neutral: return neutral;
                case EmotionType.Angry: return angry;
                case EmotionType.Happy: return happy;
                case EmotionType.Sad: return sad;
                case EmotionType.Surprised: return surprised;
            }
        }

        /// <inheritdoc/>
        public IEnumerator<KeyValuePair<EmotionType, T>> GetEnumerator()
        {
            yield return new KeyValuePair<EmotionType, T>(EmotionType.Neutral, neutral);
            yield return new KeyValuePair<EmotionType, T>(EmotionType.Angry, angry);
            yield return new KeyValuePair<EmotionType, T>(EmotionType.Happy, happy);
            yield return new KeyValuePair<EmotionType, T>(EmotionType.Sad, sad);
            yield return new KeyValuePair<EmotionType, T>(EmotionType.Surprised, surprised);
        }

        /// <inheritdoc/>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
