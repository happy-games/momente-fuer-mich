namespace HappyGames.Emotion
{
    using HappyGames.Data;
    using UnityEngine;
    using UnityEngine.Events;

    public class UnlockedEmotionCheck : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The emotion to check.")]
        protected EmotionType emotion = EmotionType.Neutral;

        [Space, SerializeField, Tooltip("Called when the emotion was unlocked.")]
        protected UnityEvent onEmotionUnlocked = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            SwitchProfile(null, ProfileManager.GetActiveProfile());
            ProfileManager.OnProfileActivated += SwitchProfile;
        }

        private void OnDisable()
        {
            ProfileManager.OnProfileActivated -= SwitchProfile;
            SwitchProfile(ProfileManager.GetActiveProfile(), null);
        }

        /// <summary>
        /// Changes the listener for the emotion unlocking.
        /// </summary>
        /// <param name="oldProfil">The previously active profile.</param>
        /// <param name="newProfile">The now active profile.</param>
        protected void SwitchProfile(UserProfile oldProfil, UserProfile newProfile)
        {
            if (oldProfil != null)
            {
                oldProfil.OnEmotionUnlocked -= CheckEmotion;
            }
            if (newProfile != null)
            {
                newProfile.OnEmotionUnlocked += CheckEmotion;
            }
        }

        /// <summary>
        /// Checks if an emotion matches the emotion to unlock and calls the <see cref="onEmotionUnlocked"/> event if that's the case.
        /// </summary>
        /// <param name="emotion">The emotion to compare.</param>
        protected void CheckEmotion(EmotionType emotion)
        {
            if (this.emotion == emotion)
            {
                onEmotionUnlocked?.Invoke();
            }
        }
    }
}
