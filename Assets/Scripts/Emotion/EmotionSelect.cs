namespace HappyGames.Emotion
{
    using HappyGames.Events;
    using HappyGames.TimeManagement;
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    /// Handles an emotions selection.
    /// </summary>
    public class EmotionSelect : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("True if the selection is enabled.")]
        protected bool isEnabled = true;
        /// <summary>
        /// True if the selection is enabled.
        /// </summary>
        public bool IsEnabled
        {
            get => isEnabled;
            set
            {
                isEnabled = value;
                if (!isEnabled && IsSelecting)
                {
                    CancelSelection();
                }
            }
        }

        [SerializeField, Min(0f), Tooltip("The maximum time availabble to select an emotion.")]
        protected float maxSelectionTime = 5f;

        [SerializeField, Min(0f), Tooltip("The time it takes to change the selection.")]
        protected float switchTime = 0.2f;

        [SerializeField, Tooltip("The timeline to use to calculate the selection duration.")]
        protected Timeline timeline = null;

        [SerializeField, Tooltip("The script detecting the emotions.")]
        protected EmotionManager manager = null;

        [Space, SerializeField, Tooltip("Called when the selection starts.")]
        private UnityEvent onSelectionStart = default;
        /// <summary>
        /// Called when the selection starts.
        /// </summary>
        public event UnityAction OnSelectionStart
        {
            add => onSelectionStart.AddListener(value);
            remove => onSelectionStart.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called when the selection ends.")]
        private EmotionTypeEvent onSelectionEnd = default;
        /// <summary>
        /// Called when the selection ends.
        /// </summary>
        public event UnityAction<EmotionType> OnSelectionEnd
        {
            add => onSelectionEnd.AddListener(value);
            remove => onSelectionEnd.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called when the selection was canceled.")]
        private EmotionTypeEvent onSelectionCanceled = default;
        /// <summary>
        /// Called when the selection was canceled.
        /// </summary>
        public event UnityAction<EmotionType> OnSelectionCanceled
        {
            add => onSelectionCanceled.AddListener(value);
            remove => onSelectionCanceled.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called when the selection changes.")]
        private EmotionTypeEvent onSelectionChange = default;

        public event UnityAction<EmotionType> OnSelectionChange
        {
            add => onSelectionChange.AddListener(value);
            remove => onSelectionChange.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called during the selection.")]
        private FloarRangeEvent onSelectionDurationUpdate = default;
        /// <summary>
        /// Called during the selection.
        /// </summary>
        public event UnityAction<float, float> OnSelectionDurationUpdate
        {
            add => onSelectionDurationUpdate.AddListener(value);
            remove => onSelectionDurationUpdate.RemoveListener(value);
        }


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The emotion to default to.
        /// </summary>
        protected const EmotionType DEFAULT_EMOTION = EmotionType.Neutral;

        /// <summary>
        /// The currently selected emotion.
        /// </summary>
        public EmotionType Selection { get; protected set; } = DEFAULT_EMOTION;

        protected EmotionType targetEmotion = DEFAULT_EMOTION;
        protected Coroutine emotionSwitch = null;
        /// <summary>
        /// True if the emotion is beeing switched.
        /// </summary>
        protected bool IsSwitchingEmotion => emotionSwitch != null;

        protected Coroutine selectionHandler = null;
        /// <summary>
        /// True if a selection is active.
        /// </summary>
        public bool IsSelecting => selectionHandler != null;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Starts the selection.
        /// </summary>
        public void StartSelection()
        {
            if (IsEnabled && !IsSelecting)
            {
                selectionHandler = StartCoroutine(DoSelect());
            }
        }

        /// <summary>
        /// Stops the selection.
        /// </summary>
        public void StopSelection()
        {
            if (IsEnabled && IsSelecting)
            {
                manager.OnEmotionDetected -= UpdateSelection;
                StopCoroutine(selectionHandler);
                selectionHandler = null;
                onSelectionEnd?.Invoke(Selection);
            }
        }

        /// <summary>
        /// Toggles between starting and stopping the selection.
        /// </summary>
        public void ToggleSelection()
        {
            if (IsSelecting)
            {
                StopSelection();
            }
            else
            {
                StartSelection();
            }
        }

        /// <summary>
        /// Cancels the selection.
        /// </summary>
        public void CancelSelection()
        {
            if (IsSelecting)
            {
                manager.OnEmotionDetected -= UpdateSelection;
                StopCoroutine(selectionHandler);
                selectionHandler = null;
                onSelectionCanceled?.Invoke(Selection);
            }
        }

        /// <summary>
        /// Performs the selection over time.
        /// </summary>
        protected IEnumerator DoSelect()
        {
            manager.OnEmotionDetected += UpdateSelection;
            UpdateSelection(manager.CurrentEmotion);
            onSelectionStart?.Invoke();
            float time = 0f;
            onSelectionDurationUpdate?.Invoke(time, maxSelectionTime);
            while (time < maxSelectionTime)
            {
                yield return null;
                time = Mathf.Clamp(time + timeline.DeltaTime, 0f, maxSelectionTime);
                onSelectionDurationUpdate?.Invoke(time, maxSelectionTime);
            }
            StopSelection();
        }

        /// <summary>
        /// Updates the selection.
        /// </summary>
        /// <param name="emotion">The emotion to select.</param>
        protected void UpdateSelection(EmotionType emotion)
        {
            if (targetEmotion != emotion)
            {
                if (switchTime > 0f)
                {
                    if (emotionSwitch != null)
                    {
                        StopCoroutine(emotionSwitch);
                    }
                    if (Selection != emotion)
                    {
                        emotionSwitch = StartCoroutine(DoSwitch(emotion));
                    }
                    else
                    {
                        targetEmotion = emotion;
                        emotionSwitch = null;
                    }
                }
                else
                {
                    targetEmotion = emotion;
                    SetSelection(emotion);
                }
            }
        }

        /// <summary>
        /// Sets the new selected emotion.
        /// </summary>
        /// <param name="emotion">The emotion to select.</param>
        protected void SetSelection(EmotionType emotion)
        {
            if (Selection != emotion)
            {
                Selection = emotion;
                onSelectionChange?.Invoke(Selection);
            }
        }

        /// <summary>
        /// Resets the selection back to the default value.
        /// </summary>
        public void ResetSelection()
        {
            if (!IsSelecting)
            {
                targetEmotion = DEFAULT_EMOTION;
                SetSelection(DEFAULT_EMOTION);
            }
        }

        /// <summary>
        /// Switches the emotion after the delay.
        /// </summary>
        /// <param name="emotion">The emotion to switch to.</param>
        protected IEnumerator DoSwitch(EmotionType emotion)
        {
            targetEmotion = emotion;
            float time = 0f;
            while (time < switchTime)
            {
                yield return null;
                time += timeline.DeltaTime;
            }
            emotionSwitch = null;
            SetSelection(emotion);
        }
    }
}
