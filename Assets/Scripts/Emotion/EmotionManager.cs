namespace HappyGames.Emotion
{
    using HappyGames.Data;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    /// Manages the emotion detection.
    /// </summary>
    public sealed partial class EmotionManager : MonoBehaviour
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        private EmotionType currentEmotion = EmotionType.Neutral;
        /// <summary>
        /// The currently selected emotion.
        /// </summary>
        public EmotionType CurrentEmotion
        {
            get => currentEmotion;
            private set
            {
                if (currentEmotion != value)
                {
                    currentEmotion = value;
                    onEmotionDetected?.Invoke(currentEmotion);
                }
            }
        }

        /// <summary>
        /// True if the emotions are currently tracked.
        /// </summary>
        public bool IsTracking { get; private set; }

        /// <summary>
        /// Called when the tracking starts.
        /// </summary>
        public event UnityAction OnTrackingStart;

        /// <summary>
        /// Called when the tracking ends.
        /// </summary>
        public event UnityAction OnTrackingEnd;

        private UnityAction<EmotionType> onEmotionDetected;
        /// <summary>
        /// Called when the detected emotion changes.
        /// </summary>
        public event UnityAction<EmotionType> OnEmotionDetected
        {
            add
            {
                onEmotionDetected += value;
                if (onEmotionDetected != null)
                {
                    StartTracking();
                }
            }
            remove
            {
                onEmotionDetected -= value;
                if (onEmotionDetected == null)
                {
                    StopTracking();
                }
            }
        }
        private HashSet<EmotionType> selectableEmotions;



        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            SwitchProfile(null, ProfileManager.GetActiveProfile());
            ProfileManager.OnProfileActivated += SwitchProfile;
        }

        private void OnDisable()
        {
            ProfileManager.OnProfileActivated -= SwitchProfile;
            SwitchProfile(ProfileManager.GetActiveProfile(), null);
        }

        private void Update()
        {
            if (IsTracking)
            {
                EmotionType selection = UpdateEmotion();
                CurrentEmotion = selectableEmotions.Contains(selection) ? selection : default;
            }
        }

        /// <summary>
        /// Starts the detection of emotions
        /// </summary>
        private void StartTracking()
        {
            if (!IsTracking)
            {
                IsTracking = true;
                TrackingStart();
                OnTrackingStart?.Invoke();
            }
        }

        /// <summary>
        /// Stops the detection of emotions.
        /// </summary>
        private void StopTracking()
        {
            if (IsTracking)
            {
                TrackingStop();
                IsTracking = false;
                CurrentEmotion = EmotionType.Neutral;
                OnTrackingEnd?.Invoke();
            }
        }

        /// <summary>
        /// Switches the used profile.
        /// </summary>
        /// <param name="oldProfile">The previously used profile.</param>
        /// <param name="newProfile">The profile now in use.</param>
        private void SwitchProfile(UserProfile oldProfile, UserProfile newProfile)
        {
            if (oldProfile != null)
            {
                oldProfile.OnEmotionUnlocked -= AddEmotion;
            }
            if (newProfile != null)
            {
                selectableEmotions = new HashSet<EmotionType>(newProfile.UnlockedEmotions);
                newProfile.OnEmotionUnlocked += AddEmotion;
            }
        }

        /// <summary>
        /// Adds an emotion to the selectable emotions.
        /// </summary>
        /// <param name="emotion">The emotion to add.</param>
        private void AddEmotion(EmotionType emotion) => selectableEmotions.Add(emotion);
    }

    /// <summary>
    /// The type of emotion.
    /// </summary>
    public enum EmotionType { Neutral, Angry, Happy, Sad, Surprised }

    /// <summary>
    /// An <see cref="UnityEvent"/> with an <see cref="EmotionType"/> as attribute.
    /// </summary>
    [System.Serializable]
    public class EmotionTypeEvent : UnityEvent<EmotionType> { }
}
