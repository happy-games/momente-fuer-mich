namespace HappyGames.Emotion
{
    using UnityEngine;

    /// <summary>
    /// Provides colors for all emotions.
    /// </summary>
    [CreateAssetMenu(fileName = "EmotionColorSet", menuName = "Set/Emotion/Color")]
    public class EmotionColorSet : EmotionSet<Color> { }
}
