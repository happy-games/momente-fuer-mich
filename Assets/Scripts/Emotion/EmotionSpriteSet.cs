namespace HappyGames.Emotion
{
    using UnityEngine;

    /// <summary>
    /// Provides sprites for all emotions.
    /// </summary>
    [CreateAssetMenu(fileName = "EmotionSpriteSet", menuName = "Set/Emotion/Sprite")]
    public class EmotionSpriteSet : EmotionSet<Sprite> { }
}
