namespace HappyGames.Emotion
{
    using UnityEngine;
    using UnityEngine.UI;
    
    [CreateAssetMenu(fileName = nameof(EmotionColorBlockSet), menuName = "Set/Emotion/Color Block")]
    public class EmotionColorBlockSet : EmotionSet<ColorBlock> { }
}
