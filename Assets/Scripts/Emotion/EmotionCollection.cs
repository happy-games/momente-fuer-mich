namespace HappyGames.Emotion
{
    using UnityEngine;

    /// <summary>
    /// Provides values for all emtoions.
    /// </summary>
    /// <typeparam name="T">The type of the value.</typeparam>
    public class EmotionCollection<T> : EmotionCollection<T, T, T, T, T, T> { }
    /// <summary>
    /// Provides values for all emtoions.
    /// </summary>
    /// <typeparam name="T">The type of the value.</typeparam>
    public class EmotionCollection<T, TNeutral, TAngry, THappy, TSad, TSurprised> 
        where TNeutral : T where TAngry : T where THappy : T where TSad : T where TSurprised : T
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The value for the neutral emotion.")]
        protected TNeutral neutral = default;
        /// <summary>
        /// The value for the neutral emotion.
        /// </summary>
        public TNeutral Neutral => neutral;

        [SerializeField, Tooltip("The value for the angry emotion.")]
        protected TAngry angry = default;
        /// <summary>
        /// The value for the angry emotion.
        /// </summary>
        public TAngry Angry => angry;

        [SerializeField, Tooltip("The value for the happy emotion.")]
        protected THappy happy = default;
        /// <summary>
        /// The value for the happy emotion.
        /// </summary>
        public THappy Happy => happy;

        [SerializeField, Tooltip("The value for the sad emotion.")]
        protected TSad sad = default;
        /// <summary>
        /// The value for the sad emotion.
        /// </summary>
        public TSad Sad => sad;

        [SerializeField, Tooltip("The value for the surpised emotion.")]
        protected TSurprised surprised = default;
        /// <summary>
        /// The value for the surprised emotion.
        /// </summary>
        public TSurprised Surprised => surprised;


        // --- | Constructor | ---------------------------------------------------------------------------------------------

        /// <summary>
        /// </summary>
        public EmotionCollection() : this(default, default, default, default, default) { }
        /// <summary>
        /// Creates a new emotion collection.
        /// </summary>
        /// <param name="neutral">The value for the neutral emotion.</param>
        /// <param name="angry">The value for the angry emotion.</param>
        /// <param name="happy">The value for the happy emotion.</param>
        /// <param name="sad">The value for the sad emotion.</param>
        /// <param name="surprised">The value for the surprised emotion.</param>
        public EmotionCollection(TNeutral neutral, TAngry angry, THappy happy, TSad sad, TSurprised surprised)
        {
            this.neutral = neutral;
            this.angry = angry;
            this.happy = happy;
            this.sad = sad;
            this.surprised = surprised;
        }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns the value for the emotion.
        /// </summary>
        /// <param name="emotion">The emotion whos value to return.</param>
        /// <returns>The value for the emotion.</returns>
        public T GetValue(EmotionType emotion)
        {
            switch (emotion)
            {
                default: return default(T);
                case EmotionType.Neutral: return neutral;
                case EmotionType.Angry: return angry;
                case EmotionType.Happy: return happy;
                case EmotionType.Sad: return sad;
                case EmotionType.Surprised: return surprised;
            }
        }
    }
}
