namespace HappyGames.Audio
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public class DialogAudioPlayer : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The source to play the audio.")]
        protected AudioSource source = null;

        [SerializeField, Tooltip("The audio lines to play.")]
        protected DialogAudio[] dialogAudios = default;

        [Space, SerializeField, Tooltip("Called when a line starts playing.")]
        protected UnityEvent onLineStarted = default;

        [SerializeField, Tooltip("Called when the line finished playing.")]
        protected UnityEvent onLineFinished = default;

        [SerializeField, Tooltip("Called when the line is stopped before finishing.")]
        protected UnityEvent onLineCanceled = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected Dictionary<string, AudioClip> lines;
        protected Coroutine finishDelay = null;
        /// <summary>
        /// True if a delay is active.
        /// </summary>
        protected bool IsPlaying => finishDelay != null;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        protected virtual void Awake()
        {
            ConvertArrayToDictionary();
        }

        private void OnDisable()
        {
            if (IsPlaying)
            {
                StopCoroutine(finishDelay);
                onLineCanceled?.Invoke();
            }
        }

        /// <summary>
        /// Plays the audio with the given tag.
        /// </summary>
        /// <param name="tag">The tag of the line.</param>
        public void PlayLine(string tag)
        {
            if (enabled)
            {
                if (IsPlaying)
                {
                    StopCoroutine(finishDelay);
                    onLineCanceled?.Invoke();
                }
                if (lines.TryGetValue(tag, out AudioClip clip) && clip)
                {
                    source.clip = clip;
                    source.Play();
                    onLineStarted?.Invoke();
                    finishDelay = StartCoroutine(DoWaitForLineEnd(clip));
                }
            }
        }

        /// <summary>
        /// Converts the <see cref="dialogAudios"/> array to the <see cref="lines"/> dictionary.
        /// </summary>
        protected void ConvertArrayToDictionary()
        {
            lines = new Dictionary<string, AudioClip>();
            foreach (DialogAudio audio in dialogAudios)
            {
                foreach (var pair in audio)
                {
                    lines.Add(pair.Key, pair.Value);
                }
            }
        }

        /// <summary>
        /// Waits for a <see cref="AudioClip"/> to finish playing and invokes the <see cref="onLineFinished"/> sevent.
        /// </summary>
        /// <param name="clip">The clip to wait for.</param>
        protected IEnumerator DoWaitForLineEnd(AudioClip clip)
        {
            yield return new WaitForSeconds(clip.length);
            finishDelay = null;
            onLineFinished?.Invoke();
        }
    }
}
