namespace HappyGames.Dialog
{
    using HappyGames.Data;
    using HappyGames.Emotion;
    using HappyGames.Translations;
    using UnityEngine;

    [CreateAssetMenu(fileName = "Character", menuName = "Character")]
    public class Character : ScriptableObject
    {
        // --- | Inspector | -------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The name the character is referenced in the dialog.")]
        protected string referenceName = "";
        /// <summary>
        /// The name the character is referenced in the dialog.
        /// </summary>
        public string ReferenceName => referenceName;

        [SerializeField, Tooltip("The name to display of the character.")]
        protected TranslatableCollection<string> displayName = default;
        /// <summary>
        /// The name to display of the character.
        /// </summary>
        public string DisplayName => displayName.GetValue(ProfileManager.GetActiveProfile().Settings.Language);

        [SerializeField, Tooltip("The pictures of the character.")]
        protected EmotionSpriteSet sprites = null;
        /// <summary>
        /// The pictures of the character.
        /// </summary>
        public EmotionSpriteSet Sprites => sprites;
        
        
        // --- | Methods | -------------------------------------------------------------------------------------------------

        public override string ToString()
        {
            return $"({ nameof(Character) }): { nameof(ReferenceName) }: { ReferenceName }, { nameof(DisplayName) }: { DisplayName }";
        }
    }
}
