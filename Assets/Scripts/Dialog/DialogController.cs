namespace HappyGames.Dialog
{
    using HappyGames.Emotion;
    using System;
    using System.Linq;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using UnityEngine;
    using UnityEngine.Events;
    using Yarn;
    using Yarn.Unity;
    using HappyGames.Data;
    using HappyGames.Translations;

    public class DialogController : DialogueUIBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The characters in the dialogs.")]
        protected Character[] characters = default;

        [SerializeField, Min(0f), Tooltip("The time it takes one letter to be written.")]
        protected float textSpeed = 0.025f;

        [SerializeField, Tooltip("The script to select an emotion.")]
        protected EmotionSelect select = null;

        [SerializeField, Tooltip("Called when the character speaking changes.")]
        protected UnityEvent<Character> onCharacterChange = default;

        [SerializeField, Tooltip("Called when the characters emotion changes.")]
        protected UnityEvent<EmotionType> onEmotionChange = default;

        [SerializeField, Tooltip("Called when a new line is started. The attribute is the lines tag.")]
        protected UnityEvent<string> onNewLine = default;

        [SerializeField, Tooltip("Called when the lines content changes.")]
        protected UnityEvent<string> onLineContentChange = default;

        [Space, SerializeField, Tooltip("Called when the dialog starts.")]
        protected UnityEvent onStart = default;

        [SerializeField, Tooltip("Called when the line was completed.")]
        protected UnityEvent onLineCompleted = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// True if a line is currently running.
        /// </summary>
        public bool IsRunning { get; protected set; } = false;
        protected bool userRequestedNextLine = false;
        private LineInfo currentLine = null;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override void DialogueStarted()
        { 
            base.DialogueStarted();
            IsRunning = true;
            SwitchProfile(null, ProfileManager.GetActiveProfile());
            ProfileManager.OnProfileActivated += SwitchProfile;
            onStart?.Invoke();
        }

        /// <inheritdoc/>
        public override void DialogueComplete()
        {
            IsRunning = false;
            ProfileManager.OnProfileActivated -= SwitchProfile;
            SwitchProfile(ProfileManager.GetActiveProfile(), null);
            base.DialogueComplete();
        }

        /// <inheritdoc/>
        public override Dialogue.HandlerExecutionType RunLine(Line line, ILineLocalisationProvider localisationProvider, Action onLineComplete)
        {
            StartCoroutine(DoRunLine(line, localisationProvider, onLineComplete));
            return Dialogue.HandlerExecutionType.PauseExecution;
        }
        
        /// <summary>
        /// Handles a single line.
        /// </summary>
        /// <param name="line">The line to display.</param>
        /// <param name="localisationProvider">The script translating.</param>
        /// <param name="onComplete">The action to call when the line was displayed.</param>
        private IEnumerator DoRunLine(Yarn.Line line, ILineLocalisationProvider localisationProvider, System.Action onComplete)
        {
            // Make sure the dialog is not scipted.
            userRequestedNextLine = false;

            // The final text we'll be showing for this line.
            currentLine = new LineInfo(localisationProvider.GetLocalisedTextForLine(line), characters);
            onCharacterChange?.Invoke(currentLine.Character);
            onEmotionChange?.Invoke(currentLine.Emotion);
            onNewLine?.Invoke(line.ID.Replace("line:", ""));
            string text = currentLine.Content;

            // Check if any text is given.
            if (text == null)
            {
                Debug.LogWarning($"Line { line.ID } doesn't have any localised text.");
                text = line.ID;
            }

            if (textSpeed > 0.0f)
            {
                // Display the line one character at a time
                var stringBuilder = new StringBuilder();

                foreach (char c in text)
                {
                    stringBuilder.Append(c);
                    onLineContentChange?.Invoke(stringBuilder.ToString());
                    if (userRequestedNextLine)
                    {
                        // We've requested a skip of the entire line.
                        // Display all of the text immediately.
                        onLineContentChange?.Invoke(text);
                        break;
                    }
                    yield return new WaitForSeconds(textSpeed);
                }
            }
            else
            {
                // Display the entire line immediately if textSpeed <= 0
                onLineContentChange?.Invoke(text);
            }

            // We're now waiting for the player to move on to the next line
            userRequestedNextLine = false;

            // Indicate to the rest of the game that the line has finished being delivered
            onLineCompleted?.Invoke();

            // Wait for the user to continue.
            yield return new WaitUntil(() => userRequestedNextLine);

            // Avoid skipping lines if textSpeed == 0
            yield return new WaitForEndOfFrame();

            // Tell the DialogRunner that the line was completed.
            onComplete();
        }

        /// <inheritdoc/>
        public override void RunOptions(OptionSet optionSet, ILineLocalisationProvider localisationProvider, Action<int> onOptionSelected)
        {
            UnityAction<EmotionType> onEmotionOptionSelected = null;
            onEmotionOptionSelected = (e) => 
            {
                for (int i = 0; i < optionSet.Options.Length; i++)
                {
                    if (localisationProvider.GetLocalisedTextForLine(optionSet.Options[i].Line).Trim() == e.ToString())
                    {
                        select.OnSelectionEnd -= onEmotionOptionSelected;
                        onOptionSelected?.Invoke(i);
                        return;
                    }
                }
                select.StartSelection();
            };
            select.StartSelection();
            select.OnSelectionEnd += onEmotionOptionSelected;
        }

        /// <inheritdoc/>
        public override Dialogue.HandlerExecutionType RunCommand(Command command, Action onCommandComplete)
        {
            return Dialogue.HandlerExecutionType.ContinueExecution;
        }

        /// <summary>
        /// Goes to the next line.
        /// </summary>
        [ContextMenu("NextLine")]
        public void NextLine() => userRequestedNextLine = true;



        private void SwitchProfile(UserProfile oldProfile, UserProfile newProfile)
        {
            if (oldProfile != null)
            {
                oldProfile.Settings.OnLanguageChanged -= UpdateLanguage;
            }
            if (newProfile != null)
            {
                newProfile.Settings.OnLanguageChanged -= UpdateLanguage;
                UpdateLanguage(newProfile.Settings.Language);
            }
        }

        private void UpdateLanguage(Language arg0)
        {
            if (currentLine != null)
            {
                onCharacterChange?.Invoke(currentLine.Character);
            }
        }


        // --- | Classes | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Contains the content of a line.
        /// </summary>
        protected class LineInfo
        {
            // Variables --------------------------------------------------------------------------

            /// <summary>
            /// The character talking.
            /// </summary>
            public Character Character { get; protected set; }

            /// <summary>
            /// The emotion of the character.
            /// </summary>
            public EmotionType Emotion { get; protected set; }

            /// <summary>
            /// The spoken text.
            /// </summary>
            public string Content { get; protected set; }

            // Constructors -----------------------------------------------------------------------

            /// <summary>
            /// Creates a new object containg info of the line.
            /// </summary>
            /// <param name="line">The line to analise.</param>
            public LineInfo(string line, IEnumerable<Character> characters)
            {
                int headerEnd = line.IndexOf(':');
                string header = line.Substring(0, headerEnd);
                int nameEnd = header.IndexOf(',');
                if (nameEnd >= 0)
                {
                    Character = characters.Where(c => c.ReferenceName.ToLower() == header.Substring(0, nameEnd).ToLower()).FirstOrDefault();
                    Emotion = Enum.TryParse(header.Substring(nameEnd + 1).Trim(), out EmotionType emotion) ? emotion : default;
                }
                else
                {
                    Character = characters.Where(c => c.ReferenceName.ToLower() == header.ToLower()).FirstOrDefault();
                    Emotion = default;
                }
                Content = line.Substring(headerEnd + 1).Trim();
            }
        }
    }
}
