namespace HappyGames.Utilities
{
    using UnityEngine;
    using UnityEngine.Events;

    public class EventSplitter : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called when the fitlered value is true.")]
        private UnityEvent onTrue = default;

        [SerializeField, Tooltip("Called when the fitlered value is false.")]
        private UnityEvent onFalse = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Calls the events in the script depending on the given value.
        /// </summary>
        /// <param name="value">The decider which event to call.</param>
        public void Split(bool value) => (value ? onTrue : onFalse).Invoke();
    }
}
