namespace HappyGames.UI
{
    using HappyGames.Data;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;

    public class RulePanel : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The image to display the rules picture.")]
        protected Image image = default;

        [SerializeField, Tooltip("The textfield to display the heading.")]
        protected TMP_Text heading = default;

        [SerializeField, Tooltip("The textfield to display the rules text.")]
        protected TMP_Text text = default;

        [SerializeField, Tooltip("The text to put at the start of a hilighted area.")]
        protected string highlightPrefix = "color=#00B46E";

        [SerializeField, Tooltip("The text to put at the end of a hilighted area.")]
        protected string highlightSufix = "color";


        // --- | Variagbles | ----------------------------------------------------------------------------------------------

        /// <summary>
        /// The <see cref="UnityEngine.RectTransform"/> component of the object.
        /// </summary>
        public RectTransform RectTransform => (RectTransform)transform;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Clones the panel.
        /// </summary>
        /// <param name="rule">The <see cref="GameRule"/> to display in the panel.</param>
        /// <param name="parent">The parent of the panel.</param>
        /// <returns>The cloned panel.</returns>
        public RulePanel Clone(GameRule rule, Transform parent)
        {
            RulePanel clone = Instantiate(this, parent);
            clone.Display(rule);
            return clone;
        }

        /// <summary>
        /// Displays the given rule.
        /// </summary>
        /// <param name="rule">The rule to display.</param>
        public void Display(GameRule rule)
        {
            image.sprite = rule.Iamge;
            heading.text = rule.Headig;
            text.text = rule.GetText(highlightPrefix, highlightSufix);
            Vector2 size = RectTransform.sizeDelta;
            size.y = Mathf.Max(image.rectTransform.rect.height, text.preferredHeight) + heading.rectTransform.rect.height;
            RectTransform.sizeDelta = size;
        }
    }
}
