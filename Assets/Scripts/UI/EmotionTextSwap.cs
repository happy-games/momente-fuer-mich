namespace HappyGames.UI
{
    using UnityEngine;
    using TMPro;

    /// <summary>
    /// Changes the value of a textfield depending on the given emotion.
    /// </summary>
    public class EmotionTextSwap : EmotionValueSwap<string>
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The textfield to display the text in.")]
        protected TMP_Text target = null;
        /// <inheritdoc/>
        protected override string Target 
        { 
            get => target.text; 
            set => target.text = value; 
        }
    }
}
