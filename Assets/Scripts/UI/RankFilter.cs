namespace HappyGames.UI
{
    using HappyGames.Data;

    public class RankFilter : TrophyFilter
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected RankType filteredRank = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Sets the rank to filter.
        /// </summary>
        /// <param name="rank">The filtered rank.</param>
        public void SetFilter(RankType rank)
        {
            useFilter = true;
            filteredRank = rank;
            onFilterUpdate?.Invoke();
        }

        /// <inheritdoc/>
        public override bool CheckFitler(Trophy trophy) => (
            !useFilter || 
            trophy is FruitBasketTrophy && ((FruitBasketTrophy)trophy).Rank == filteredRank ||
            trophy is BaloonTrophy && ((BaloonTrophy)trophy).Rank == filteredRank
        );
    }
}
