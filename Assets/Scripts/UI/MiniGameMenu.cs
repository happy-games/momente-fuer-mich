namespace HappyGames.UI
{
    using UnityEngine;
    using UnityEngine.Events;

    [RequireComponent(typeof(Canvas))]
    public class MiniGameMenu : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called when the menu is opened.")]
        protected UnityEvent onOpen = default;
        /// <summary>
        /// Called when the menu is opened.
        /// </summary>
        public event UnityAction OnOpen
        {
            add => onOpen.AddListener(value);
            remove => onOpen.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called when the menu is closed.")]
        protected UnityEvent onClose = default;
        /// <summary>
        /// Called when the menu is closed.
        /// </summary>
        public event UnityAction OnClose
        {
            add => onClose.AddListener(value);
            remove => onClose.RemoveListener(value);
        }


        // --- | Varaibles | -----------------------------------------------------------------------------------------------

        protected Canvas canvas;
        /// <summary>
        /// True if the menu is open.
        /// </summary>
        public bool IsOpen => canvas.enabled;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        protected virtual void Awake()
        {
            canvas = GetComponent<Canvas>();
            canvas.enabled = false;
        }

        private void OnDisable()
        {
            Close();
        }

        /// <summary>
        /// Opens the menu.
        /// </summary>
        [ContextMenu("Open")]
        public virtual void Open()
        {
            if (enabled && !IsOpen)
            {
                gameObject.SetActive(true);
                canvas.enabled = true;
                onOpen?.Invoke();
            }
        }

        /// <summary>
        /// Closes the menu.
        /// </summary>
        [ContextMenu("Close")]
        public virtual void Close()
        {
            if (IsOpen)
            {
                canvas.enabled = false;
                onClose?.Invoke();
                gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Closes an open menu, opens a closed one.
        /// </summary>
        public void Toggle()
        {
            if (IsOpen)
            {
                Close();
            }
            else
            {
                Open();
            }
        }
    }
}
