namespace HappyGames.UI
{
    using HappyGames.Data;
    using UnityEngine;
    using UnityEngine.Events;

    public class TrophyDisplay<T> : MonoBehaviour where T : Trophy
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called when the a trophy is set to eb displayed.")]
        protected UnityEvent<T> onTrophyDisplayed = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Displays the resource.
        /// </summary>
        /// <param name="trophy">The trophy to display.</param>
        public virtual void Display(T trophy)
        {
            onTrophyDisplayed?.Invoke(trophy);
        }

        /// <summary>
        /// Clones the display.
        /// </summary>
        /// <param name="parent">The parent object of the clone.</param>
        /// <param name="position">There to place the clone.</param>
        /// <param name="size">The size of the display.</param>
        /// <param name="trophy">The kind of trophy to display.</param>
        /// <returns>The cloned display.</returns>
        public TrophyDisplay<T> Clone(Transform parent, Vector2 position, float size, T trophy)
        {
            TrophyDisplay<T> display = Instantiate(this, position, Quaternion.identity, parent);
            ((RectTransform)display.transform).sizeDelta = Vector2.one * size;
            display.Display(trophy);
            return display;
        }
    }
}
