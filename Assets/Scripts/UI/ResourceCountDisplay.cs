namespace HappyGames.UI
{
    using HappyGames.Data;
    using TMPro;
    using UnityEngine;

    public class ResourceCountDisplay : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The textfield to display the count.")]
        protected TMP_Text countDisplay = null;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            UpdateCount(ProfileManager.GetActiveProfile().Inventory.ResourceCount);
            ProfileManager.GetActiveProfile().Inventory.OnResourceCountUpdate += UpdateCount;
        }

        private void OnDisable()
        {
            ProfileManager.GetActiveProfile().Inventory.OnResourceCountUpdate -= UpdateCount;
        }

        /// <summary>
        /// Updates the displayed count.
        /// </summary>
        /// <param name="count">The new number to display.</param>
        protected void UpdateCount(int count) => countDisplay.text = count.ToString();
    }
}
