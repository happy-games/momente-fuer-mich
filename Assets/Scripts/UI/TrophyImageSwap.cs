namespace HappyGames.UI
{
    using UnityEngine;
    using UnityEngine.UI;

    public class TrophyImageSwap : TrophyValueSwap<Sprite>
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The renderer whos sprite to change.")]
        protected Image target = null;
        /// <inheritdoc/>
        protected override Sprite Target 
        {
            get => target.sprite;
            set => target.sprite = value;
        }
    }
}
