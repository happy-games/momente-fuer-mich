namespace HappyGames.UI
{
    using HappyGames.Data;
    using HappyGames.Utilities;
    using TMPro;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;

    [RequireComponent(typeof(Canvas))]
    public class SimpleQuestionDisplay : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [Header("Message")] // --------------------------------------------------------------------
        [SerializeField, Tooltip("The field to display the message.")]
        protected TMP_Text messageField = null;

        [Header("Accept")] // ---------------------------------------------------------------------
        [SerializeField, Tooltip("The button to for accepting.")]
        protected Button acceptButton = null;

        [SerializeField, Tooltip("The text on the accept button.")]
        protected TMP_Text acceptLabel = null;

        [SerializeField, Tooltip("The image to display the accept icon.")]
        protected Image acceptIcon = null;

        [Header("Deny")] // ---------------------------------------------------------------------
        [SerializeField, Tooltip("The button to for denying.")]
        protected Button denyButton = null;

        [SerializeField, Tooltip("The text on the deny button.")]
        protected TMP_Text denyLabel = null;

        [SerializeField, Tooltip("The image to display the deny icon.")]
        protected Image denyIcon = null;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected Canvas canvas;
        protected UnityAction accept;
        protected UnityAction deny;
        protected UnityAction stop;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            canvas = GetComponent<Canvas>();
            canvas.enabled = false;
        }

        private void OnEnable()
        {
            acceptButton.onClick.AddListener(Accept);
            denyButton.onClick.AddListener(Deny);
        }

        private void OnDisable()
        {
            acceptButton.onClick.RemoveListener(Accept);
            denyButton.onClick.RemoveListener(Deny);
        }

        /// <summary>
        /// Displays the question.
        /// </summary>
        /// <param name="question">The question to display.</param>
        public void Display(ISimpleQuestion question)
        {
            accept = question.AcceptResonse;
            deny = question.DenyResponse;
            stop = question.AnsweredResponse;
            messageField.text = ProfileManager.GetActiveProfile().Settings.Language == HappyGames.Translations.Language.German ? question.Message_ger: question.Message_engl;
            acceptLabel.text = ProfileManager.GetActiveProfile().Settings.Language == HappyGames.Translations.Language.German ? question.AcceptLabel_ger : question.AcceptLabel_engl;
            denyLabel.text = ProfileManager.GetActiveProfile().Settings.Language == HappyGames.Translations.Language.German ? question.DenyLabel_ger : question.DenyLabel_engl; ;
            acceptIcon.sprite = question.AcceptIcon;
            denyIcon.sprite = question.DenyIcon;
            question.StatedResponse?.Invoke();
            Show();
        }
        /// <summary>
        /// Displays the question.
        /// </summary>
        /// <param name="question">The question to display.</param>
        public void Display(SimpleQuestion question) => Display((ISimpleQuestion)question);

        /// <summary>
        /// The response for accepting.
        /// </summary>
        protected void Accept()
        {
            Hide();
            accept?.Invoke();
            ResetResponses();
        }

        /// <summary>
        /// The response for denying.
        /// </summary>
        protected void Deny()
        {
            Hide();
            deny?.Invoke();
            ResetResponses();
        }

        /// <summary>
        /// Resets all responses.
        /// </summary>
        protected void ResetResponses()
        {
            UnityAction action = stop;
            accept = null;
            deny = null;
            stop = null;
            action.Invoke();
        }

        /// <summary>
        /// Shows the question panel.
        /// </summary>
        protected void Show()
        {
            Resize();
            canvas.enabled = true;
        }
        /// <summary>
        /// Hides the qustion panel.
        /// </summary>
        protected void Hide() => canvas.enabled = false;

        /// <summary>
        /// Updates the size of the display.
        /// </summary>
        protected void Resize()
        {
            Vector2 deltaSize = ((RectTransform)transform).sizeDelta;
            deltaSize.y = 1080 - ((RectTransform)messageField.transform).rect.height + messageField.preferredHeight;
            ((RectTransform)transform).sizeDelta = deltaSize;
        }
    }
}
