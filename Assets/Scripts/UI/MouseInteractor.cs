namespace HappyGames.UI
{
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.EventSystems;

    public class MouseInteractor : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called when the object was clicked.")]
        protected UnityEvent onClick = default;

        [SerializeField, Tooltip("Called when the object was released.")]
        protected UnityEvent onRelease = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------


        /// <inheritdoc/>
        public void OnPointerDown(PointerEventData eventData) => onClick?.Invoke();

        /// <inheritdoc/>
        public void OnPointerUp(PointerEventData eventData) => onRelease?.Invoke();
    }
}
