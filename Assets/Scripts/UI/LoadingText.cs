namespace HappyGames.UI
{
    using System.Collections;
    using UnityEngine;
    using TMPro;

    /// <summary>
    /// Handles the content of a textfield.
    /// </summary>
    public class LoadingText : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The text field to display the text.")]
        protected TMP_Text textField = null;

        [SerializeField, Tooltip("The delay between the adding of dots.")]
        private float delay = 0.5f;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Start()
        {
            StartCoroutine(DoDisplayText());
        }

        /// <summary>
        /// Updates the textfields message.
        /// </summary>
        protected IEnumerator DoDisplayText()
        {
            string dots = "";
            while (true)
            {
                if (dots.Length < 3)
                {
                    dots += ".";
                }
                else
                {
                    dots = "";
                }
                textField.text = "Loading" + dots;
                yield return new WaitForSeconds(delay);
            }
        }
    }
}
