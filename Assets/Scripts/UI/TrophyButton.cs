namespace HappyGames.UI
{
    using HappyGames.Data;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;
   
    
    public class TrophyButton<T> : TrophyDisplay<T> where T : Trophy
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The button on the display.")]
        protected Button button = null;

        public Button Button => button;

        [Space, SerializeField, Tooltip("Called when the display is clicked.")]
        protected UnityEvent<T> onDisplayClick = default;
        /// <summary>
        /// Called when the display is clicked.
        /// </summary>
        public event UnityAction<T> OnDisplayClick
        {
            add => onDisplayClick.AddListener(value);
            remove => onDisplayClick.RemoveListener(value);
        }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Displays the resource.
        /// </summary>
        /// <param name="trophy">The trophy to display.</param>
        public override void Display(T trophy)
        {
            base.Display(trophy);
            button.onClick.AddListener(() => { onDisplayClick?.Invoke(trophy); });
        }

        /// <summary>
        /// Clones the button.
        /// </summary>
        /// <param name="parent">The parent object of the clone.</param>
        /// <param name="position">There to place the clone.</param>
        /// <param name="size">The size of the button.</param>
        /// <param name="trophy">The kind of trophy to button.</param>
        /// <returns>The cloned button.</returns>       
        public new TrophyButton<T> Clone(Transform parent, Vector2 position, float size, T trophy) => (TrophyButton<T>)base.Clone(parent, position, size, trophy);
    }
}
