namespace HappyGames.UI
{
    using UnityEngine;
    using UnityEngine.UI;

    public class SpriteGradientSetter : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The target to recolor.")]
        protected SpriteRenderer target = null;

        [SerializeField, Tooltip("The gradient to apply.")]
        protected Gradient gradient = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Recolors the <see cref="SpriteRenderer"/>.
        /// </summary>
        /// <param name="value">The value to take from the gradient.</param>
        public void Recolor(float value) => target.color = gradient.Evaluate(value);
        /// <summary>
        /// Recolors the <see cref="SpriteRenderer"/>.
        /// </summary>
        /// <param name="current">The current value.</param>
        /// <param name="max">The maximum value.</param>
        public void Recolor(float current, float max) => Recolor(current / max);
    }
}
