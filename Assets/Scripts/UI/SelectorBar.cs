namespace HappyGames.UI
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;

    public abstract class SelectorBar<T> : MonoBehaviour where T : System.Enum
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The prefab for the selector buttons.")]
        protected TypeSelector<T> selectorPrefab = null;

        [SerializeField, Tooltip("The are to put the selectors in.")]
        protected RectTransform container = null;

        [SerializeField, Tooltip("The group the selectors are part of.")]
        protected ToggleGroup toggleGroup = null;

        [Space, SerializeField, Tooltip("Called when a value was selected.")]
        protected UnityEvent<T> onSelected = default;

        [SerializeField, Tooltip("Called when a value was deselected.")]
        protected UnityEvent<T> onDeselected = default;

        [SerializeField, Tooltip("Called when the amount of tabs updates.")]
        protected UnityEvent onTabCountUpdate = default;

        [SerializeField, Tooltip("The selectors enabled by default.")]
        protected List<T> enabledSelectors = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected SortedList<T, RectTransform> selectors;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            selectors = new SortedList<T, RectTransform>();
            AddTabs(enabledSelectors);
        }

        /// <summary>
        /// Adds a new tabs.
        /// </summary>
        /// <param name="values">The value of the new tab.</param>
        public void AddTabs(IEnumerable<T> values)
        {
            foreach (T value in values)
            {
                if (!enabledSelectors.Contains(value))
                {
                    enabledSelectors.Add(value);
                }
                if (selectors != null && !selectors.ContainsKey(value))
                {
                    selectors.Add(value, (RectTransform)BuildTab(value).transform);
                }
            }
            onTabCountUpdate?.Invoke();
        }

        /// <summary>
        /// Adds a new tabs.
        /// </summary>
        /// <param name="value">The value of the new tab.</param>
        public void AddTab(T value)
        {
            selectors.Add(value, (RectTransform)BuildTab(value).transform);
            onTabCountUpdate?.Invoke();
        }

        /// <summary>
        /// Builds a selector.
        /// </summary>
        /// <param name="value">The value to build the selector for.</param>
        /// <returns>The newly build selector.</returns>
        protected virtual TypeSelector<T> BuildTab(T value)
        {
            TypeSelector<T> selector = selectorPrefab.Clone(container, Vector2.zero, Vector2.zero, value, toggleGroup);
            selector.OnSelect += onSelected.Invoke;
            selector.OnDeselect += onDeselected.Invoke;
            return selector;
        }
    }
}
