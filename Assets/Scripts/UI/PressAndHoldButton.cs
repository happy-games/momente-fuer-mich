namespace HappyGames.Emotion
{
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.EventSystems;

    /// <summary>
    /// Handels press interactions.
    /// </summary>
    public class PressAndHoldButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [Header("States")] // ---------------------------------------------------------------------
        [SerializeField, Tooltip("True if the button is interactable.")]
        protected bool isInteractable = true;
        /// <summary>
        /// True if the button is interactable
        /// </summary>
        public bool IsInteractable
        {
            get => isInteractable;
            set => SetInteractable(value);
        }

        [Header("Hover")] // ----------------------------------------------------------------------
        [SerializeField, Tooltip("Called when the button gets hovered.")]
        protected UnityEvent onHoverStart = default;
        /// <summary>
        /// Called when the button gets hovered.
        /// </summary>
        public event UnityAction OnHoverStart
        {
            add => onHoverStart.AddListener(value);
            remove => onHoverStart.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called when the button stops beeing hovered.")]
        protected UnityEvent onHoverEnd = default;
        /// <summary>
        /// Called when the button stops beeing hovered.
        /// </summary>
        public event UnityAction OnHoverEnd
        {
            add => onHoverEnd.AddListener(value);
            remove => onHoverEnd.RemoveListener(value);
        }

        [Header("Interactions")] // ---------------------------------------------------------------
        [SerializeField, Tooltip("Called when the button gets clicked.")]
        protected UnityEvent onClick = default;
        /// <summary>
        /// Called when the button gets clicked.
        /// </summary>
        public event UnityAction OnClick
        {
            add => onClick.AddListener(value);
            remove => onClick.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called when the button is released.")]
        protected UnityEvent onRelease = default;
        /// <summary>
        /// Called when the button is released.
        /// </summary>
        public event UnityAction OnRelease
        {
            add => onRelease.AddListener(value);
            remove => onRelease.RemoveListener(value);
        }


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected bool isHovered = false;
        /// <summary>
        /// True if the button is hovered.
        /// </summary>
        public bool IsHovered => isInteractable && isHovered;

        /// <summary>
        /// True if the button is pressed.
        /// </summary>
        public bool IsPressed { get; protected set; }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        public void OnPointerDown(PointerEventData eventData)
        {
            if (isInteractable && !IsPressed)
            {
                IsPressed = true;
                onClick?.Invoke();
            }
        }

        public void OnPointerUp(PointerEventData eventData) => Realease();

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!isHovered)
            {
                isHovered = true;
                if (isInteractable)
                {
                    onHoverStart?.Invoke();
                }
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (isHovered)
            {
                isHovered = false;
                if (isInteractable)
                {
                    onHoverEnd?.Invoke();
                }
            }
            Realease();
        }

        /// <summary>
        /// Releases the button press.
        /// </summary>
        protected void Realease()
        {
            if (isInteractable && IsPressed)
            {
                IsPressed = false;
                onRelease?.Invoke();
            }
        }

        /// <summary>
        /// Sets the interactable state.
        /// </summary>
        /// <param name="value">True to set the button interactable.</param>
        protected void SetInteractable(bool value)
        {
            if (isInteractable != value)
            {
                if (!value)
                {
                    if (IsPressed)
                    {
                        Realease();
                    }
                    if (isHovered)
                    {
                        onHoverEnd?.Invoke();
                    }
                }
                else if (isHovered)
                {
                    onHoverStart?.Invoke();
                }
                isInteractable = value;
            }
        }
    }
}
