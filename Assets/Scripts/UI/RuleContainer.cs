namespace HappyGames.UI
{
    using HappyGames.Data;
    using UnityEngine;

    public class RuleContainer : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The rules to display.")]
        protected RuleSet rules = default;

        [SerializeField, Tooltip("The prefab to display the rules.")]
        protected RulePanel prefab = null;

        [SerializeField, Min(0f), Tooltip("The space between the sections.")]
        protected float spacing = 25f;


        // --- | Varaibles | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The container to put the rules in.
        /// </summary>
        protected RectTransform Container => (RectTransform)transform;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            bool isFirst = true;
            foreach (GameRule rule in rules)
            {
                RulePanel panel = prefab.Clone(rule, Container);
                panel.RectTransform.localPosition = Vector3.down * (Container.rect.height + (isFirst ? 0f : spacing));
                Container.sizeDelta += Vector2.up * (panel.RectTransform.rect.height + (isFirst ? 0f : spacing));
                isFirst = false;
            }
        }
    }
}
