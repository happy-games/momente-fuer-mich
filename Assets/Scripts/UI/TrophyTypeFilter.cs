namespace HappyGames.UI
{
    using HappyGames.Data;

    public class TrophyTypeFilter : TrophyFilter
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected TrophyType filteredTrophy = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Sets the emotion to filter.
        /// </summary>
        /// <param name="emotion">The filtered emotion.</param>
        public void SetFilter(TrophyType emotion)
        {
            useFilter = true;
            filteredTrophy = emotion;
            onFilterUpdate?.Invoke();
        }

        /// <inheritdoc/>
        public override bool CheckFitler(Trophy trophy) => (!useFilter || trophy.TrophyType == filteredTrophy);
    }
}
