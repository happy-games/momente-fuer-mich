namespace HappyGames.UI
{
    using HappyGames.Emotion;

    public class EmotionTypeSelector : TypeSelector<EmotionType> { }
}
