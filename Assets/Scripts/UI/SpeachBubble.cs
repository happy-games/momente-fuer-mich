namespace HappyGames.UI
{
    using HappyGames.TimeManagement;
    using System;
    using System.Collections;
    using System.Text;
    using TMPro;
    using UnityEngine;

    public class SpeachBubble : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The field to display the text.")]
        protected TMP_Text text = default;

        [SerializeField, Tooltip("The max size of the bubble.")]
        protected Vector2 maxSize = new Vector2(500f, 200f);

        [SerializeField, Tooltip("The timeline to animate the text.")]
        protected Timeline timeline = default;

        [SerializeField, Tooltip("True if the bubble resizes by animated text.")]
        protected bool isDynamic = true;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The objects <see cref="UnityEngine.RectTransform"/> component.
        /// </summary>
        protected RectTransform RectTransform => (RectTransform)transform;
        protected Vector2 padding;
        protected Coroutine textUpdate = null;
        /// <summary>
        /// True if the bubble is updating.
        /// </summary>
        public bool IsUpdating => text != null;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            padding = RectTransform.sizeDelta - text.rectTransform.sizeDelta;
            timeline ??= new Timeline();
        }

        /// <summary>
        /// Displays a message.
        /// </summary>
        /// <param name="message">The message to display.</param>
        public void Display(string message)
        {
            text.text = message;
            Resize(false);
        }
        /// <summary>
        /// Displays a message.
        /// </summary>
        /// <param name="message">The message to display.</param>
        /// <param name="speed">The time it takes to write a character in seconds.</param>
        public void Display(string message, float speed)
        {
            if (speed < 0f)
            {
                throw new ArgumentOutOfRangeException(nameof(speed), speed, $"The { nameof(speed) } parameter must be greather than 0.");
            }
            if (speed == 0f)
            {
                Display(message);
            }
            else
            {
                if (IsUpdating)
                {
                    StopCoroutine(textUpdate);
                }
                if (!isDynamic)
                {
                    Display(message);
                    text.text = string.Empty;
                }
                textUpdate = StartCoroutine(DoDisplay(message, speed, RectTransform.sizeDelta, null));
            }
        }

        /// <summary>
        /// Updates the bubble over time.
        /// </summary>
        /// <param name="message">The message to display.</param>
        /// <param name="speed">The time it takes to write a character in seconds.</param>
        /// <param name="onDone">Called when the message was displayed.</param>
        protected IEnumerator DoDisplay(string message, float speed, Vector2 max, Action onDone)
        {
            float time = 0f;
            int letters = 0;
            int current = 0;
            StringBuilder builder = new StringBuilder();
            while (builder.Length < message.Length)
            {
                yield return null;
                time += timeline.DeltaTime;
                letters = Mathf.FloorToInt(time / speed);
                while (letters > 0)
                {
                    builder.Append(message[current]);
                    text.text = builder.ToString();
                    time -= speed;
                    letters--;
                    current++;
                    if (isDynamic)
                    {
                        Resize(true);
                    }
                }
            }
            onDone?.Invoke();
        }

        /// <summary>
        /// Resizes the bubble to fit the text.
        /// </summary>
        /// <param name="growOnly">True if the area only gows.</param>
        protected void Resize(bool growOnly)
        {
            if (!growOnly || RectTransform.sizeDelta.x < maxSize.x)
            {
                RectTransform.sizeDelta = new Vector2(Mathf.Min(text.preferredWidth + padding.x, maxSize.x), RectTransform.sizeDelta.y);
            }
            if (!growOnly || RectTransform.sizeDelta.y < maxSize.y)
            {
                RectTransform.sizeDelta = new Vector2(RectTransform.sizeDelta.x, Mathf.Min(text.preferredWidth + padding.y, maxSize.y));
            }
        }
    }
}
