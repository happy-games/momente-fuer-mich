﻿namespace HappyGames.UI
{
    using HappyGames.TimeManagement;
    using System;
    using System.Collections;
    using System.Text;
    using TMPro;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;

    public class BubbleController : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [Header("References")]
        [SerializeField, Tooltip("The bubbles transform.")]
        protected RectTransform bubble = default;
        /// <summary>
        /// The rect transform of the bubble.
        /// </summary>
        public RectTransform Bubble => bubble;

        [SerializeField, Tooltip("The bubble image.")]
        protected Image image = default;

        [SerializeField, Tooltip("The text of the bubble.")]
        protected TMP_Text text = default;

        [SerializeField, Tooltip("The canvas group og the bubble.")]
        protected CanvasGroup group = default;

        [Header("Sprites")]
        [SerializeField, Tooltip("The sprite when disconnected.")]
        protected Sprite disconnectedBubble = default;

        [SerializeField, Tooltip("The sprite for the bubble.")]
        protected Sprite connectedBubble = default;

        [Header("Layout")] // ---------------------------------------------------------------------
        [SerializeField, Min(0f), Tooltip("The maximum width of the bubble.")]
        protected float maxWidth = 500f;

        [Header("Animations")]
        [SerializeField, Tooltip("The timelint to update the bubble.")]
        protected Timeline timeline = null;

        [SerializeField, Min(0f), Tooltip("The time it takes one letter to be displayed in seconds.")]
        protected float textSpeed = 0.025f;

        [SerializeField, Tooltip("True if the bubble size updates with the message.")]
        protected bool isDynamic = false;

        [SerializeField, Tooltip("The motion of the fade.")]
        protected AnimationCurve fadeMotion = new AnimationCurve(new Keyframe(0f, 1f), new Keyframe(0.5f, 0f));

        [Space, SerializeField, Tooltip("Called when the text is finished building.")]
        protected UnityEvent onCompleted = default;
        /// <summary>
        /// Called when the text is finished building.
        /// </summary>
        public event UnityAction OnCompleted
        {
            add => onCompleted.AddListener(value);
            remove => onCompleted.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called before the bubble is destroyed.")]
        protected UnityEvent onPoped = default;
        /// <summary>
        /// Called before the bubble is destroyed.
        /// </summary>
        public event UnityAction OnPoped
        {
            add => onPoped.AddListener(value);
            remove => onPoped.RemoveListener(value);
        }


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The <see cref="UnityEngine.RectTransform"/> of the object.
        /// </summary>
        protected RectTransform RectTransform => ((RectTransform)transform);
        protected Vector2 padding;
        protected Coroutine textUpdate = default;
        /// <summary>
        /// True if the bubbles message is currently updating.
        /// </summary>
        public bool IsUpdatingMessage => textUpdate != null;

        /// <summary>
        /// True if the bubble is poped.
        /// </summary>
        public bool IsPopped { get; protected set; }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            padding = -text.rectTransform.sizeDelta;
            timeline ??= new Timeline();
        }

        // Copy -----------------------------------------------------------------------------------

        /// <summary>
        /// Copies the bubble.
        /// </summary>
        /// <param name="parent">The parent to place the bubble inside.</param>
        /// <returns>The copy of the bubble.</returns>
        public BubbleController Copy(Transform parent)
        {
            BubbleController bubble = Instantiate(this, parent);
            return bubble;
        }
        /// <summary>
        /// Copies the bubble.
        /// </summary>
        /// <param name="parent">The parent to place the bubble inside.</param>
        /// <param name="message">The message to display.</param>
        /// <param name="onMessageCompleted">Called when the message was completed.</param>
        /// <returns>The copy of the bubble.</returns>
        public BubbleController Copy(Transform parent, string message, Action onMessageCompleted)
        {
            BubbleController bubble = Copy(parent);
            bubble.Display(message, onMessageCompleted);
            return bubble;
        }

        // Text -----------------------------------------------------------------------------------

        /// <summary>
        /// Displays a message in the bubble.
        /// </summary>
        /// <param name="message">The message to display.</param>
        /// <param name="onCompleted">Called when the message was displayed.</param>
        public void Display(string message, Action onCompleted)
        {
            // Clear any active updates.
            if (IsUpdatingMessage)
            {
                StopCoroutine(textUpdate);
                textUpdate = null;
            }
            // Check if the text is animated.
            if (textSpeed > 0f)
            {
                // Check if the bubble is dynamix.
                if (isDynamic)
                {
                    text.text = message[0].ToString();
                    UpdateSize();
                }
                else
                {
                    text.text = message;
                    UpdateSize();
                    text.text = message[0].ToString();
                }
                // Start the animation.
                textUpdate = StartCoroutine(DoUpdate(message, onCompleted));
            }
            else
            {
                text.text = message;
                UpdateSize();
                this.onCompleted?.Invoke();
                onCompleted?.Invoke();
            }
        }
        /// <summary>
        /// Displays a message in the bubble.
        /// </summary>
        /// <param name="message">The message to display.</param>
        public void Display(string message) => Display(message, null);

        /// <summary>
        /// Animates the message.
        /// </summary>
        /// <param name="message">The message to display.</param>
        /// <param name="onCompleted">Called when the message was displayed.</param>
        protected IEnumerator DoUpdate(string message, Action onCompleted)
        {
            float time = 0f;
            int current = 1;
            StringBuilder builder = new StringBuilder(text.text);
            // Loop until the whole message was build.
            while (builder.Length < message.Length)
            {
                // Wait for next frame.
                yield return null;
                // Update the time.
                time += timeline.DeltaTime;
                // Add all letters for the durations.
                while (time > textSpeed && builder.Length < message.Length)
                {
                    time -= textSpeed;
                    builder.Append(message[current]);
                    text.text = builder.ToString();
                    if (isDynamic)
                    {
                        UpdateSize();
                    }
                    current++;
                }
            }
            textUpdate = null;
            this.onCompleted?.Invoke();
            onCompleted?.Invoke();
        }

        [ContextMenu("Debug/Message")]
        private void TestMessage() => Display("This is my test message.");

        // Size -----------------------------------------------------------------------------------

        /// <summary>
        /// Updates the size of the bubble to fit its content.
        /// </summary>
        protected void UpdateSize()
        {
            RectTransform.sizeDelta = new Vector2(Mathf.Min(text.preferredWidth + padding.x, maxWidth), RectTransform.sizeDelta.y);
            RectTransform.sizeDelta = new Vector2(RectTransform.sizeDelta.x, text.preferredHeight + padding.y);
        }

        // Position -------------------------------------------------------------------------------

        /// <summary>
        /// Updates the bubbles positon.
        /// </summary>
        /// <param name="postion">The position to place the bubble.</param>
        public void SetPosition(Vector2 postion)
        {
            transform.position = postion;
        }

        public void Pop()
        {
            IsPopped = true;
            onPoped?.Invoke();
            Destroy(gameObject);
        }
    }
}
