﻿namespace HappyGames.UI
{
    using System.Collections;
    using UnityEngine;

    public class PopUpAnimation : MonoBehaviour
    {
        // --- | Inspector | ---------------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The scaling animation when instantiating.")]
        protected AnimationCurve popupMotion = new AnimationCurve(
            new Keyframe(),
            new Keyframe(0.07f, 1.1f),
            new Keyframe(0.09f, 0.95f),
            new Keyframe(0.1f, 1f)
        );


        // --- | Methods | -----------------------------------------------------------------------------------------------------

        private void Start()
        {
            StartCoroutine(DoScaleUp());
        }

        protected IEnumerator DoScaleUp()
        {
            float time = 0f;
            float maxTime = popupMotion[popupMotion.length - 1].time;
            transform.localScale = Vector2.zero;
            while (time < maxTime)
            {
                yield return null;
                time += Time.deltaTime;
                transform.localScale = (Vector3)Vector2.one * popupMotion.Evaluate(time) + Vector3.forward;
            }
            transform.localScale = Vector2.one;

        }
    }
}
