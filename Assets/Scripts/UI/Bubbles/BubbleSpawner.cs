﻿namespace HappyGames.UI
{
    using HappyGames.TimeManagement;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;

    public class BubbleSpawner : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The prefab for the speach bubbles.")]
        protected BubbleController speachBubble = default;

        [SerializeField, Tooltip("The prefab for the speach respones.")]
        protected BubbleController selectBubble = default;

        [SerializeField, Tooltip("The timeline to manage the spawning.")]
        protected Timeline timeline = default;

        [SerializeField, Tooltip("The are to spawn the bubbles inside.")]
        protected BubbleArea area = default;

        [SerializeField, Tooltip("The question to display.")]
        protected string question = default;

        [SerializeField, Tooltip("The questions to display.")]
        protected BubbleQuestion[] questions = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            timeline ??= new Timeline();
        }

        // Message --------------------------------------------------------------------------------

        /// <summary>
        /// Displays a message.
        /// </summary>
        /// <param name="message">The message to display.</param>
        /// <param name="onCompleted">Called when the message was displayed.</param>
        public void DisplayMessage(string message, Action onCompleted) => area.AddBubble(speachBubble.Copy(area.transform, message, onCompleted));
        /// <summary>
        /// Displays a message.
        /// </summary>
        /// <param name="message">The message to display.</param>
        public void DisplayMessage(string message) => DisplayMessage(message, null);

        /// <summary>
        /// Enables the quesions.
        /// </summary>
        [ContextMenu("Debug/Quesions")]
        public void EnableQuestions()
        {
            // Add the initial question.
            area.AddBubble(speachBubble.Copy(area.transform, question, SpawnQuesions));

            // Spawns the response quesions.
            void SpawnQuesions()
            {
                // Holds all spawned qustions.
                (BubbleController controller, Button button)[] bubbles = new (BubbleController, Button)[questions.Length];
                // Adds the events to the buttons.
                void LinkBubbles()
                {
                    for (int i = 0; i < questions.Length; i++)
                    {
                        for (int j = 0; j < questions.Length; j++)
                        {
                            if (j != i)
                            {
                                AddSelectionListeners(bubbles[i].button, bubbles[j].controller);
                            }
                        }
                        bubbles[i].button.onClick.AddListener(questions[i].OnSelect);
                        RemoveButtonOnClick(bubbles[i].button);
                    }
                };
                // Adds the selection listener to the button.
                void AddSelectionListeners(Button target, BubbleController other)
                {
                    target.onClick.AddListener(() => { other?.Pop(); });
                }
                // Removes the button component when clicked.
                void RemoveButtonOnClick(Button target)
                {
                    target.onClick.AddListener(() => Destroy(target));
                }

                List<BubbleController> bubbleQuestions = new List<BubbleController>();
                // Spawn the questions.
                for (int i = 0; i < questions.Length; i++)
                {
                    BubbleController bubble = selectBubble.Copy(area.transform, questions[i].Message, null);
                    bubbles[i] = (bubble, bubble.GetComponent<Button>());
                    bubbleQuestions.Add(bubble);
                }
                LinkBubbles();
                area.AddBubbles(bubbleQuestions);
            }
        }

#if UNITY_EDITOR

        /// <summary>
        /// Spawns a bubble with a debug message.
        /// </summary>
        [ContextMenu("Debug/Message")]
        private void TestMessage() => DisplayMessage("This is a test message.");

#endif

        // --- | Classes | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// The class holding the data to display the bubble questions.
        /// </summary>
        [Serializable]
        protected class BubbleQuestion
        {
            [SerializeField, Tooltip("The question to display.")]
            protected string message = default;
            /// <summary>
            /// The question to display.
            /// </summary>
            public string Message => message;

            [Space, SerializeField, Tooltip("The action to perform on selection.")]
            protected UnityEvent onSelect = default;
            /// <summary>
            /// The action to perform on selection.
            /// </summary>
            public UnityAction OnSelect => onSelect.Invoke;
        }
    }
}
