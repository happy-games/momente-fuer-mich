﻿namespace HappyGames.UI
{
    using HappyGames.Utilities;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [RequireComponent(typeof(Canvas))]
    public class BubbleArea : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The origin of the circle.")]
        protected Vector2 origin = Vector2.zero;

        [SerializeField, Tooltip("The offset of the bubbles to the spawn.")]
        protected Vector2 spawnOffset = Vector2.zero;

        [SerializeField, Tooltip("The radius of the circle.")]
        protected float radius = 200;

        [SerializeField, Tooltip("The size of the bubbles.")]
        protected float bubbleSize = 50f;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The center of the circle.
        /// </summary>
        protected Vector2 Center => (Vector2)transform.position + origin;

        protected Vector2[] bubbleSpawns;

        /// <summary>
        /// The amount of spawnpoints.
        /// </summary>
        protected const int SPAWN_COUNT = 4;
        /// <summary>
        /// The default spawnpoint.
        /// </summary>
        protected const int DEFAULT_SPAWN = 1;

        protected BubbleController[] bubbles = new BubbleController[SPAWN_COUNT];


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            bubbleSpawns = CalculateSpawns(SPAWN_COUNT, true);
        }

#if UNITY_EDITOR

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = new Color(0f, 0f, 1f);
            Gizmos.DrawWireSphere(Center, radius);
            foreach (Vector2 spawn in bubbleSpawns == null ? CalculateSpawns(SPAWN_COUNT, true) : bubbleSpawns)
            {
                Gizmos.DrawRay(spawn + spawnOffset, Vector2.left * 100f);
                Gizmos.color += new Color(1f / (SPAWN_COUNT + 1), 1f / (SPAWN_COUNT + 1), 0f);
            }
        }

#endif

        // Message --------------------------------------------------------------------------------

        /// <summary>
        /// Displays a spoken message in the bubble area.
        /// </summary>
        /// <param name="bubble">The bubble to add.</param>
        /// <param name="spawnID">The index of the spawn.</param>
        protected void AddBubble(BubbleController bubble, int spawnID)
        {
            bubble.SetPosition(bubbleSpawns[spawnID] + spawnOffset);
            while (bubbles[spawnID] != null && !bubbles[spawnID].IsPopped)
            {
                MoveUp(spawnID);
                MoveTogether(spawnID);
            }
            bubbles[spawnID] = bubble;
            bubble.OnPoped += () => MoveTogether(spawnID);
        }
        /// <summary>
        /// Displays a spoken message in the bubble area.
        /// </summary>
        /// <param name="bubble">The bubble to add.</param>
        public void AddBubble(BubbleController bubble) => AddBubble(bubble, DEFAULT_SPAWN);

        /// <summary>
        /// Displays a spoken message in the bubble area.
        /// </summary>
        /// <param name="bubbles">The bubbles to add.</param>
        public void AddBubbles(IEnumerable<BubbleController> bubbles)
        {
            int i = 0;
            foreach (BubbleController bubble in bubbles)
            {
                AddBubble(bubble, DEFAULT_SPAWN + i);
                i++;
            }
        }

        // Position -------------------------------------------------------------------------------

        /// <summary>
        /// Moves a bubble one position up.
        /// </summary>
        /// <param name="index">The idnex of the bubble.</param>
        protected void MoveUp(int index)
        {
            if (index == 0)
            {
                bubbles[index].Pop();
                bubbles[index] = null;
            }
            else
            {
                if (bubbles[index - 1] != null && !bubbles[index - 1].IsPopped)
                {
                    MoveUp(index - 1);
                }
                bubbles[index - 1] = bubbles[index];
                bubbles[index - 1]?.SetPosition(bubbleSpawns[index - 1]);
                bubbles[index] = null;
            }
        }

        /// <summary>
        /// Clears spaces between bubbles.
        /// </summary>
        /// <param name="startIndex">The index to start looking.</param>
        protected void MoveTogether(int startIndex = 0)
        {
            int foundSpace = -1;
            for (int i = startIndex + 1; i < SPAWN_COUNT; i++)
            {
                if (bubbles[i - 1] == null || bubbles[i - 1].IsPopped)
                {
                    MoveUp(i);
                    if (foundSpace < 0)
                    {
                        foundSpace = i;
                    }
                }
            }
            if (foundSpace >= 0)
            {
                MoveTogether(foundSpace);
            }
        }

        // Calculations ---------------------------------------------------------------------------

        /// <summary>
        /// Calculates the spawn points.
        /// </summary>
        /// <param name="count">The amount of spawnpoints on the circle.</param>
        /// <param name="leftSide">True if the points are to the left of the circle.</param>
        /// <returns>The spawn points.</returns>
        protected Vector2[] CalculateSpawns(int count, bool leftSide)
        {
            Vector2[] spawns = new Vector2[count];

            int min = count - Mathf.CeilToInt(count * 0.5f);
            for (int i = 0; i < count; i++)
            {
                spawns[i] = (new List<Vector2>(IntersectionCalculation.Intersection(
                new IntersectionCalculation.Circle(Center, radius),
                new IntersectionCalculation.Line
                (
                    Center + Vector2.right * (radius + 5f) + Vector2.up * bubbleSize * (min - i),
                    Center + Vector2.left * (radius + 5f) + Vector2.up * bubbleSize * (min - i)
                )
                ))[leftSide ? 0 : 1]);
            }
            return spawns;
        }
    }
}
