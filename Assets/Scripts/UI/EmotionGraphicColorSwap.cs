namespace HappyGames.UI
{
    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    /// Provides a method for changes a <see cref="Graphic"/>s color depending on the emotion.
    /// </summary>
    public class EmotionGraphicColorSwap : EmotionValueSwap<Color>
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The images whos color to change.")]
        protected Graphic target = null;
        /// <inheritdoc/>
        protected override Color Target 
        {
            get => target.color; 
            set => target.color = value; 
        }
    }
}