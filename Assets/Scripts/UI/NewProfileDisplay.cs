namespace HappyGames.UI.Start
{
    using UnityEngine;
    using TMPro;
    using UnityEngine.Events;
    using HappyGames.Data;

    public sealed class NewProfileDisplay : MonoBehaviour
    {
        // --- | Inspactor | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The field to enter a name.")]
        private TMP_InputField inputField = null;

        [Space, SerializeField, Tooltip("Called when a new profile was added.")]
        private UnityEvent<UserProfile> onProfileAdded = default;

        [SerializeField, Tooltip("Called if a profile with the given name was already created.")]
        private UnityEvent onNameUsed = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Adds the profile.
        /// </summary>
        public void AddProfile()
        {
            string name = inputField.text.Trim();
            if (!ProfileManager.CheckForProfile(name))
            {
                UserProfile profile = ProfileManager.CreateProfile(name);
                onProfileAdded?.Invoke(profile);
                inputField.text = string.Empty;
            }
            else
            {
                onNameUsed?.Invoke();
            }
        }
    }
}
