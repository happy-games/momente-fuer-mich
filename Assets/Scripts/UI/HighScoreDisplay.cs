namespace HappyGames.UI
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using TMPro;

    public class HighScoreDisplay : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The script to display the score.")]
        protected NumberDisplay currentScore;

        [SerializeField, Tooltip("The field to display the highscore.")]
        protected TMP_Text highScore;

        [SerializeField, Tooltip("The container of the old highscore.")]
        protected GameObject highScoreContainer;

        [SerializeField, Tooltip("The field to show on a new highscore.")]
        protected GameObject newHighScoreMessage;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Displays the scores.
        /// </summary>
        /// <param name="score">The score reached.</param>
        /// <param name="highScore">The current highscore.</param>
        public void DisplayScores(int score, int highScore)
        {
            currentScore.UpdateValue(score, (c) =>
            {
                if (c > highScore)
                {
                    DisplayNewRectodMessage();
                }
            });
            this.highScore.text = highScore.ToString();
        }

        /// <summary>
        /// Enables the new record message.
        /// </summary>
        private void DisplayNewRectodMessage()
        {
            if (highScoreContainer.activeSelf)
            {
                highScoreContainer.SetActive(false);
                newHighScoreMessage.SetActive(true);
            }
        }
    }
}
