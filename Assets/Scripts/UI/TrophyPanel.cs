namespace HappyGames.UI
{
    using HappyGames.Data;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public class TrophyPanel : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [Header("Prefabs")] // --------------------------------------------------------------------
        [SerializeField, Tooltip("The prefab to display flower trophies.")]
        protected TrophyButton<FlowerTrophy> flowerPrefab = null;

        [SerializeField, Tooltip("The prefab to display fruit trophies.")]
        protected TrophyButton<FruitBasketTrophy> fruitPrefab = null;

        [SerializeField, Tooltip("The prefab to display baloon trophies.")]
        protected TrophyButton<BaloonTrophy> baloonPrefab = null;

        [Header("Fitler")] // ---------------------------------------------------------------------
        [SerializeField, Tooltip("The script fitlering the displayed trophies.")]
        protected TrophyFilter filter = null;

        [Header("Layout")] // ---------------------------------------------------------------------
        [SerializeField, Tooltip("The container for the displays.")]
        protected RectTransform container = null;

        [SerializeField, Min(1), LockInPlaymode, Tooltip("The column count of the displays.")]
        protected int columns = 8;

        [SerializeField, LockInPlaymode, Tooltip("The spacing between the displays.")]
        protected Vector2 spacing = Vector2.one * 10f;

        [Header("Events")] // ---------------------------------------------------------------------
        [SerializeField, Tooltip("Called when a trophy was clickled.")]
        protected UnityEvent<Trophy> onTrophyClick = default;
        /// <summary>
        /// Called when a trophy was clickled.
        /// </summary>
        public event UnityAction<Trophy> OnTrophyClick
        {
            add => onTrophyClick.AddListener(value);
            remove => onTrophyClick.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called when a trophy was added.")]
        protected UnityEvent<Trophy> onTrophyAdded = default;
        /// <summary>
        /// Called when a trophy was added.
        /// </summary>
        public event UnityAction<Trophy> OnTrophyAdded
        {
            add => onTrophyAdded.AddListener(value);
            remove => onTrophyAdded.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called when a trophy was removed.")]
        protected UnityEvent<Trophy> onTrophyRemoved = default;
        /// <summary>
        /// Called when a trophy was removed.
        /// </summary>
        public event UnityAction<Trophy> OnTrophyRemoved
        {
            add => onTrophyRemoved.AddListener(value);
            remove => onTrophyRemoved.RemoveListener(value);
        }


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected List<(Trophy trophy, RectTransform display)> displays = new List<(Trophy, RectTransform)>();
        protected HashSet<Trophy> trophies = new HashSet<Trophy>();
        protected List<Trophy> trophiesToDisplay = new List<Trophy>();
        protected float displaySize;
        protected Vector2 displayOffset;
        private bool isSetUp = false;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Start()
        {
            displaySize = (container.rect.width - spacing.x * (columns - 1)) / columns;
            displayOffset = container.lossyScale.x * (Vector2.one * displaySize + spacing);
            isSetUp = true;
            if (filter)
            {
                filter.OnFilterUpdate += delegate { SortDisplay(0, 0, true); };
            }
            AddListedTrophies();
        }

        private void OnEnable()
        {
            AddListedTrophies();
        }

        /// <summary>
        /// Adds all trophies that were registred before the setup.
        /// </summary>
        private void AddListedTrophies()
        {
            if (isSetUp)
            {
                while (trophiesToDisplay.Count > 0)
                {
                    AddTrophy(trophiesToDisplay[0], false);
                    trophiesToDisplay.RemoveAt(0);
                }
                SortDisplay(0, 0);
            }
        }

        /// <summary>
        /// Displays the <see cref="T"/>s.
        /// </summary>
        /// <param name="trophies">The trophies to display.</param>
        public void SetContent(IEnumerable<Trophy> trophies)
        {
            HashSet<Trophy> previousContent = new HashSet<Trophy>(this.trophies);
            foreach (Trophy trophy in trophies)
            {
                if (!previousContent.Contains(trophy))
                {
                    AddTrophy(trophy, false);
                }
                else
                {
                    previousContent.Remove(trophy);
                }
            }
            foreach (Trophy trophy in previousContent)
            {
                RemoveTropohy(trophy);
            }
            SortDisplay(0, 0, false);
        }

        /// <summary>
        /// Adds a <see cref="T"/> to the display.
        /// </summary>
        /// <param name="trophy">The trophy to add.</param>
        /// <param name="sort">True if the content should be sorted.</param>
        protected void AddTrophy(Trophy trophy, bool sort)
        {
            if (isSetUp)
            {
                // Get the index for the new trophy.
                (int index, int count) position = GetDisplayIndex(trophy);
                // Create a new display.
                switch (trophy.TrophyType)
                {
                    case TrophyType.Flower: AddButton(flowerPrefab, (FlowerTrophy)trophy, position.count); break;
                    case TrophyType.FruitBasket: AddButton(fruitPrefab, (FruitBasketTrophy)trophy, position.count); break;
                    case TrophyType.Baloon: AddButton(baloonPrefab, (BaloonTrophy)trophy, position.count); break;
                }
                // Move all resources after the new one.
                if (sort)
                {
                    SortDisplay(position.index, position.count);
                }
                onTrophyAdded?.Invoke(trophy);
            }
            else
            {
                trophiesToDisplay.Add(trophy);
            }
        }
        /// <summary>
        /// Adds a <see cref="T"/> to the display.
        /// </summary>
        /// <param name="trophy">The trophy to add.</param>
        public void AddTrophy(Trophy trophy) => AddTrophy(trophy, true);

        /// <summary>
        /// Adds a button to the panel.
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="Trophy"/> to add.</typeparam>
        /// <param name="prefab">The prefab to use to display the <see cref="Trophy"/></param>
        /// <param name="trophy">The <see cref="Trophy"/> to display.</param>
        /// <param name="position">The position in the panel.</param>
        protected void AddButton<T>(TrophyButton<T> prefab, T trophy, int position) where T : Trophy
        {
            TrophyButton<T> display = prefab.Clone(container, GetDisplayPosition(position), displaySize, trophy);
            display.OnDisplayClick += onTrophyClick.Invoke;
            displays.Insert(position, (trophy, (RectTransform)display.transform));
            trophies.Add(trophy);
            if (filter && !filter.CheckFitler(trophy))
            {
                display.gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Removes a <see cref="T"/> from the area.
        /// </summary>
        /// <param name="trophy">The trophy to remove.</param>
        public void RemoveTropohy(Trophy trophy)
        {
            if (trophiesToDisplay.Contains(trophy))
            {
                trophiesToDisplay.Remove(trophy);
            }
            else
            {
                (int index, int count) position = GetDisplayIndex(trophy);
                if (displays[position.index].trophy.Equals(trophy))
                {
                    Destroy(displays[position.index].display.gameObject);
                    displays.RemoveAt(position.index);
                    trophies.Remove(trophy);
                    SortDisplay(position.index, position.count);
                    onTrophyRemoved?.Invoke(trophy);
                }
            }
        }

        /// <summary>
        /// Sorts the displays.
        /// </summary>
        /// <param name="index">The index of the display to start sorting at.</param>
        /// <param name="count">The position in the area of the first display.</param>
        /// <param name="changeActiveState">
        /// True if the displays should be disabled or enabled depending on the filter.
        /// </param>
        protected void SortDisplay(int index, int count, bool changeActiveState = false)
        {
            for (; index < displays.Count; index++)
            {
                displays[index].display.transform.position = GetDisplayPosition(count);
                if (!filter || filter.CheckFitler(displays[index].trophy))
                {
                    count++;
                    if (changeActiveState)
                    {
                        displays[index].display.gameObject.SetActive(true);
                    }
                }
                else
                {
                    if (changeActiveState)
                    {
                        displays[index].display.gameObject.SetActive(false);
                    }
                }
            }
            int rows = Mathf.CeilToInt(count / columns);
            container.sizeDelta = new Vector2(container.sizeDelta.x,  Mathf.Max(displaySize * rows + spacing.y * (rows - 1), 0f));
        }

        /// <summary>
        /// Clones the area.
        /// </summary>
        /// <param name="parent">The parent to place the are in.</param>
        /// <param name="position">The position to set the area.</param>
        /// <param name="trophies">The trophies to display in the area.</param>
        /// <returns>The cloned area.</returns>
        public TrophyPanel Clone(RectTransform parent, Vector2 position, IEnumerable<Trophy> trophies)
        {
            TrophyPanel area = Instantiate(this, position, Quaternion.identity, parent);
            area.SetContent(trophies);
            return area;
        }

        /// <summary>
        /// Calculates the displys position from its index.
        /// </summary>
        /// <param name="count">The index whos position to calculate.</param>
        /// <returns>The position where to put the display with the index.</returns>
        protected Vector2 GetDisplayPosition(int count)
        {
            return (Vector2)container.position + new Vector2(count % columns, -count / columns) * displayOffset;
        }

        /// <summary>
        /// Returns the index of the display for the <see cref="Trophy"/>.
        /// </summary>
        /// <param name="trophy">The trophy whos index to find.</param>
        /// <returns>The index of the trophy.</returns>
        protected (int unfilteredIndex, int filteredIndex) GetDisplayIndex(Trophy trophy)
        {
            (int index, int count) position = (0, 0);
            while (position.index < displays.Count && CompareTrophies(trophy, displays[position.index].trophy) < 0)
            {
                if (!filter || filter.CheckFitler(trophy))
                {
                    position.count++;
                }
                position.index++;
            }
            return position;
        }

        /// <summary>
        /// Compares two <see cref="Trophy"/>s.
        /// </summary>
        /// <param name="x">The trophy to compare.</param>
        /// <param name="y">The trophy to compare to.</param>
        /// <returns>0 => equal, -1 => less, 1 => geather.</returns>
        protected int CompareTrophies(Trophy x, Trophy y) => x.Date.CompareTo(y.Date);

#if (UNITY_EDITOR)

        /// <summary>
        /// A debug methods that fills the area with displays of random resources.
        /// </summary>
        [ContextMenu("Debug/Fill with Randoms")]
        private void FillAreaWithRandomResources()
        {
            for (int i = 0; i < columns * 4; i++)
            {
                int r = Random.Range(1, 6);
                AddTrophy(new FlowerTrophy((Emotion.EmotionType)r, Random.Range(1, 1000)));
            }
        }

#endif


        public void DebugFillForVideo()
        {
            for (int i = 0; i < columns * 4; i++)
            {
                int r = Random.Range(1, 6);
                AddTrophy(new FlowerTrophy((Emotion.EmotionType)r, Random.Range(1, 1000)));
            }

        }
    }

}
