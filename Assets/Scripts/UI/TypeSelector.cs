namespace HappyGames.UI
{
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;

    public abstract class TypeSelector<T> : MonoBehaviour where T: System.Enum
    {
        [SerializeField, Tooltip("The button on the selctor.")]
        protected Toggle toggle = null;

        [SerializeField, Tooltip("The value represented by the selector.")]
        protected T value = default;
        /// <summary>
        /// The value represented by the selector.
        /// </summary>
        public T Value => value;

        [Space, SerializeField, Tooltip("Called when the selector is turned on.")]
        protected UnityEvent<T> onSelect = default;
        /// <summary>
        /// Called when the selector is turned on.
        /// </summary>
        public event UnityAction<T> OnSelect
        {
            add => onSelect.AddListener(value);
            remove => onSelect.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called when the selector is turned off.")]
        protected UnityEvent<T> onDeselect = default;
        /// <summary>
        /// Called when the selector is turned off.
        /// </summary>
        public event UnityAction<T> OnDeselect
        {
            add => onDeselect.AddListener(value);
            remove => onDeselect.RemoveListener(value);
        }

        [SerializeField, Tooltip("Called when the buttons value was set.")]
        protected UnityEvent<T> onValueSet = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            toggle.onValueChanged.AddListener(InvokeClick);
        }

        private void OnDisable()
        {
            toggle.onValueChanged.RemoveListener(InvokeClick);
        }

        protected virtual void Start()
        {
            onValueSet?.Invoke(value);
        }

        /// <summary>
        /// Clones the selector.
        /// </summary>
        /// <param name="parent">The transform to use as parent.</param>
        /// <param name="positon">The position to put the selector.</param>
        /// <param name="size">The size of the selector.</param>
        /// <param name="value">The value of the selector.</param>
        /// <returns>The cloned selector.</returns>
        public TypeSelector<T> Clone(Transform parent, Vector2 positon, Vector2 size, T value, ToggleGroup group)
        {
            TypeSelector<T> selector = Instantiate(this, positon, Quaternion.identity, parent);
            ((RectTransform)selector.transform).sizeDelta = size;
            selector.value = value;
            selector.name = $"{ name } ({ value })";
            selector.toggle.group = group;
            return selector;
        }

        /// <summary>
        /// Invokes the <see cref="onSelect"/> event with the <see cref="value"/>.
        /// </summary>
        private void InvokeClick(bool isOn)
        {
            if (isOn)
            {
                onSelect?.Invoke(value);
            }
            else
            {
                onDeselect?.Invoke(value);
            }
        }
    }
}
