namespace HappyGames.UI
{
    using HappyGames.Translations;
    using UnityEngine;
    using UnityEngine.UI;

    public class LanguageToggleActivator : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The toggles for the languages.")]
        private TranslatableCollection<Toggle> toggles = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Activates the toggle for the given language.
        /// </summary>
        /// <param name="language">The languege whos toggle to activate.</param>
        public void ActivateToggle(Language language) => toggles.GetValue(language).isOn = true;
    }
}
