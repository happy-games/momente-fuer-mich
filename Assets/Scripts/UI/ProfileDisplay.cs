namespace HappyGames.UI.Start
{
    using UnityEngine;
    using TMPro;
    using HappyGames.Data;
    using UnityEngine.UI;

    public class ProfileDisplay : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The textfield to display the profile name.")]
        protected TMP_Text nameField = null;

        [SerializeField, Tooltip("The button to select the profile.")]
        protected Button selectButton = default;
        /// <summary>
        /// The button to select the profile.
        /// </summary>
        public Button SelectButton => selectButton;

        [SerializeField, Tooltip("The button to delete the profile.")]
        protected Button deleteButton = default;
        /// <summary>
        /// The button to delete the profile.
        /// </summary>
        public Button DeleteButton => deleteButton;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Displays the profile.
        /// </summary>
        /// <param name="profile">The profile to display.</param>
        public void Display(UserProfile profile)
        {
            nameField.text = profile.Name;
        }
    }
}
