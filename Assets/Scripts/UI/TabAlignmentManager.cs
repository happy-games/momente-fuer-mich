namespace HappyGames.UI
{
    using UnityEngine;

    public class TabAlignmentManager : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The container.")]
        protected RectTransform container = null;

        [SerializeField, Tooltip("Aligns the areas horizontaly.")]
        protected bool alignHorizontaly = true;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Updates the sizes of the elements in the container.
        /// </summary>
        public void UpdateSize()
        {
            float singeSize = 1f / (container.childCount);
            for (int i = 0; i < container.childCount; i++)
            {
                RectTransform element = (RectTransform)container.GetChild(i);
                if (alignHorizontaly)
                {
                    element.anchorMin = new Vector2(singeSize * i, 0f);
                    element.anchorMax = new Vector2(singeSize * (i + 1), 1f);
                }
                else
                {
                    element.anchorMin = new Vector2(0f, singeSize * i);
                    element.anchorMax = new Vector2(1f, singeSize * (i + 1));
                }
                element.anchoredPosition = Vector2.zero;
                element.sizeDelta = Vector2.zero;
            }
        }
    }
}
