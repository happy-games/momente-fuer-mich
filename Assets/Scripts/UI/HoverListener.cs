namespace HappyGames.UI
{
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.EventSystems;

    public class HoverListener : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called when the object gets hovered.")]
        protected UnityEvent onHoverStart = default;

        [SerializeField, Tooltip("Called when the object is no longer hovered.")]
        protected UnityEvent onHoverEnd = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public void OnPointerEnter(PointerEventData eventData) => onHoverStart?.Invoke();

        /// <inheritdoc/>
        public void OnPointerExit(PointerEventData eventData) => onHoverEnd?.Invoke();
    }
}
