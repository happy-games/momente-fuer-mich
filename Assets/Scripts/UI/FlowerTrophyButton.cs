namespace HappyGames.UI
{
    using HappyGames.Data;
    using HappyGames.Emotion;
    using UnityEngine;
    using UnityEngine.Events;

    public class FlowerTrophyButton : TrophyButton<FlowerTrophy>
    {
        [SerializeField]
        protected UnityEvent<EmotionType> onFlowerEmotionSet = default;

        [SerializeField]
        protected UnityEvent<int> onFlowerScoreSet = default;


        public override void Display(FlowerTrophy trophy)
        {
            base.Display(trophy);
            onFlowerEmotionSet?.Invoke(trophy.Emotion);
            onFlowerScoreSet?.Invoke(trophy.Score);
        }
    } 
}