namespace HappyGames.Emotion
{
    using HappyGames.UI;
    using UnityEngine;

    /// <summary>
    /// Provides a method that switches sprites depending on the emotion.
    /// </summary>
    public class EmotionSpriteSwap : EmotionValueSwap<Sprite>
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The renderer whos sprite to change.")]
        protected SpriteRenderer target = null;
        /// <inheritdoc/>
        protected override Sprite Target
        {
            get => target.sprite;
            set => target.sprite = value;
        }
    }
}
