using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILayoutController : MonoBehaviour
{
    [SerializeField]
    private RectTransform leftSide = default;
    [SerializeField]
    private RectTransform rightSide = default;

    [SerializeField]
    private bool leftIsDynamic = default;

    [SerializeField, Min(0f)]
    private float padding = default;

    private RectTransform DynamcTransform => leftIsDynamic ? leftSide : rightSide;
    private RectTransform StaticTransform => leftIsDynamic ? rightSide : leftSide;


    public void SetWidth(float width)
    {
        DynamcTransform.sizeDelta = Vector2.left * (width + (width != 0 ? padding : 0f));
        StaticTransform.sizeDelta = new Vector2(width, StaticTransform.sizeDelta.y);
    }
}
