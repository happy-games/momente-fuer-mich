namespace HappyGames.UI
{
    using HappyGames.Data;
    using UnityEngine;
    using UnityEngine.Events;

    public class FruitTrophyButton : TrophyButton<FruitBasketTrophy>
    {
        [SerializeField]
        protected UnityEvent<RankType> onRankSet = default;

        [SerializeField]
        protected UnityEvent<int> onScoreSet = default;


        public override void Display(FruitBasketTrophy trophy)
        {
            base.Display(trophy);
            onRankSet?.Invoke(trophy.Rank);
            onScoreSet?.Invoke(trophy.Score);
        }
    }
}