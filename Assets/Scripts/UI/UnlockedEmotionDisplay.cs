namespace HappyGames.UI
{
    using HappyGames.Data;
    using HappyGames.Emotion;
    using System.Collections.Generic;
    using UnityEngine;

    public class UnlockedEmotionDisplay : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The prefab to display the emotion.")]
        protected EmotionImageSwap prefab = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The container to hold the displays.
        /// </summary>
        protected RectTransform Container => (RectTransform)transform;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            if (ProfileManager.HasActiveProfile)
            {
                SwitchProfiles(null, ProfileManager.GetActiveProfile());
            }
        }

        private void OnEnable()
        {
            ProfileManager.OnProfileActivated += SwitchProfiles;
        }

        private void OnDisable()
        {
            ProfileManager.OnProfileActivated -= SwitchProfiles;
            if (ProfileManager.HasActiveProfile)
            {
                ProfileManager.GetActiveProfile().OnEmotionUnlocked -= Display;
            }
        }

        /// <summary>
        /// Switches the emotions when the <see cref="UserProfile"/> changes.
        /// </summary>
        /// <param name="oldProfile">The old profile.</param>
        /// <param name="newProfile">The new profile.</param>
        private void SwitchProfiles(UserProfile oldProfile, UserProfile newProfile)
        {
            if (oldProfile != null)
            {
                oldProfile.OnEmotionUnlocked -= Display;
            }
            if (newProfile != null)
            {
                Display(newProfile.UnlockedEmotions);
                newProfile.OnEmotionUnlocked += Display;
            }
        }

        /// <summary>
        /// Displays the emotion.
        /// </summary>
        /// <param name="emotion">The emotion to display.</param>
        protected void Display(EmotionType emotion)
        {
            EmotionImageSwap display = Instantiate(prefab, Container);
            display.Display(emotion);
            display.transform.localPosition = Vector2.right * Container.rect.width;
            Container.sizeDelta += Vector2.right * ((RectTransform)display.transform).rect.width;
#if (!UNITY_WEBGL)
            Transform keyDisplay = display.transform.GetChild(display.transform.childCount - 1);
            keyDisplay.gameObject.SetActive(true);
            keyDisplay.GetChild(keyDisplay.childCount - 1).GetComponent<TMPro.TMP_Text>().text = ((int)emotion).ToString();
#endif
        }
        /// <summary>
        /// Displays the emotions.
        /// </summary>
        /// <param name="emotions">The emotions to display.</param>
        protected void Display(IEnumerable<EmotionType> emotions)
        {
            foreach (EmotionType emotion in emotions)
            {
                Display(emotion);
            }
        }
    }
}
