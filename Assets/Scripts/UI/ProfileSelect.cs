namespace HappyGames.UI.Start
{
    using HappyGames.Data;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public class ProfileSelect : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The container object holding the profiles.")]
        protected RectTransform container = null;

        [SerializeField, Tooltip("The prefab for the profile display.")]
        protected ProfileDisplay profilePrefab = null;

        [SerializeField, Tooltip("The space bwtween the profiles.")]
        protected float spacing = 10f;

        [Space, SerializeField, Tooltip("Called when a profile gets selected.")]
        protected UnityEvent<UserProfile> onProfileSelected = default;

        [SerializeField, Tooltip("Called when a profile was deleted.")]
        protected UnityEvent<UserProfile> onProfileDeleted = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private List<(UserProfile profile, ProfileDisplay display)> displays = new List<(UserProfile, ProfileDisplay)>();


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            ProfileManager.OnProfileAdded += BuildProfileNoReturn;
            ProfileManager.OnProfileRemoved += RemoveProfile;
        }

        private void OnDisable()
        {
            ProfileManager.OnProfileAdded -= BuildProfileNoReturn;
            ProfileManager.OnProfileRemoved -= RemoveProfile;
        }


        private void Start()
        {
            foreach (UserProfile profile in ProfileManager.GetProfiles())
            {
                BuildProfile(profile);
            }
        }

        /// <summary>
        /// Adds a new profile display to the container.
        /// </summary>
        /// <param name="profile">The profile to display.</param>
        /// <returns>The new display in the container.</returns>
        protected ProfileDisplay BuildProfile(UserProfile profile)
        {
            ProfileDisplay display = Instantiate(profilePrefab, container.position + Vector3.down * (container.sizeDelta.y * container.lossyScale.y + spacing), Quaternion.identity, container);
            container.sizeDelta += Vector2.up * (((RectTransform)display.transform).sizeDelta.y + spacing);
            display.Display(profile);
            display.SelectButton?.onClick.AddListener(() => SelectProfile(profile));
            display.DeleteButton?.onClick.AddListener(() => onProfileDeleted.Invoke(profile));
            displays.Add((profile, display));
            return display;
        }

        /// <summary>
        /// Calls <see cref="BuildProfile(UserProfile)"/> without a return value.
        /// </summary>
        /// <param name="profile">The profile to display.</param>
        private void BuildProfileNoReturn(UserProfile profile) => BuildProfile(profile);

        /// <summary>
        /// Removes a profile from the list.
        /// </summary>
        /// <param name="profile">The profile to remove.</param>
        protected void RemoveProfile(UserProfile profile)
        {
            int index = 0;
            float offset = 0f;
            bool wasFound = false;
            for (int i = 0; i < displays.Count; i++)
            {
                if (wasFound)
                {
                    displays[i].display.transform.position += Vector3.up * offset;
                }
                else if (displays[i].profile.Equals(profile))
                {
                    index = i;
                    offset = ((RectTransform)displays[i].display.transform).rect.height + spacing;
                    Destroy(displays[i].display.gameObject);
                    wasFound = true;
                }
            }
            if (wasFound)
            {
                container.sizeDelta += Vector2.down * offset;
                displays.RemoveAt(index);
            }
        }

        /// <summary>
        /// Selects a profile.
        /// </summary>
        /// <param name="profile">The profile to select.</param>
        public void SelectProfile(UserProfile profile) => onProfileSelected?.Invoke(profile);

        /// <summary>
        /// Sets a profile as active.
        /// </summary>
        /// <param name="profile">The profile to use.</param>
        public void SetProfileAsActive(UserProfile profile) => ProfileManager.SetActiveProfile(profile);

        /// <summary>
        /// Deletes the profile.
        /// </summary>
        /// <param name="profile">The deleted profile.</param>
        public void DeleteProfile(UserProfile profile) => ProfileManager.DeleteProfile(profile);
    }
}
