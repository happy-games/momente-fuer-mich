namespace HappyGames.Utilities
{
    using UnityEngine;
    using UnityEngine.Events;

    public class RangeModder : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called with the modded (%) value.")]
        protected UnityEvent<float> onValueModded = default;

        [SerializeField, Tooltip("Called with the ceiled value.")]
        protected UnityEvent<int> onValueCeiled = default;


        // --- | Varaiables | ----------------------------------------------------------------------------------------------

        protected int ceil = int.MaxValue;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Updates the progress of the bar.
        /// </summary>
        /// <param name="current">The current value.</param>
        /// <param name="max">The max value.</param>
        public void ModValue(float current, float max)
        {
            onValueModded?.Invoke(current % 1f);
            int value = Mathf.CeilToInt(current);
            if (ceil != value)
            {
                ceil = value;
                onValueCeiled?.Invoke(ceil);
            }
        }
    }
}
