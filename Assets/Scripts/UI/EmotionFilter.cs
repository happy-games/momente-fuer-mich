namespace HappyGames.UI
{
    using HappyGames.Data;
    using HappyGames.Emotion;

    public class EmotionFilter : TrophyFilter
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected EmotionType filteredEmotion = default;

        // --- | Methods | -------------------------------------------------------------------------------------------------

        /*void Awake()
        {
            useFilter = false;
        }*/

        /// <summary>
        /// Sets the emotion to filter.
        /// </summary>
        /// <param name="emotion">The filtered emotion.</param>
        public void SetFilter(EmotionType emotion)
        {
            useFilter = true;
            filteredEmotion = emotion;
            onFilterUpdate?.Invoke();
        }

        /// <inheritdoc/>
        public override bool CheckFitler(Trophy trophy) => (!useFilter || trophy is FlowerTrophy && ((FlowerTrophy)trophy).Emotion == filteredEmotion);
    }
}
