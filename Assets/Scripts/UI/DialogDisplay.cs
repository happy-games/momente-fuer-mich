namespace HappyGames.UI
{
    using HappyGames.Dialog;
    using HappyGames.Emotion;
    using UnityEngine;
    using UnityEngine.Events;

    public class DialogDisplay : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called when the lines content is updated.")]
        protected UnityEvent<string> onLineContentUpdate = default;

        [SerializeField, Tooltip("Called when the characters name is updated.")]
        protected UnityEvent<string> onCharacterNameUpdate = default;

        [SerializeField, Tooltip("Called when the characters emotion updates.")]
        protected UnityEvent<EmotionType> onEmotionUpdate = default;

        [SerializeField, Tooltip("Called when the sprite set updates.")]
        protected UnityEvent<EmotionSpriteSet> onSpriteSetUpdate = default;

        [Space, SerializeField, Tooltip("Called when the display is enabled.")]
        protected UnityEvent onShow = default;

        [SerializeField, Tooltip("Called when the display is disabled.")]
        protected UnityEvent onHide = default;

        [Space, SerializeField, Tooltip("Called when the line is stated.")]
        protected UnityEvent onLineStart = default;

        [SerializeField, Tooltip("Called when the line is finished.")]
        protected UnityEvent onLineEnd = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Updates the lines content.
        /// </summary>
        /// <param name="content">The content to display.</param>
        public void UpdateLine(string content) => onLineContentUpdate?.Invoke(content);

        /// <summary>
        /// Updates the displayed emotion.
        /// </summary>
        /// <param name="emotion">The emotion to display.</param>
        public void UpdateEmotion(EmotionType emotion) => onEmotionUpdate?.Invoke(emotion);

        /// <summary>
        /// Updates the displayed character.
        /// </summary>
        /// <param name="character">The character to display.</param>
        public void UpdateCharacter(Character character)
        {
            onSpriteSetUpdate?.Invoke(character?.Sprites);
            onCharacterNameUpdate?.Invoke(character?.DisplayName);
        }

        /// <summary>
        /// Displays that the line has started.
        /// </summary>
        public void StartLine() => onLineStart?.Invoke();

        /// <summary>
        /// Displays that the line ins finished.
        /// </summary>
        public void FinishLine() => onLineEnd?.Invoke();

        /// <summary>
        /// Shows the display.
        /// </summary>
        public void Show() => onShow?.Invoke();

        /// <summary>
        /// Hides the display.
        /// </summary>
        public void Hide() => onHide?.Invoke();
    }
}
