namespace HappyGames.UI
{
    using HappyGames.Emotion;
    using UnityEngine;

    public abstract class EmotionValueSwap<T> : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The values to use to display the emotion.")]
        protected EmotionSet<T> values = null;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The value to update.
        /// </summary>
        protected abstract T Target { get; set; }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            Display(default);
        }

        /// <summary>
        /// Sets the value for the emotion.
        /// </summary>
        /// <param name="emotion">The emotion to display.</param>
        public void Display(EmotionType emotion) => Target = values ? values.GetValue(emotion) : default;

        /// <summary>
        /// Updates the values to display.
        /// </summary>
        /// <param name="values">The values to display.</param>
        public void SetValues(EmotionSet<T> values)
        {
            EmotionType emotion = default;
            if (this.values)
            {
                foreach (var emotionAndValue in this.values)
                {
                    if (Target.Equals(emotionAndValue.Value))
                    {
                        emotion = emotionAndValue.Key;
                        break;
                    }
                }
            }
            this.values = values;
            Display(emotion);
        }
    }
}
