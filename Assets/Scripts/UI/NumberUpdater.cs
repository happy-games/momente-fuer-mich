namespace HappyGames.UI
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Events;
    using TMPro;
    using HappyGames.TimeManagement;
    using System;
    using System.Collections.Generic;

    public class NumberUpdater : NumberDisplay
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------
        
        [SerializeField, Tooltip("The textfield to display the number.")]
        protected TMP_Text display = null;

        [SerializeField, Tooltip("The timline to use for updating the value.")]
        protected Timeline timeline = null;

        [SerializeField, Min(0f), Tooltip("The time it takes to update the value.")]
        protected float updateDuration = 2f;

        [SerializeField, Min(1), Tooltip("The amount of values to add per update.")]
        protected int steps = 1;

        [SerializeField, Tooltip("The current score.")]
        protected int current = 0;
        /// <summary>
        /// The current score.
        /// </summary>
        public int Current => current;

        [Space, SerializeField, Tooltip("Called when the values change.")]
        protected UnityEvent<int, int> onValuesChanged = default;
        /// <summary>
        /// Called when the values change.
        /// </summary>
        public event UnityAction<int, int> OnValuesChanged
        {
            add => onValuesChanged.AddListener(value);
            remove => onValuesChanged.RemoveListener(value);
        }


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected int target = 0;
        /// <summary>
        /// The score to reach.
        /// </summary>
        public int Target => target;

        private Dictionary<int, Action<int>> updateEvents = new Dictionary<int, Action<int>>();


        // --- | Methods | -------------------------------------------------------------------------------------------------

        protected virtual void Awake()
        {
            target = current;
        }

        /// <inheritdoc/>
        public override void SetValue(int value)
        {
            current = target = value;
            display.text = current.ToString();
            onValuesChanged?.Invoke(current, target);
        }
        /// <inheritdoc/>
        public override void UpdateValue(int value, Action<int> onUpdate)
        {
            bool isUpdating = current != target;
            target = value;
            onValuesChanged?.Invoke(current, target);
            if (!updateEvents.ContainsKey(value))
            {
                updateEvents.Add(value, onUpdate);
            }
            else
            {
                updateEvents[value] += onUpdate;
            }
            if (!isUpdating)
            {
                StartCoroutine(DoUpdate());
            }
        }

        /// <summary>
        /// Updates the value over time.
        /// </summary>
        protected IEnumerator DoUpdate()
        {
            if (!timeline)
            {
                timeline = new Timeline();
            }
            float time = 0f;
            int prev = current;
            Vector2 position = display.transform.localPosition;
            if (updateDuration <= 0f)
            {
                current = target;
                display.text = current.ToString();
                InvokeUpdateEvents(prev, current);
            }
            else
            {
                int start = current;
                while (time < updateDuration)
                {
                    yield return null;
                    time += timeline.DeltaTime;
                    float add = Mathf.Lerp(start, target, time / updateDuration) - current;
                    while (Mathf.Abs(add) >= steps)
                    {
                        if (add > 0f)
                        {
                            current += steps;
                            add -= steps;
                        }
                        else
                        {
                            current -= steps;
                            add += steps;
                        }
                    }
                    if (prev != current)
                    {
                        display.text = current.ToString();
                        InvokeUpdateEvents(prev, current);
                        prev = current;
                    }
                }
                current = target;
                display.text = current.ToString();
                InvokeUpdateEvents(prev, current);
            }
        }

        /// <summary>
        /// Invokes the update events.
        /// </summary>
        /// <param name="prev">The value previously.</param>
        /// <param name="current">The current value.</param>
        private void InvokeUpdateEvents(int prev, int current)
        {
            List<int> removedEventKeys = new List<int>();
            foreach (var update in updateEvents)
            {
                update.Value?.Invoke(current);
                if (update.Key > prev && update.Key < current)
                {
                    removedEventKeys.Add(update.Key);
                }
            }
            foreach (int key in removedEventKeys)
            {
                updateEvents.Remove(key);
            }
            onValuesChanged?.Invoke(current, target);
        }
    }
}
