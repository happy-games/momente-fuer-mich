namespace HappyGames.UI
{
    using HappyGames.Data;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;

    public class RulesPopup : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The rules to display.")]
        protected RuleSet rules = default;

        [SerializeField, Tooltip("The rule panels.")]
        protected RulePanel prefab = default;

        [SerializeField, Tooltip("The selector for the panels.")]
        protected Button selector = default;

        [SerializeField, Tooltip("The canvas of the popup.")]
        protected Canvas canvas = default;

        [SerializeField, Tooltip("The container to hold the panels.")]
        protected RectTransform container = default;

        [Space, SerializeField, Tooltip("Called when the popup was opened.")]
        protected UnityEvent onOpen = default;

        [SerializeField, Tooltip("Called when the popup was closed.")]
        protected UnityEvent onClose = default;

        [Space, SerializeField, Tooltip("Called when a panel was opened.")]
        protected UnityEvent<int> onPanelOpened = default;

        [SerializeField, Tooltip("Called when a panel was closed.")]
        protected UnityEvent<int> onPanelClosed = default;

        [Space, SerializeField, Tooltip("Called when the first panel was opened.")]
        protected UnityEvent onFirstPanelOpened = default;

        [SerializeField, Tooltip("Called when the first panel was closed.")]
        protected UnityEvent onFirstPanelClosed = default;

        [Space, SerializeField, Tooltip("Called when the last panel was opened.")]
        protected UnityEvent onLastPanelOpened = default;

        [SerializeField, Tooltip("Called when the last panel was closed.")]
        protected UnityEvent onLastPanelClosed = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected int current = 0;
        protected RectTransform selectorParent;
        protected RulePanel[] panels;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            selectorParent = (RectTransform)selector.transform.parent;
            panels = new RulePanel[rules.Count];
            float spacing = selectorParent.rect.width / (rules.Count * 2f);
            int i = 0;
            foreach (GameRule rule in rules)
            {
                if (i == rules.Count - 1)
                {
                    SetupRule(rule, selector, i, spacing);
                }
                else
                {
                    SetupRule(rule, Instantiate(selector, selectorParent), i, spacing);
                }
                i++;
            }
            canvas.enabled = false;
        }

        /// <summary>
        /// Selects the panel at the given index.
        /// </summary>
        /// <param name="index">The index of the panel to select.</param>
        private void Select(int index)
        {
            index = Mathf.Clamp(index, 0, rules.Count - 1);

            panels[current].gameObject.SetActive(false);
            InvokeEvents(false);

            current = index;

            panels[current].gameObject.SetActive(true);
            InvokeEvents(true);
        }

        /// <summary>
        /// Selects the next panel.
        /// </summary>
        [ContextMenu("Control/Next")]
        public void Next() => Select(current + 1);

        /// <summary>
        /// Selects the previous panel.
        /// </summary>
        [ContextMenu("Control/Previous")]
        public void Previous() => Select(current - 1);

        /// <summary>
        /// Sets up the selector.
        /// </summary>
        /// <param name="rule">The rule to display.</param>
        /// <param name="selector">The selector to set up.</param>
        /// <param name="index">The index of the selector.</param>
        /// <param name="spacing">The space between the selectors.</param>
        private void SetupRule(GameRule rule, Button selector, int index, float spacing)
        {
            panels[index] = prefab.Clone(rule, container);
            panels[index].RectTransform.anchorMin = Vector2.zero;
            panels[index].RectTransform.anchorMax = Vector2.one;
            panels[index].RectTransform.sizeDelta = Vector2.zero;

            selector.transform.localPosition = Vector3.right * (-selectorParent.rect.width * 0.5f + spacing * (2f * index + 1f));
            selector.onClick.AddListener(() => Select(index));
            panels[index].gameObject.SetActive(index == current);
        }

        /// <summary>
        /// Opens the popup.
        /// </summary>
        [ContextMenu("Control/Open")]
        public void Open()
        {
            if (!canvas.enabled)
            {
                current = 0;
                panels[current].gameObject.SetActive(true);
                canvas.enabled = true;
                onOpen?.Invoke();
                InvokeEvents(true);
            }
        }

        /// <summary>
        /// Closes the popup.
        /// </summary>
        [ContextMenu("Control/Close")]
        public void Close()
        {
            if (canvas.enabled)
            {
                panels[current].gameObject.SetActive(false);
                canvas.enabled = false;
                InvokeEvents(false);
                onClose?.Invoke();
            }
        }

        /// <summary>
        /// Invokes the open and close events.
        /// </summary>
        /// <param name="open">True if the current panel was opened.</param>
        private void InvokeEvents(bool open)
        {
            if (open)
            {
                onPanelOpened?.Invoke(current);
                if (current == 0)
                {
                    onFirstPanelOpened?.Invoke();
                }
                if (current == panels.Length - 1)
                {
                    onLastPanelOpened?.Invoke();
                }
            }
            else
            {
                onPanelClosed?.Invoke(current);
                if (current == 0)
                {
                    onFirstPanelClosed?.Invoke();
                }
                if (current == panels.Length - 1)
                {
                    onLastPanelClosed?.Invoke();
                }
            }
        }
    }
}
