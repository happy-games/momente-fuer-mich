namespace HappyGames.UI
{
    using System;
    using UnityEngine;

    public abstract class NumberDisplay : MonoBehaviour
    {
        /// <summary>
        /// Sets the value to display.
        /// </summary>
        /// <param name="value">The value to display.</param>
        public abstract void SetValue(int value);

        /// <summary>
        /// Updates the value over time.
        /// </summary>
        /// <param name="value">The value to display.</param>
        /// <param name="onUpdate">Called when the value changes.</param>
        public abstract void UpdateValue(int value, Action<int> onUpdate);
        /// <summary>
        /// Updates the value over time.
        /// </summary>
        /// <param name="value">The value to display.</param>
        public void UpdateValue(int value) => UpdateValue(value, null);
    }
}
