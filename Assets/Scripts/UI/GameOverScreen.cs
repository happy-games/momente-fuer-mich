namespace HappyGames.UI
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;

    public class GameOverScreen : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The canvas controlling the screen.")]
        protected Canvas canvas = default;

        [SerializeField, Tooltip("The image to display the trophy.")]
        protected Image trophyIcon = null;

        [SerializeField, Tooltip("The script updating the resource display.")]
        protected NumberDisplay resourceDisplay = null;

        [SerializeField, Min(0f), Tooltip("The delay until the resources get updated in seconds.")]
        protected float resourceUpdateDelay = 2f;

        [Space, SerializeField, Tooltip("Called when the screen gets enabled.")]
        protected UnityEvent onDisplay = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// True if the screen is enabled.
        /// </summary>
        public bool IsVisible => canvas.enabled;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            canvas.enabled = false;
        }

        /// <summary>
        /// Enables the screen.
        /// </summary>
        /// <param name="icon">The trophy icon to display.</param>
        /// <param name="oldResources">The resource count before the new ones were added.</param>
        /// <param name="newResources">The resource count after the new ones were added.</param>
        public void Show(Sprite icon, int oldResources, int newResources)
        {
            if (!IsVisible)
            {
                canvas.enabled = true;
                trophyIcon.sprite = icon;
                StartCoroutine(DoDelayResources(oldResources, newResources));
                onDisplay?.Invoke();
            }
        }

        /// <summary>
        /// Updates the resources after a delay.
        /// </summary>
        protected IEnumerator DoDelayResources(int oldResources, int newResources)
        {
            resourceDisplay?.SetValue(oldResources);
            yield return new WaitForSecondsRealtime(resourceUpdateDelay);
            resourceDisplay?.UpdateValue(newResources);
        }
    }
}
