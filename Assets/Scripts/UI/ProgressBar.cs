namespace HappyGames.UI
{
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;

    /// <summary>
    /// Manages an <see cref="Image"/> to display progress.
    /// </summary>
    public class ProgressBar : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The images whos fill amount to update.")]
        protected Image target = null;

        [SerializeField, Tooltip("True if the progress should be inverted.")]
        protected bool invertValue = false;

        [Space, SerializeField, Tooltip("Called when the value is updated.")]
        protected UnityEvent<float> onProgressUpdated = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Updates the progress of the bar.
        /// </summary>
        /// <param name="progress">The new progress.</param>
        public void UpdateProgress(float progress)
        {
            target.fillAmount = invertValue ? 1f - progress : progress;
            onProgressUpdated?.Invoke(target.fillAmount);
        }
        /// <summary>
        /// Updates the progress of the bar.
        /// </summary>
        /// <param name="current">The current value.</param>
        /// <param name="max">The max value.</param>
        public void UpdateProgress(float current, float max) => UpdateProgress(current / max);
        /// <summary>
        /// Updates the progress of the bar.
        /// </summary>
        /// <param name="current">The current value.</param>
        /// <param name="max">The max value.</param>
        public void UpdateProgress(int current, int max) => UpdateProgress((float)current, max);
    }
}
