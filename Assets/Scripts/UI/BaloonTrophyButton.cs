namespace HappyGames.UI
{
    using HappyGames.Data;
    using UnityEngine;
    using UnityEngine.Events;

    public class BaloonTrophyButton : TrophyButton<BaloonTrophy>
    {
        [SerializeField]
        protected UnityEvent<RankType> onRankSet = default;

        [SerializeField]
        protected UnityEvent<int> onScoreSet = default;


        public override void Display(BaloonTrophy trophy)
        {
            base.Display(trophy);
            onRankSet?.Invoke(trophy.Rank);
            onScoreSet?.Invoke(trophy.Score);
        }
    }
}
