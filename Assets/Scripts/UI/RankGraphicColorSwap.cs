namespace HappyGames.UI
{
    using UnityEngine;
    using UnityEngine.UI;

    public class RankGraphicColorSwap : RankValueSwap<Color>
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The images whos color to change.")]
        protected Graphic target = null;
        /// <inheritdoc/>
        protected override Color Target
        {
            get => target.color;
            set => target.color = value;
        }
    }
}
