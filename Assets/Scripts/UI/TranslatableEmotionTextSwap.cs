namespace HappyGames.UI
{
    using HappyGames.Data;
    using HappyGames.Emotion;
    using HappyGames.Translations;
    using TMPro;
    using UnityEngine;

    public class TranslatableEmotionTextSwap : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The textfield whos text to manage.")]
        private TMP_Text taret = default;

        [SerializeField, Tooltip("The different texts to display.")]
        private TranslatableCollection<EmotionSet<string>> texts = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private UserProfile profile = null;
        private EmotionType emotion = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            if (ProfileManager.HasActiveProfile)
            {
                SwitchProfile(null, ProfileManager.GetActiveProfile());
            }
            else
            {
                SwitchLanguage(default);
            }
            ProfileManager.OnProfileActivated += SwitchProfile;
        }

        private void OnDisable()
        {
            ProfileManager.OnProfileActivated -= SwitchProfile;
            SwitchProfile(profile, null);
        }

        /// <summary>
        /// Changes the used profile.
        /// </summary>
        /// <param name="oldProfile">The previously used profile.</param>
        /// <param name="newProfile">The profile now in use.</param>
        private void SwitchProfile(UserProfile oldProfile, UserProfile newProfile)
        {
            if (oldProfile != null)
            {
                oldProfile.Settings.OnLanguageChanged -= SwitchLanguage;
            }
            profile = newProfile;
            if (newProfile != null)
            {
                SwitchLanguage(newProfile.Settings.Language);
                newProfile.Settings.OnLanguageChanged += SwitchLanguage;
            }
        }

        /// <summary>
        /// Changes the language of the textfield.
        /// </summary>
        /// <param name="language">The language to display.</param>
        private void SwitchLanguage(Language language)
        {
            taret.text = texts.GetValue(language).GetValue(emotion);
        }

        /// <summary>
        /// Displays the text for the given emotion.
        /// </summary>
        /// <param name="emotion">The emotion to display.</param>
        public void Display(EmotionType emotion)
        {
            this.emotion = emotion;
            taret.text = texts.GetValue(profile != null ? profile.Settings.Language : default).GetValue(emotion);
        }
    }
}
