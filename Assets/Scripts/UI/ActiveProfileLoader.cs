namespace HappyGames.UI
{
    using UnityEngine;
    using HappyGames.Data;
    using UnityEngine.Events;

    public class ActiveProfileLoader : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called when a profile was activated.")]
        protected UnityEvent<UserProfile> onProfileLoaded = default;

        [SerializeField, Tooltip("Called when a profile was activated.")]
        protected UnityEvent<string> onNameLoaded = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            if (ProfileManager.HasActiveProfile)
            {
                UserProfile profile = ProfileManager.GetActiveProfile();
                onProfileLoaded?.Invoke(profile);
                onNameLoaded?.Invoke(profile.Name);
            }
            ProfileManager.OnProfileActivated += (o, n) => onProfileLoaded?.Invoke(n);
            ProfileManager.OnProfileActivated += (o, n) => onNameLoaded?.Invoke(n.Name);
        }

        private void OnDisable()
        {
            ProfileManager.OnProfileActivated -= (o, n) => onProfileLoaded?.Invoke(n);
            ProfileManager.OnProfileActivated -= (o, n) => onNameLoaded?.Invoke(n.Name);
        }
    }
}
