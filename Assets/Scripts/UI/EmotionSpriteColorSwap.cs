using HappyGames.Emotion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmotionSpriteColorSwap : MonoBehaviour
{
    public SpriteRenderer sprite;
    public EmotionColorSet collorSet;

    public void Display(EmotionType emotion)
    {
        sprite.color = collorSet.GetValue(emotion);
    }
}