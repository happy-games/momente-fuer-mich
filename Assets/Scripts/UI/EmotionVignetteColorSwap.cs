using HappyGames.Emotion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class EmotionVignetteColorSwap : MonoBehaviour
{
    [SerializeField, Tooltip("a set of colors mapped to EmotionTypes")]
    public EmotionColorSet colorSet;

    [SerializeField, Tooltip("The Volume containing the Vignette")]
    public Volume volume;

    [SerializeField, Tooltip("intensyty of the vignette")]
    public float intensity;

    [SerializeField, Tooltip("smoothness of the vignette")]
    public float smoothness;

    public float lerpSpeed = 0.25f, time;

    public EmotionType currentEmotion;

    //actual vignette object
    private Vignette vignette = null;
    //the color of the vignette with no emotions
    private Color basecolor, currentColor;
    //the intensity and smoothness of the vignette with no emotions
    private float baseIntensity, basesmoothness, currentIntensity, currentSmoothness;

    //bool to check if the colors are fading
    private bool lerp = false;

    // Start is called before the first frame update
    void Start()
    {
        if(volume.profile.TryGet<Vignette>(out vignette))
        {
            vignette.color.overrideState = true;
            vignette.smoothness.overrideState = true;
            vignette.intensity.overrideState = true;
        }
        basecolor = vignette.color.value;
        basesmoothness = vignette.smoothness.value;
        baseIntensity = vignette.intensity.value;
    }

    private void Update()
    {
        if (lerp)
        {
            time += Time.deltaTime;
            if (currentEmotion == EmotionType.Neutral)
            {
                vignette.color.value = Color.Lerp(currentColor, basecolor, time * lerpSpeed);
                vignette.intensity.Override(Mathf.Lerp(currentIntensity, baseIntensity, time * lerpSpeed));
                vignette.smoothness.Override(Mathf.Lerp(currentSmoothness, basesmoothness, time * lerpSpeed));
            }
            else
            {
                vignette.color.value = Color.Lerp(currentColor, colorSet.GetValue(currentEmotion), time * lerpSpeed);
                vignette.intensity.Override(Mathf.Lerp(currentIntensity, intensity, time * lerpSpeed));
                vignette.smoothness.Override(Mathf.Lerp(currentSmoothness, smoothness, time * lerpSpeed));
            }

            if(time >= lerpSpeed)
            {
                lerp = false;
            }
        }
    }

    /// <summary>
    /// Changes the color of the vignette to the color in colorset corresponding to the param emotion and displays it
    /// </summary>
    /// <param name="emotion"></param> the currently active emotion
    public void Display(EmotionType emotion)
    {
        Debug.Log(currentEmotion);
        if (emotion != currentEmotion)
        {
            Debug.Log("Vignettechange to " + emotion);
            currentEmotion = emotion;
            lerp = true;
            time = 0f;
            currentColor = vignette.color.value;
            currentIntensity = vignette.intensity.value;
            currentSmoothness = vignette.smoothness.value;
        }
        //vignette.color.value = colorSet.GetValue(emotion);
        //vignette.intensity.Override(intensity);
        //vignette.smoothness.Override( smoothness);
    }

    public void ResetVignette()
    {
        Display(EmotionType.Neutral);
        //vignette.color.value = basecolor;
        //vignette.smoothness.value = basesmoothness;
        //vignette.intensity.value = baseIntensity;
    }
}
