namespace HappyGames.UI
{
    using UnityEngine;
    using UnityEngine.UI;

    public class ImageGradientSetter : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The target to recolor.")]
        protected Graphic target = null;

        [SerializeField, Tooltip("The gradient to apply.")]
        protected Gradient gradient = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Recolors the <see cref="Graphic"/>.
        /// </summary>
        /// <param name="value">The value to take from the gradient.</param>
        public void Recolor(float value) => target.color = gradient.Evaluate(value);
    }
}
