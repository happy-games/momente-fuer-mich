namespace HappyGames.UI
{
    using HappyGames.Data;
    using UnityEngine;
    using UnityEngine.Events;

    public class RuleCheck : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The rules to check for.")]
        protected RuleSet rules = default;

        [SerializeField, Tooltip("Called if the rule was explaned before.")]
        protected UnityEvent onRuleExplaned = default; 

        [SerializeField, Tooltip("Called if the rule wasn't explaned before.")]
        protected UnityEvent onNewRule = default; 


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Checks of the rule was explaned before.
        /// </summary>
        public void CheckRule()
        {
            if (ProfileManager.GetActiveProfile().CheckForRule(rules))
            {
                onRuleExplaned?.Invoke();
            }
            else
            {
                onNewRule?.Invoke();
            }
        }

        /// <summary>
        /// Adds the rule to the explaned ones.
        /// </summary>
        public void SetRuleAsExplaned() => ProfileManager.GetActiveProfile().AddRule(rules);
    }
}
