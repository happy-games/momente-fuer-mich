namespace HappyGames.UI
{
    using HappyGames.Data;
    using UnityEngine;
    using UnityEngine.Events;

    public abstract class TrophyFilter : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, LockInPlaymode, Tooltip("True if a filter should be used.")]
        protected bool useFilter = false;

        [Space, SerializeField, Tooltip("Called when the filter updates.")]
        protected UnityEvent onFilterUpdate = default;
        /// <summary>
        /// Called when the filter updates.
        /// </summary>
        public event UnityAction OnFilterUpdate
        {
            add => onFilterUpdate.AddListener(value);
            remove => onFilterUpdate.RemoveListener(value);
        }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Checks if the <see cref="Trophy"/> is filtered.
        /// </summary>
        /// <param name="trophy">The trophy to check.</param>
        /// <returns>True if the <see cref="Trophy"/> is filtered.</returns>
        public abstract bool CheckFitler(Trophy trophy);

        /// <summary>
        /// Removes the filtering.
        /// </summary>
        public virtual void RemoveFilter(bool reset)
        {
            useFilter = !reset;
            onFilterUpdate?.Invoke();
        }
    }
}
