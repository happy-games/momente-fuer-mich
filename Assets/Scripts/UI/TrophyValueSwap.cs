namespace HappyGames.UI
{
    using HappyGames.Data;
    using UnityEngine;

    public abstract class TrophyValueSwap<T> : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The values to use to display the trophy type.")]
        protected TrophySet<T> values = null;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The value to update.
        /// </summary>
        protected abstract T Target { get; set; }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Sets the value for the <see cref="TrophyType"/>.
        /// </summary>
        /// <param name="trophy">The <see cref="TrophyType"/> to display.</param>
        public void Display(TrophyType trophy) => Target = values.GetValue(trophy);

        /// <summary>
        /// Updates the values to display.
        /// </summary>
        /// <param name="values">The values to display.</param>
        public void SetValues(TrophySet<T> values)
        {
            TrophyType trophy = default;
            foreach (var trophyAndValue in this.values)
            {
                if (Target.Equals(trophyAndValue.Value))
                {
                    trophy = trophyAndValue.Key;
                    break;
                }
            }
            this.values = values;
            Display(trophy);
        }
    }
}
