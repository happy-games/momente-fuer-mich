using HappyGames.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;

public class IntroSequenz : MonoBehaviour
{
    public VideoPlayer intro;
    public Animator transitionAnim, menuUiAnim;
    public GameObject MenuGraphics;
    public bool DEBUGAlwaysSkipIntro;
    public GameObject skipButton;

    public UnityEvent OnIntroEnded;

    private void Start()
    {
        intro.loopPointReached += VideoEnded;
    }

    private void VideoEnded(VideoPlayer vp)
    {
        OnIntroEnded.Invoke();
    }

    private void StartSequenz()
    {
        if (!ProfileManager.GetActiveProfile().StoryStarted && !DEBUGAlwaysSkipIntro)
        {
            intro.Play();
            MenuGraphics.SetActive(false);
        } else
        {
            OnIntroEnded.Invoke();
        }
    }

    public void StartIntro()
    {
        skipButton.SetActive(true);
        transitionAnim.enabled = true;
        transitionAnim.SetTrigger("Transition");
        menuUiAnim.enabled = true;
        menuUiAnim.SetTrigger("Transition");
    }

    public void SkipIntro()
    {
        OnIntroEnded.Invoke();
    }

}
