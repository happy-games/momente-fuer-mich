namespace HappyGames.UI
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Events;
    using TMPro;
    using HappyGames.TimeManagement;
    using System;
    using System.Collections.Generic;

    public class NumberCounter : NumberDisplay
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------
        
        [SerializeField, Tooltip("The textfield to display the number.")]
        protected TMP_Text display = null;

        [SerializeField, Tooltip("The timline to use for updating the value.")]
        protected Timeline timeline = null;

        [SerializeField, Min(0f), Tooltip("The speed of the counting, +1step ever x seconds.")]
        protected float updateSpeed = 0.2f;

        [SerializeField, Min(1), Tooltip("The amount of values to add per update.")]
        protected int steps = 1;

        [SerializeField, Tooltip("The current score.")]
        protected int current = 0;
        /// <summary>
        /// The current score.
        /// </summary>
        public int Current => current;

        [Space, SerializeField, Tooltip("Called when the values change.")]
        protected UnityEvent<int, int> onValuesChanged = default;
        /// <summary>
        /// Called when the values change.
        /// </summary>
        public event UnityAction<int, int> OnValuesChanged
        {
            add => onValuesChanged.AddListener(value);
            remove => onValuesChanged.RemoveListener(value);
        }
        [SerializeField, Tooltip("Called when the values is set.")]
        protected UnityEvent<int> onValueSet = default;
        /// <summary>
        /// Called when the values is set.
        /// </summary>
        public event UnityAction<int> OnValueSet
        {
            add => onValueSet.AddListener(value);
            remove => onValueSet.RemoveListener(value);
        }
        [SerializeField, Tooltip("Called when the values is updated.")]
        protected UnityEvent<int, int> onValueUpdated = default;
        /// <summary>
        /// Called when the values is updated.
        /// </summary>
        public event UnityAction<int, int> OnValueUpdated
        {
            add => onValueUpdated.AddListener(value);
            remove => onValueUpdated.RemoveListener(value);
        }


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected int target = 0;
        /// <summary>
        /// The score to reach.
        /// </summary>
        public int Target => target;

        private Dictionary<int, Action<int>> updateEvents = new Dictionary<int, Action<int>>();


        // --- | Methods | -------------------------------------------------------------------------------------------------

        protected virtual void Awake()
        {
            target = current;
        }

        /// <inheritdoc/>
        public override void SetValue(int value)
        {
            current = target = value;
            display.text = current.ToString();
            onValueSet?.Invoke(current);
            onValuesChanged?.Invoke(current, target);
        }
        /// <inheritdoc/>
        public override void UpdateValue(int value, Action<int> onUpdate)
        {
            bool isUpdating = current != target;
            target = value;
            onValuesChanged?.Invoke(current, target);
            if (!updateEvents.ContainsKey(value))
            {
                updateEvents.Add(value, onUpdate);
            }
            else
            {
                updateEvents[value] += onUpdate;
            }
            if (!isUpdating)
            {
                StartCoroutine(DoUpdate());
            }
        }

        /// <summary>
        /// Updates the value over time.
        /// </summary>
        protected IEnumerator DoUpdate()
        {
            if (!timeline)
            {
                timeline = new Timeline();
            }
            float time = 0f;
            int prev = current;
            Vector2 position = display.transform.localPosition;
            if (updateSpeed <= 0f)
            {
                current = target;
                display.text = current.ToString();
                InvokeUpdateEvents(prev, current);
            }
            else
            {
                while (current != target)
                {
                    while (current != target && time < updateSpeed)
                    {
                        yield return null;
                        time += timeline.DeltaTime;
                        display.transform.localPosition = Vector2.Lerp(display.transform.localPosition, position, time % updateSpeed / updateSpeed);
                    }
                    if (current != target)
                    {
                        while (time >= updateSpeed)
                        {
                            current = (int)Mathf.MoveTowards(current, target, steps);
                            time -= updateSpeed;
                        }
                    }
                    display.transform.localPosition += Vector3.up * 5f;
                    display.text = current.ToString();
                    InvokeUpdateEvents(prev, current);
                    prev = current;
                }
                while (time < updateSpeed)
                {
                    yield return null;
                    time += timeline.DeltaTime;
                    display.transform.localPosition = Vector2.Lerp(display.transform.localPosition, position, time % updateSpeed / updateSpeed);
                }
                display.transform.localPosition = position;
            }
        }

        /// <summary>
        /// Invokes the update events.
        /// </summary>
        /// <param name="prev">The value previously.</param>
        /// <param name="current">The current value.</param>
        private void InvokeUpdateEvents(int prev, int current)
        {
            List<int> removedEventKeys = new List<int>();
            foreach (var update in updateEvents)
            {
                update.Value?.Invoke(current);
                if (update.Key > prev && update.Key < current)
                {
                    removedEventKeys.Add(update.Key);
                }
            }
            foreach (int key in removedEventKeys)
            {
                updateEvents.Remove(key);
            }
            onValueUpdated?.Invoke(current, target);
            onValuesChanged?.Invoke(current, target);
        }
    }
}
