namespace HappyGames.UI
{
    using HappyGames.Emotion;
    using UnityEngine;
    using UnityEngine.Events;

    public class EmotionDisplay : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [Header("Essentials")] // -----------------------------------------------------------------
        [SerializeField, Tooltip("Called when the emotion to display changes.")]
        protected UnityEvent<EmotionType> onEmotionChanged = default;

        [SerializeField, Tooltip("Called when the progress of selection changes.")]
        protected UnityEvent<float, float> onProgressChanged = default;

        [Header("Show / Hide")] //-----------------------------------------------------------------
        [SerializeField, Tooltip("Called when the display is shown.")]
        protected UnityEvent onShow = default;

        [SerializeField, Tooltip("Called when the display is hidden.")]
        protected UnityEvent onHide = default;

        [Header("Other")] // ----------------------------------------------------------------------
        [SerializeField, Tooltip("Called when the sprite set to display changes.")]
        protected UnityEvent<EmotionSpriteSet> onSpriteChanged = default;

        [SerializeField, Tooltip("Called when the label set to display changes.")]
        protected UnityEvent<EmotionStringSet> onLableChanged = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Updates the displayed emotion.
        /// </summary>
        /// <param name="emotion">The emotion to display.</param>
        public void UpdateEmotion(EmotionType emotion) => onEmotionChanged?.Invoke(emotion);

        /// <summary>
        /// Updates the selection progress.
        /// </summary>
        /// <param name="current">The current value.</param>
        /// <param name="max">The max value.</param>
        public void UpdateProgress(float current, float max) => onProgressChanged?.Invoke(current, max);
        /// <summary>
        /// Updates the selection progress.
        /// </summary>
        /// <param name="current">The current value.</param>
        /// <param name="max">The max value.</param>
        public void UpdateProgress(int current, int max) => UpdateProgress((float)current, max);
        /// <summary>
        /// Updates the selection progress.
        /// </summary>
        /// <param name="progress">The progress to display.</param>
        public void UpdateProgress(float progress) => UpdateProgress(progress, 1f);

        /// <summary>
        /// Shows the display.
        /// </summary>
        public void Show() => onShow?.Invoke();

        /// <summary>
        /// Hides the display.
        /// </summary>
        public void Hide() => onHide?.Invoke();

        /// <summary>
        /// Changes the sprites to display the emotion.
        /// </summary>
        /// <param name="spriteSet">The sprites to display.</param>
        public void ChangeSpriteSet(EmotionSpriteSet spriteSet) => onSpriteChanged?.Invoke(spriteSet);

        /// <summary>
        /// Changes the labels to display the emotion.
        /// </summary>
        /// <param name="labelSet">The labels to display.</param>
        public void ChangeLabelSet(EmotionStringSet labelSet) => onLableChanged?.Invoke(labelSet);
    }
}
