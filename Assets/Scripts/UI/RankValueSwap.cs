namespace HappyGames.UI
{
    using HappyGames.Data;
    using UnityEngine;

    public abstract class RankValueSwap<T> : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The values to use to display the emotion.")]
        protected RankSet<T> values = null;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The value to update.
        /// </summary>
        protected abstract T Target { get; set; }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            Display(default);
        }

        /// <summary>
        /// Sets the value for the emotion.
        /// </summary>
        /// <param name="emotion">The emotion to display.</param>
        public void Display(RankType rank) => Target = values ? values.GetValue(rank) : default;

        /// <summary>
        /// Updates the values to display.
        /// </summary>
        /// <param name="values">The values to display.</param>
        public void SetValues(RankSet<T> values)
        {
            RankType rank = default;
            if (this.values)
            {
                foreach (var emotionAndValue in this.values)
                {
                    if (Target.Equals(emotionAndValue.Value))
                    {
                        rank = emotionAndValue.Key;
                        break;
                    }
                }
            }
            this.values = values;
            Display(rank);
        }
    }
}
