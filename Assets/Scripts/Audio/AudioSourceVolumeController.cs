namespace HappyGames.Audio
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Events;

    public class AudioSourceVolumeController : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The source whos volume to control.")]
        protected AudioSource source = default;

        [SerializeField, Min(0f), Tooltip("The maximum volume change per second.")]
        protected float fadeSpeed = 2f;

        [Space, SerializeField, Tooltip("Called when the volume was changed.")]
        protected UnityEvent<float> onVolumeChanged = default;

        [SerializeField, Tooltip("Called when the volume is turned on.")]
        protected UnityEvent onEnabled = default;

        [SerializeField, Tooltip("Called when the volume is turned off.")]
        protected UnityEvent onDisabled = default;


        // --- | Varaibles | -----------------------------------------------------------------------------------------------

        protected float defaultVolume;
        /// <summary>
        /// The currently set volume, can differentiate with <see cref="CurrentVolume"/> during fades.
        /// </summary>
        public float TartgetVolume { get; protected set; }
        /// <summary>
        /// The current volume of the source.
        /// </summary>
        public float CurrentVolume => source.volume;

        protected Coroutine fadeUpdate = null;
        /// <summary>
        /// True if the volume is currently changing.
        /// </summary>
        public bool IsFading => fadeUpdate != null;

        /// <summary>
        /// True if the volume is off.
        /// </summary>
        public bool IsMuted => CurrentVolume == 0f;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        protected virtual void Awake()
        {
            defaultVolume = TartgetVolume = source.volume;
            if (IsMuted)
            {
                onDisabled?.Invoke();
            }
            else
            {
                onEnabled?.Invoke();
            }
        }

        /// <summary>
        /// Sets the volume of the source.
        /// </summary>
        /// <param name="volume">The volume to apply.</param>
        public void SetVolume(float volume)
        {
            bool wasOff = IsMuted;
            source.volume = volume;
            onVolumeChanged?.Invoke(volume);
            if (wasOff != IsMuted)
            {
                if (wasOff)
                {
                    onEnabled?.Invoke();
                }
                else
                {
                    onDisabled?.Invoke();
                }
            }
        }

        /// <summary>
        /// Sets the volume to 0.
        /// </summary>
        public void SetOff() => SetVolume(0f);

        /// <summary>
        /// Sets the volume back to the default volume.
        /// </summary>
        public void ResetVolume() => SetVolume(defaultVolume);

        /// <summary>
        /// Changes the volume by fading.
        /// </summary>
        /// <param name="volume">The volume to target.</param>
        public void FadeVolume(float volume)
        {
            TartgetVolume = volume;
            if (!IsFading)
            {
                fadeUpdate = StartCoroutine(DoFade());
            }
        }

        /// <summary>
        /// Fades the volume out to 0.
        /// </summary>
        public void FadeOut() => FadeVolume(0f);

        /// <summary>
        /// Fades the volume back to the default value.
        /// </summary>
        public void FadeIn() => FadeVolume(defaultVolume);

        /// <summary>
        /// Changes the volume over time to match the <see cref="TartgetVolume"/>.
        /// </summary>
        protected IEnumerator DoFade()
        {
            while (CurrentVolume != TartgetVolume)
            {
                SetVolume(Mathf.MoveTowards(CurrentVolume, TartgetVolume, fadeSpeed * Time.deltaTime));
                yield return null;
            }
            fadeUpdate = null;
        }
    }
}
