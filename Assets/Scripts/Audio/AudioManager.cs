namespace HappyGames
{
    using System;
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Audio;

    public class AudioManager : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The master mixer managing the audio.")]
        protected AudioMixer mixer = default;

        [SerializeField, Tooltip("The snapshot for the main state.")]
        protected AudioMixerSnapshot main = default;

        [SerializeField, Tooltip("The snapshot for the paused state.")]
        protected AudioMixerSnapshot paused = default;

        [SerializeField, Tooltip("The audio source plaing the music.")]
        protected AudioSource musicSource = default;

        [SerializeField, Min(0f), Tooltip("The duration of the fade between snapshots.")]
        protected float fadeDuration = 0.5f;


        // --- | Varaibles | -----------------------------------------------------------------------------------------------

        protected Coroutine musicTransition = null;
        /// <summary>
        /// True if the music is currently changing.
        /// </summary>
        public bool IsInMusicTransition => musicTransition != null;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Switches to the paused audio state.
        /// </summary>
        [ContextMenu("Pause")]
        public void SwitchToPaused()
        {
            paused.TransitionTo(fadeDuration);
        }

        /// <summary>
        /// Switches to the main audio state.
        /// </summary>
        [ContextMenu("Main")]
        public void SwitchToMain()
        {
            main.TransitionTo(fadeDuration);
        }

        /// <summary>
        /// Changes the music.
        /// </summary>
        /// <param name="clip">The clip to play.</param>
        /// <param name="fadeDuration">The duration of the fade between the clips.</param>
        public void ChangeMusic(AudioClip clip, float fadeDuration)
        {
            if (IsInMusicTransition)
            {
                return;
            }
            if (musicSource.clip != clip)
            {
                if (fadeDuration < 0f)
                {
                    musicSource.clip = clip;
                }
                else
                {
                    AudioSource newSource = Util.CopyComponent(musicSource, musicSource.gameObject);
                    newSource.clip = clip;
                    musicTransition = StartCoroutine(DoChangeMusic(musicSource, newSource, 1f, () => 
                    {
                        Destroy(musicSource);
                        musicSource = newSource;
                    }));
                }
            }
        }

        /// <summary>
        /// Fades the volumes of both sources.
        /// </summary>
        /// <param name="oldSource">The old source to fade out.</param>
        /// <param name="newSource">The new source to fade in.</param>
        /// <param name="duration">The duration of the fade.</param>
        /// <param name="onDone">Called when the fade is over.</param>
        protected IEnumerator DoChangeMusic(AudioSource oldSource, AudioSource newSource, float duration, Action onDone)
        {
            float time = 0f;
            float oldStart = oldSource.volume;
            float newTarget = newSource.volume;
            while (time < duration)
            {
                float progress = time / duration;
                oldSource.volume = oldStart * (1f - progress);
                newSource.volume = newTarget * progress;
                yield return null;
                time += Time.deltaTime;
            }
            newSource.volume = newTarget;
            musicTransition = null;
            onDone?.Invoke();
        }

        /// <summary>
        /// Changes the pitch of the sound effects.
        /// </summary>
        /// <param name="value">The new pitch.</param>
        public void ChangeSFXPitch(float value) => mixer.SetFloat("SFX_Pitch", value);
    }
}
