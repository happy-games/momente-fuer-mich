﻿namespace HappyGames.Audio
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Plays <see cref="AudioClip"/>s on a <see cref="AudioSource"/>.
    /// </summary>
    public class AudioClipPlayer : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The source to play the clips from.")]
        protected AudioSource source = null;
        /// <summary>
        /// The source to play the clips from.
        /// </summary>
        public AudioSource Source => source;

        [SerializeField]
        protected float minPitch = 1f;

        [SerializeField]
        protected float maxPitch = 1f;

        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected Queue<AudioClip> queue = new Queue<AudioClip>();
        protected Coroutine queueUpdate = null;
        /// <summary>
        /// True if a audio queue is currently playing.
        /// </summary>
        public bool HasActiveQueue => queueUpdate != null;

        protected AudioClip[] loop = null;
        protected Coroutine loopUpdate = null;
        /// <summary>
        /// True if a audio loop is currently playing.
        /// </summary>
        public bool HasActiveLoop => loopUpdate != null;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            if (!source)
            {
                source = gameObject.AddComponent<AudioSource>();
            }
        }

        /// <summary>
        /// Plays the given <see cref="AudioClip"/> by instantiating a new <see cref="AudioSource"/>.
        /// </summary>
        /// <param name="clip">The clip to play.</param>
        public void PlayClipSave(AudioClip clip)
        {
            GameObject target = new GameObject("SavePlayer");
            DontDestroyOnLoad(target);
            AudioSource source = target.AddComponent<AudioSource>();
            source.outputAudioMixerGroup = this.source.outputAudioMixerGroup;
            source.volume = this.source.volume;
            source.PlayOneShot(clip);
            target.AddComponent<AudoDistory>().Destory(clip.length);
        }

        /// <summary>
        /// Plays the given <see cref="AudioClip"/> on the source.
        /// </summary>
        /// <param name="clip">The clip to play.</param>
        public void PlayClip(AudioClip clip)
        {
            source.pitch = Random.Range(minPitch, maxPitch);
            source.PlayOneShot(clip);
        }

        /// <summary>
        /// Plays a reandom audio clips from the group.
        /// </summary>
        /// <param name="clips">The clips to select from.</param>
        public void PlayRandomClip(AudioClipGroup clips)
        {
            PlayClip(clips.Count > 0 ? clips.Clips.Random() : null);
        }

        /// <summary>
        /// Adds a clip to the audio queue.
        /// </summary>
        /// <param name="clip">The clip to add.</param>
        public void QueueClip(AudioClip clip)
        {
            queue.Enqueue(clip);
            if (!HasActiveQueue)
            {
                queueUpdate = StartCoroutine(DoPlayQueue());
            }
        }

        /// <summary>
        /// Enqueues a random clip from the group.
        /// </summary>
        /// <param name="clips">The group of clips to select the random one.</param>
        public void QueueRandomClip(AudioClipGroup clips) => QueueClip(clips.Clips.Random());

        /// <summary>
        /// Enqueues clips from the group randomly.
        /// </summary>
        /// <param name="clips">The group to select the clips from.</param>
        /// <param name="count">The amount of clips to select.</param>
        /// <param name="type">How to select the clips.</param>
        public void QueueRandomClips(AudioClipGroup clips, int count, RandomSelectionType type = RandomSelectionType.Regular)
        {
            switch (type)
            {
                case RandomSelectionType.Regular:
                    for (int i = 0; i < count; i++)
                    {
                        QueueRandomClip(clips);
                    }
                    break;

                case RandomSelectionType.AvoidPrevious:
                    int prev = -1;
                    for (int i = 0; i < count; i++)
                    {
                        int index = Random.Range(0, clips.Count);
                        if (index == prev)
                        {
                            index = (index + 1) % clips.Count;
                        }
                        QueueClip(clips[index]);
                        prev = index;
                    }
                    break;

                case RandomSelectionType.AvoidDuplicates:
                    if (count > clips.Count)
                    {
                        throw new System.ArgumentOutOfRangeException(nameof(count), count, $"The amount of clips to play can't be higher than the amount of clips in thhe group.");
                    }
                    List<AudioClip> clipList = new List<AudioClip>(clips.Clips);
                    for (int i = 0; i < count; i++)
                    {
                        int index = Random.Range(0, clipList.Count);
                        QueueClip(clipList[index]);
                        clipList.RemoveAt(index);
                    }
                    break;
            }
        }
        /// <summary>
        /// Enqueues the clips in the group in a random order.
        /// </summary>
        /// <param name="clips">The clips to queue.</param>
        public void QueueRandomClips(AudioClipGroup clips)
            => QueueRandomClips(clips, clips.Count, RandomSelectionType.AvoidDuplicates);

        /// <summary>
        /// Enqueues clips from the group.
        /// </summary>
        /// <param name="clips">The group containing the clips.</param>
        /// <param name="start">The index of the first clip to enqueue.</param>
        /// <param name="count">The amount of clips to enqueue.</param>
        public void QueueClips(AudioClipGroup clips, int start, int count)
        {
            if (start < 0)
            {
                start = 0;
            }
            for (int i = start; i < clips.Count && i < start + count; i++)
            {
                QueueClip(clips[i]);
            }
        }
        /// <summary>
        /// Enqueues the clips in the group.
        /// </summary>
        /// <param name="clips">The clips to enqueue.</param>
        public void QueueClips(AudioClipGroup clips) => QueueClips(clips, 0, clips.Count);

        /// <summary>
        /// Stops the queue and removes all clips.
        /// </summary>
        public void ClearQueue()
        {
            if (HasActiveQueue)
            {
                StopCoroutine(queueUpdate);
                queueUpdate = null;
            }
            queue.Clear();
        }

        /// <summary>
        /// Loops over the given clips.
        /// </summary>
        /// <param name="clips">The clips to loop.</param>
        public void LoopClips(AudioClip[] clips)
        {
            if (HasActiveLoop)
            {
                StopLoop();
            }
            loop = clips;
            loopUpdate = StartCoroutine(DoPlayLoop());
        }
        /// <summary>
        /// Loops over the clips in the group.
        /// </summary>
        /// <param name="clips">The group with the clips to loop.</param>
        public void LoopClips(AudioClipGroup clips) => LoopClips(clips.Clips);
        /// <summary>
        /// Loops over the clips in the group.
        /// </summary>
        /// <param name="clips">The group with the clips to loop.</param>
        /// <param name="loopStart">The clip to start the loop at.</param>
        public void LoopClips(AudioClipGroup clips, int loopStart)
        {
            if (loopStart < clips.Count)
            {
                if (loopStart > 0)
                {
                    ClearQueue();
                    float loopDelay = 0f;
                    for (int i = 0; i < loopStart; i++)
                    {
                        QueueClip(clips[i]);
                        loopDelay += clips[i].length;
                    }
                    AudioClip[] loop = new AudioClip[clips.Count - loopStart];
                    for (int i = loopStart; i < clips.Count; i++)
                    {
                        loop[i - loopStart] = clips[i];
                    }
                    loopUpdate = StartCoroutine(DoWaitForLoop(loopDelay, loop));
                }
                else
                {
                    LoopClips(clips);
                }
            }
            else
            {
                QueueClips(clips);
            }
        }

        /// <summary>
        /// Stops the loop.
        /// </summary>
        public void StopLoop()
        {
            if (HasActiveLoop)
            {
                StopCoroutine(loopUpdate);
                loop = null;
                loopUpdate = null;
            }
        }

        /// <summary>
        /// Starts the audio queue.
        /// </summary>
        protected IEnumerator DoPlayQueue()
        {
            while (queue.Count > 0)
            {
                AudioClip clip = queue.Dequeue();
                PlayClip(clip);
                yield return new WaitForSecondsRealtime(clip.length);
            }
            queueUpdate = null;
        }

        /// <summary>
        /// Do wait for the loop to start.
        /// </summary>
        /// <param name="delay">The time to wait befor starting the loop.</param>
        /// <param name="clips">The clops to loop.</param>
        protected IEnumerator DoWaitForLoop(float delay, AudioClip[] clips)
        {
            yield return new WaitForSecondsRealtime(delay);
            loopUpdate = null;
            LoopClips(clips);
        }

        /// <summary>
        /// Starts the audio loop.
        /// </summary>
        protected IEnumerator DoPlayLoop()
        {
            int index = 0;
            while (true)
            {
                PlayClip(loop[index]);
                yield return new WaitForSecondsRealtime(loop[index].length);
                index++;
                if (index >= loop.Length)
                {
                    index = 0;
                }
            }
        }

        /// <summary>
        /// How to select the random clips.
        /// </summary>
        public enum RandomSelectionType { Regular, AvoidPrevious, AvoidDuplicates }

        private class AudoDistory : MonoBehaviour
        {
            public void Destory(float duration)
            {
                MonoBehaviour.Destroy(gameObject, duration);
            }
        }
    }
}
