namespace HappyGames.Audio
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    [CreateAssetMenu(fileName = "DialogAudio", menuName = "Audio/Dialog")]
    public class DialogAudio : ScriptableObject, IEnumerable<KeyValuePair<string, AudioClip>>
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The text to the audio.")]
        protected YarnProgram dialogText = null;
        /// <summary>
        /// The text to the audio.
        /// </summary>
        public YarnProgram DialogText => dialogText;

        [SerializeField, Tooltip("The audio lines in the dialog.")]
        protected AudioLine[] lines = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        protected Dictionary<string, AudioClip> clips;

        public AudioClip this[string tag]
        {
            get
            {
                if (clips == null)
                {
                    ConvertArrayToDictionary();
                }
                return clips.TryGetValue(tag, out AudioClip clip) ? clip  : null;
            }
        }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Adds the values from the array to the dictionary.
        /// </summary>
        private void ConvertArrayToDictionary()
        {
            clips = new Dictionary<string, AudioClip>();
            foreach (AudioLine line in lines)
            {
                clips[line.Tag] = line.Clip;
            }
        }

        /// <inheritdoc/>
        public IEnumerator<KeyValuePair<string, AudioClip>> GetEnumerator()
        {
            if (clips == null)
            {
                ConvertArrayToDictionary();
            }
            foreach (var pair in clips)
            {
                yield return pair;
            }
        }
        /// <inheritdoc/>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();


        // --- | Classes | -------------------------------------------------------------------------------------------------

        [System.Serializable]
        protected class AudioLine
        {
            [SerializeField, Tooltip("The tag of the line.")]
            protected string tag;
            /// <summary>
            /// The tag of the line.
            /// </summary>
            public string Tag => tag;

            [SerializeField, Tooltip("The audio clip to play.")]
            protected AudioClip clip;
            /// <summary>
            /// The audio clip to play.
            /// </summary>
            public AudioClip Clip => clip;
        }

#if (UNITY_EDITOR)

        [UnityEditor.CustomPropertyDrawer(typeof(AudioLine))]
        protected class AudioLineDrawer : UnityEditor.PropertyDrawer
        {
            public override void OnGUI(Rect position, UnityEditor.SerializedProperty property, GUIContent label)
            {
                UnityEditor.EditorGUI.PropertyField(
                    new Rect(position.x, position.y, UnityEditor.EditorGUIUtility.labelWidth, position.height), 
                    property.FindPropertyRelative("tag"), GUIContent.none);
                UnityEditor.EditorGUI.PropertyField(
                    new Rect(position.x + UnityEditor.EditorGUIUtility.labelWidth, position.y, position.width - UnityEditor.EditorGUIUtility.labelWidth, position.height), 
                    property.FindPropertyRelative("clip"), GUIContent.none);
            }
        }

#endif
    }
}
