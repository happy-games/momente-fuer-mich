﻿namespace HappyGames.Audio
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Contains a list of <see cref="AudioClip"/>s.
    /// </summary>
    [CreateAssetMenu(fileName = "AudioClipGroup", menuName = "Audio/Clip Group")]
    public class AudioClipGroup : ScriptableObject, IEnumerable<AudioClip>
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The clips registerd in the group.")]
        protected AudioClip[] clips = new AudioClip[0];
        /// <summary>
        /// The clips registerd in the group.
        /// </summary>
        public AudioClip[] Clips => clips;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The amount of audio clips in the group.
        /// </summary>
        public int Count => clips.Length;

        /// <summary>
        /// Returns the <see cref="AudioClip"/> with the given key.
        /// </summary>
        /// <param name="key">The key/index of the <see cref="AudioClip"/>.</param>
        /// <returns>The <see cref="AudioClip"/> at the index.</returns>
        public AudioClip this[int key] => clips[key];


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public IEnumerator<AudioClip> GetEnumerator()
        {
            foreach (AudioClip clip in clips)
            {
                yield return clip;
            }
        }

        /// <inheritdoc/>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
