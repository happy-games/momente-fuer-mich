namespace HappyGames.Audio
{
    using HappyGames.Data;
    using UnityEngine;
    using UnityEngine.Audio;
    using UnityEngine.Events;

    public class MixerGroupVolumeController : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The mixer contolling the audio.")]
        protected AudioMixer mixer = default;

        [SerializeField, Tooltip("The names of the volume parameters to control.")]
        protected string[] parameterNames = default;

        [SerializeField, Tooltip("The value of the volume that counts as muted.")]
        private float mutedVolume = -20f;

        [Space, SerializeField, Tooltip("Called when the initial volume was loaded.")]
        protected UnityEvent<float> onVolumeLoaded = default;

        [SerializeField, Tooltip("Called when the volume was changed.")]
        protected UnityEvent<float> onVolumeChanged = default;

        [SerializeField, Tooltip("Called when the muted state gets updated.")]
        protected UnityEvent<bool> onMuteUpdat = default;


        // --- | Varaibles | -----------------------------------------------------------------------------------------------

        protected ProfileSettings settings;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        protected virtual void OnEnable()
        {
            if (ProfileManager.HasActiveProfile)
            {
                SwitchProfile(null, ProfileManager.GetActiveProfile());
            }
            ProfileManager.OnProfileActivated += SwitchProfile;
        }

        protected virtual void OnDisable()
        {
            ProfileManager.OnProfileActivated -= SwitchProfile;
            if (ProfileManager.HasActiveProfile)
            {
                SwitchProfile(ProfileManager.GetActiveProfile(), null);
            }
        }

        private void SwitchProfile(UserProfile oldProfile, UserProfile newProfile)
        {
            if (oldProfile != null)
            {
                oldProfile.Settings.OnVolumeChanged -= OnVolumeChagned;
            }
            if (newProfile != null)
            {
                settings = newProfile.Settings;
                newProfile.Settings.OnVolumeChanged += OnVolumeChagned;

                (float value, bool muted) volume = settings.GetVolume(parameterNames[0]);
                SetVolume(volume.muted ? -80f : volume.value);
                onVolumeLoaded?.Invoke(volume.muted ? -80f : volume.value);
                onMuteUpdat?.Invoke(volume.muted);
            }
            else
            {
                settings = new ProfileSettings();
            }
        }

        /// <summary>
        /// Sets the volume of a audio group.
        /// </summary>
        /// <param name="volume">The volume to apply.</param>
        public void SetVolume(float volume)
        {
            foreach (string parameter in parameterNames)
            {
                mixer.SetFloat(parameter, volume <= mutedVolume ? -80f : volume);
            }
            onVolumeChanged?.Invoke(volume <= mutedVolume ? mutedVolume : volume);
            onMuteUpdat?.Invoke(volume <= mutedVolume);
        }

        /// <summary>
        /// Mutes the volume.
        /// </summary>
        /// <param name="isMuted">True if the volume should be muted.</param>
        public void MuteVolume(bool isMuted) => SetVolume(isMuted ? -80f : settings.GetVolume(parameterNames[0]).value);

        /// <summary>
        /// Involkes the <see cref="onVolumeLoaded"/> event, if the parameters match. 
        /// </summary>
        /// <param name="name">The name of the volume that changed.</param>
        /// <param name="value">The value of the volume.</param>
        private void OnVolumeChagned(string name, float value)
        {
            if (parameterNames.Contains(ArrayExtention.ConparisonRequirements.Any, name))
            {
                onVolumeLoaded?.Invoke(value);
            }
        }

        /// <summary>
        /// Save the volume.
        /// </summary>
        public void SaveVolume()
        {
            foreach (string parameter in parameterNames)
            {
                if (mixer.GetFloat(parameter, out float volume))
                {
                    if (volume > mutedVolume)
                    {
                        settings.SetVolume(parameter, volume, false);
                    }
                    else
                    {
                        settings.SetVolume(parameter, true);
                    }
                }
            }
        }
    }
}
