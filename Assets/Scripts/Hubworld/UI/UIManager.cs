using HappyGames.Emotion;
using HappyGames.UI;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance { get; private set; }
    public Button inventory, map, emotion;
    public MiniGameMenu mapImage, inventoryObject;
    public CollectibleCounter collectible;
    public EmotionSelect emotionSelect;
    public EmotionImageSwap face;
    public EmotionGraphicColorSwap background, frame;
    public EmotionVignetteColorSwap vignette;
    private bool inputBlocked = false;

    public float neutralTimer = 5f;
    private float emotionTimer = -1f;

    private enum OpenMenu { None, Emotion, Inventory, Map }
    private OpenMenu currentMenu = OpenMenu.None;

    protected virtual void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (emotionTimer >= 0)
        {
            emotionTimer += Time.deltaTime;
            if (emotionTimer >= neutralTimer)
            {
                emotionTimer = -1;
                face.Display(EmotionType.Neutral);
                background.Display(EmotionType.Neutral);
                frame.Display(EmotionType.Neutral);
                Debug.Log("neutral time up");
                vignette.ResetVignette();
            }
        }
    }

    public void InventoryToggle()
    {
        if (currentMenu != OpenMenu.Emotion && !inputBlocked)
        {
            if (inventoryObject.IsOpen)
            {
                inventoryObject.Close();
                currentMenu = OpenMenu.None;
                Hubworld.Instance.EnableInputs();
                collectible.StartTimer();
            }
            else
            {
                inventoryObject.Open();
                CloseSubMenus(inventoryObject);
                currentMenu = OpenMenu.Inventory;
                Hubworld.Instance.DisableInputs();
                collectible.ShowCollectibles();
            }
        }
    }

    public void MapToggle()
    {
        Debug.Log(inputBlocked);
        if (currentMenu != OpenMenu.Emotion && !inputBlocked)
        {
            if (mapImage.IsOpen)
            {
                mapImage.Close();
                currentMenu = OpenMenu.None;
                Hubworld.Instance.EnableInputs();
            }
            else
            {
                mapImage.Open();
                currentMenu = OpenMenu.Map;
                CloseSubMenus(mapImage);
                Hubworld.Instance.DisableInputs();
            }
        }
    }

    private void CloseSubMenus(MiniGameMenu exception)
    {
        if (mapImage != exception)
        {
            mapImage.Close();
        }
        if (inventoryObject != exception)
        {
            inventoryObject.Close();
        }
    }

    public void CloseAllMenus()
    {
        mapImage.Close();
        inventoryObject.Close();
        currentMenu = OpenMenu.None;

    }

    public void ToggleEmotionSelection()
    {
        if (currentMenu == OpenMenu.None && !inputBlocked || currentMenu == OpenMenu.Emotion && !inputBlocked) {
            if (!emotionSelect.IsSelecting)
            {
                currentMenu = OpenMenu.Emotion;
                emotionSelect.StartSelection();

            }
            else
            {
                emotionSelect.CancelSelection();
                currentMenu = OpenMenu.None;
                emotionTimer = 0;
            }
        }
    }

    public void NeutralizeEmotion(float timer)
    {
        if(timer == 0)
        {
            Debug.Log("neutralize");
            face.Display(EmotionType.Neutral);
            background.Display(EmotionType.Neutral);
            frame.Display(EmotionType.Neutral);
            vignette.ResetVignette();
        }
        else
        {
            emotionTimer = 0;

        }
    }

    public void EmotionBlockToggle()
    {
        if (currentMenu == OpenMenu.Emotion)
        {
            currentMenu = OpenMenu.None;
        }
        else if (currentMenu == OpenMenu.None)
        {
            currentMenu = OpenMenu.Emotion;
        }
        //Debug.Log("EmotionBlock =" + IsInputBlocked());
    }

    public void UnBlockEmotion()
    {
        currentMenu = OpenMenu.None;
    }

    public bool IsInputBlocked()
    {
        return currentMenu == OpenMenu.Emotion;
    }

    public void BlockAllInputs()
    {
        inputBlocked = true;
    }

    public void UnBlockAllInputs()
    {
        inputBlocked = false;
    }
}
