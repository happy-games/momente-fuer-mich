using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuperMentiMessage : MonoBehaviour
{
    public Text message;
    public GameObject speachBubble;
    public Image mentiFace;
    public Animator windowAnim;
    public bool isDisplayingMessage = false;
    public float messageTime;
    private float timer = -1;


    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(timer >= 0)
        {
            timer += Time.deltaTime;
            if(timer >= messageTime)
            {
                timer = -1;
                EndMessage();
            }
        }
    }

    public void DisplayMessage(string text)
    {
        Debug.Log("Display Menti");
        isDisplayingMessage = true;
        this.gameObject.SetActive(true);
        windowAnim.SetTrigger("open");
        message.text = text;
        timer = 0;
    }

    public void EndMessage()
    {
        Debug.Log("End Menti");
        windowAnim.SetTrigger("close");
    }

    public void DeactivateObject()
    {
        Debug.Log("Deactiv Menti");
        this.gameObject.SetActive(true);
        isDisplayingMessage = false;
    }
}
