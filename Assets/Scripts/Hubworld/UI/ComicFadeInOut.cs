using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComicFadeInOut : MonoBehaviour
{
    public RectTransform mask,canvas;
    public Transform playerFace;
    public Animator anim;


    private void Update()
    {
        FadeIn();
    }
    public void FadeIn()
    {
        Vector2 ViewportPosition = Camera.main.WorldToViewportPoint(playerFace.transform.position);
        Vector2 WorldObject_ScreenPosition = new Vector2(
        ((ViewportPosition.x * canvas.sizeDelta.x) - (canvas.sizeDelta.x * 0.5f)),
        ((ViewportPosition.y * canvas.sizeDelta.y) - (canvas.sizeDelta.y * 0.5f)));

        //now you can set the position of the ui element
        mask.anchoredPosition = WorldObject_ScreenPosition;
        anim.SetTrigger("FadeIn");

    }
    public void FadeOut()
    {
        Vector2 ViewportPosition = Camera.main.WorldToViewportPoint(playerFace.transform.position);
        Vector2 WorldObject_ScreenPosition = new Vector2(
        ((ViewportPosition.x * canvas.sizeDelta.x) - (canvas.sizeDelta.x * 0.5f)),
        ((ViewportPosition.y * canvas.sizeDelta.y) - (canvas.sizeDelta.y * 0.5f)));

        //now you can set the position of the ui element
        mask.anchoredPosition = WorldObject_ScreenPosition;
        anim.SetTrigger("FadeOut");

    }
}
