using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class OkCancelWindow : MonoBehaviour
{
    public static bool someWindowOpen;
    public GameObject Content;
    public Text textfield;
    public Button accept, cancel;
    [HideInInspector]
    public bool isOpen = false;
    public bool closeOnCancel, closeOnAccept = true;

    [SerializeField, Tooltip("Called when the window opens up.")]
    protected UnityEvent onPopUp = default;

    [SerializeField, Tooltip("Called when the accept button is pressed.")]
    public  UnityEvent onAccept = default;

    [SerializeField, Tooltip("Called when the cancel button is pressed.")]
    public  UnityEvent onCancel = default;

    [SerializeField, Tooltip("Called when the window is closed.")]
    protected UnityEvent onClosed = default;

    [SerializeField, Tooltip("Called when an error occurred.")]
    protected UnityEvent onError = default;


    // Start is called before the first frame update
    void Start()
    {
        someWindowOpen = false;
        Content.SetActive(false);

        accept.onClick.AddListener(delegate () { Accept(); });
        cancel.onClick.AddListener(delegate () { Cancel(); });
    }


    public void PopUp()
    {
        someWindowOpen = true;
        isOpen = true;
        Hubworld.Instance.DisableInputs();
        onPopUp.Invoke();
        Content.SetActive(true);
        GetComponent<Animator>().SetTrigger("PopUp");
    }

    public void PopDown()
    {
        GetComponent<Animator>().SetTrigger("PopDown");
    }

    public void Cancel()
    {
        onCancel.Invoke();
        if (closeOnCancel)
        {
            GetComponent<Animator>().SetTrigger("PopDown");
        }
    }

    private void Close()
    {
        onClosed.Invoke();
        Content.SetActive(false);
        someWindowOpen = false;
        isOpen = false;
        Hubworld.Instance.EnableInputs();
    }

    protected virtual void Accept()
    {
        Debug.Log("Doing the Thing");
        if (closeOnAccept)
        {
            GetComponent<Animator>().SetTrigger("PopDown");
        }
        onAccept.Invoke();
        
    }

    public void Error()
    {
        textfield.color = Color.red;
        GetComponent<Animator>().SetTrigger("Shake");
        onError?.Invoke();
    }

    protected void ErrorEnd()
    {
        textfield.color = Color.black;
    }

}
