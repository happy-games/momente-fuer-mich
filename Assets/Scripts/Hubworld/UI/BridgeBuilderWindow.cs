using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BridgeBuilderWindow : OkCancelWindow
{
    public int cost;
    public CollectibleManager collectibleManager;

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void Accept()
    {
        if(collectibleManager.GetCollected() > cost)
        {
            collectibleManager.PayCollects(cost);
            base.Accept();
        } else
        {
            Error();
        }
    }
}
