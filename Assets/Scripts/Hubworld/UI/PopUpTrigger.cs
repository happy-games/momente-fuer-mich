using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PopUpTrigger : MonoBehaviour
{

    public double rangeToPlayer;
    public OkCancelWindow popUpWindow;

    private void OnMouseOver()
    {

    }

    public void OnMouseDown()
    {
        if (!Hubworld.Instance.IsInputBlocked() && !OkCancelWindow.someWindowOpen && !EventSystem.current.IsPointerOverGameObject())
        {
            Debug.Log("Mousedown");
            if (Vector3.Distance(Player.Instance.transform.position, transform.position) < rangeToPlayer)
            {
                popUpWindow.PopUp();
            }
        }
    }

}
