using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialVisuals : MonoBehaviour
{
    public GameObject[] visuals;

    // Start is called before the first frame update
    void Start()
    {
        foreach(GameObject v in visuals)
        {
            v.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
