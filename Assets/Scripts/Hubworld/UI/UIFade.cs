using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIFade : MonoBehaviour
{
    public enum UIELEMENTS {Text, Image, TextMeshPro};
    public UIELEMENTS element;
    public UnityEvent OnFadeIn = new UnityEvent();
    public UnityEvent OnFadeOut = new UnityEvent();
    public float TimeToFade = 0.3f;
    private float fadeTimer = -1f;
    private float startAlpha, alpha = 0, step = 0;
    private Image img;
    private TMP_Text tmp;
    private Text txt;
    private Button btn;
    public float onFadeInDelay = 0, onFadeOutDelay = 0;
    public bool fadeOnAwake = false;

    void Start()
    {
        if(element == UIELEMENTS.Text)
        {
            txt = this.gameObject.GetComponent<Text>();
            startAlpha = txt.color.a;
        } else if(element == UIELEMENTS.Image)
        {
            img = this.gameObject.GetComponent<Image>();
            startAlpha = img.color.a;
        }
        else if (element == UIELEMENTS.TextMeshPro)
        {
            tmp = this.gameObject.GetComponent<TMP_Text>();
            startAlpha = tmp.color.a;
        }

        if (fadeOnAwake)
        {
            FadeIn();
        }
        else
        {
            if (element == UIELEMENTS.Text)
            {
                txt.color = new Color(txt.color.r, txt.color.g, txt.color.b, 0);
            }
            else if (element == UIELEMENTS.Image)
            {
                img.color = new Color(img.color.r, img.color.g, img.color.b, 0);
            }
            else if (element == UIELEMENTS.TextMeshPro)
            {
                tmp.color = new Color(tmp.color.r, tmp.color.g, tmp.color.b, 0);
            }
        }
    }

    void Update()
    {
        if (fadeTimer >= 0)
        {
            fadeTimer += Time.deltaTime;
            if (element == UIELEMENTS.Text)
            {
                txt.color = new Color(txt.color.r, txt.color.g, txt.color.b, alpha);
            }
            else if (element == UIELEMENTS.Image)
            {
                img.color = new Color(img.color.r, img.color.g, img.color.b, alpha);
            }
            else if (element == UIELEMENTS.TextMeshPro)
            {
                tmp.color = new Color(tmp.color.r, tmp.color.g, tmp.color.b, alpha);
                
            }

            alpha += step * Time.deltaTime;

            if (fadeTimer >= TimeToFade)
            {
                if(alpha >= startAlpha)
                {
                    fadeTimer = -1;
                    StartCoroutine(FinishedFadeIn());
                }
                else if (alpha <= 0)
                {
                    fadeTimer = -1;
                    StartCoroutine(FinishedFadeOut());
                }
            }
        }
    }

    IEnumerator FinishedFadeIn()
    {
        yield return new WaitForSeconds(onFadeInDelay);
        OnFadeIn.Invoke();
    }

    IEnumerator FinishedFadeOut()
    {
        yield return new WaitForSeconds(onFadeOutDelay);
        OnFadeOut.Invoke();
    }

    public void FadeIn()
    {
        Debug.Log("FadeIn" + startAlpha);
        fadeTimer = 0;
        step = startAlpha / TimeToFade;
        alpha = 0;

    }

    public void FadeOut()
    {
        fadeTimer = 0;
        step = (alpha / TimeToFade) * -1;
        alpha = startAlpha;

    }

    
}
