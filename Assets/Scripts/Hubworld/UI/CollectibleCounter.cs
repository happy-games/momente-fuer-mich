using UnityEngine;
using UnityEngine.UI;

public class CollectibleCounter : MonoBehaviour
{
    public Text collectileCount;
    public float visibleTime, fadeTime;
    private float timer = -1, fadeTimer = -1;
    public Image coin;

    private void Update()
    {
        if(timer >= 0)
        {
            timer += Time.deltaTime;
            if(timer >= visibleTime)
            {
                fadeTimer = 0;
                timer = -1;
            }
        }

        if(fadeTimer >= 0)
        {
            fadeTimer += Time.deltaTime;
            float progress = fadeTimer / fadeTime; //collectileCount.color.a - (Time.deltaTime / fadeTime);
            collectileCount.color = EditAlpha(collectileCount.color, 1-progress); //new Color(collectileCount.color.r, collectileCount.color.g, collectileCount.color.b, );
            coin.color = EditAlpha(coin.color, 1-progress); //new Color(collectileCount.color.r, collectileCount.color.g, collectileCount.color.b, collectileCount.color.a-(Time.deltaTime/fadeTime));
            if (fadeTimer >= fadeTime)
            {
                fadeTimer = -1;
                gameObject.SetActive(false);
                
            }
        }
    }

    public void ShowCollectibles()
    {
        timer = -1;
        fadeTimer = -1;
        gameObject.SetActive(true);
        collectileCount.color = new Color(collectileCount.color.r, collectileCount.color.g, collectileCount.color.b, 1);
        coin.color = Color.white;
        
    }

    public void StartTimer()
    {
        timer = 0;
        fadeTimer = -1;
    }

    public void SetCollectibleCounter(int collectibles)
    {
        collectileCount.text = collectibles.ToString();
    }

    private Color EditAlpha(Color original, float alpha)
    {
        original.a = alpha;
        return original;
    }
}
