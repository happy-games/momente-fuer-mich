using HappyGames.Data;
using HappyGames.Emotion;
using System.Collections;
using System.Collections.Generic;
using HappyGames.Translations;
using UnityEngine;


public class DialogStartWithEmotionSelect : DialogueStart
{
    public EmotionType emotion;
    public bool autoStart = true;
    protected bool played = false;
    private TranslatableCollection<string> mentiMessage = new TranslatableCollection<string>
    (
        "Versuche es nocheinmal",
        "Try Again"
    );

    private void Update()
    {
        if (active && !played)
        {
            played = true;
            StartDialog();
        }
    }

    public void subscribeToEmotionSelect()
    {
        Hubworld.Instance.emotionSelection.OnSelectionEnd += FinishDialog;
        Hubworld.Instance.emotionSelection.OnSelectionCanceled += FinishDialog;

    }
    public void unSubscribeToEmotionSelect()
    {
        Hubworld.Instance.emotionSelection.OnSelectionEnd -= FinishDialog;
        Hubworld.Instance.emotionSelection.OnSelectionCanceled -= FinishDialog;

    }

    public void FinishDialog(EmotionType emotion)
    {

        if (emotion == this.emotion)
        {
            FinishDialog();
        } 
        else
        {
            Hubworld.Instance.DisplayMentiMessage(mentiMessage.GetValue(ProfileManager.GetActiveProfile().Settings.Language));
        }
    }
}
