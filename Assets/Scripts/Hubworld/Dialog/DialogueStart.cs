using HappyGames.Data;
using HappyGames.Emotion;
using UnityEngine;
using UnityEngine.Events;

public class DialogueStart : MonoBehaviour
{
    public string nodeName;
    public DialogueStart nextNode;
    [HideInInspector]
    public StoryElement storyIndex;
    protected bool active;
    public GameObject sprite;
    public bool noAdditionalActions, spriteStays = false;
    protected bool dialogEnded = false;
    private bool movementNode = false;

    public UnityEvent OnDialogStart;
    public UnityEvent OnDialogEnd;
    public UnityEvent OnFinishedThingToDo;


    private void Start()
    {
        OnDialogEnd.AddListener(setDialogEnded);
        if (noAdditionalActions)
        {
            OnDialogEnd.AddListener(FinishDialog);
        }
    }

    private void Update()
    {
    }

    public void StartDialog()
    {
        if (active)
        {
            Debug.Log("starting" + nodeName);
            StoryManager.Instance.startDialogue(nodeName);
            StoryManager.Instance.currentDialog = this;
            OnDialogStart.Invoke();
            if (nodeName != "Tutorial_3")
            {
                Player.Instance.SetDestination(Player.Instance.transform.position);
            }
        }
    }


    public void FinishDialog()
    {
        if (StoryManager.Instance.currentDialog == this && active && dialogEnded)
        {
            if (nextNode != null)
            {
                nextNode.Activate();
            }
            else
            {
                StoryManager.Instance.storyBlock = false;
            }

            OnFinishedThingToDo.Invoke();
            Debug.Log("Finish" + nodeName);
            Deactivate();
        }
    }
    public void SaveFinishedDialog()
    {
        ProfileManager.GetActiveProfile().SetStoryState(storyIndex, StoryState.Completed);
        //StoryManager.Instance.currentDialog = null;
        Debug.Log("saved "+storyIndex.ToString());
    }


    public void Activate()
    {
        if (sprite != null)
        {
            sprite.SetActive(true);
        }
        active = true;  
    } 
    public void Deactivate()
    {
        if (sprite != null && !spriteStays)
        {
            sprite.SetActive(false);

        }
        active = false;
    }

    public void subscribeToMovement()
    {
        movementNode = true;
        Hubworld.Instance.OnMoveInput.AddListener(FinishDialog);
    }

    public void unSubscribeToMovement()
    {
        Hubworld.Instance.OnMoveInput.RemoveListener(FinishDialog);
    }


    public bool HasListenersOnEnd()
    {
        return OnDialogEnd.GetPersistentEventCount() != 0;
    }

    private void setDialogEnded()
    {
        dialogEnded = true;
        if (noAdditionalActions)
        {
            FinishDialog();
        }
    }

    public bool isMoveNode()
    {
        return movementNode;
    }
}

