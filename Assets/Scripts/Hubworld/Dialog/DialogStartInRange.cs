using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogStartInRange : DialogueStart
{
    public float range = 4f;
    protected bool played = false;

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            if (InRange(Player.Instance.transform.position) && !played)
            {
                played = true;
                StartDialog();
            }
        }
    }


    protected bool InRange(Vector3 player)
    {
        if (Vector3.Distance(transform.position, player) < range)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
