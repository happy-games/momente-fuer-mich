using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogStartAuto : DialogueStart
{

    public bool autoStart = true;
    protected bool played = false;


    // Update is called once per frame
    void Update()
    {
        if (active && !played)
        {
            played = true;
            StartDialog();
        }
    }
}
