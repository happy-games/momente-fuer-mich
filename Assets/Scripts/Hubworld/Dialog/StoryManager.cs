using HappyGames.Data;
using HappyGames.Emotion;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Yarn.Unity;

public class StoryManager : MonoBehaviour
{
    public static StoryManager Instance;

    private StoryElement[] beforeHappyCollected = { StoryElement.Intro, StoryElement.Menu , StoryElement.Happy};
    private StoryElement[] beforeSadCollected = {StoryElement.Map, StoryElement.EmotionJump};
    private StoryElement[] beforeAngryCollected = { StoryElement.Sad, StoryElement.Interactable, StoryElement.Bridge, StoryElement.Backpack, StoryElement.Island2, StoryElement.Fruitbasket };
    private StoryElement[] beforeSurprisedCollected = {StoryElement.Angry, StoryElement.Island3, StoryElement.SimonSays };
    private StoryElement[] beforeEnd = { StoryElement.Surprised, StoryElement.End };

    public DialogueRunner runner;
    public DialogueStart[] dialogsStartNodes;
    [HideInInspector]
    public DialogueStart currentDialog = null;
    [HideInInspector]
    public bool storyBlock = false;

    protected UserProfile profile;
    public bool TURNOFF;
    public int DebugStoryPart = -1;

    public GameObject map, backpack, supermenti, wind, vulcan, angry, sad, surprised;
    public PostproFade postRain, postVulcan, postWind;

    private void Awake()
    {
        Instance = this;
        profile = ProfileManager.GetActiveProfile();


        profile.SetStoryPart(DebugStoryPart != -1 ? DebugStoryPart : profile.StoryPart);
        

        NextStoryPart();


        if (profile.GetStoryState(StoryElement.Map) != StoryState.Completed && !TURNOFF)
        {
            map.SetActive(false);
        }
        if (profile.GetStoryState(StoryElement.Bridge) != StoryState.Completed && profile.StoryPart < 2 && !TURNOFF)
        {
            backpack.SetActive(false);
        }
        if (profile.StoryPart >= 0)
        {
            Destroy(supermenti);
        }
        if (profile.StoryPart >= 1)
        {
            Destroy(sad);
        }
        if (profile.GetStoryState(StoryElement.Island2) == StoryState.Completed && profile.GetStoryState(StoryElement.Fruitbasket) != StoryState.Completed)
        {
            //vulcan is active
        }
        else
        {
            angry.SetActive(false);
            vulcan.SetActive(false);
        }
        if (profile.GetStoryState(StoryElement.Island3) == StoryState.Completed && profile.GetStoryState(StoryElement.SimonSays) != StoryState.Completed)
        {
            //wind is active
        } else { 
            wind.SetActive(false);
            surprised.SetActive(false);
        }

    }


    public void startDialogue(string name)
    {
            storyBlock = true;
            runner.StartDialogue(name);
    }

    public void endDialog()
    {
        if (currentDialog != null)
        {
            Debug.Log("ending Dialog" + currentDialog.nodeName);
            currentDialog.OnDialogEnd.Invoke();
            if (currentDialog.nextNode == null || currentDialog.isMoveNode())
            {
                storyBlock = false;
            }
        }
    }

    public void NextStoryPart()
    {
        UserProfile profile = ProfileManager.GetActiveProfile();
        int StoryPart = profile.StoryPart;
        Debug.Log("STORYPART = " + StoryPart);
        StoryElement[] elementArray = StoryPart == -1 ? beforeHappyCollected : StoryPart == 0 ? beforeSadCollected : StoryPart == 1 ? beforeAngryCollected : StoryPart == 2 ? beforeSurprisedCollected : StoryPart == 3 ? beforeEnd : null;
        foreach (StoryElement elmt in elementArray)
        {
            if (profile.GetStoryState(elmt) == StoryState.Inactive)
            {
                Debug.Log(elmt.ToString() +" activated");
                profile.SetStoryState(elmt, StoryState.Active);
            }
        }

        for (int i = 0; i < dialogsStartNodes.Length; i++)
        {
            dialogsStartNodes[i].storyIndex = (StoryElement)i;

            if (profile.GetStoryState((StoryElement)i) == StoryState.Active && !TURNOFF)
            {
                dialogsStartNodes[i].Activate();
            }
            else
            {
                dialogsStartNodes[i].Deactivate();

            }
        }
    }

    public void UnlockNextEmotion()
    {
        profile = ProfileManager.GetActiveProfile();
        if (!profile.HasEmotionUnlocked(EmotionType.Happy))
        {
            profile.UnlockEmotion(EmotionType.Happy);
        } 
        else if (!profile.HasEmotionUnlocked(EmotionType.Sad))
        {
            profile.UnlockEmotion(EmotionType.Sad);
            foreach (FlowerPot i in FindObjectsOfType<FlowerPot>())
            {
                i.Unlock();
            }
        }
        else if (!profile.HasEmotionUnlocked(EmotionType.Angry))
        {
            profile.UnlockEmotion(EmotionType.Angry);
            foreach (TableFruitbasket i in FindObjectsOfType<TableFruitbasket>())
            {
                i.Unlock();
            }
        }
        else if (!profile.HasEmotionUnlocked(EmotionType.Surprised))
        {
            profile.UnlockEmotion(EmotionType.Surprised);
            foreach (BalloonPost i in FindObjectsOfType<BalloonPost>())
            {
                i.Unlock();
            }
        }

        foreach(Interactable i in FindObjectsOfType<Interactable>())
        {
            i.CheckIfUnlocked();
        }
    }

    public void AdvanceStoryPart()
    {
        UserProfile profile = ProfileManager.GetActiveProfile();
        profile.SetStoryPart(profile.StoryPart + 1);
    }
}

public enum StoryElement 
{ 
    Intro,
    Menu, 
    Happy, 
    Map,
    EmotionJump,
    Sad, 
    Interactable,
    Bridge,
    Backpack,
    Island2,
    Fruitbasket, 
    Angry, 
    Island3, 
    SimonSays, 
    Surprised, 
    End 
};

public enum StoryState
{
    /// <summary>
    /// The story is locked at the current state and will be unlocked at a later time.
    /// </summary>
    Inactive,
    /// <summary>
    /// The story is currently unlocked and can be played.
    /// </summary>
    Active,
    /// <summary>
    /// The story was completed and can no longer be played.
    /// </summary>
    Completed
}
