using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Frog : MonoBehaviour
{
    [SerializeField, Tooltip("the animator of the frog")]
    public Animator animator;

    [SerializeField, Tooltip("probabilities for animation triggers")]
    public int probabilityEat, probabilityWink, probabilityQuak;

    /// <summary>
    /// randomly sets the state of the frog animation according to the probabilities
    /// </summary>
    public void SetState()
    {
        int r = Random.Range(0, 100);
        int max = Mathf.Max(probabilityWink, probabilityEat); 
        int min = Mathf.Min(probabilityWink, probabilityEat);
        int state;

        if(r < min)
        {
            state = min == probabilityEat ? 2 : 1;
            animator.SetInteger("state", state);
        }
        else if(r < max)
        {
            state = max == probabilityEat ? 2 : 1;
            animator.SetInteger("state", state);
        }
        else
        {
            animator.SetInteger("state", 0);
        }
        if (r < probabilityQuak)
        {
            GetComponent<AudioSource>().Play();
        }
    }
}
