using HappyGames.Data;
using UnityEngine;
using UnityEngine.Events;

public class CollectibleManager : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("Called when collected item amount changes.")]
    protected UnityEvent<int> onCollectedChange = default;


    // --- | Variables | ---------------------------------------------------------------------------------------------------

    protected Inventory inventory;


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    private void Awake()
    {
        // Switch to the active profile.
        SwitchInventory(null, ProfileManager.GetActiveProfile());
        // Add listener for profile changes.
        ProfileManager.OnProfileActivated += SwitchInventory;
    }

    private void OnDestroy()
    {
        // Remove listener for profile changes.
        ProfileManager.OnProfileActivated -= SwitchInventory;
        // Switch back to no inventory.
        SwitchInventory(ProfileManager.GetActiveProfile(), null);
    }

    /// <summary>
    /// Returns the amount of resources collected by the player.
    /// </summary>
    /// <returns></returns>
    public int GetCollected() => inventory.ResourceCount;

    /// <summary>
    /// Adds one resource to the inventory.
    /// </summary>
    public void CollectOne() => CollectAmount(1);

    /// <summary>
    /// Adds resources to the inventory.
    /// </summary>
    /// <param name="amount">The amount of resources to add.</param>
    public void CollectAmount(int amount) => inventory.AddResources(amount);

    /// <summary>
    /// Removes resouces from the players inventory.
    /// </summary>
    /// <param name="cost">The amount of resources to remove.</param>
    public void PayCollects(int cost) => inventory.RemoveResources(cost);

    /// <summary>
    /// Switches the inventory from one user to another. Also registers the <see cref="Inventory.OnResourceCountUpdate"/> event.
    /// </summary>
    /// <param name="oldProfile">The previous profile.</param>
    /// <param name="newProfile">The new profile.</param>
    private void SwitchInventory(UserProfile oldProfile, UserProfile newProfile)
    {
        // Check if the inventory was set.
        if (inventory != null)
        {
            // Remove the listener for resource changes.
            inventory.OnResourceCountUpdate -= onCollectedChange.Invoke;
        }
        // Set the new inventory.
        inventory = newProfile?.Inventory;
        // Check if there is a new inventory.
        if (inventory != null)
        {
            // Add the listener for resource changes.
            inventory.OnResourceCountUpdate += onCollectedChange.Invoke;
            // Call the event to signal the current resource count.
            onCollectedChange?.Invoke(inventory.ResourceCount);
        }
    }
}
