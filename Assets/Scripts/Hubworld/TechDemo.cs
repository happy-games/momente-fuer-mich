using HappyGames.Emotion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TechDemo : MonoBehaviour
{
    public EmotionSelect manager;
    public GameObject text;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("TechDemo");
            manager.StartSelection();
            text.SetActive(false);
        }

        Debug.Log(manager.Selection);

        if(manager.IsSelecting == false)
        {
            text.SetActive(true);
        }
    }
}
