using HappyGames.Controller;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Script that can perform Camera Pans to a target and back to the Player with a given CameraController and Player in scene
/// </summary>
public class CameraMove : MonoBehaviour
{
    [SerializeField, Tooltip("the camera controller script")]
    public CameraController cameraController;

    [SerializeField, Tooltip("Events that get triggered if the camera reaches the target or the player")]
    public UnityEvent OnReached, OnBackToPlayer;

    [SerializeField, Tooltip("the duration to and from target in seconds")]
    public float durationTo, durationBack;

    /// <summary>
    /// the transform of the target position
    /// </summary>
    public Transform target;

    /// <summary>
    /// starts the pan to the target
    /// </summary>
    public void  MoveToTarget()
    {
        cameraController.MoveToTarget(target, durationTo, OnReached.Invoke);
    }

    /// <summary>
    /// starts the pan to the player
    /// </summary>
    public void MoveBackToPlayer()
    {
        cameraController.MoveToTarget(Hubworld.Instance.player.transform, durationBack, OnBackToPlayer.Invoke);

    }
}
