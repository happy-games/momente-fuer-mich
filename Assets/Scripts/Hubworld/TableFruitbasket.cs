using HappyGames.Data;
using HappyGames.Emotion;
using HappyGames.Translations;
using HappyGames.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TableFruitbasket : MonoBehaviour
{
    [SerializeField, Tooltip("unique index of each table")]
    public int INDEX;

    [SerializeField, Tooltip("canvas that displays the score")]
    public WorldCanvas scoreDisplay;

    [SerializeField, Tooltip("renderer of the table and the fruitbasket on top")]
    public SpriteRenderer tableSprite, fruitBasketSprite;

    [SerializeField, Tooltip("set containing sprites of all trophytypes")]
    public RankSpriteSet fruitBasketSet;

    [SerializeField, Tooltip("the UI panel that shows all trophys")]
    public TrophyPanel panel;

    [SerializeField, Tooltip("the close button of the trophy panel")]
    public Button closeButton;

    [SerializeField, Tooltip("the throphy UIs gameObject")]
    public GameObject trophyUI;

    [SerializeField, Tooltip("the PopUpwindow containing an error message")]
    public OkCancelWindow errorMessage;

    [SerializeField, Tooltip("range from where this pot can be clicked")]
    public float range;

    public ParticleSystem place;

    // outline material of this table
    private Material outline;
    public Material defaultMaterial;
    const string SHADER_WIDTH = "_Size", SHADER_COLOR = "_Color", SHADER_SHIMMER = "_Intensity";
    [SerializeField, Tooltip("with of the outline shader")]
    public float inRangeWidth = 3f, onMouseWidth = 7f;
    [SerializeField, Tooltip("color of the outline shader")]
    public Color inRangeColor, onHoverColor;
    private bool outlineIsSet = false;
    private bool unlocked = false;

    //currently stored trophy of type FruitBasket
    private FruitBasketTrophy fruitBasket;

    private UserProfile profile = null;
    private TranslatableCollection<string> pointsLabel = new TranslatableCollection<string>("Punkte", "Points");

    private TranslatableCollection<string> unknownEmotionMessage = new TranslatableCollection<string>
(
   "Finde zuerst Wut im Früchtekorb-Spiel.",
   "Find Angry in the Fruitbasket-game fist."
);

    private void OnEnable()
    {
        SwitchProfile(profile, ProfileManager.GetActiveProfile());
    }

    private void OnDisable()
    {
        SwitchProfile(profile, null);
    }

    // Start is called before the first frame update
    void Start()
    {
        outline = tableSprite.material;
        CheckIfUnlocked();
        
        //check if trophy is in the pot in save file
        if(ProfileManager.GetActiveProfile().MapState.GetSavedTrophyDisplay(INDEX) != null)
        {
            AddFruitBasket(ProfileManager.GetActiveProfile().MapState.GetSavedTrophyDisplay(INDEX));

        }
    }

    public void CheckIfUnlocked()
    {
        if (ProfileManager.GetActiveProfile().HasEmotionUnlocked(EmotionType.Angry))
        {
            unlocked = true;
            tableSprite.material = outline;
        }
        else
        {
            unlocked = false;
            tableSprite.material = defaultMaterial;
        }
    }


    public void Unlock()
    {
        unlocked = true;
        tableSprite.material = outline;
    }

    // Update is called once per frame
    void Update()
    {
        if (unlocked)
        {
            //update outline
            if (!outlineIsSet && InRange(Hubworld.Instance.player.transform.position))
            {

                SetOutline(inRangeWidth, inRangeColor);
                outlineIsSet = true;
            }
            else if (outlineIsSet && !InRange(Hubworld.Instance.player.transform.position))
            {

                ResetOutline();
                outlineIsSet = false;
            }
            //update blocks in world
            if (trophyUI.activeSelf)
            {
                if (!Hubworld.Instance.IsInputBlocked())
                {
                    Hubworld.Instance.DisableInputs();
                }
                if (!UIManager.Instance.IsInputBlocked())
                {
                    UIManager.Instance.EmotionBlockToggle();
                }
            }
        }
    }

    /// <summary>
    /// opens the Trophy UI or the error message if this table is clicked and the player is in range
    /// </summary>
    private void OnMouseOver()
    {
        
            if (Input.GetMouseButtonDown(0) && InRange(Hubworld.Instance.player.transform.position) && !EventSystem.current.IsPointerOverGameObject())
            {
                if (unlocked)
                {
                    if (fruitBasket != null)
                    {
                        errorMessage.PopUp();
                        errorMessage.onAccept.AddListener(SwapFruitBasket);
                        errorMessage.onCancel.AddListener(RemoveErrorListener);
                    }
                    else
                    {
                        OpenFruitBasketTrophyUI();
                    }
                }
                else
                {
                        Hubworld.Instance.DisplayMentiMessage(unknownEmotionMessage.GetValue(ProfileManager.GetActiveProfile().Settings.Language));
                    
                }
            }
    }

    /// <summary>
    /// updates the outline and opens the score display
    /// </summary>
    private void OnMouseEnter()
    {
            if (unlocked && InRange(Hubworld.Instance.player.transform.position))
            {
                SetOutline(onMouseWidth, onHoverColor);

                if (fruitBasket != null)
                {
                    scoreDisplay.gameObject.SetActive(true);
                }
            }
        
    }

    /// <summary>
    /// closes the score display and updates the outline
    /// </summary>
    private void OnMouseExit()
    {
        if (unlocked)
        {
            scoreDisplay.gameObject.SetActive(false);

            if (InRange(Hubworld.Instance.player.transform.position))
            {
                SetOutline(inRangeWidth, inRangeColor);

            }
            else
            {
                ResetOutline();
            }
        }
    }

    /// <summary>
    /// resets the outline to default
    /// </summary>
    private void ResetOutline()
    {
        outline.SetFloat(SHADER_WIDTH, 0f);
    }

    /// <summary>
    /// sets the outline of this material
    /// </summary>
    /// <param name="width"></param> with of the outline
    /// <param name="color"></param> color of the outline
    private void SetOutline(float width, Color color)
    {
        outline.SetFloat(SHADER_WIDTH, width);
        outline.SetColor(SHADER_COLOR, color);
    }

    /// <summary>
    /// checks if the player is in range of this table
    /// </summary>
    private bool InRange(Vector3 player)
    {
        if (Vector3.Distance(transform.position, player) < range)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// puts the current trophy back in the inventory and opens the Trophy UI
    /// </summary>
    private void SwapFruitBasket()
    {
        profile.Inventory.AddTrophy(fruitBasket);
        profile.MapState.SaveTophyDisplayContent(INDEX, null);
        fruitBasket = null;
        fruitBasketSprite.sprite = null;
        scoreDisplay.ChangeText("");
        RemoveErrorListener();
        OpenFruitBasketTrophyUI();
    }

    /// <summary>
    /// opens the Trophy UI
    /// </summary>
    private void OpenFruitBasketTrophyUI()
    {
        closeButton.onClick.AddListener(CloseFruitBasketTrophyUI);
        panel.OnTrophyClick += AddFruitBasket;
        trophyUI.SetActive(true);
    }
    
    /// <summary>
    /// removes the listeners from the error UI panel
    /// </summary>
    private void RemoveErrorListener()
    {
        errorMessage.onCancel.RemoveListener(RemoveErrorListener);
        errorMessage.onAccept.RemoveListener(OpenFruitBasketTrophyUI);
        errorMessage.onAccept.RemoveListener(SwapFruitBasket);
    }


    /// <summary>
    /// closes the FruitBasketTrophy UI
    /// </summary>
    public void CloseFruitBasketTrophyUI()
    {
        closeButton.onClick.RemoveListener(CloseFruitBasketTrophyUI);
        panel.OnTrophyClick -= AddFruitBasket;
        trophyUI.SetActive(false);
        UIManager.Instance.EmotionBlockToggle();
        Hubworld.Instance.EnableInputs("FlowerPot");
    }

    /// <summary>
    /// Adds a FruitBasket Trophy
    /// </summary>
    /// <param name="fruitBasket"></param> the Trophy to add to this pot
    public void AddFruitBasket(Trophy fruitBasket)
    {
        GetComponent<AudioSource>().Play();
        profile.Inventory.RemoveTrophy(fruitBasket);
        profile.MapState.SaveTophyDisplayContent(INDEX, fruitBasket);
        this.fruitBasket = (FruitBasketTrophy)fruitBasket;
        UpdateScoreDisplay();
        Debug.Log("Before Switch");
        ChangeSprite();
        Debug.Log("After Switch");
        place.Play();
        CloseFruitBasketTrophyUI();
        
    }

    /// <summary>
    /// changes the sprite of the fruit basket to the current trophy
    /// </summary>
    private void ChangeSprite()
    {
        fruitBasketSprite.sprite = fruitBasketSet.GetValue(fruitBasket.Rank);
        Debug.Log("Inside Switch");
    }

    private void SwitchProfile(UserProfile oldProfile, UserProfile newProfile)
    {
        if (oldProfile != null)
        {
            oldProfile.Settings.OnLanguageChanged -= UpdateScoreDisplay;
        }
        profile = newProfile;
        if (newProfile != null)
        {
            newProfile.Settings.OnLanguageChanged += UpdateScoreDisplay;
        }
    }

    private void UpdateScoreDisplay(Language language)
    {
        scoreDisplay.ChangeText($"{ pointsLabel.GetValue(language) }: { fruitBasket.Score }");
    }
    private void UpdateScoreDisplay() => UpdateScoreDisplay(profile.Settings.Language);
}
