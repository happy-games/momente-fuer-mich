using HappyGames.Data;
using HappyGames.Emotion;
using HappyGames.Translations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Interactable : MonoBehaviour
{
    public enum GermanEmotionType { Neutral, Wut, Freude, Trauer, Überrascht}
    public EmotionType[] reactTo;
    public CollectibleManager collectibleManager;
    public EmotionSelect selection;
    public Animator animator;
    public int giveCollectibles;
    private EmotionType state = EmotionType.Neutral;
    public float range;
    public SpriteRenderer spriteRenderer;
    private Material outline;
    public Material defaultMaterial;
    public bool oneTimeInteractable = false;
    public float resetTime;
    private float timer = -1;
    public EmotionColorSet colorset;
    private bool unlocked = false;

    const string SHADER_WIDTH = "_Size", SHADER_COLOR = "_OutlineColor", SHADER_SHIMMER = "_Intensity";
    public float inRangeWidth = 3f, onMouseWidth = 7f;
    public Color inRangeColor, onHoverColor;
    private bool outlineIsSet = false;

    [SerializeField, Tooltip("Called when the interaction is started.")]
    private UnityEvent onInteract = default;

    [SerializeField, Tooltip("Called when emotion is right")]
    private EmotionEffectEvent OnRightEmotion = default; 
    
    [SerializeField, Tooltip("Called whent emotion is wrong")]
    private EmotionEffectEvent OnWrongEmotion = default;

    [SerializeField, Tooltip("Called whent emotion is wrong")]
    private UnityEvent OnTimerRunOut = default;


    private TranslatableCollection<string> wrongEmotionMessate = new TranslatableCollection<string>
    (
        "Das war hier leider nicht die passende Kraft", 
        "This was not the right emotion"
    );
    private TranslatableCollection<string> unknownEmotionMessage;

    // Start is called before the first frame update
    void Start()
    {
        outline = spriteRenderer.material;
        CheckIfUnlocked();
        //Debug.Log("Color=" + reactTo[0]);
        unknownEmotionMessage = new TranslatableCollection<string>
        (
            "Schalte zuerst die " + (GermanEmotionType)((int)(reactTo[0])) + "-Kraft im Minispiel frei",
            "You have to unlock the "+reactTo[0]+"-power in the Minigame first."
        );

    }

    public void CheckIfUnlocked()
    {
        if (ProfileManager.GetActiveProfile().HasEmotionUnlocked(reactTo[0]))
        {
            spriteRenderer.material = outline;
            spriteRenderer.gameObject.GetComponent<ParticleSystem>().Play();
            inRangeColor = colorset.GetValue(reactTo[0]);
            onHoverColor = Color.white;
            unlocked = true;
        }
        else
        {
            spriteRenderer.gameObject.GetComponent<ParticleSystem>().Stop();
            spriteRenderer.material = defaultMaterial;
            unlocked = false;
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (unlocked)
        {
            if (state == EmotionType.Neutral && !outlineIsSet && InRange(Hubworld.Instance.player.transform.position))
            {

                SetOutline(inRangeWidth, inRangeColor);
                outlineIsSet = true;
            }
            else if (outlineIsSet && !InRange(Hubworld.Instance.player.transform.position))
            {

                resetOutline();
                outlineIsSet = false;
            }

            if (timer >= 0)
            {
                timer += Time.deltaTime;
                if (timer >= resetTime)
                {
                    ResetObject();
                    OnTimerRunOut.Invoke();
                }
            }
        }
    }

    private void OnMouseOver()
    {
        if (unlocked)
        {
            if (state == EmotionType.Neutral && Input.GetMouseButtonDown(0) && InRange(Hubworld.Instance.player.transform.position) && !EventSystem.current.IsPointerOverGameObject())
            {
                selection.StartSelection();
                selection.OnSelectionEnd += React;
                selection.OnSelectionCanceled += React;
                EmotionEffects.Instance.UnListen();
                onInteract?.Invoke();
            }
        } else
        {
            if(Input.GetMouseButtonDown(0) && InRange(Hubworld.Instance.player.transform.position) && !EventSystem.current.IsPointerOverGameObject())
            {
                Hubworld.Instance.DisplayMentiMessage(unknownEmotionMessage.GetValue(ProfileManager.GetActiveProfile().Settings.Language));
            }
        }
    }

    private void OnMouseEnter()
    {
        if (state == EmotionType.Neutral && unlocked)
        {
            if (InRange(Hubworld.Instance.player.transform.position))
            {
                SetOutline(onMouseWidth, onHoverColor);

            }
        }
    }

    private void OnMouseExit()
    {
        if (state == EmotionType.Neutral && unlocked)
        {
            if (InRange(Hubworld.Instance.player.transform.position))
            {
                SetOutline(inRangeWidth, inRangeColor);
            }
            else
            {
                resetOutline();
            }
        }
    }


    public virtual void React(EmotionType emotion)
    {
        Debug.Log("Reacting");
        bool foundOne = false;
        foreach (EmotionType emo in reactTo)
        {
            if (emo.CompareTo(emotion) == 0)
            {
                RightEmotionDetected(emotion);
                OnRightEmotion.Invoke(emotion, transform.position);

                foundOne = true;
            }
        }
        if (!foundOne)
        {
            OnWrongEmotion.Invoke(emotion, transform.position);
            bool hasEmotion = true;
            UserProfile profile = ProfileManager.GetActiveProfile();
            foreach (EmotionType emo in reactTo)
            {
                if (!profile.HasEmotionUnlocked(emo))
                {
                    hasEmotion = false;
                }
            }
            if (hasEmotion)
            {
                Hubworld.Instance.DisplayMentiMessage(wrongEmotionMessate.GetValue(profile.Settings.Language));
            }
            else
            {
                Hubworld.Instance.DisplayMentiMessage(unknownEmotionMessage.GetValue(profile.Settings.Language));
            }
        }
        selection.OnSelectionEnd -= React;
        selection.OnSelectionCanceled -= React;
        EmotionEffects.Instance.Listen();
    }

    private void RightEmotionDetected(EmotionType emotion)
    {
        if (animator != null)
        {
            animator.SetTrigger(emotion.ToString());
        }
            collectibleManager.CollectAmount(giveCollectibles);
            outline.SetFloat(SHADER_SHIMMER, 0f);
            timer = 0;
            UpdateState(emotion);
        
    }

    private void ResetObject()
    {
        timer = -1;
        state = EmotionType.Neutral;
        outline.SetFloat(SHADER_SHIMMER, 0f);
        animator.SetTrigger(EmotionType.Neutral.ToString());
    }

    private void resetOutline()
    {
        outline.SetFloat(SHADER_WIDTH, 0f);
    }
    
    private void SetOutline( float width, Color color)
    {
        outline.SetFloat(SHADER_WIDTH, width);
        outline.SetColor(SHADER_COLOR, color);
    }

  

    public EmotionType GetState()
    {
        return state;
    }

    protected void UpdateState(EmotionType emotion)
    {
        state = emotion;
    }

    protected bool InRange(Vector3 player)
    {
        if(Vector3.Distance(transform.position, player) < range)
        {
            return true;
        } else
        {
            return false;
        }
    }

    public void ResetInteractable()
    {
        timer = resetTime;
    }

    public void SetRange(float range)
    {
        this.range = range;
    }

}
