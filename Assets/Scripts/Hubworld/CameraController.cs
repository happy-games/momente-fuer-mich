namespace HappyGames.Controller
{
    using UnityEngine;
    using HappyGames.Utilities;
    using System.Collections;

    public class CameraController : MonoBehaviour
    {
        // --- | Inspector | ---------------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The camera to control.")]
        private Camera mainCamera = null;

        [SerializeField, Tooltip("The object to target.")]
        private Transform target = null;

        [SerializeField, Min(0f), Tooltip("The speed the camera uses to follow the player.")]
        private float speed = 20f;

        [SerializeField, Tooltip("The offset to apply when focusing the target.")]
        private Vector2 offset = Vector2.zero;

        [SerializeField, Range(0, 50), Tooltip("The area where the camera will move towards the target.")]
        private float softMarginSize = 20f;

        [SerializeField, Range(0, 50), Tooltip("The are that the target will never leave.")]
        private float hardMarginSize = 20f;

        [SerializeField]
        private AnimationCurve zoomCurve = default;

        [SerializeField, Min(0f), Tooltip("The speed of the camera shake.")]
        private float shakeSpeed = 1f;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private Vector2 targetPosition = Vector2.zero;
        private Vector2 camraPosition = Vector2.zero;
        private float zoomFactor = 0f;
        private float targetZoom = 0f;
        private Keyframe MinZoom => zoomCurve[0];
        private Keyframe MaxZoom => zoomCurve[zoomCurve.length - 1];
        private Rect softMargin;
        private Rect hardMargin;
        private Vector2 shakeOffset;
        private float shakeIntensity = 0f;
        private Coroutine cameraShake = null;
        private Coroutine cameraMove = null;
        /// <summary>
        /// True if the cameras movement is animated.
        /// </summary>
        public bool IsAnimated => cameraMove != null;


        // --- | Methods | -------------------------------------------------------------------------------------------------

#if (UNITY_EDITOR)

        // Editor ---------------------------------------------------------------------------------

        private void OnDrawGizmosSelected()
        {
            if (target)
            {
                Gizmos.color = Color.blue;
                CustomGizmos.DrawPoint2D(Application.isPlaying ? targetPosition : (Vector2)target.position + offset, Vector2.one);

            }
            if (mainCamera)
            {
                Gizmos.color = Color.black;
                Gizmos.DrawWireCube(mainCamera.transform.position, new Vector2(mainCamera.aspect * mainCamera.orthographicSize, mainCamera.orthographicSize) * ((50 - softMarginSize) * 0.04f));
                Gizmos.DrawWireCube(mainCamera.transform.position, new Vector2(mainCamera.aspect * mainCamera.orthographicSize, mainCamera.orthographicSize) * ((50 - hardMarginSize) * 0.04f));

                Gizmos.color = Color.red;
                Vector2 maxZoomSize = new Vector2(mainCamera.aspect, 1f) * MinZoom.value;
                Vector2 minZoomSize = new Vector2(mainCamera.aspect, 1f) * MaxZoom.value;
                Gizmos.DrawWireCube(mainCamera.transform.position, maxZoomSize * 2f);
                Gizmos.DrawWireCube(mainCamera.transform.position, minZoomSize * 2f);
                Gizmos.DrawLine((Vector2)mainCamera.transform.position + maxZoomSize * new Vector2(1f, 1f), (Vector2)mainCamera.transform.position + minZoomSize * new Vector2(1f, 1f));
                Gizmos.DrawLine((Vector2)mainCamera.transform.position + maxZoomSize * new Vector2(-1f, 1f), (Vector2)mainCamera.transform.position + minZoomSize * new Vector2(-1f, 1f));
                Gizmos.DrawLine((Vector2)mainCamera.transform.position + maxZoomSize * new Vector2(-1f, -1f), (Vector2)mainCamera.transform.position + minZoomSize * new Vector2(-1f, -1f));
                Gizmos.DrawLine((Vector2)mainCamera.transform.position + maxZoomSize * new Vector2(1f, -1f), (Vector2)mainCamera.transform.position + minZoomSize * new Vector2(1f, -1f));

                if (target && softMargin != null && !softMargin.Contains((Vector2)target.position + offset))
                {
                    Gizmos.color = Color.white;
                    Gizmos.DrawLine(targetPosition, (Vector2)target.position + offset);
                }
            }
        }

#endif

        private void OnEnable()
        {
            for (float i = MinZoom.time; i < MaxZoom.time; i++)
            {
                if (mainCamera.orthographicSize > zoomCurve.Evaluate(i))
                {
                    zoomFactor = i;
                }
                else
                {
                    break;
                }
            }
            targetZoom = zoomCurve.Evaluate(zoomFactor);
            if (target)
            {
                targetPosition = (Vector2)target.position + offset;
            }
        }

        private void Update()
        {
            if (target)
            {
                targetPosition = (Vector2)target.position + offset;
            }
            if (shakeIntensity != 0f)
            {
                shakeOffset = new Vector2(RandomizeShake(shakeOffset.x), RandomizeShake(shakeOffset.y));
            }

            zoomFactor = Mathf.Clamp(zoomFactor - Input.mouseScrollDelta.y, MinZoom.time, MaxZoom.time);
            Zoom(zoomCurve.Evaluate(zoomFactor));
        }

        private void LateUpdate()
        {
            CalculateMarings();

            // Update zoom.
            if (mainCamera.orthographicSize != targetZoom)
            {
                mainCamera.orthographicSize = targetZoom;
            }

            // Update position.
            if (!hardMargin.Contains(targetPosition))
            {
                Vector2 delta = targetPosition - camraPosition;
                Vector2 abs = new Vector2(Mathf.Abs(delta.x), Mathf.Abs(delta.y));
                if (abs.x / hardMargin.width * 0.5f > abs.y / hardMargin.height * 0.5f)
                {
                    delta *= (abs.x - hardMargin.width * 0.5f) / abs.x;
                }
                else
                {
                    delta *= (abs.y - hardMargin.height * 0.5f) / abs.y;
                }
                camraPosition += delta * (1f + Time.deltaTime);
            }
            else if (!softMargin.Contains(targetPosition))
            {
                camraPosition += (targetPosition - camraPosition) * speed * Time.deltaTime;
            }
            transform.position = camraPosition + shakeOffset * shakeIntensity * targetZoom * 0.1f;
        }

        /// <summary>
        /// Calculates the margin areas.
        /// </summary>
        private void CalculateMarings()
        {
            softMargin = new Rect()
            {
                size = new Vector2(mainCamera.aspect, 1f) * mainCamera.orthographicSize * (50 - softMarginSize) * 0.04f,
                center = camraPosition
            };
            hardMargin = new Rect()
            {
                size = new Vector2(mainCamera.aspect, 1f) * mainCamera.orthographicSize * (50 - hardMarginSize) * 0.04f,
                center = camraPosition
            };
        }

        /// <summary>
        /// Sets the position to target, unbinds the target object.
        /// </summary>
        /// <param name="position">The position to look at.</param>
        public void SetTargetPosition(Vector2 position)
        {
            ResetAnimation();
            target = null;
            targetPosition = position;
        }

        /// <summary>
        /// Animates the camera to move to a position.
        /// </summary>
        /// <param name="position">The position to move to.</param>
        /// <param name="duration">The time it takes to reach the position.</param>
        /// <param name="callback">Called when the target was reached.</param>
        public void MoveToPosition(Vector2 position, float duration, System.Action callback)
        {
            ResetAnimation();
            cameraMove = StartCoroutine(DoMoveToPosition(position, duration, new AnimationCurve(new Keyframe(), new Keyframe(1f, 1f)), () => { SetTargetPosition(position); callback?.Invoke(); }));
        }

        /// <summary>
        /// Animates the camera to move to a position.
        /// </summary>
        /// <param name="target">The target to move to.</param>
        /// <param name="duration">The time it takes to reach the position.</param>
        /// <param name="callback">Called when the target was reached.</param>
        public void MoveToTarget(Transform target, float duration, System.Action callback)
        {
            ResetAnimation();
            cameraMove = StartCoroutine(DoMoveToPosition(target.position, duration, new AnimationCurve(new Keyframe(), new Keyframe(1f, 1f)), () => { SetTarget(target); callback?.Invoke(); }));
        }

        /// <summary>
        /// Animates the camera to move to a position.
        /// </summary>
        /// <param name="position">The position to move to.</param>
        /// <param name="duration">The time it takes to reach the position.</param>
        /// <param name="motion">The motion of the movement.</param>
        private IEnumerator DoMoveToPosition(Vector2 position, float duration, AnimationCurve motion, System.Action callback)
        {
            float time = 0;
            Vector2 start = camraPosition;
            while (time < duration)
            {
                time += Time.deltaTime;
                yield return null;
                targetPosition = Vector2.Lerp(start, position, motion.Evaluate(time / duration));
            }
            callback?.Invoke();
            cameraMove = null;
        }

        /// <summary>
        /// Resets the animation.
        /// </summary>
        private void ResetAnimation()
        {
            if (IsAnimated)
            {
                StopCoroutine(cameraMove);
                cameraMove = null;
                if (target == null)
                {
                    SetTargetPosition(camraPosition);
                }
            }
        }

        /// <summary>
        /// Sets the target object.
        /// </summary>
        /// <param name="target">The object to target.</param>
        /// <param name="offset">The offset to add to the targets position.</param>
        public void SetTarget(Transform target, Vector2 offset)
        {
            ResetAnimation();
            this.target = target;
            targetPosition = (Vector2)target.position + offset;
        }
        /// <summary>
        /// Sets the target object.
        /// </summary>
        /// <param name="target">The object to target.</param>
        public void SetTarget(Transform target) => SetTarget(target, offset);

        /// <summary>
        /// Updates the zoom.
        /// </summary>
        /// <param name="zoom">The zoom to use.</param>
        public void Zoom(float zoom)
        {
            if (targetZoom != zoom)
            {
                if (targetZoom > zoom)
                {
                    CalculateMarings();
                    camraPosition += (targetPosition - camraPosition) * (targetZoom - zoom) / targetZoom;
                }
                targetZoom = zoom;
            }
        }

        /// <summary>
        /// Sets the camera shake.
        /// </summary>
        /// <param name="intensity">The intensity of the shake.</param>
        public void Shake(float intensity)
        {
            shakeIntensity = intensity;
            StopShakeReset();
        }
        /// <summary>
        /// Sets the camera shake for a duration.
        /// </summary>
        /// <param name="intensity">The intensity of the shake.</param>
        /// <param name="duration">The duration of the camera shake in seconds.</param>
        public void Shake(float intensity, float duration)
        {
            Shake(intensity);
            StopShakeReset();
            cameraShake = StartCoroutine(DoResetShake(duration));
        }

        /// <summary>
        /// Stops the camera shake from resetting.
        /// </summary>
        private void StopShakeReset()
        {
            if (cameraShake != null)
            {
                StopCoroutine(cameraShake);
                cameraMove = null;
            }
        }
        /// <summary>
        /// Resets the camera shake after a couple of seconds.
        /// </summary>
        /// <param name="duration">The duration of the shake.</param>
        private IEnumerator DoResetShake(float duration)
        {
            yield return new WaitForSeconds(duration);
            shakeIntensity = 0f;
            cameraShake = null;
        }

        /// <summary>
        /// Calculates a new shake value.
        /// </summary>
        /// <param name="current">The current value.</param>
        /// <returns>The modified value.</returns>
        private float RandomizeShake(float current)
        {
            return Mathf.Clamp(Random.Range(current - shakeSpeed * Time.deltaTime, current + shakeSpeed * Time.deltaTime), -1f, 1f);
        }
    }
}