using HappyGames.Emotion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.Tilemaps;
using HappyGames;
using HappyGames.UI;
using Yarn.Unity;
using HappyGames.Data;
using UnityEngine.Events;
using static StoryManager;

public class Hubworld : MonoBehaviour
{
    public static bool webcamIsActive = false;
    public static Hubworld Instance;

    public SuperMentiMessage mentiMessage;
    private bool BlockInputs = false;
    public Player player;
    public EmotionSelect emotionSelection;
    public EmotionEffects emotionEffects;
    private bool mousePressed;
    public LineRenderer lineRenderer;
    private NavMeshPath path;
    public DialogueRunner dialog;
    public UIManager MenuBar;
    public Transform beginningSpawn;
    public UnityEvent OnMoveInput;
    public ComicFadeInOut fade;
    public bool DEBUGUnlockAllEmotions = false;
    public ParticleSystem mouseclick;
    private AudioSource moveSound;

    public TrophyPanel DebugTrophyfill;
    // Start is called before the first frame update
    void Start()
    {
        moveSound = GetComponent<AudioSource>();
        UserProfile profile = ProfileManager.GetActiveProfile();
        path = new NavMeshPath();
        Instance = this;
        player.agent.enabled = false;

        if (DEBUGUnlockAllEmotions)  //DEBUG
        {
            Debug.Log("all emotions unlocked in hubworld script");
            profile.UnlockEmotion(EmotionType.Happy);
            profile.UnlockEmotion(EmotionType.Sad);
            profile.UnlockEmotion(EmotionType.Angry);
            profile.UnlockEmotion(EmotionType.Surprised);
        }

        if (!ProfileManager.GetActiveProfile().MapState.IsPositionSet)
        {
            player.transform.position = beginningSpawn.position;
            if (profile.GetStoryState(StoryElement.Intro) != StoryState.Completed)
            {
                fade.gameObject.SetActive(true);
            }
        }
        else
        {
            player.transform.position = new Vector3(profile.MapState.GetSavedPosition().x, profile.MapState.GetSavedPosition().y, 0);
        }
        player.agent.enabled = true;

        //if (!webcamIsActive)
        //{
        //    webcamIsActive = true;
        //    emotionSelection.StartSelection();  //DEBUG
        //}
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        player.Move();

       

        if (!BlockInputs && !dialog.IsDialogueRunning && !emotionSelection.IsSelecting && !StoryManager.Instance.storyBlock)
        { 
            if (!player.isMoving())
            {
                NavMesh.CalculatePath(player.transform.position, worldPosition, NavMesh.AllAreas, path);
                DrawLineOnMap(path.corners);
            }

            if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                NavMeshHit hit;
                if (NavMesh.SamplePosition(worldPosition, out hit, 1f, NavMesh.AllAreas))
                {
                    moveSound.pitch = Random.Range(0.5f, 1.8f);
                    moveSound.Play();
                    mouseclick.gameObject.transform.position = worldPosition;
                    mouseclick.Play();
                    player.SetDestination(hit.position);
                }
                mousePressed = true;
            }
            if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                if (OnMoveInput != null)
                {
                    OnMoveInput.Invoke();
                }
                ProfileManager.GetActiveProfile().MapState.SavePositzion(new Vector2(player.GetDestination().x, player.GetDestination().y));  //SAVE SPAWNPOINT
                mousePressed = false;
            }
             
            if (Input.GetKeyDown(KeyCode.F))   //DEBUG
            {
                DebugTrophyfill.DebugFillForVideo();
            }

            /*if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("Space" + BlockInputs);
                MenuBar.ToggleEmotionSelection();

            }*/

            if (Input.GetKeyDown(KeyCode.A))
            {
                if (mentiMessage.isDisplayingMessage)
                {
                    mentiMessage.EndMessage();

                }
                else
                {
                    mentiMessage.DisplayMessage("Hi Menti!");

                }

            }

        } else
        {
            EndLineDrawing();
        }


    }

    public void DisplayMentiMessage(string message)
    {
        if (mentiMessage.isDisplayingMessage)
        {
            mentiMessage.EndMessage();

        }
        mentiMessage.DisplayMessage(message);
    }

   

    public void DisableInputs(string name = "default")
    {
        //Debug.Log("Disable" + name);
        BlockInputs = true;
    }
    public void EnableInputs(string name = "default")
    {
        //Debug.Log("Enable" + name);

        BlockInputs = false;
    }

    public void ToggleInputBlock()
    {
        BlockInputs = !BlockInputs;
    }

    public bool IsInputBlocked()
    {
        return BlockInputs;
    }

    public void DrawLineOnMap(Vector3[] path)
    {
        lineRenderer.enabled = true;
        lineRenderer.SetVertexCount(path.Length);

        for (int i = 0; i < path.Length; i++)
        {
            lineRenderer.SetPosition(i, path[i]);
        }
    }

    public void EndLineDrawing()
    {
        lineRenderer.enabled = false;
    }


    public bool InRangeOfPlayer(Vector3 target, int range)
    {
        if (Vector3.Distance(target, player.transform.position) < range)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}