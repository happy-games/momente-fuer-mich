using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldCanvas : MonoBehaviour
{
    public Text text;
    public bool invisibleOnStart;
    // Start is called before the first frame update
    void Start()
    {
        if (invisibleOnStart)
        {
            this.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeText(string txt)
    {
        text.text = txt;
    }

}
