using HappyGames.Controller;
using HappyGames.Data;
using HappyGames.Emotion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EmotionEffects : MonoBehaviour
{
    public static EmotionEffects Instance;

    [SerializeField, Tooltip("EmotionSelect script thst tracks emotions")]
    public EmotionSelect emotionSelect;

    [SerializeField, Tooltip("GameObjects that contain the clouds for effects on an object")]
    public GameObject Sunbeam, Cloud, WindBlow, lightningStrike;

    [SerializeField, Tooltip("global sunray effect")]
    public Animator Sunray;

    [SerializeField, Tooltip("global rain effect")]
    public Rain rain;
    
    [SerializeField, Tooltip("vulcan effect")]
    public GameObject vulcan;

    [SerializeField, Tooltip("global wind effect")]
    public ParticleSystem shortWind;

    [SerializeField, Tooltip("camera for global earthquake effect")]
    public CameraController earthquake;

    //timer till global rain is deaktivated afer beeing activated
    private float deactivateRainTime = 5f;
    //timer for deaktivating global rain
    private float deactiveTimer = -1;


    private void Start()
    {
        Instance = this;
        Listen();

        if (ProfileManager.GetActiveProfile().StoryPart == 2)
        {
            Destroy(vulcan);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (deactiveTimer >= 0)
        {
            deactiveTimer += Time.deltaTime;
            if (deactiveTimer >= deactivateRainTime)
            {
                deactiveTimer = -1;
                rain.activateRain();
            }
        }
    }

    /// <summary>
    /// Triggers an effect of the given emotion on the object at objPosition 
    /// </summary>
    /// <param name="emotion"></param> the emotion name 
    /// <param name="objPosition"></param> the position of the object
    public void EffectOnObject(EmotionType emotion, Vector3 objPosition)
    {
        Debug.Log("Triggered");

        switch (emotion)
        {
            case EmotionType.Happy:
                Debug.Log("Instanciate happy");
                Instantiate(Sunbeam, objPosition, Sunbeam.transform.rotation);
                break;

            case EmotionType.Sad:
                Debug.Log("Instanciate sad");
                Instantiate(Cloud, objPosition, Cloud.transform.rotation);
                break;

            case EmotionType.Angry:
                Debug.Log("Instanciate angry");
                Instantiate(lightningStrike, objPosition, Cloud.transform.rotation);
                break;

            case EmotionType.Surprised:
                Debug.Log("Instanciate surprised");
                Instantiate(WindBlow, objPosition, Cloud.transform.rotation);
                break;
        }
    }

    /// <summary>
    /// activates the effect of the given emotion globally
    /// </summary>
    /// <param name="emotion"></param>
    public void EffectOnWorld(EmotionType emotion)
    {
        switch (emotion)
        {
            case EmotionType.Happy:
                Sunray.SetTrigger("shine");
                rain.deactivateRain();
                deactiveTimer = 0;
                break;

            case EmotionType.Sad:
                rain.playShortRain();
                break;

            case EmotionType.Surprised:
                shortWind.Play();
                break;

            case EmotionType.Angry:
                earthquake.Shake(0.5f, 3);
                break;
        }

    }

    /// <summary>
    /// listens to the On selectionEnd Event of the emotionselection
    /// </summary>
    public void Listen()
    {
        emotionSelect.OnSelectionEnd += EffectOnWorld;
        emotionSelect.OnSelectionCanceled += EffectOnWorld;
    }
    /// <summary>
    /// stops to listens to the On selectionEnd Event of the emotionselection
    /// </summary>
    public void UnListen()
    {
        emotionSelect.OnSelectionEnd -= EffectOnWorld;
        emotionSelect.OnSelectionCanceled -= EffectOnWorld;
    }
}

/// <summary>
/// An <see cref="UnityEvent"/> with an <see cref="EmotionType"/> as attribute.
/// </summary>
[System.Serializable]
public class EmotionEffectEvent : UnityEvent<EmotionType, Vector3> { }