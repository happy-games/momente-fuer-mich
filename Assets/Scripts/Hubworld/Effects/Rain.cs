using HappyGames.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Rain : MonoBehaviour
{
    public ParticleSystem longRain, shortRain;
    public GameObject colorGrading;
    public float clearTime, cloudTime;
    public Volume colorGradingVolume;
    public GameObject sadBlob;

    private bool rain = true;

    private void Awake()
    {
        if (ProfileManager.GetActiveProfile().StoryPart > 0)
        {
            destroyRain();
            Destroy(sadBlob);
        }
    }

    private void Update()
    {
        if (rain && colorGradingVolume.weight < 1)
        {
            colorGradingVolume.weight += Time.deltaTime * (1/cloudTime);
        } 
        else if (!rain && colorGradingVolume.weight >=0)
        {
            colorGradingVolume.weight -= Time.deltaTime * (1/clearTime);
        }
    }

    public void deactivateRain()
    {
        //var emissionM = longRain.emission;
        //emissionM.rateOverTime = 0;
        longRain?.Stop();
        rain = false;
    } 
    public void destroyRain()
    {
        Destroy(longRain);
        Destroy(colorGrading);
    }
    public void activateRain()
    {
        rain = true;
        longRain.Play();

    }

    public void playShortRain()
    {
        shortRain.Play();
    }


}
