using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationDestroy : MonoBehaviour
{
    public GameObject destroy;
    public void destroyGameObject()
    {
        Destroy(destroy);
    }
}
