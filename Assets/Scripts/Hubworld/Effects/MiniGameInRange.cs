using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGameInRange : MonoBehaviour
{
    public int triggerRange;
    public Animator anim;
    private bool isInRange = false;
    public OkCancelWindow popUpWindow;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Hubworld.Instance.InRangeOfPlayer(this.transform.position, triggerRange))
        {
            if (!isInRange)
            {
                isInRange = true;
                Debug.Log("inrange popup");
                anim.SetTrigger("PopUp");
            }

        } else if ( isInRange == true)
        {
            isInRange = false;
            Debug.Log("inrange popdown");
            anim.SetTrigger("PopDown");
        }
    }

    public void OnMouseDown()
    {
        Debug.Log("Mousedown");
        if (Hubworld.Instance.InRangeOfPlayer(this.transform.position, triggerRange) && !popUpWindow.isOpen)
        {
            popUpWindow.PopUp();
        }
    }
}
