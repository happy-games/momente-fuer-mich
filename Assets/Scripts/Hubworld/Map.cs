using HappyGames.Data;
using UnityEngine;

public class Map : MonoBehaviour
{
    public Transform realLeftUp, realRightDown;
    public RectTransform mapLeftUp, mapRightDown;
    public RectTransform mentiPin;
    public GameObject[] pins;

    private void Start()
    {
        UpdateActivePins();
    }


    public void UpdatePinPositions()
    {
        Debug.Log(realLeftUp.position.x); Debug.Log(realRightDown.position.x); 
        Debug.Log(mapLeftUp.anchoredPosition.x);
        float x = map(Hubworld.Instance.player.transform.position.x, realLeftUp.position.x, realRightDown.position.x, mapLeftUp.anchoredPosition.x, mapRightDown.anchoredPosition.x);
        float y = map(Hubworld.Instance.player.transform.position.y, realLeftUp.position.y, realRightDown.position.y, mapLeftUp.anchoredPosition.y, mapRightDown.anchoredPosition.y);

        mentiPin.anchoredPosition = new Vector3(x, y, 0);
    }


    static public float map(float value,
                               float start1, float stop1,
                               float start2, float stop2)
    {
        float outgoing =
          start2 + (stop2 - start2) * ((value - start1) / (stop1 - start1));
        
        if (float.IsInfinity(outgoing))
        {
            Debug.LogError("map is infinity");
        }
        return outgoing;
    }

    public void UpdateActivePins()
    {
        UserProfile profile = ProfileManager.GetActiveProfile();
        int storyPart = profile.StoryPart;
        foreach (GameObject pin in pins)
        {
            pin.SetActive(false);
        }

        pins[0].SetActive(true);

        if (storyPart == 0)
        {
            pins[1].SetActive(true);
        }
        else if (storyPart == 1)
        {
            if (profile.GetStoryState(StoryElement.Island2) == StoryState.Completed)
            {
                pins[3].SetActive(true);
            }
            else
            {
                pins[2].SetActive(true);
            }
        }
        else if (storyPart == 2)
        {
            if (profile.GetStoryState(StoryElement.Island3) == StoryState.Completed)
            {
                pins[5].SetActive(true);
            }
            else
            {
                pins[4].SetActive(true);
            }
        }
        else if (storyPart == 3)
        {
            pins[6].SetActive(true);
        }
    }
}
