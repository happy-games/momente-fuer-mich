using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class OnClickEvent : MonoBehaviour
{
    public UnityEvent onClick;
    private bool triggerable;


    // Start is called before the first frame update
    private void OnMouseDown()
    {
        Debug.Log("click ");
        if (triggerable && !EventSystem.current.IsPointerOverGameObject())
        {
            Debug.Log("click and active");
            onClick.Invoke();
        }
    }

    
    private void OnMouseOver()
    {
    }
    

    public void ActivateTrigger()
    {
        triggerable = true;
        Debug.Log("activated trigger = "+ triggerable);
    }

    public void DeActivateTrigger()
    {
        triggerable = false;
        Debug.Log("deactivated trigger = " + triggerable);
    }
}
