using HappyGames.Data;
using HappyGames.Emotion;
using HappyGames.Translations;
using HappyGames.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BalloonPost : MonoBehaviour
{
    [SerializeField, Tooltip("unique index of each flowerPot")]
    public int INDEX;

    [SerializeField, Tooltip("canvas that displays the score")]
    public WorldCanvas scoreDisplay;

    [SerializeField, Tooltip("renderer of the pot and the flower inside")]
    public SpriteRenderer flowerPot, flowerSprite;

    [SerializeField, Tooltip("set containing sprites of all trophytypes")]
    public RankSpriteSet balloonset;

    [SerializeField, Tooltip("the UI panel that shows all trophys")]
    public TrophyPanel panel;

    [SerializeField, Tooltip("the close button of the trophy panel")]
    public Button closeButton;

    [SerializeField, Tooltip("the throphy UIs gameObject")]
    public GameObject trophyUI;

    [SerializeField, Tooltip("the PopUpwindow containing an error message")]
    public OkCancelWindow errorMessage;

    [SerializeField, Tooltip("range from where this pot can be clicked")]
    public float range;

    // otline material of this pot
    private Material outline;
    public Material defaultMaterial;
    const string SHADER_WIDTH = "_Size", SHADER_COLOR = "_Color", SHADER_SHIMMER = "_Intensity";
    [SerializeField, Tooltip("with of the outline shader")]
    public float inRangeWidth = 3f, onMouseWidth = 7f;
    [SerializeField, Tooltip("color of the outline shader")]
    public Color inRangeColor, onHoverColor;
    private bool outlineIsSet = false;
    private UserProfile profile = null;
    private bool unlocked  = false;

    public ParticleSystem place;

    //currently stored trophy of type FlowerTrophy
    private BaloonTrophy balloon;
    private TranslatableCollection<string> pointsLabel = new TranslatableCollection<string>("Punkte", "Points");

    private TranslatableCollection<string> unknownEmotionMessage = new TranslatableCollection<string>
  (
      "Finde zuerst Überrascht im Ballonspiel.",
      "Find Surprised in the Balloon-game first."
  );



    private void OnEnable()
    {
        ChangeProfile(profile, ProfileManager.GetActiveProfile());
    }

    private void OnDisable()
    {
        ChangeProfile(profile, null);
    }

    // Start is called before the first frame update
    void Start()
    {
        outline = flowerPot.material;
        CheckIfUnlocked();

        //check if trophy is in the pot in save file
        if (profile.MapState.GetSavedTrophyDisplay(INDEX) != null)
        {
            AddFlower(profile.MapState.GetSavedTrophyDisplay(INDEX));

        }
    }

    public void CheckIfUnlocked()
    {
        if (ProfileManager.GetActiveProfile().HasEmotionUnlocked(EmotionType.Surprised))
        {
            unlocked = true;
            flowerPot.material = outline;
        } else
        {
            unlocked = false;
            flowerPot.material = defaultMaterial;
        }
    }


    public void Unlock()
    {
        unlocked = true;
        flowerPot.material = outline;
    }

    // Update is called once per frame
    void Update()
    {
        if (unlocked)
        {
            //update outline
            if (!outlineIsSet && InRange(Hubworld.Instance.player.transform.position))
            {

                SetOutline(inRangeWidth, inRangeColor);
                outlineIsSet = true;
            }
            else if (outlineIsSet && !InRange(Hubworld.Instance.player.transform.position))
            {

                ResetOutline();
                outlineIsSet = false;
            }
            //update blocks in world
            if (trophyUI.activeSelf)
            {
                if (!Hubworld.Instance.IsInputBlocked())
                {
                    Hubworld.Instance.DisableInputs();
                }
                if (!UIManager.Instance.IsInputBlocked())
                {
                    UIManager.Instance.EmotionBlockToggle();
                }
            }
        }
    }

    /// <summary>
    /// opens the Tropy UI or the error message if this pot is clicked and the player is in range
    /// </summary>
    private void OnMouseOver()
    {
        
            if (Input.GetMouseButtonDown(0) && InRange(Hubworld.Instance.player.transform.position) && !EventSystem.current.IsPointerOverGameObject())
            {
                if (unlocked)
                {
                    if (balloon != null)
                    {
                        errorMessage.PopUp();
                        errorMessage.onAccept.AddListener(SwapBalloon);
                        errorMessage.onCancel.AddListener(RemoveErrorListener);
                    }
                    else
                    {
                        OpenFlowerTrophyUI();
                    }
                }
                else
                {
                        Hubworld.Instance.DisplayMentiMessage(unknownEmotionMessage.GetValue(ProfileManager.GetActiveProfile().Settings.Language));
                }
            } 
        

    }

    /// <summary>
    /// updates the outline and opens the score display
    /// </summary>
    private void OnMouseEnter()
    {
        if (unlocked && InRange(Hubworld.Instance.player.transform.position))
        {
            SetOutline(onMouseWidth, onHoverColor);

            if (balloon != null)
            {
                scoreDisplay.gameObject.SetActive(true);
            }
        }

    }

    /// <summary>
    /// closes the score display and updates the outline
    /// </summary>
    private void OnMouseExit()
    {
        if (unlocked)
        {
            scoreDisplay.gameObject.SetActive(false);

            if (InRange(Hubworld.Instance.player.transform.position))
            {
                SetOutline(inRangeWidth, inRangeColor);

            }
            else
            {
                ResetOutline();
            }
        }
    }

    /// <summary>
    /// resets the outline to default
    /// </summary>
    private void ResetOutline()
    {
        outline.SetFloat(SHADER_WIDTH, 0f);
    }

    /// <summary>
    /// sets the outline of this material
    /// </summary>
    /// <param name="width"></param> with of the outline
    /// <param name="color"></param> color of the outline
    private void SetOutline(float width, Color color)
    {
        outline.SetFloat(SHADER_WIDTH, width);
        outline.SetColor(SHADER_COLOR, color);
    }

    /// <summary>
    ///     checks if the player is in range of this pot
    /// </summary>
    private bool InRange(Vector3 player)
    {
        if (Vector3.Distance(transform.position, player) < range)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// puts the current trophy back in the inventory and opens the Trophy UI
    /// </summary>
    private void SwapBalloon()
    {
        profile.Inventory.AddTrophy(balloon);
        profile.MapState.SaveTophyDisplayContent(INDEX, null);
        balloon = null;
        flowerSprite.sprite = null;
        scoreDisplay.ChangeText("");
        RemoveErrorListener();
        OpenFlowerTrophyUI();
    }

    /// <summary>
    /// opens the Trophy UI
    /// </summary>
    private void OpenFlowerTrophyUI()
    {
        closeButton.onClick.AddListener(CloseBalloonTrophyUI);
        panel.OnTrophyClick += AddFlower;
        trophyUI.SetActive(true);
    }

    /// <summary>
    /// removes the listeners from the error UI panel
    /// </summary>
    private void RemoveErrorListener()
    {
        errorMessage.onCancel.RemoveListener(RemoveErrorListener);
        errorMessage.onAccept.RemoveListener(OpenFlowerTrophyUI);
        errorMessage.onAccept.RemoveListener(SwapBalloon);
    }


    /// <summary>
    /// closes the Flowertrophy UI
    /// </summary>
    public void CloseBalloonTrophyUI()
    {
        closeButton.onClick.RemoveListener(CloseBalloonTrophyUI);
        panel.OnTrophyClick -= AddFlower;
        trophyUI.SetActive(false);
        UIManager.Instance.EmotionBlockToggle();
        Hubworld.Instance.EnableInputs("BalloonPost");

    }

    /// <summary>
    /// Adds a flower Trophy
    /// </summary>
    /// <param name="flower"></param> the Trophy to add to this pot
    public void AddFlower(Trophy balloon)
    {
        GetComponent<AudioSource>().Play();
        place.Play();
        profile.Inventory.RemoveTrophy(balloon);
        profile.MapState.SaveTophyDisplayContent(INDEX, balloon);
        this.balloon = (BaloonTrophy)balloon;
        // panel.RemoveTropohy(flower);
        UpdateScoreDisplay(profile.Settings.Language);
        ChangeSprite();
        CloseBalloonTrophyUI();
    }

    /// <summary>
    /// changes the sprite of the flower to the current trophy
    /// </summary>
    private void ChangeSprite()
    {
        flowerSprite.sprite = balloonset.GetValue(balloon.Rank);
    }

    private void ChangeProfile(UserProfile oldProfile, UserProfile newProfile)
    {
        if (oldProfile != null)
        {
            oldProfile.Settings.OnLanguageChanged -= UpdateScoreDisplay;
        }
        profile = newProfile;
        if (newProfile != null)
        {
            newProfile.Settings.OnLanguageChanged += UpdateScoreDisplay;
        }
    }

    private void UpdateScoreDisplay(Language language)
    {
        scoreDisplay.ChangeText(pointsLabel.GetValue(language) + balloon.Score);
    }

}
