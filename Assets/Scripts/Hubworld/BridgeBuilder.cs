using HappyGames.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

/// <summary>
/// Class that opens a OkCancelWidow and can Build a bridge by setting the corresponding gameObject with collider active and buidling the navMesh
/// must have a collider!
/// </summary>
public class BridgeBuilder : MonoBehaviour
{
    [SerializeField, Tooltip("the popUp Window to open on click")]
    public OkCancelWindow popUpWindow;

    [SerializeField, Tooltip("the navMesh that is influenced by the BridgeBuilder")]
    public NavMeshSurface2d navMesh;

    [SerializeField, Tooltip("the GameObject containing the collider of the Bridge")]
    public GameObject Bridge;

    [SerializeField, Tooltip("The GameObject of the Bridge Builder")]
    private GameObject BuilderObject;

    [SerializeField, Tooltip("the Collider containing the onClick Event")]
    private BoxCollider2D ClickCollider;

    [SerializeField, Tooltip("the collectibles of CollectibleManager are checked aainst cost")]
    public CollectibleManager collectibles;

    [SerializeField, Tooltip("range of being able to click the collider of this BridgeBuilder")]
    public double range;

    [SerializeField, Tooltip("cost to build the Bridge in collectibles")]
    public int cost;

    [SerializeField, Tooltip("unique nam for each brdge in the game for saving progress")]
    public string bridgeName;

    /// <summary>
    /// builds all bridges that have been built in previouse saves
    /// </summary>
    void Start()
    {
        if (ProfileManager.GetActiveProfile().MapState.CheckBridge(bridgeName))
        {
            SpawnBridge();
            navMesh.BuildNavMesh();
            this.enabled = false;
        }
        
    }

    private void OnMouseOver()
    {
        Debug.Log("OVER");
    }

    /// <summary>
    /// activates popWindow if in range
    /// </summary>
    public void OnMouseDown()
    {
        //Debug.Log(Vector3.Distance(Player.Instance.transform.position, transform.position));
        if (Vector3.Distance(Player.Instance.transform.position, transform.position) < range && !EventSystem.current.IsPointerOverGameObject())
        {
            Debug.Log("MouseDown");
            popUpWindow.PopUp();
        }
    }

    /// <summary>
    /// Builds the Bridge into the NavMesh and saves the progress
    /// </summary>
    public void BuildBridge()
    {
        if (collectibles.GetCollected() >= cost)
        {
            GetComponent<AudioSource>().Play();
            SpawnBridge();
            ClickCollider.enabled = false; 
            navMesh.BuildNavMesh();
            collectibles.PayCollects(cost);
            popUpWindow.PopDown();
            ProfileManager.GetActiveProfile().MapState.AddBridge(bridgeName);

        } else
        {
            popUpWindow.Error() ;
        }
    }

    /// <summary>
    /// sets this Bridge active in the scene
    /// </summary>
    public void SpawnBridge()
    {
        Bridge.SetActive(true);
        BuilderObject.SetActive(false);
    }
}
