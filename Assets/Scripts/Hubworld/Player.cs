using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Player : MonoBehaviour
{
    public static Player Instance;

    public NavMeshAgent agent;

    public Animator animator;
    public float MaxSpeed;
    public float Acceleration, Deceleration;
    private float Speed;
    private List<Vector3> path; //liste an paths (von k�stchen zu k�stchen = ein path)
    private float pathlength, stopLength;
    private int pathIndex;
    public bool haspath = false; //gibt an ob der player gerade wohin moved

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        agent.updateRotation = false;
        agent.updateUpAxis = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (agent.hasPath)
        {

            float angle = Vector3.Angle(new Vector3(1, 0), agent.velocity);

            if (angle <= 22.5)
            {
                animator.SetInteger("angle", 2);
            }
            else if (angle <= 67.5)
            {
                if (agent.velocity.y > 0)
                {
                    animator.SetInteger("angle", 1);
                }
                else
                {
                    animator.SetInteger("angle", 3);
                }
            }
            else if (angle <= 112.5)
            {
                if (agent.velocity.y > 0)
                {
                    animator.SetInteger("angle", 0);
                }
                else
                {
                    animator.SetInteger("angle", 4);
                }
            }
            else if (angle <= 157.5)
            {
                if (agent.velocity.y > 0)
                {
                    animator.SetInteger("angle", 7);
                }
                else
                {
                    animator.SetInteger("angle", 5);
                }
            }
            else
            {
                animator.SetInteger("angle", 6);
            }
        }
        else
        {
            animator.SetBool("hasTarget", false);
        }
    }

    public void SetDestination(Vector3 position)
    {
        agent.SetDestination(position);
        animator.SetBool("hasTarget", true);
        Hubworld.Instance.EndLineDrawing();
    }

    public void Move()
    {
        agent.Move(Vector3.zero);
    } 
    public void Move(Vector3 offset )
    {
        agent.Move(offset);
    }

    public bool isMoving()
    {
        return agent.hasPath;
    }


    private Vector3 GetPosition()
    {
        return transform.position;
    }
    
    public Vector3 GetDestination()
    {
        return agent.destination;
    }
}
