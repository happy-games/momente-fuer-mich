namespace HappyGames.Saving
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    using UnityEngine;

    public static class SaveManager
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The dictionary for all saves.
        /// </summary>
        private static readonly string SAVE_DIRECTORY = Application.persistentDataPath;

        private static Dictionary<Type, ISerializationSurrogate> surrogates = new Dictionary<Type, ISerializationSurrogate>();


        // --- | Methods | -------------------------------------------------------------------------------------------------
        // Save -----------------------------------------------------------------------------------

        /// <summary>
        /// Saves data to a file.
        /// </summary>
        /// <typeparam name="T">The type of the data to save.</typeparam>
        /// <param name="path">The path to the fiel.</param>
        /// <param name="data">The data to save.</param>
        public static void Save<T>(string path, T data)
        {
            BinaryFormatter formatter = GetFormatter();
            using (FileStream stream = new FileStream(SAVE_DIRECTORY + "/" + path, FileMode.Create))
            {
                formatter.Serialize(stream, data);
            }
#if (UNITY_EDITOR)
            Debug.Log($"Data saved to: \"{ SAVE_DIRECTORY }/{ path }\".");
#endif
        }

        // Load -----------------------------------------------------------------------------------

        /// <summary>
        /// Loads data from a file.
        /// </summary>
        /// <typeparam name="T">The type of the data to save.</typeparam>
        /// <param name="path">The path to the file.</param>
        /// <returns>The save file converted to data.</returns>
        private static T LoadFromPath<T>(string fullPath)
        {
            if (File.Exists(fullPath))
            {
                T data;
                BinaryFormatter formatter = GetFormatter();
#if (UNITY_EDITOR)
                Debug.Log($"Data loading from: \"{ fullPath }\".");
#endif
                using (FileStream stream = new FileStream(fullPath, FileMode.Open))
                {
                    try
                    {
                        data = (T)formatter.Deserialize(stream);
                    }
                    catch (SerializationException ex)
                    {
                        throw ex;
                    }
                }
                return data;
            }
            else
            {
                throw new FileNotFoundException($"The file to load at { fullPath } was not found.", fullPath);
            }
        }

        /// <summary>
        /// Loads data from a file.
        /// </summary>
        /// <typeparam name="T">The type of the data to save.</typeparam>
        /// <param name="path">The path to the file.</param>
        /// <returns>The save file converted to data.</returns>
        public static T Load<T>(string path) => LoadFromPath<T>(SAVE_DIRECTORY + "/" + path);
        /// <summary>
        /// Loads all save at a path, with a spesific extension.
        /// </summary>
        /// <typeparam name="T">The type of the data.</typeparam>
        /// <param name="path">The path to the data.</param>
        /// <param name="extension">The extension of the save files.</param>
        /// <returns>The loaded data.</returns>
        public static IEnumerable<T> Load<T>(string path, string extension)
        {
            string fullPath = SAVE_DIRECTORY + "/" + path;
            foreach (string file in Directory.GetFiles(fullPath, '*' + extension))
            {
                T data = default;
                bool errorFree = true;
                try
                {
                    data = LoadFromPath<T>(file);
                }
                catch (InvalidCastException)
                {
                    Debug.LogWarning($"There is a outdated save in the diectory \'{ SAVE_DIRECTORY }{ (string.IsNullOrEmpty(path) ? "" : "/" + path) }\'.");
                    errorFree = false;
                }
                if (errorFree)
                {
                    yield return data;
                }
            }
        }

        // File Check -----------------------------------------------------------------------------

        /// <summary>
        /// Checks if a file exists.
        /// </summary>
        /// <param name="path">The path to the file.</param>
        /// <returns>True if a file exists.</returns>
        public static bool CheckForFile(string path) => File.Exists(SAVE_DIRECTORY + "/" + path);

        // Delete ---------------------------------------------------------------------------------

        /// <summary>
        /// Deletes the save at the path.
        /// </summary>
        /// <param name="path">The path to the file.</param>
        public static void Delete(string path)
        {
            File.Delete(SAVE_DIRECTORY + "/" + path);
        }

        // Surrogates -----------------------------------------------------------------------------

        /// <summary>
        /// Adds a surrogate to the data serialization.
        /// </summary>
        /// <param name="type">The type that can be serialized with this surrogate.</param>
        /// <param name="surrogate">The surrogate to add.</param>
        public static void AddSurrogate(Type type, ISerializationSurrogate surrogate)
        {
            if (!surrogates.ContainsKey(type))
            {
                surrogates.Add(type, surrogate);
            }
        }

        // Binary Formatter ---------------------------------------------------------------------------

        /// <summary>
        /// Returns a formatter.
        /// </summary>
        /// <returns>The formatter with the surrogates.</returns>
        private static BinaryFormatter GetFormatter()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            SurrogateSelector selector = new SurrogateSelector();

            foreach (KeyValuePair<Type, ISerializationSurrogate> surrogate in surrogates)
            {
                selector.AddSurrogate(surrogate.Key, new StreamingContext(StreamingContextStates.All), surrogate.Value);
            }

            formatter.SurrogateSelector = selector;

            return formatter;
        }
    }
}
