namespace HappyGames.TimeManagement
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Events;

    public class Countdown : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Min(0f), Tooltip("The duration of the countdow.")]
        protected float duration = 1f;

        [SerializeField, Tooltip("The timeline to use to update the countdown.")]
        protected Timeline timeline = null;

        [Space, SerializeField, Tooltip("Called when the countdow is started.")]
        protected UnityEvent onStart = default;

        [SerializeField,  Tooltip("Called when the cooldown is canceled.")]
        protected UnityEvent onCancel = default;

        [SerializeField, Tooltip("Called when the cooldown is over.")]
        protected UnityEvent onEnd = default;

        [SerializeField, Tooltip("Called when the countdow is updated.")]
        protected UnityEvent<float, float> onUpdate = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The current value of the <see cref="Countdown"/>.
        /// </summary>
        public float Current { get; protected set; } = 0f;
        protected Coroutine countdown = null;
        /// <summary>
        /// True if the countdown is active.
        /// </summary>
        public bool IsActive => countdown != null;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Starts the <see cref="Countdown"/>.
        /// </summary>
        [ContextMenu("Start")]
        public void StartCounting()
        {
            if (!IsActive)
            {
                countdown = StartCoroutine(DoCountDown());
            }
        }

        /// <summary>
        /// Stops the countdown.
        /// </summary>
        [ContextMenu("Stop")]
        public void CancelCounting()
        {
            if (IsActive)
            {
                StopCoroutine(countdown);
                countdown = null;
                Current = 0f;
                onUpdate?.Invoke(0f, duration);
                onCancel?.Invoke();
            }
        }

        /// <summary>
        /// Updates the countdown.
        /// </summary>
        protected IEnumerator DoCountDown()
        {
            float duration = this.duration;
            Current = duration;
            onStart?.Invoke();
            onUpdate?.Invoke(Current, duration);
            timeline ??= new Timeline();
            while (Current > 0f)
            {
                yield return null;
                if (!timeline.IsPaused)
                {
                    Current -= timeline.DeltaTime;
                    if (Current < 0f)
                    {
                        Current = 0f;
                    }
                    onUpdate?.Invoke(Current, duration);
                }
            }
            onEnd?.Invoke();
        }

        /// <summary>
        /// Sets the duration of the cooldown.
        /// </summary>
        /// <param name="duration">The duration of the cooldown.</param>
        public void SetDuration(float duration)
        {
            this.duration = duration;
        }
    }
}
