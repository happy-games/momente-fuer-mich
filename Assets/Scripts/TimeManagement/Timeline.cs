namespace HappyGames.TimeManagement
{
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(fileName = "Timeline", menuName = "Timeline")]
    public class Timeline : ScriptableObject
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Min(0f), LockInPlaymode, Tooltip("The time scale at start of the scene.")]
        private float defaultTimeScale = 1f;

        [SerializeField, LockInPlaymode, Tooltip("True if the timeline is dependend on the global time scale.")]
        private bool useGlobalTimescale = true;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The default state of time.
        /// </summary>
        private const bool DEFAULT_PAUSED_STATE = false;

        [System.NonSerialized]
        private List<(float timeStemp, float scale)> scaleChanges = new List<(float, float)>();

        [System.NonSerialized]
        private static HashSet<Timeline> timelines = new HashSet<Timeline>();

        [System.NonSerialized]
        private static HashSet<Object> globalPausingObjects = new HashSet<Object>();

        /// <summary>
        /// True if the global time scale is paused.
        /// </summary>
        public static bool IsGlobalyPaused => globalPausingObjects.Count > 0;

        /// <summary>
        /// The global time scale when unpaused.
        /// </summary>
        [System.NonSerialized]
        private static float globalUnpausedTimeScale = 1f;

        [System.NonSerialized]
        private bool globalTimeScaleSet = false;
        [System.NonSerialized]
        private bool globalTimeScale;
        /// <summary>
        /// True if the timeline is dependend on the global time scale.
        /// </summary>
        public bool UseGlobalTimescale
        {
            get
            {
                if (!globalTimeScaleSet)
                {
                    globalTimeScaleSet = true;
                    globalTimeScale = useGlobalTimescale;
                }
                return globalTimeScale;
            }
        }

        [System.NonSerialized]
        private bool isPaused = DEFAULT_PAUSED_STATE;
        /// <summary>
        /// True if the timeline is paused.
        /// </summary>
        public bool IsPaused
        {
            get => isPaused;
            set
            {
                if (isPaused != value)
                {
                    isPaused = value;
                    if (isPaused)
                    {
                        scaleChanges.Add((UseGlobalTimescale ? Time.time : Time.unscaledTime, 0f));
                    }
                    else
                    {
                        scaleChanges.Add((UseGlobalTimescale ? Time.time : Time.unscaledTime, localTimeScale));
                    }
                    if (!timelines.Contains(this))
                    {
                        timelines.Add(this);
                    }
                }
            }
        }

        [System.NonSerialized]
        private float localTimeScale = float.NaN;
        /// <summary>
        /// The time scale of this time line.
        /// </summary>
        public float LocalTimeScale
        {
            get
            {
                if (float.IsNaN(localTimeScale))
                {
                    localTimeScale = defaultTimeScale;
                }
                return localTimeScale * (IsPaused ? 0f : 1f);
            }
            set
            {
                localTimeScale = value;
                if (!timelines.Contains(this))
                {
                    timelines.Add(this);
                }
                if (!isPaused)
                {
                    scaleChanges.Add((UseGlobalTimescale ? Time.time : Time.unscaledTime, value));
                }
            }
        }

        /// <summary>
        /// The global and local time scale.
        /// </summary>
        public float TimeScale => (UseGlobalTimescale ? Time.timeScale : 1f) * LocalTimeScale;

        /// <summary>
        /// The time since the last frame.
        /// </summary>
        public float DeltaTime => (UseGlobalTimescale ? Time.deltaTime : Time.unscaledDeltaTime) * LocalTimeScale;

        /// <summary>
        /// The time since the last fixed frame.
        /// </summary>
        public float FixedDeltaTime => (UseGlobalTimescale ? Time.fixedDeltaTime : Time.fixedUnscaledDeltaTime) * LocalTimeScale;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Pauses unities time.
        /// </summary>
        /// <param name="component">The component disabling the time progession.</param>
        public static void PauseGlobal(Object component)
        {
            bool wasPaused = IsGlobalyPaused;
            if (!globalPausingObjects.Contains(component))
            {
                globalPausingObjects.Add(component);
            }
            if (!wasPaused)
            {
                globalUnpausedTimeScale = Time.timeScale;
                Time.timeScale = 0f;
            }
        }

        /// <summary>
        /// Pauses unities time.
        /// </summary>
        /// <param name="component">The component disabling the time progession.</param>
        /// <seealso cref="PauseGlobal(Object)"/>
        public void PauseGlobalTime(Object component) => PauseGlobal(component);

        /// <summary>
        /// Unpauses unities time.
        /// </summary>
        /// <param name="component">The component enabling the time progression.</param>
        public static void UnpauseGlobal(Object component)
        {
            if (globalPausingObjects.Contains(component))
            {
                globalPausingObjects.Remove(component);
            }
            if (!IsGlobalyPaused)
            {
                Time.timeScale = globalUnpausedTimeScale;
            }
        }

        /// <summary>
        /// Unpauses unities time.
        /// </summary>
        /// <param name="component">The component enabling the time progression.</param>
        /// <seealso cref="UnpauseGlobal(Object)"/>
        public void UnpauseGlobalTime(Object component) => UnpauseGlobal(component);

        /// <summary>
        /// Sets the time scale of unities time.
        /// </summary>
        /// <param name="scale">The scale to apply.</param>
        public static void SetGlobalTimeSacle(float scale)
        {
            globalUnpausedTimeScale = scale;
        }

        /// <summary>
        /// Checks if the time is paused by the given component.
        /// </summary>
        /// <param name="component">The component potentialy disabling time progression.</param>
        /// <returns>True if the component is disabling time progression.</returns>
        public static bool IsPausedBy(Object component) => globalPausingObjects.Contains(component);

        /// <summary>
        /// Resets all timelines.
        /// </summary>
        public static void ResetTimelines()
        {
            globalPausingObjects.Clear();
            Time.timeScale = 1f;
            foreach (Timeline timeline in timelines)
            {
                timeline.isPaused = DEFAULT_PAUSED_STATE;
                timeline.localTimeScale = timeline.defaultTimeScale;
            }
        }

        /// <summary>
        /// Returns the time in seconds since the start of the game.
        /// </summary>
        /// <returns>The time since the start of the game.</returns>
        public float GetTime()
        {
            float time = 0f;
            float currentScale = defaultTimeScale;
            float timeStemp = 0f;
            foreach (var scaleChange in scaleChanges)
            {
                time += (scaleChange.timeStemp - timeStemp) * currentScale;
                currentScale = scaleChange.scale;
                timeStemp = scaleChange.timeStemp;
            }
            time += ((UseGlobalTimescale ? Time.time : Time.unscaledTime) - timeStemp) * currentScale;
            return time;
        }
    }
}
