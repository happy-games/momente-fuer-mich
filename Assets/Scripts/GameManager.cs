namespace HappyGames
{
    using HappyGames.TimeManagement;
    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    /// Handles the games states.
    /// </summary>
    public sealed class GameManager : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [Space, SerializeField, Tooltip("Called when the game gets paused.")]
        private UnityEvent onPaused = default;

        [SerializeField, Tooltip("Called when the game continues after pausing.")]
        private UnityEvent onUnpaused = default;

        [Space, SerializeField, Tooltip("Called when the game starts.")]
        private UnityEvent onGameStart = default;

        [SerializeField, Tooltip("Called whent he game has ended.")]
        private UnityEvent onGameOver = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private bool isPaused = false;

        /// <summary>
        /// True if the game is active.
        /// </summary>
        public bool GameIsActive { get; private set; } = true;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Start()
        {
            onGameStart?.Invoke();
        }

        /// <summary>
        /// Pauses the game.
        /// </summary>
        [ContextMenu("Game State/Pause")]
        public void Pause()
        {
            if (!isPaused)
            {
                isPaused = true;
                Timeline.PauseGlobal(this);
                onPaused?.Invoke();
            }
        }

        /// <summary>
        /// Continues a paused game.
        /// </summary>
        [ContextMenu("Game State/Unpause")]
        public void Unpause()
        {
            if (isPaused)
            {
                isPaused = false;
                Timeline.UnpauseGlobal(this);
                onUnpaused?.Invoke();
            }
        }

        /// <summary>
        /// Ends the game.
        /// </summary>
        public void EndGame()
        {
            if (GameIsActive)
            {
                GameIsActive = false;
                onGameOver?.Invoke();
            }
        }

        /// <summary>
        /// Exits and closes the game.
        /// </summary>
        [ContextMenu("Game State/Quit")]
        public void Quit()
        {
#if (UNITY_EDITOR)

            UnityEditor.EditorApplication.isPlaying = false;

#else
            Application.Quit();
#endif
        }
    }
}
