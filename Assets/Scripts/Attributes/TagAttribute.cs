using UnityEngine;

/// <summary>
/// Used to limit a string to the unity tags.
/// </summary>
public class TagAttribute : PropertyAttribute { }