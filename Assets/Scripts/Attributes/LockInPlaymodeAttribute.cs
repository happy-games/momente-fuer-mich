﻿using UnityEngine;

/// <summary>
/// Locks the property in the inspector in play mode.
/// </summary>
public class LockInPlaymodeAttribute : PropertyAttribute
{
    /// <summary>
    /// Creates a new attribute instance.
    /// </summary>
    public LockInPlaymodeAttribute() : base() { }
}
