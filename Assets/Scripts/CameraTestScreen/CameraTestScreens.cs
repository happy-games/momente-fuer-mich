using HappyGames.Emotion;
using System.Collections;
using HappyGames.Translations;
using System.Collections.Generic;
using UnityEngine;
using HappyGames.Data;
using UnityEngine.Events;
using UnityEngine.UI;

public class CameraTestScreens : MonoBehaviour
{
    public EmotionSelect selection;
    private EmotionType[] emotionOrder = { EmotionType.Happy, EmotionType.Sad, EmotionType.Angry, EmotionType.Surprised };
    public GameObject[] emotionDescriptions;
    public GameObject skipButton, startButton;
    public GameObject fullscreenScreen, languageScreen;
    private bool Fullscreen = false;
    private int orderIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        skipButton.SetActive(false);
        startButton.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        if (Fullscreen) {
            if (!selection.IsSelecting)
            {
                selection.StartSelection();
            }
        }
    }


    public void DetectEmotion(EmotionType emotion)
    {
        if (orderIndex < emotionOrder.Length && emotion.Equals(emotionOrder[orderIndex])){

            skipButton.SetActive(true);
            
        }

    }

    public void Skip()
    {
        skipButton.SetActive(false);
        emotionDescriptions[orderIndex].GetComponent<UIFade>().FadeOut();
        orderIndex++;
        if (orderIndex < emotionDescriptions.Length-1)
        {
            emotionDescriptions[orderIndex].SetActive(true);
        } else
        {
            emotionDescriptions[orderIndex].SetActive(true);
            startButton.SetActive(true);
        }
    }

    public void GoFullscreen()
    {
        fullscreenScreen.SetActive(false);
        Fullscreen = true;
    }

    public void SelectGerman()
    {
        Debug.Log("ENGLISH");
        ProfileManager.GetActiveProfile().Settings.ChangeLanguage(Language.German);
    }
    public void SelectEnglish()
    {
        Debug.Log("GERMAN");
        ProfileManager.GetActiveProfile().Settings.ChangeLanguage(Language.English);
    }


}


