namespace HappyGames.SceneManagement
{
    using HappyGames.TimeManagement;
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.SceneManagement;

    /// <summary>
    /// Handles scene loading.
    /// </summary>
    public static class LevelLoader
    {

        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// True if the script is currently loading a scene.
        /// </summary>
        public static bool IsLoading { get; private set; }

        /// <summary>
        /// Called when a scene load was started.
        /// </summary>
        public static event UnityAction OnLoad;

        /// <summary>
        /// Called when the progress of the scene loading changed.
        /// </summary>
        public static event UnityAction<float> OnProgressUpdated;

        /// <summary>
        /// Called when the scene was finished loading.
        /// </summary>
        public static event UnityAction OnFinished;

        /// <summary>
        /// Async operations who are not allowed to activate the scene will stop at 90%.
        /// </summary>
        private const float MAX_PROGRESS_WHEN_DISABLED = 0.9f;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Loads the scenes.
        /// </summary>
        /// <param name="level">The level to load.</param>
        public static void Load(SceneGroup level)
        {
            if (!IsLoading)
            {
                MonoBehaviour loader = new GameObject($"SceneLoader({ level.DisplayName })").AddComponent<Loader>();
                loader.StartCoroutine(DoLoadScenes(loader, level));
            }
        }

        /// <summary>
        /// Perform the loading process asynconiously.
        /// </summary>
        /// <param name="loader">The object used for loading.</param>
        /// <param name="level">The level to load.</param>
        private static IEnumerator DoLoadScenes(MonoBehaviour loader, SceneGroup level)
        {
            IsLoading = true;
            Object.DontDestroyOnLoad(loader.gameObject);
            OnLoad?.Invoke();

            // Load loading screen.
            AsyncOperation screenOperation = SceneManager.LoadSceneAsync(level.LoadingScreen, LoadSceneMode.Additive);
            yield return new WaitUntil(() => screenOperation.isDone);
            float timestemp = Time.unscaledTime;
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(level.LoadingScreen));

            // Unload old scenes.
            while (SceneManager.sceneCount > 1)
            {
                AsyncOperation operation = SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(0));
                yield return new WaitUntil(() => operation.isDone);
            }

            // Reset timescale in case it was changed (pausing).
            Timeline.ResetTimelines();

            // Load new scnenes.
            AsyncOperation[] loadingOperations = new AsyncOperation[level.SceneCount];
            int loopIndex = 0;
            foreach (string scene in level)
            {
                loadingOperations[loopIndex] = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
                loadingOperations[loopIndex].allowSceneActivation = false;
                loopIndex++;
            }

            // Wait until the scenes are ready to be activated.
            if (OnProgressUpdated != null)
            {
                // Only do the progress calculations if there are any listeners for it.
                float progress;
                do
                {
                    progress = 0f;
                    foreach (AsyncOperation operation in loadingOperations)
                    {
                        progress += operation.progress / MAX_PROGRESS_WHEN_DISABLED;
                    }
                    progress /= loadingOperations.Length;
                    OnProgressUpdated?.Invoke(progress);
                    yield return null;
                } while (progress < 1f);
            }
            else
            {
                yield return new WaitUntil(() => loadingOperations.Check(true, true, o => o.progress >= MAX_PROGRESS_WHEN_DISABLED));
            }

            // Wait the min loading time.
            if (Time.unscaledTime < timestemp + level.MinDuration)
            {
                yield return new WaitUntil(() => Time.unscaledTime >= timestemp + level.MinDuration);
            }

            // Activate all scenes.
            foreach (AsyncOperation operation in loadingOperations)
            {
                operation.allowSceneActivation = true;
            }
            // Wait for the scenes to be activated.
            yield return new WaitUntil(() => loadingOperations.Check(true, true, o => o.isDone));

            // Unload loading screen.
            SceneManager.SetActiveScene(SceneManager.GetSceneAt(1));
            screenOperation = SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName(level.LoadingScreen));
            yield return new WaitUntil(() => screenOperation.isDone);

            // Set the progcess as done.
            IsLoading = false;
            OnFinished?.Invoke();
            Object.Destroy(loader.gameObject);
        }

        private class Loader : MonoBehaviour { }
    }
}
