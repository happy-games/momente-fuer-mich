namespace HappyGames.SceneManagement
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Contains imformation of a level.
    /// </summary>
    [CreateAssetMenu(fileName = "SceneGroup", menuName = "Level/Group")]
    public class SceneGroup : ScriptableObject, IEnumerable<string>
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The name of the group.")]
        protected string displayName = string.Empty;
        /// <summary>
        /// The name of the group.
        /// </summary>
        public string DisplayName => displayName;

        [SerializeField, Scene, Tooltip("The scenes in the group.")]
        protected string[] scenes = default;
        /// <summary>
        /// The scenes in the group.
        /// </summary>
        public IEnumerable<string> Scenes => scenes;

        [SerializeField, Scene, Tooltip("The the scene to use for blending when loading.")]
        protected string loadingScreen = string.Empty;
        /// <summary>
        /// The the scene to use for blending when loading.
        /// </summary>
        public string LoadingScreen => loadingScreen;

        [SerializeField, Min(0f), Tooltip("The minimum duration the loadingscreen should be visible in seconds.")]
        protected float minDuration = 0f;
        /// <summary>
        /// The minimum duration the loadingscreen should be visible in seconds.
        /// </summary>
        public float MinDuration => minDuration;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The amount of scenes in the level.
        /// </summary>
        public int SceneCount => scenes.Length;

        /// <summary>
        /// Returns the scene with the given key.
        /// </summary>
        /// <param name="key">The key/index of the scene.</param>
        /// <returns>The scene at the index.</returns>
        public string this[int key] => scenes[key];


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public IEnumerator<string> GetEnumerator()
        {
            foreach (string scene in scenes)
            {
                yield return scene;
            }
        }

        /// <inheritdoc/>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        /// <summary>
        /// Loads the scene.
        /// </summary>
        public void Load() => LevelLoader.Load(this);
    }
}
