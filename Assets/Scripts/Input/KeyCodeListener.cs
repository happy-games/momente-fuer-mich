namespace HappyGames.Input
{
    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    /// Listens for a <see cref="KeyCode"/> to be pressed or released.
    /// </summary>
    public class KeyCodeListener : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The key code to listen for.")]
        protected KeyCode key = KeyCode.None;

        [Space, SerializeField, Tooltip("Called when the key was pressed.")]
        protected UnityEvent onKeyDown = default;

        [SerializeField, Tooltip("Called when the key was released.")]
        protected UnityEvent onKeyUp = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        protected virtual void Update()
        {
            if (Input.GetKeyDown(key))
            {
                onKeyDown?.Invoke();
            }
            else if (Input.GetKeyUp(key))
            {
                onKeyUp?.Invoke();
            }
        }
    }
}
