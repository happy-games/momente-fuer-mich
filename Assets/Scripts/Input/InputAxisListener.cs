namespace HappyGames.Input
{
    using HappyGames.Events;
    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    /// Listens for an input axis.
    /// </summary>
    public class InputAxisListener : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The name of the axis to listen to.")]
        protected string axisName = "Horizontal";

        [Space, SerializeField, Tooltip("Called with the current value of the axis.")]
        protected FloatEvent onAxisUpdate = default;
        /// <summary>
        /// Called with the current value of the axis.
        /// </summary>
        public event UnityAction<float> OnHorizontal
        {
            add => onAxisUpdate.AddListener(value);
            remove => onAxisUpdate.RemoveListener(value);
        }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        protected virtual void Update()
        {
            onAxisUpdate?.Invoke(Input.GetAxisRaw(axisName));
        }
    }
}
