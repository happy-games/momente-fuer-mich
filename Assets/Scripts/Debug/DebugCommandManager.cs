namespace HappyGames.Debugging
{
    using HappyGames.Data;
    using System.Collections.Generic;

    public static class DebugCommandManager
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        private static Dictionary<string, DebugCommand> commands = new Dictionary<string, DebugCommand>() 
        { 
            { EmotionEnableCommand.COMMAND_NAME, new EmotionEnableCommand() }
        };


        // --- | Methods | -------------------------------------------------------------------------------------------------

        public static IEnumerable<DebugCommand.Feedback> ParseCommand(string command)
        {
            string[] parts = command.Split(' ');
            return ParseCommand(parts);
        }
        private static IEnumerable<DebugCommand.Feedback> ParseCommand(params string[] attr)
        {
            if (commands.ContainsKey(attr[0]))
            {
                return commands[attr[0]].Perform(attr);
            }
            else
            {
                return new DebugCommand.Feedback[] { new DebugCommand.Feedback(DebugCommand.FeedbackType.Error, $"{ attr[0] } is a unknown command.") };
            }
        }

        public static IEnumerable<DebugCommand.Feedback> RegisterCommand(DebugCommand command)
        {
            if (commands.ContainsKey(command.CommandName))
            {
                yield return new DebugCommand.Feedback(DebugCommand.FeedbackType.Error, $"A command with the name of { command.CommandName } has already been registed.");
            }
            else
            {
                commands.Add(command.CommandName, command);
                yield return new DebugCommand.Feedback(DebugCommand.FeedbackType.Message, $"The { command.CommandName } command has been added.");
            }
        }

        public static IEnumerable<DebugCommand.Feedback> UregisterCommand(string commandName)
        {
            if (commands.ContainsKey(commandName))
            {
                commands.Remove(commandName);
                yield return new DebugCommand.Feedback(DebugCommand.FeedbackType.Message, $"The { commandName } command has been removed.");
            }
            else
            {
                yield return new DebugCommand.Feedback(DebugCommand.FeedbackType.Error, $"A command with the name of { commandName } does not exist.");
            }
        }
    }

    public abstract class DebugCommand
    {
        public abstract IEnumerable<Feedback> Perform(string[] attr);

        public abstract string CommandName { get; }

        public sealed class Feedback
        {
            private FeedbackType feedbacktype;
            public FeedbackType Feedbacktype => feedbacktype;

            private string message;
            public string Message => message;

            public Feedback(FeedbackType type, string message)
            {
                feedbacktype = type;
                this.message = message;
            }
        }
        public enum FeedbackType { Message, Warning, Error }

    }
    public class EmotionEnableCommand : DebugCommand
    {
        public const string COMMAND_NAME = "unlockemotion";
        public override string CommandName => COMMAND_NAME;

        public override IEnumerable<Feedback> Perform(string[] attr)
        {
            if (attr.Length < 2)
            {
                yield return new Feedback(FeedbackType.Error, "No emotion");
            }
            else
            {
                for (int i = 1; i < attr.Length; i++)
                {
                    if (attr[i] == "all")
                    {
                        foreach (Emotion.EmotionType emotion in System.Enum.GetValues(typeof(Emotion.EmotionType)))
                        {
                            ProfileManager.GetActiveProfile().UnlockEmotion(emotion);
                        }
                        yield return new Feedback(FeedbackType.Message, "All emotions have been enabled.");
                    }
                    else
                    {
                        foreach (Emotion.EmotionType emotion in System.Enum.GetValues(typeof(Emotion.EmotionType)))
                        {
                            if (attr[i] == emotion.ToString())
                            {
                                ProfileManager.GetActiveProfile().UnlockEmotion(emotion);
                                yield return new Feedback(FeedbackType.Message, $"{ emotion } has been enabled.");
                            }
                            else
                            {
                                yield return new Feedback(FeedbackType.Warning, $"{ attr[i] } is not a valid emotion.");
                            }
                        }
                    }
                }
            }
        }
    }
}
