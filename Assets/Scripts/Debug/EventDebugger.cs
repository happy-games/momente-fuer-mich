namespace HappyGames.Debugging
{
    using UnityEngine;

    public class EventDebugger : MonoBehaviour
    {
        /// <summary>
        /// Prints a string into the unity console.
        /// </summary>
        /// <param name="attr">The string to log.</param>
        public void Log(string attr) => Debug.Log(attr);

        public void Log(float attr) => Log(attr.ToString());
    }
}
