namespace HappyGames.Emotion
{
    using System.Runtime.InteropServices;
    using UnityEngine;

    public sealed partial class EmotionManager : MonoBehaviour
    {
        public int currentMAXEmotion;  
#if UNITY_WEBGL && !UNITY_EDITOR
          

        private Emotion currentEmotionData = new Emotion();

        //is called by the API to set the tracked Emotion as currentEmotion
        private void EmotionDeliveryFunction(string json)
        {
            currentEmotionData = JsonUtility.FromJson<Emotion>(json);
        }
        private void MaxEmotionDeliveryFunction(int emotion)
        {
            currentMAXEmotion = emotion;
            Debug.Log("API sent " + (EmotionType)emotion);
        }

        private EmotionType UpdateEmotion()
        {
            return (EmotionType)currentMAXEmotion;
            //return (EmotionType)currentEmotionData.maxEmotion();
        }


        //Import of the Javascipt function from the JavaScript Library file
        [DllImport("__Internal")]
        private static extern void VideoStart();

        private void TrackingStart()
        {
            VideoStart();
            Debug.Log("Turned on tracking");
        }

        //Import of the Javascipt function from the JavaScript Library file
        [DllImport("__Internal")]
        private static extern void VideoStop();

        private void TrackingStop()
        {
            currentMAXEmotion = 0;
            VideoStop();
            Debug.Log("Turned off tracking");
        }

        /*
        //Import of the Javascipt function from the JavaScript Library file
        [DllImport("__Internal")]
        private static extern void SendAPIEmotion();

        private void GetAPIEmotion()
        {
            SendAPIEmotion();
            Debug.Log("getting the api max emotion");
        }*/

#endif
    }

    public class Emotion
    {
        public double angry;
        public double disgusted;
        public double fearful;
        public double happy;
        public double neutral;
        public double sad;
        public double surprised;

        public double[] GetEmotionArray()
        {
            double[] emo = { neutral, angry,  happy, sad, surprised };
            return emo;
        }

        public int maxEmotion()
        {
            double max = 0.0;
            int index = 0;
            double[] emo = GetEmotionArray();
            for (int i = 0; i < emo.Length; i++)
            {
                if (emo[i] > max)
                {
                    max = emo[i];
                    index = i;

                }
            }
            return index;

        }

        public override string ToString()
        {
            return "neutral= " + neutral + "angry= " + angry + "disgusted= " + disgusted + ", fearful= " + fearful + ", happy= " + happy + ", sad= " + sad + ", surprised= " + surprised;
        }

    }
}
