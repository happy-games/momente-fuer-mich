namespace HappyGames.Data
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "TrophySpriteSet", menuName = "Set/Trophy/Sprite")]
    public class TrophySpriteSet : TrophySet<Sprite> { }
}
