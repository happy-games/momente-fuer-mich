namespace HappyGames.Translations
{
    using HappyGames.Data;
    using TMPro;
    using UnityEngine;
    using UnityEngine.UI;

    public class TranslationTextField : MonoBehaviour
    {
        public string german;
        public string english;

        [SerializeField]
        private TranslatableCollection<string> translations = default;

        private TMP_Text tmpTextField;
        private Text textField;
        private string Text
        {
            get => tmpTextField ? tmpTextField.text : textField.text;
            set
            {
                if (tmpTextField)
                {
                    tmpTextField.text = value;
                }
                else if (textField)
                {
                    textField.text = value;
                }
            }
        }

        private void Awake()
        {
            tmpTextField = GetComponent<TMP_Text>();
            textField = GetComponent<Text>();
        }

        private void OnEnable()
        {
            ProfileManager.OnProfileActivated += ChangeProfile;
            if (ProfileManager.HasActiveProfile)
            {
                ChangeProfile(null, ProfileManager.GetActiveProfile());
            }
            else
            {
                ChangeText(default);
            }
        }

        private void OnDisable()
        {
            if (ProfileManager.HasActiveProfile)
            {
                ChangeProfile(ProfileManager.GetActiveProfile(), null);
            }
            ProfileManager.OnProfileActivated -= ChangeProfile;
        }

        private void ChangeText(Language l)
        {
            //Text = translations.GetValue(l);
            if (l == Language.German)
            {
                Text = german;
            } 
            else if (l == Language.English)
            {
                Text = english;
            }
        }

        private void ChangeProfile(UserProfile oldProfile, UserProfile newProfile)
        {
            if (oldProfile != null)
            {
                oldProfile.Settings.OnLanguageChanged -= ChangeText;
            }
            if (newProfile != null)
            {
                newProfile.Settings.OnLanguageChanged += ChangeText;
                ChangeText(newProfile.Settings.Language);
            }
        }
    }

    public enum Language { German, English };
}
