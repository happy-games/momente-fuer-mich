namespace HappyGames.Data
{
    public class BaloonTrophy : Trophy
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override TrophyType TrophyType => TrophyType.Baloon;

        private RankType rank = default;
        /// <summary>
        /// The rank reached in the mini game.
        /// </summary>
        public RankType Rank => rank;

        /// <summary>
        /// The rank reached in the mini game.
        /// </summary>
        public int Score;


        // --- | Constructors | --------------------------------------------------------------------------------------------

        public BaloonTrophy(RankType rank)
        {
            this.rank = rank;
        }
    }


    // --- | Classes | -------------------------------------------------------------------------------------------------

    /// <summary>
    /// The ranks that can be reached in the simon says minigame.
    /// </summary>
    public enum RankType { Gold = 4, Silver = 3, Bronze = 2, Wood = 1 }
}
