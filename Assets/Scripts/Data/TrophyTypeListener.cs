namespace HappyGames.Data
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public class TrophyTypeListener : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called when a new trophy type was unlocked.")]
        protected UnityEvent<TrophyType> onTrophyTypeContained = default;

        [SerializeField, Tooltip("Called at the start with all unlocked trophy types.")]
        protected UnityEvent<IEnumerable<TrophyType>> onStartTypes = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        List<TrophyType> displayedTypes = new List<TrophyType>();


        // --- | Methods | -------------------------------------------------------------------------------------------------

        protected virtual void Awake()
        {
            foreach(TrophyType type in System.Enum.GetValues(typeof(TrophyType)))
            {
                if (ProfileManager.GetActiveProfile().Inventory.GetTrophyCount(type) > 0)
                {
                    displayedTypes.Add(type);
                }
            }
            onStartTypes?.Invoke(displayedTypes);
        }

        private void OnEnable()
        {
            ProfileManager.GetActiveProfile().Inventory.OnTrophyAdded += InventoryUpdate;
        }

        private void OnDisable()
        {
            ProfileManager.GetActiveProfile().Inventory.OnTrophyAdded -= InventoryUpdate;
        }

        /// <summary>
        /// Checks if a tab must be added.
        /// </summary>
        /// <param name="trophy">The trophy added to the inventory.</param>
        private void InventoryUpdate(Trophy trophy)
        {
            if (!displayedTypes.Contains(trophy.TrophyType))
            {
                onTrophyTypeContained?.Invoke(trophy.TrophyType);
            }
        }
    }
}
