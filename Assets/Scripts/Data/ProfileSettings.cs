namespace HappyGames.Data
{
    using HappyGames.Translations;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using UnityEngine.Events;

    [Serializable]
    public class ProfileSettings
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        [NonSerialized]
        private Action onSettingsUpdated;
        /// <summary>
        /// Called when the settings change.
        /// </summary>
        public event Action OnSettingsUpdated
        {
            add
            {
                if (onSettingsUpdated == null)
                {
                    OnLanguageChanged += l => { onSettingsUpdated?.Invoke(); };
                    OnVolumeChanged += (n, v) => { onSettingsUpdated?.Invoke(); };
                }
                onSettingsUpdated += value;
            }
            remove
            {
                onSettingsUpdated -= value;
                if (onSettingsUpdated == null)
                {
                    OnVolumeChanged -= (n, v) => { onSettingsUpdated?.Invoke(); };
                    OnLanguageChanged -= l => { onSettingsUpdated?.Invoke(); };
                }
            }
        }

        // Language -------------------------------------------------------------------------------
        public static Language defLanguage = Language.German;

        private Language language = defLanguage;
        /// <summary>
        /// The set language of the game.
        /// </summary>
        public Language Language => language;

        /// <summary>
        /// Called when the language was changed.
        /// </summary>
        [field: NonSerialized]
        public event UnityAction<Language> OnLanguageChanged = null;

        // Volume ---------------------------------------------------------------------------------

        protected Dictionary<string, (float value, bool muted)> volume = new Dictionary<string, (float, bool)>();

        /// <summary>
        /// Called when the value of the volume changes.
        /// </summary>
        [field: NonSerialized]
        public event Action<string, float> OnVolumeChanged;

        // Cursor ---------------------------------------------------------------------------------

        protected float cursorSize = 0.2f;
        /// <summary>
        /// The scale of the cursor.
        /// </summary>
        public float CursorSize => cursorSize;

        /// <summary>
        /// Called when the scale of the cursor changes.
        /// </summary>
        [field: NonSerialized]
        public event Action<float> OnCursorScaleUpdated;


        // --- | Methods | -------------------------------------------------------------------------------------------------
        // Volume ---------------------------------------------------------------------------------

        /// <summary>
        /// Returns the saved volume.
        /// </summary>
        /// <param name="key">The name of the volume to return.</param>
        /// <returns>The volume with the given name.</returns>
        public (float value, bool isMuted) GetVolume(string key)
        {
            if (!volume.ContainsKey(key))
            {
                volume.Add(key, (0f, false));
            }
            return volume[key];
        }

        /// <summary>
        /// Sets the volume and the muted state.
        /// </summary>
        /// <param name="key">The name of the volume.</param>
        /// <param name="volume">The value of the volume.</param>
        /// <param name="isMuted">True if the volume is muted.</param>
        public void SetVolume(string key, float volume, bool isMuted)
        {
            this.volume[key] = (volume, isMuted);
            OnVolumeChanged?.Invoke(key, volume);
        }
        /// <summary>
        /// Sets the volume.
        /// </summary>
        /// <param name="key">The name of the volume.</param>
        /// <param name="volume">The value of the volume.</param>
        public void SetVolume(string key, float volume) => SetVolume(key, volume, this.volume.TryGetValue(key, out (float, bool muted) v) ? v.muted : false);
        /// <summary>
        /// Sets the muted state of the volume.
        /// </summary>
        /// <param name="key">The name of the volume.</param>
        /// <param name="isMuted">True if the volume is muted.</param>
        public void SetVolume(string key, bool isMuted) => SetVolume(key, volume.TryGetValue(key, out (float value, bool) v) ? v.value : 0f, isMuted);

        // Language ----------------------------------------------------------------------------------

        /// <summary>
        /// Changes the language.
        /// </summary>
        public void ChangeLanguage(Language language)
        {
            defLanguage = language;
            this.language = language;
            OnLanguageChanged?.Invoke(language);
        }

        // Cursor ---------------------------------------------------------------------------------

        /// <summary>
        /// Called when the cursors scale changes.
        /// </summary>
        /// <param name="scale">The scle to save.</param>
        public void UpdateCursorScale(float scale)
        {
            cursorSize = scale;
            OnCursorScaleUpdated?.Invoke(scale);
        }

        // Surrogates -----------------------------------------------------------------------------

        /// <summary>
        /// Returns the surrogate for saving the highscores.
        /// </summary>
        /// <returns>The surrogate for the highscores.</returns>
        public static IEnumerable<(Type, ISerializationSurrogate)> GetSurrogates()
        {
            yield return (typeof(ProfileSettings), new MapStateSurrogate());
        }


        // --- | Classes | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Used to save highscores.
        /// </summary>
        private class MapStateSurrogate : ISerializationSurrogate
        {
            /// <summary>
            /// The name to save the volume under.
            /// </summary>
            private static readonly string VOlUEM_SAVE_NAME = $"{ nameof(ProfileSettings) }.Volumes";


            /// <inheritdoc/>
            public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
            {
                // Dictionaries aren't serializabel, so we convert it to a list to store its data.
                // Create the list for the data.
                List<(string, float, bool)> volume = new List<(string, float, bool)>();
                // Iterate over the data.
                foreach (var value in ((ProfileSettings)obj).volume)
                {
                    // Add the data to the list.
                    volume.Add((value.Key, value.Value.value, value.Value.muted));
                }
                // Save the list.
                info.AddValue(VOlUEM_SAVE_NAME, volume);
            }

            /// <inheritdoc/>
            public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
            {
                ProfileSettings settings = new ProfileSettings();
                // Make a new dictionary to store the saved data.
                settings.volume = new Dictionary<string, (float, bool)>();
                // Create a new list with the saved data.
                List<(string, float, bool)> list = (List<(string, float, bool)>)info.GetValue(VOlUEM_SAVE_NAME, typeof(List<(string, float, bool)>));
                // Iterate over the list.
                foreach ((string key, float value, bool muted) data in list)
                {
                    settings.volume[data.key] = (data.value, data.muted);
                }
                // Return the settings.
                return settings;
            }
        }
    }
}
