namespace HappyGames.Data
{
    using HappyGames.Translations;
    using UnityEngine;
    using UnityEngine.Events;

    public sealed class LanguageManager : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called when the languages changes.")]
        private UnityEvent<Language> onLanguageChange = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            if (ProfileManager.HasActiveProfile)
            {
                SwitchProfile(null, ProfileManager.GetActiveProfile());
            }
            else
            {
                onLanguageChange?.Invoke(default);
            }
        }

        private void OnDisable()
        {
            if (ProfileManager.HasActiveProfile)
            {
                SwitchProfile(ProfileManager.GetActiveProfile(), null);
            }
        }

        /// <summary>
        /// Switches between the used profiles.
        /// </summary>
        /// <param name="oldProfile">The previously used profile.</param>
        /// <param name="newProfile">The profile now in use.</param>
        private void SwitchProfile(UserProfile oldProfile, UserProfile newProfile)
        {
            if (oldProfile != null)
            {
                oldProfile.Settings.OnLanguageChanged -= onLanguageChange.Invoke;
            }
            if (newProfile != null)
            {
                newProfile.Settings.OnLanguageChanged += onLanguageChange.Invoke;
            }
        }

        /// <summary>
        /// Changes the language of the game.
        /// </summary>
        /// <param name="language">The language to switch to.</param>
        public void ChangeLanguage(Language language)
        {
            if (ProfileManager.HasActiveProfile)
            {
                ProfileManager.GetActiveProfile().Settings.ChangeLanguage(language);
            }
            else
            {
                onLanguageChange?.Invoke(language);
            }
        }
        /// <summary>
        /// Changes the language of the game.
        /// </summary>
        /// <param name="language">The language to switch to.</param>
        public void ChangeLanguge(string language)
        {
            if (System.Enum.TryParse(language, out Language l))
            {
                ChangeLanguage(l);
            }
        }
    }
}
