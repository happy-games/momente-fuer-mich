namespace HappyGames.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using UnityEngine;

    [Serializable]
    public class MapState
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        [NonSerialized]
        private Action onMapStateUpadated;
        /// <summary>
        /// Called when the map state changes.
        /// </summary>
        public event Action OnMapStateUpadated
        {
            add
            {
                if (onMapStateUpadated == null)
                {
                    OnPositionSaved += p => onMapStateUpadated();
                    OnInteratableUsed += i => { onMapStateUpadated?.Invoke(); };
                    OnInteratableReset += i => { onMapStateUpadated?.Invoke(); };
                    OnTrophyDisplayed += (id, t) => { onMapStateUpadated?.Invoke(); };
                    OnTrophyRemoved += (id, t) => { onMapStateUpadated?.Invoke(); };
                    OnBridgeAdded += b => { onMapStateUpadated?.Invoke(); };
                }
                onMapStateUpadated += value;
            }
            remove
            {
                onMapStateUpadated -= value;
                if (onMapStateUpadated == null)
                {
                    OnBridgeAdded -= b => { onMapStateUpadated?.Invoke(); };
                    OnTrophyRemoved -= (id, t) => { onMapStateUpadated?.Invoke(); };
                    OnTrophyDisplayed -= (id, t) => { onMapStateUpadated?.Invoke(); };
                    OnInteratableReset -= i => { onMapStateUpadated?.Invoke(); };
                    OnInteratableUsed -= i => { onMapStateUpadated?.Invoke(); };
                    OnPositionSaved -= p => onMapStateUpadated();
                }
            }
        }

        // Position -------------------------------------------------------------------------------

        /// <summary>
        /// True if the players hubworld position was saved.
        /// </summary>
        public bool IsPositionSet { get; private set; } = false;
        private Vector2 position = new Vector2(float.NaN, float.NaN);

        /// <summary>
        /// Called when a new player position was saved.
        /// </summary>
        [field: NonSerialized]
        public event Action<Vector2> OnPositionSaved;

        // Interactables --------------------------------------------------------------------------

        private HashSet<string> interactables = new HashSet<string>();
        /// <summary>
        /// Called when an interactable was used.
        /// </summary>
        [field: NonSerialized]
        public event Action<string> OnInteratableUsed;

        /// <summary>
        /// Called when an interactable resets.
        /// </summary>
        [field: NonSerialized]
        public event Action<string> OnInteratableReset;

        // Displays -------------------------------------------------------------------------------

        private Dictionary<int, Trophy> trophyDisplays = new Dictionary<int, Trophy>();
        /// <summary>
        /// Called when a new <see cref="Trophy"/> display was saved.
        /// </summary>
        [field: NonSerialized]
        public event Action<int, Trophy> OnTrophyDisplayed;

        /// <summary>
        /// Called when a <see cref="Trophy"/> was removed from a display.
        /// </summary>
        [field: NonSerialized]
        public event Action<int, Trophy> OnTrophyRemoved;

        // Bridges --------------------------------------------------------------------------------

        private HashSet<string> bridges = new HashSet<string>();
        /// <summary>
        /// Callde when a bridge was added.
        /// </summary>
        [field: NonSerialized]
        public event Action<string> OnBridgeAdded;


        // --- | Methods | -------------------------------------------------------------------------------------------------
        // Position -------------------------------------------------------------------------------

        /// <summary>
        /// Returns the last position.
        /// </summary>
        /// <returns>The last position</returns>
        public Vector2 GetSavedPosition() => position;

        /// <summary>
        /// Sets the position of the player.
        /// </summary>
        /// <param name="position">The position to save.</param>
        public void SavePositzion(Vector2 position)
        {
            IsPositionSet = true;
            this.position = position;
            OnPositionSaved?.Invoke(position);
        }

        // Interactables --------------------------------------------------------------------------

        /// <summary>
        /// Updates the state of the interactables.
        /// </summary>
        /// <param name="name">The name of the interactable (must be unique).</param>
        /// <param name="isUsed">True if the interactable is used.</param>
        public void UpdateInteractable(string name, bool isUsed)
        {
            if (isUsed)
            {
                if (!interactables.Contains(name))
                {
                    interactables.Add(name);
                    OnInteratableUsed?.Invoke(name);
                }
            }
            else
            {
                if (interactables.Contains(name))
                {
                    interactables.Remove(name);
                    OnInteratableReset?.Invoke(name);
                }
            }
        }

        /// <summary>
        /// Checks if an interactable is used.
        /// </summary>
        /// <param name="name">the name of the interactable.</param>
        /// <returns>true if the interactable was used.</returns>
        public bool CheckInteractable(string name) => interactables.Contains(name);

        // Displays -------------------------------------------------------------------------------

        /// <summary>
        /// Returns the saved <see cref="Trophy"/> of a display.
        /// </summary>
        /// <param name="id">The id of the display.</param>
        /// <returns>The trophy saved for the display.</returns>
        public Trophy GetSavedTrophyDisplay(int id)
        {
            if (trophyDisplays.ContainsKey(id))
            {
                return trophyDisplays[id];
            }
            return null;
        }

        /// <summary>
        /// Saves the trophy of a display.
        /// </summary>
        /// <param name="id">The id of the display.</param>
        /// <param name="trophy">The trophy to save.</param>
        public void SaveTophyDisplayContent(int id, Trophy trophy)
        {
            if (trophy != null)
            {
                trophyDisplays[id] = trophy;
                OnTrophyDisplayed?.Invoke(id, trophy);
            }
            else if (trophyDisplays.ContainsKey(id))
            {
                trophyDisplays.Remove(id);
                OnTrophyRemoved?.Invoke(id, trophy);
            }
        }

        // Bridges --------------------------------------------------------------------------------

        /// <summary>
        /// Add a bridge to the saved data.
        /// </summary>
        /// <param name="name">The name of the bridge.</param>
        public void AddBridge(string name)
        {
            if (!bridges.Contains(name))
            {
                bridges.Add(name);
                OnBridgeAdded?.Invoke(name);
            }
        }

        /// <summary>
        /// Checks if a bridge with the given name has been saved.
        /// </summary>
        /// <param name="name">The name of the bridge.</param>
        /// <returns>True if the bridge was added.</returns>
        public bool CheckBridge(string name) => bridges.Contains(name);

        // Surrogates -----------------------------------------------------------------------------

        /// <summary>
        /// Returns the surrogate for saving the map state.
        /// </summary>
        /// <returns>The surrogate for the map state.</returns>
        public static IEnumerable<(Type, ISerializationSurrogate)> GetSurrogates()
        {
            yield return (typeof(MapState), new MapStateSurrogate());
        }


        // --- | Classes | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Used to save map relevant data.
        /// </summary>
        private class MapStateSurrogate : ISerializationSurrogate
        {
            /// <summary>
            /// The name to save the <see cref="Trophy"/>s in displays.
            /// </summary>
            private static readonly string TROPHY_DISPLYS_SAVE_NAME = $"{ nameof(MapState) }.TrophyDisplays";
            /// <summary>
            /// The name to save the position set variable.
            /// </summary>
            private static readonly string POSITION_SET_SAVE_NAME = $"{ nameof(MapState) }.PositionSet";
            /// <summary>
            /// The name to save the x position of the player.
            /// </summary>
            private static readonly string POSITION_X_SAVE_NAME = $"{ nameof(MapState) }.PositionX";
            /// <summary>
            /// The name to save the y position of the player.
            /// </summary>
            private static readonly string POSITION_Y_SAVE_NAME = $"{ nameof(MapState) }.PositionY";
            /// <summary>
            /// The name to save the bridges under.
            /// </summary>
            private static readonly string BRIDGES_SAVE_NAME = $"{ nameof(MapState) }.Bridges";
            /// <summary>
            /// The name to save the interactables.
            /// </summary>
            private static readonly string INTERACTABLES_SAVE_NAME = $"{ nameof(MapState) }.Interactables";

            /// <inheritdoc/>
            public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
            {
                // Dictionaries aren't serializabel, so we convert it to a list to store its data.
                // Create the list for the data.
                List<(int, Trophy)> displays = new List<(int, Trophy)>();
                // Iterate over the data.
                foreach (var interactable in ((MapState)obj).trophyDisplays)
                {
                    // Add the data to the list.
                    displays.Add((interactable.Key, interactable.Value));
                }
                List<string> bridges = new List<string>();
                foreach (string bridge in ((MapState)obj).bridges)
                {
                    bridges.Add(bridge);
                }
                List<string> interactables = new List<string>();
                foreach (string interactable in ((MapState)obj).interactables)
                {
                    bridges.Add(interactable);
                }
                // Save the list.
                info.AddValue(TROPHY_DISPLYS_SAVE_NAME, displays);
                info.AddValue(POSITION_SET_SAVE_NAME, ((MapState)obj).IsPositionSet);
                info.AddValue(POSITION_X_SAVE_NAME, ((MapState)obj).position.x);
                info.AddValue(POSITION_Y_SAVE_NAME, ((MapState)obj).position.y);
                info.AddValue(BRIDGES_SAVE_NAME, bridges);
                info.AddValue(INTERACTABLES_SAVE_NAME, interactables);
            }

            /// <inheritdoc/>
            public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
            {
                MapState state = new MapState();
                // Set the position.
                state.position = new Vector2
                (
                    (float)info.GetValue(POSITION_X_SAVE_NAME, typeof(float)), 
                    (float)info.GetValue(POSITION_Y_SAVE_NAME, typeof(float))
                );
                // Set the saved position state.
                state.IsPositionSet = (bool)info.GetValue(POSITION_SET_SAVE_NAME, typeof(bool));
                // Make a new dictionary to store the saved data.
                state.trophyDisplays = new Dictionary<int, Trophy>();
                // Create a new list with the saved data.
                List<(int, Trophy)> displays = (List<(int, Trophy)>)info.GetValue(TROPHY_DISPLYS_SAVE_NAME, typeof(List<(int, Trophy)>));
                // Iterate over the list.
                foreach ((int key, Trophy value) data in displays)
                {
                    state.trophyDisplays[data.key] = data.value;
                }
                List<string> bridges = (List<string>)info.GetValue(BRIDGES_SAVE_NAME, typeof(List<string>));
                foreach (string bridge in bridges)
                {
                    state.bridges.Add(bridge);
                }
                List<string> interactables = (List<string>)info.GetValue(INTERACTABLES_SAVE_NAME, typeof(List<string>));
                foreach (string interactable in interactables)
                {
                    state.interactables.Add(interactable);
                }
                // Return the state.
                return state;
            }
        }
    }
}
