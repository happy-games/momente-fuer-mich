namespace HappyGames.Data
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "RankSet", menuName = "Set/Rank/Sprite")]
    public class RankSpriteSet : RankSet<Sprite> { }
}
