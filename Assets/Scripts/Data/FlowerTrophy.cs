namespace HappyGames.Data
{
    using HappyGames.Emotion;

    [System.Serializable]
    public sealed class FlowerTrophy : Trophy
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override TrophyType TrophyType => TrophyType.Flower;

        /// <summary>
        /// The emotion performed to complete the minigame and get the trophy.
        /// </summary>
        public EmotionType Emotion { get; private set; }

        /// <summary>
        /// The score reached when obtaining the resource.
        /// </summary>
        public int Score { get; private set; }


        // --- | Constructors | --------------------------------------------------------------------------------------------

        /// <summary>
        /// Creates a new resource of the flower type.
        /// </summary>
        /// <param name="emotion">The emotion performed to get the trophy.</param>
        /// <param name="score">The points scored when completing the minigame.</param>
        public FlowerTrophy(EmotionType emotion, int score) : base()
        {
            Emotion = emotion;
            Score = score;
        }
    }
}
