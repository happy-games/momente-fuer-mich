namespace HappyGames.Data
{
    using HappyGames.Saving;
    using System.Collections.Generic;
    using UnityEngine.Events;

    public static class ProfileManager
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The user profile currently in use.
        /// </summary>
        private static UserProfile ActiveProfile { get; set; } = null;

        /// <summary>
        /// Called when a user profile was added.
        /// </summary>
        public static event UnityAction<UserProfile> OnProfileAdded;

        /// <summary>
        /// Called when a user profile was removed.
        /// </summary>
        public static event UnityAction<UserProfile> OnProfileRemoved;

        /// <summary>
        /// Called when a profile was activated.
        /// </summary>
        public static event UnityAction<UserProfile, UserProfile> OnProfileActivated;

        /// <summary>
        /// The extension for the save files.
        /// </summary>
        private const string SAVE_FILE_EXTENSION = ".save";

        /// <summary>
        /// The subfolder for the saves.
        /// </summary>
        private static readonly string SAVE_FOLDER_NAME = string.Empty;

        /// <summary>
        /// True if a active profile was set.
        /// </summary>
        public static bool HasActiveProfile => ActiveProfile != null;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns the active profile and creates a new one of none is active.
        /// </summary>
        /// <returns>The active profile.</returns>
        public static UserProfile GetActiveProfile()
        {
            if (ActiveProfile == null)
            {
                // Creat a new profile and set it active, without registering it for saving.
                ActiveProfile = new UserProfile(string.Empty, new Emotion.EmotionType[] { Emotion.EmotionType.Angry, Emotion.EmotionType.Happy, Emotion.EmotionType.Surprised, Emotion.EmotionType.Sad });
                OnProfileActivated?.Invoke(null, ActiveProfile);
            }
            return ActiveProfile;
        }

        /// <summary>
        /// Returns all profiles.
        /// </summary>
        /// <returns>The profiles.</returns>
        public static IEnumerable<UserProfile> GetProfiles()
        {
            UpdateSurrogates();
            return SaveManager.Load<UserProfile>(SAVE_FOLDER_NAME, SAVE_FILE_EXTENSION);
        }

        /// <summary>
        /// Sets the active profile.
        /// </summary>
        /// <param name="profile">The profile to use.</param>
        public static void SetActiveProfile(UserProfile profile)
        {
            UserProfile deactivadedProfile = ActiveProfile;
            if (ActiveProfile != null)
            {
                ActiveProfile.OnProfileUpdated -= () => { SaveProfile(profile); };
            }
            ActiveProfile = profile;
            if (ActiveProfile != null)
            {
                ActiveProfile.OnProfileUpdated += () => { SaveProfile(profile); };
            }
            OnProfileActivated?.Invoke(deactivadedProfile, ActiveProfile);
        }

        /// <summary>
        /// Creates a new profile.
        /// </summary>
        /// <param name="name">The name of the profile.</param>
        /// <returns>The created profile.</returns>
        public static UserProfile CreateProfile(string name)
        {
            UserProfile profile = new UserProfile(name);
            SaveProfile(profile);
            OnProfileAdded?.Invoke(profile);
            return profile;
        }

        /// <summary>
        /// Deletes a profile.
        /// </summary>
        /// <param name="name">The name of the profile.</param>
        public static void DeleteProfile(string name)
        {
            UserProfile profile = GetProfileByName(name);
            SaveManager.Delete(SAVE_FOLDER_NAME + ProfileNameToFileName(name) + SAVE_FILE_EXTENSION);
            OnProfileRemoved?.Invoke(profile);
        }
        /// <summary>
        /// Deletes a profile.
        /// </summary>
        /// <param name="profile">The profile to delete.</param>
        public static void DeleteProfile(UserProfile profile) => DeleteProfile(profile.Name);

        /// <summary>
        /// Looks for a profile with the given name and returns it.
        /// </summary>
        /// <param name="name">The name of the profile.</param>
        /// <returns>The profile with the name.</returns>
        public static UserProfile GetProfileByName(string name) => SaveManager.Load<UserProfile>(SAVE_FOLDER_NAME + ProfileNameToFileName(name) + SAVE_FILE_EXTENSION);

        /// <summary>
        /// Checks if a profile with the name exists.
        /// </summary>
        /// <param name="name">The name of the profile.</param>
        /// <returns>True if a profile with the given name exists.</returns>
        public static bool CheckForProfile(string name) => SaveManager.CheckForFile(SAVE_FOLDER_NAME + ProfileNameToFileName(name) + SAVE_FILE_EXTENSION);

        /// <summary>
        /// Converts the users name into a name that can be used for the file.
        /// </summary>
        /// <param name="profileName">The name of the profile.</param>
        /// <returns>The name of the save file.</returns>
        private static string ProfileNameToFileName(string profileName)
        {
            foreach (char c in System.IO.Path.GetInvalidFileNameChars())
            {
                profileName = profileName.Replace(c, '_');
            }
            return profileName;
        }

        /// <summary>
        /// Saves a profile.
        /// </summary>
        /// <param name="profile">The profile to save.</param>
        private static void SaveProfile(UserProfile profile)
        {
            if (profile != null)
            {
                UpdateSurrogates();
                ProfileUpdateManager.SetDirty(profile);
            }
        }

        /// <summary>
        /// Adds all requiered surrogates.
        /// </summary>
        private static void UpdateSurrogates()
        {
            foreach (var surrogate in UserProfile.GetSurrogates())
            {
                SaveManager.AddSurrogate(surrogate.Item1, surrogate.Item2);
            }
        }


        // --- | Classes | -------------------------------------------------------------------------------------------------

        private class ProfileUpdateManager : UnityEngine.MonoBehaviour
        {
            // Variables --------------------------------------------------------------------------

            private static Dictionary<string, ProfileUpdateManager> instances = new Dictionary<string, ProfileUpdateManager>();

            private bool isDirty = false;

            private UserProfile profile;


            // Methods ----------------------------------------------------------------------------

            /// <summary>
            /// Registers a change in the profile.
            /// </summary>
            /// <param name="profile">The profile that changed.</param>
            public static void SetDirty(UserProfile profile)
            {
                if (!instances.ContainsKey(profile.Name) || instances[profile.Name] == null)
                {
                    SetSingelton(profile.Name);
                }
                instances[profile.Name].profile = profile;
                if (!instances[profile.Name].isDirty)
                {
                    instances[profile.Name].isDirty = true;
                    instances[profile.Name].StartCoroutine(instances[profile.Name].DoWaitForUpdate());
                }
            }

            /// <summary>
            /// Waits until the end of the frame and saves all changes at once.
            /// </summary>
            /// <param name="profile">The profile to save.</param>
            private System.Collections.IEnumerator DoWaitForUpdate()
            {
                yield return new UnityEngine.WaitForEndOfFrame();
                SaveManager.Save(SAVE_FOLDER_NAME + ProfileNameToFileName(profile.Name) + SAVE_FILE_EXTENSION, profile);
                isDirty = false;
            }

            /// <summary>
            /// Creates a new <see cref="ProfileUpdateManager"/> for a given <see cref="UserProfile"/>.
            /// </summary>
            /// <param name="name">The name of the profile to create the manager for.</param>
            private static void SetSingelton(string name)
            {
                instances[name] = new UnityEngine.GameObject($"ProfileUpdateManager ({ name })").AddComponent<ProfileUpdateManager>();
                UnityEngine.SceneManagement.SceneManager.MoveGameObjectToScene(instances[name].gameObject, UnityEngine.SceneManagement.SceneManager.GetActiveScene());
            }
        }
    }
}
