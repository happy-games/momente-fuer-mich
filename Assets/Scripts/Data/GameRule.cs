namespace HappyGames.Data
{
    using HappyGames.Translations;
    using UnityEngine;

    [CreateAssetMenu(fileName = "GameRule", menuName = "Rule/Rule")]
    public class GameRule : ScriptableObject
    {
        [SerializeField, Tooltip("The iamge to display.")]
        protected Sprite image = default;
        /// <summary>
        /// The iamge to display.
        /// </summary>
        public Sprite Iamge => image;

        [SerializeField, Tooltip("The heading to display.")]
        protected string heading = string.Empty;
        /// <summary>
        /// The heading to display.
        /// </summary>
        public string Headig => heading;

        [SerializeField, Tooltip("The heading to display in english.")]
        protected string heading_engl = string.Empty;
        /// <summary>
        /// The heading to display.
        /// </summary>
        public string Headig_engl => heading_engl;

        [SerializeField, TextArea(7, 10), Tooltip("The text to display.")]
        protected string text = string.Empty;

        [SerializeField, TextArea(7, 10), Tooltip("The text to display in english.")]
        protected string text_engl = string.Empty;

        [SerializeField]
        protected TranslatableCollection<string> translations = default;

        /// <summary>
        /// Returns the text to display.
        /// </summary>
        /// <param name="highlightPrefix">The string to put before the highlited content.</param>
        /// <param name="highlightSufix">The string to put after the highlited content.</param>
        /// <returns>The text to display.</returns>
        public string GetText(string highlightPrefix, string highlightSufix)
        {
            /*
            return translations.GetValue(ProfileManager.GetActiveProfile().Settings.Language)
                .Replace("<h>", $"<{ highlightPrefix }>").Replace("</h>", $"</{ highlightSufix }>");
            */
            if (ProfileManager.GetActiveProfile().Settings.Language == Language.German)
            {
                return text.Replace("<h>", $"<{ highlightPrefix }>").Replace("</h>", $"</{ highlightSufix }>");
            } 
            else
            {
                return text_engl.Replace("<h>", $"<{ highlightPrefix }>").Replace("</h>", $"</{ highlightSufix }>");
            }
        }
    }
}
