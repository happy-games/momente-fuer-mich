namespace HappyGames.Data
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(fileName = "RuleSet", menuName = "Rule/Set")]
    public class RuleSet : ScriptableObject, IEnumerable<GameRule>
    {
        [SerializeField, Tooltip("The rules in the set.")]
        protected GameRule[] rules = default;
        /// <summary>
        /// The rules in the set.
        /// </summary>
        public IEnumerable<GameRule> Rules => rules;

        /// <summary>
        /// The amount of rules in the set.
        /// </summary>
        public int Count => rules.Length;

        /// <inheritdoc/>
        public IEnumerator<GameRule> GetEnumerator()
        {
            foreach (GameRule rule in rules)
            {
                yield return rule;
            }
        }

        /// <inheritdoc/>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
