namespace HappyGames.Data
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Provides values for all ranks of trophies.
    /// </summary>
    /// <typeparam name="T">The type of the value.</typeparam>
    public abstract class RankSet<T> : ScriptableObject, IEnumerable<KeyValuePair<RankType, T>>
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The value for the gold rank.")]
        protected T gold = default;
        /// <summary>
        /// The value for the gold rank.
        /// </summary>
        public T Gold => gold;

        [SerializeField, Tooltip("The value for the silver rank.")]
        protected T silver = default;
        /// <summary>
        /// The value for the silver rank.
        /// </summary>
        public T Silver => silver;

        [SerializeField, Tooltip("The value for the bronze rank.")]
        protected T bronze = default;
        /// <summary>
        /// The value for the bronze rank.
        /// </summary>
        public T Bronze => bronze;

        [SerializeField, Tooltip("The value for the wood rank.")]
        protected T wood = default;
        /// <summary>
        /// The value for the wood rank.
        /// </summary>
        public T Wood => wood;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns the value for the rank.
        /// </summary>
        /// <param name="rank">The rank whos value to return.</param>
        /// <returns>The value for the rank.</returns>
        public T GetValue(RankType rank)
        {
            switch (rank)
            {
                default: return default(T);
                case RankType.Gold: return gold;
                case RankType.Silver: return silver;
                case RankType.Bronze: return bronze;
                case RankType.Wood: return wood;
            }
        }

        /// <inheritdoc/>
        public IEnumerator<KeyValuePair<RankType, T>> GetEnumerator()
        {
            yield return new KeyValuePair<RankType, T>(RankType.Gold, gold);
            yield return new KeyValuePair<RankType, T>(RankType.Silver, silver);
            yield return new KeyValuePair<RankType, T>(RankType.Bronze, bronze);
            yield return new KeyValuePair<RankType, T>(RankType.Wood, wood);
        }

        /// <inheritdoc/>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}