namespace HappyGames.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using UnityEngine;
    using UnityEngine.Events;

    [Serializable]
    public class Inventory
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        [NonSerialized]
        private UnityAction onInventoryUpdated;
        /// <summary>
        /// Called when the inventory changes.
        /// </summary>
        public event UnityAction OnInventoryUpdated
        {
            add
            {
                if (onInventoryUpdated == null)
                {
                    OnResourceCountUpdate += r => { onInventoryUpdated?.Invoke(); };
                    OnTrophyAdded += t => { onInventoryUpdated?.Invoke(); };
                    OnTrophyRemoved += t => { onInventoryUpdated?.Invoke(); };
                }
                onInventoryUpdated += value;
            }
            remove
            {
                onInventoryUpdated -= value;
                if (onInventoryUpdated == null)
                {
                    OnTrophyRemoved -= t => { onInventoryUpdated?.Invoke(); };
                    OnTrophyAdded -= t => { onInventoryUpdated?.Invoke(); };
                    OnResourceCountUpdate -= r => { onInventoryUpdated?.Invoke(); };
                }
            }
        }

        // Resources ------------------------------------------------------------------------------

        private int resourceCount = 0;
        /// <summary>
        /// The amount of resources the player owns.
        /// </summary>
        public int ResourceCount => resourceCount;

        /// <summary>
        /// Called when the resource count changes.
        /// </summary>
        [field: NonSerialized]
        public event UnityAction<int> OnResourceCountUpdate;

        // Trophies -------------------------------------------------------------------------------

        protected Dictionary<TrophyType, List<Trophy>> trophies;

        /// <summary>
        /// Called when new trophy was added.
        /// </summary>
        [field: NonSerialized]
        public event UnityAction<Trophy> OnTrophyAdded;

        /// <summary>
        /// Called when trophy
        /// was removed.
        /// </summary>
        [field: NonSerialized]
        public event UnityAction<Trophy> OnTrophyRemoved;


        // --- | Constructors | --------------------------------------------------------------------------------------------

        /// <summary>
        /// Creates a new inventory.
        /// </summary>
        public Inventory()
        {
            trophies = new Dictionary<TrophyType, List<Trophy>>();
        }


        // --- | Methods | -------------------------------------------------------------------------------------------------
        // Resources ------------------------------------------------------------------------------

        /// <summary>
        /// Adds resources to the inventory.
        /// </summary>
        /// <param name="amount">The amount of resources to add.</param>
        public void AddResources(int amount)
        {
            if (amount > 0)
            {
                resourceCount += amount;
                OnResourceCountUpdate?.Invoke(ResourceCount);
            }
        }

        /// <summary>
        /// Removes resources from the inventory.
        /// </summary>
        /// <param name="amount">The amount of resources to remove.</param>
        public void RemoveResources(int amount)
        {
            if (amount > 0)
            {
                resourceCount -= amount;
                OnResourceCountUpdate?.Invoke(ResourceCount);
            }
        }

        // Trophies -------------------------------------------------------------------------------

        /// <summary>
        /// Adds a <see cref="Trophy"/> to the inventory.
        /// </summary>
        /// <param name="trophy">The trophy to add.</param>
        public void AddTrophy(Trophy trophy)
        {
            Debug.Log("Inventory Remove Trophy: " + trophy);
            if (trophy != null)
            {
                // Check if a list for the trophies exists, add one if not.
                if (!trophies.ContainsKey(trophy.TrophyType))
                {
                    trophies.Add(trophy.TrophyType, new List<Trophy>());
                }
                // Add the trophy to the list.
                trophies[trophy.TrophyType].Add(trophy);
                // Invoke the event to inform listeners from the inventory change.
                OnTrophyAdded?.Invoke(trophy);
            }
        }

        /// <summary>
        /// Removes a <see cref="Trophy"/> from the inventory.
        /// </summary>
        /// <param name="trophy">The trophy to remove.</param>
        public void RemoveTrophy(Trophy trophy)
        {
            Debug.Log("Inventory Remove Trophy: " + trophy);
            if (trophies.ContainsKey(trophy.TrophyType))
            {
                trophies[trophy.TrophyType].Remove(trophy);
                OnTrophyRemoved?.Invoke(trophy);
            }
        }

        /// <summary>
        /// Iterates over all trophies of the given type.
        /// </summary>
        /// <param name="trophyType">The type of trophy to iterate over.</param>
        /// <returns>The resourtrophiesces of the type.</returns>
        public IEnumerable<Trophy> GetTrophies(TrophyType trophyType)
        {
            if (trophies.ContainsKey(trophyType))
            {
                foreach (Trophy trophy in trophies[trophyType])
                {
                    yield return trophy;
                }
            }
        }
        /// <summary>
        /// Iterates over all trophies in the inventory.
        /// </summary>
        /// <returns>The trophies of the type.</returns>
        public IEnumerable<Trophy> GetTrophies()
        {
            foreach (TrophyType trophyType in System.Enum.GetValues(typeof(TrophyType)))
            {
                foreach (Trophy trophy in GetTrophies(trophyType))
                {
                    yield return trophy;
                }
            }
        }

        /// <summary>
        /// Returns the amount of trophies in the inventory.
        /// </summary>
        /// <param name="trophyType">The type of trophies to count.</param>
        /// <returns>The amount of trophies in the inventory.</returns>
        public int GetTrophyCount(TrophyType trophyType)
        {
            if (trophies.ContainsKey(trophyType))
            {
                return trophies[trophyType].Count;
            }
            return 0;
        }
        /// <summary>
        /// Returns the amount of trophies in the inventory.
        /// </summary>
        /// <returns>The amount of trophies in the inventory.</returns>
        public int GetTrophyCount()
        {
            int count = 0;
            foreach (TrophyType trophy in System.Enum.GetValues(typeof(TrophyType)))
            {
                count += GetTrophyCount(trophy);
            }
            return count;
        }

        // Surrogates -----------------------------------------------------------------------------

        /// <summary>
        /// Returns a surrogate for saving inventories.
        /// </summary>
        /// <returns>The surrogate to save inventories.</returns>
        public static IEnumerable<(Type, ISerializationSurrogate)> GetSurrogates()
        {
            yield return (typeof(Inventory), new InventorySurrogate());
        }


        // --- | Classes | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Used to save <see cref="Inventory"/>s.
        /// </summary>
        private class InventorySurrogate : ISerializationSurrogate
        {
            /// <summary>
            /// The name to save the trophies as.
            /// </summary>
            private static readonly string TROPHY_SAVE_NAME = $"{ nameof(Inventory) }.{ nameof(Trophy) }s";

            /// <summary>
            /// The name to save the resource count as.
            /// </summary>
            private static readonly string RESOURCE_SAVE_NAME = $"{ nameof(Inventory) }.ResourceCount";

            /// <inheritdoc/>
            public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
            {
                // Dictionaries aren't serializabel, so we convert it to a list to store its data.
                // Create the list for the data.
                List<(TrophyType, Trophy)> trophies = new List<(TrophyType, Trophy)>();
                // Iterate over the data.
                foreach (var tropyCollection in ((Inventory)obj).trophies)
                {
                    foreach (Trophy trophy in tropyCollection.Value)
                    {
                        // Add the data to the list.
                        trophies.Add((tropyCollection.Key, trophy));
                    }
                }
                // Save the list.
                info.AddValue(TROPHY_SAVE_NAME, trophies);
                // Save the resource count.
                info.AddValue(RESOURCE_SAVE_NAME, ((Inventory)obj).resourceCount);
            }

            /// <inheritdoc/>
            public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
            {
                // Make a new inventory to store the saved data.
                Inventory inventory = new Inventory();
                // Create a new list with the saved data.
                List<(TrophyType, Trophy)> resources = (List<(TrophyType, Trophy)>)info.GetValue(TROPHY_SAVE_NAME, typeof(List<(TrophyType, Trophy)>));
                // Iterate over the list.
                foreach ((TrophyType trophyType, Trophy resource) data in resources)
                {
                    // Check if the inventory has a list for the type.
                    if (!inventory.trophies.ContainsKey(data.trophyType))
                    {
                        inventory.trophies[data.trophyType] = new List<Trophy>();
                    }
                    // Add the trophy to the list in the inventory.
                    inventory.trophies[data.trophyType].Add(data.resource);
                }
                // Get the resource count.
                inventory.resourceCount = info.GetInt32(RESOURCE_SAVE_NAME);
                // Return the inventory.
                return inventory;
            }
        }
    }
}
