namespace HappyGames.Data
{
    using HappyGames.Emotion;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public class UnlockedEmotionListener : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called when a new emotion was unlocked.")]
        protected UnityEvent<EmotionType> onEmotionUnlocked = default;

        [SerializeField, Tooltip("Called at the start of the scene with the unlocked emotions.")]
        protected UnityEvent<IEnumerable<EmotionType>> onStartEmotions = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------


        protected virtual void Awake()
        {
            onStartEmotions?.Invoke(ProfileManager.GetActiveProfile().UnlockedEmotions);
        }

        private void OnEnable()
        {
            ProfileManager.GetActiveProfile().OnEmotionUnlocked += onEmotionUnlocked.Invoke;
        }

        private void OnDisable()
        {
            ProfileManager.GetActiveProfile().OnEmotionUnlocked -= onEmotionUnlocked.Invoke;
        }
    }
}
