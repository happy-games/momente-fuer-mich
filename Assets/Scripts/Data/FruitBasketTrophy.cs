namespace HappyGames.Data
{
    [System.Serializable]
    public class FruitBasketTrophy : Trophy
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override TrophyType TrophyType => TrophyType.FruitBasket;

        private RankType rank = default;
        /// <summary>
        /// The size of the basket.
        /// </summary>
        public RankType Rank => rank;

        private int score = 0;
        /// <summary>
        /// The score reached.
        /// </summary>
        public int Score => score;


        // --- | Constructors | --------------------------------------------------------------------------------------------

        public FruitBasketTrophy(RankType rank, int score)
        {
            this.rank = rank;
            this.score = score;
        }
    }
}
