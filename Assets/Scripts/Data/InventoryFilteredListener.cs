namespace HappyGames.Data
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public class InventoryFilteredListener : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The trophy to listen to.")]
        protected TrophyType trophyType = default;

        [Space, SerializeField, Tooltip("Called when a trophy was added to the inventory.")]
        protected UnityEvent<Trophy> onTrophyAdded = default;

        [SerializeField, Tooltip("Called when a trophy was removed to the inventory.")]
        protected UnityEvent<Trophy> onTrophyRemoved = default;

        [Space, SerializeField, Tooltip("Called at the start of the scene with the trophies in the inventory.")]
        protected UnityEvent<IEnumerable<Trophy>> onStartTrophies = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            onStartTrophies?.Invoke(ProfileManager.GetActiveProfile().Inventory.GetTrophies(trophyType));
        }

        /// <summary>
        /// Invokes the resource added event if the <see cref="TrophyType"/> matches.
        /// </summary>
        /// <param name="resource">The resource added to the inventory.</param>
        private void ResourceAddedListener(Trophy resource)
        {
            if (resource.TrophyType == trophyType)
            {
                onTrophyAdded?.Invoke(resource);
            }
        }

        /// <summary>
        /// Invokes the resource removed event if the <see cref="TrophyType"/> matches.
        /// </summary>
        /// <param name="resource">The resource removed to the inventory.</param>
        private void ResourceRemovedListener(Trophy resource)
        {
            if (resource.TrophyType == trophyType)
            {
                onTrophyRemoved?.Invoke(resource);
            }
        }
    }
}
