namespace HappyGames.Data
{
    using HappyGames.Emotion;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using HappyGames.Translations;
    using UnityEngine;
    using UnityEngine.Events;

    [Serializable]
    public class UserProfile : IEquatable<UserProfile>
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        private string name = string.Empty;
        /// <summary>
        /// Thhe users name.
        /// </summary>
        public string Name => name;

        private string gameVersion = string.Empty;
        /// <summary>
        /// The version of the game when saved.
        /// </summary>
        public string GameVersion => gameVersion;

        private DateTime lastActivity;
        /// <summary>
        /// The time of the last profile change.
        /// </summary>
        public DateTime LastActivity => lastActivity;

        [NonSerialized]
        private Action onProfileUpdated;
        /// <summary>
        /// Called when the profile gets updated.
        /// </summary>
        public event Action OnProfileUpdated
        {
            add
            {
                if (onProfileUpdated == null)
                {
                    inventory.OnInventoryUpdated += UpdateTime;
                    mapState.OnMapStateUpadated += UpdateTime;
                    settings.OnSettingsUpdated += UpdateTime;
                    OnEmotionUnlocked += e => UpdateTime();
                    OnStoryPartChanged += p => UpdateTime();
                    OnStoryProgressed += (i, s) => UpdateTime();
                    OnRuleAdded += r => UpdateTime();
                    OnHighscoreUpdated += (k, v) => UpdateTime();
                }
                onProfileUpdated += value;
            }
            remove
            {
                onProfileUpdated -= value;
                if (onProfileUpdated == null)
                {
                    OnHighscoreUpdated -= (k, v) => UpdateTime();
                    OnRuleAdded -= r => UpdateTime();
                    OnStoryProgressed -= (i, s) => UpdateTime();
                    OnStoryPartChanged -= p => UpdateTime();
                    OnEmotionUnlocked -= e => UpdateTime();
                    settings.OnSettingsUpdated -= UpdateTime;
                    mapState.OnMapStateUpadated -= UpdateTime;
                    inventory.OnInventoryUpdated -= UpdateTime;
                }
            }
        }

        // Inventory ------------------------------------------------------------------------------

        private Inventory inventory;
        /// <summary>
        /// The useres inventory.
        /// </summary>
        public Inventory Inventory => inventory;

        // Map state ------------------------------------------------------------------------------

        private MapState mapState;
        /// <summary>
        /// The state of the map.
        /// </summary>
        public MapState MapState => mapState;

        // Settings -------------------------------------------------------------------------------

        private ProfileSettings settings;
        /// <summary>
        /// The saved settings.
        /// </summary>
        public ProfileSettings Settings => settings;

        // Story ----------------------------------------------------------------------------------

        /// <summary>
        /// The state of the storry.
        /// </summary>
        protected Dictionary<StoryElement, StoryState> StoryStates { get; set; } = new Dictionary<StoryElement, StoryState>();

        /// <summary>
        /// CAlled when the story progresses.
        /// </summary>
        [field: NonSerialized]
        public event Action<StoryElement, StoryState> OnStoryProgressed = null;

        /// <summary>
        /// True if the story was started.
        /// </summary>
        public bool StoryStarted => StoryStates.Count > 0;

        public int StoryPart { get; protected set; } = -1;

        /// <summary>
        /// Called when the story part changes.
        /// </summary>
        [field: NonSerialized]
        public event Action<int> OnStoryPartChanged = null;

        // Emotions -------------------------------------------------------------------------------

        protected SortedSet<EmotionType> unlockedEmotions;
        /// <summary>
        /// All amotions available to the player.
        /// </summary>
        public IEnumerable<EmotionType> UnlockedEmotions => unlockedEmotions;

        /// <summary>
        /// The amount of unlocked emotions.
        /// </summary>
        public int UnlockedEmottionCount => unlockedEmotions.Count;

        /// <summary>
        /// Called when a emotion was added.
        /// </summary>
        [field: NonSerialized]
        public event UnityAction<EmotionType> OnEmotionUnlocked;

        /// <summary>
        /// Called when the <see cref="UnlockedEmottionCount"/> changes.
        /// </summary>
        [field: NonSerialized]
        public event UnityAction<int> OnUnlockedEmotionCountUpdate;

        // Rules ----------------------------------------------------------------------------------

        private HashSet<string> explainedRules;

        /// <summary>
        /// Called when a <see cref="RuleSet"/> was explaned.
        /// </summary>
        [field: NonSerialized]
        public event Action<RuleSet> OnRuleAdded;

        // Highscore ------------------------------------------------------------------------------

        private Dictionary<string, int> highscores;

        /// <summary>
        /// Called when the value of a highscore changes.
        /// </summary>
        [field: NonSerialized]
        public event UnityAction<string, int> OnHighscoreUpdated;


        // --- | Constructors | --------------------------------------------------------------------------------------------

        /// <summary>
        /// Creates a new profile for a user.
        /// </summary>
        /// <param name="name">The name of the profile.</param>
        public UserProfile(string name)
        {
            this.name = name;

            inventory = new Inventory();
            mapState = new MapState();
            settings = new ProfileSettings();
            unlockedEmotions = new SortedSet<EmotionType>();
            explainedRules = new HashSet<string>();
            highscores = new Dictionary<string, int>();

            gameVersion = Application.version;
            lastActivity = DateTime.Now;
        }
        /// <summary>
        /// Creates a new profile for a user.
        /// </summary>
        /// <param name="name">The name of the profile.</param>
        /// <param name="emotions">The emotions unlocked for the user.</param>
        public UserProfile(string name, params EmotionType[] emotions) : this(name)
        {
            unlockedEmotions = new SortedSet<EmotionType>(emotions);
        }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Updates the activey data.
        /// </summary>
        private void UpdateTime()
        {
            gameVersion = Application.version;
            lastActivity = DateTime.Now;
            onProfileUpdated?.Invoke();
        }

        // Story ----------------------------------------------------------------------------------

        /// <summary>
        /// Returns the state of the story element.
        /// </summary>
        /// <param name="element">The element to check.</param>
        /// <returns>The state of the element.</returns>
        public StoryState GetStoryState(StoryElement element)
        {
            if (!StoryStates.ContainsKey(element))
            {
                return StoryState.Inactive;
            }
            else
            {
                return StoryStates[element];
            }
        }

        /// <summary>
        /// Updates the state of a story element.
        /// </summary>
        /// <param name="element">The element to update.</param>
        /// <param name="state">The state to apply.</param>
        public void SetStoryState(StoryElement element, StoryState state)
        {
            StoryStates[element] = state;
            OnStoryProgressed?.Invoke(element, state);
        }

        /// <summary>
        /// Sets the new story part.
        /// </summary>
        /// <param name="part">The part to apply.</param>
        public void SetStoryPart(int part)
        {
            StoryPart = part;
            OnStoryPartChanged?.Invoke(part);
        }

        // Emotion --------------------------------------------------------------------------------

        /// <summary>
        /// Adds an emotion to the unlocked emotions.
        /// </summary>
        /// <param name="emotion">The emotion to unlock.</param>
        public void UnlockEmotion(EmotionType emotion)
        {
            if (!unlockedEmotions.Contains(emotion))
            {
                unlockedEmotions.Add(emotion);
                OnEmotionUnlocked?.Invoke(emotion);
                OnUnlockedEmotionCountUpdate?.Invoke(UnlockedEmottionCount);
            }
        }

        /// <summary>
        /// True if the player has the emotion unlocked.
        /// </summary>
        /// <param name="emotion">The emotion to check.</param>
        /// <returns>True if the emotion is unlocked.</returns>
        public bool HasEmotionUnlocked(EmotionType emotion) => unlockedEmotions.Contains(emotion);

        // Rules ----------------------------------------------------------------------------------

        /// <summary>
        /// Checks if a <see cref="RuleSet"/> has been displayed before.
        /// </summary>
        /// <param name="rule">The rules to check for.</param>
        /// <returns>True if the rulest was displayed before.</returns>
        public bool CheckForRule(RuleSet rule)
        {
            return explainedRules.Contains(rule.name);
        }

        /// <summary>
        /// Adds a <see cref="RuleSet"/> to the explaned ones.
        /// </summary>
        /// <param name="rule">The rule to add.</param>
        public void AddRule(RuleSet rule)
        {
            explainedRules.Add(rule.name);
            OnRuleAdded?.Invoke(rule);
        }

        // Highscore ------------------------------------------------------------------------------

        /// <summary>
        /// Returns the current highscore.
        /// </summary>
        /// <param name="key">The name of the highscore.</param>
        /// <returns>The current highscore.</returns>
        public int GetHighscore(string key)
        {
            if (highscores.ContainsKey(key))
            {
                return highscores[key];
            }
            return 0;
        }

        /// <summary>
        /// Updates the highscore.
        /// </summary>
        /// <param name="key">The name of the highscore to update.</param>
        /// <param name="value">The new value of the highscore.</param>
        public void SetHighscore(string key, int value)
        {
            if (!highscores.ContainsKey(key) || highscores[key] < value)
            {
                highscores[key] = value;
                OnHighscoreUpdated?.Invoke(key, value);
            }
        }

        /// <summary>
        /// Retuns all highscores.
        /// </summary>
        /// <returns>The name and value of all highscores.</returns>
        public IEnumerable<KeyValuePair<string, int>> GetHighscores()
        {
            foreach (var highscore in highscores)
            {
                yield return highscore;
            }
        }

        // IEquatable -----------------------------------------------------------------------------

        /// <inheritdoc/>
        public bool Equals(UserProfile other)
        {
            return name == other.Name;
        }

        // Surrogates -----------------------------------------------------------------------------

        /// <summary>
        /// Returns the surrofate for saving the explaned rules.
        /// </summary>
        /// <returns>The surrofate for the rules.</returns>
        public static IEnumerable<(Type, ISerializationSurrogate)> GetSurrogates()
        {
            foreach (var surrogate in Inventory.GetSurrogates())
            {
                yield return surrogate;
            }
            foreach (var surrogate in ProfileSettings.GetSurrogates())
            {
                yield return surrogate;
            }
            foreach (var surrogate in MapState.GetSurrogates())
            {
                yield return surrogate;
            }
            yield return (typeof(Dictionary<string, int>), new HighscoreSurrogate());
            yield return (typeof(Dictionary<StoryElement, StoryState>), new StorySurrogate());
            yield return (typeof(HashSet<string>), new RulesSurrogate());
        }

        // --- | Classes | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Used to save the story.
        /// </summary>
        private class StorySurrogate : ISerializationSurrogate
        {
            /// <summary>
            /// The name to save the story as.
            /// </summary>
            private static readonly string SAVE_NAME = $"{ nameof(UserProfile) }.Story";

            /// <inheritdoc/>
            public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
            {
                // Dictionaries aren't serializabel, so we convert it to a list to store its data.
                // Create the list for the data.
                List<(StoryElement, StoryState)> story = new List<(StoryElement, StoryState)>();
                // Iterate over the data.
                foreach (var storyPart in ((Dictionary<StoryElement, StoryState>)obj))
                {
                    // Add the data to the list.
                    story.Add((storyPart.Key, storyPart.Value));
                }
                // Save the list.
                info.AddValue(SAVE_NAME, story);
            }

            /// <inheritdoc/>
            public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
            {
                // Make a new dictionary to store the saved data.
                Dictionary<StoryElement, StoryState> story = new Dictionary<StoryElement, StoryState>();
                // Create a new list with the saved data.
                List<(StoryElement, StoryState)> savedStory = (List<(StoryElement, StoryState)>)info.GetValue(SAVE_NAME, typeof(List<(StoryElement, StoryState)>));
                // Iterate over the list.
                foreach ((StoryElement key, StoryState value) data in savedStory)
                {
                    story[data.key] = data.value;
                }
                // Return the story.
                return story;
            }
        }
        /// <summary>
        /// Used to save highscores.
        /// </summary>
        private class HighscoreSurrogate : ISerializationSurrogate
        {
            /// <summary>
            /// The name to save the highscores as.
            /// </summary>
            private static readonly string HIGHSCORE_SAVE_NAME = $"{ nameof(UserProfile) }.Highscores";

            /// <inheritdoc/>
            public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
            {
                // Dictionaries aren't serializabel, so we convert it to a list to store its data.
                // Create the list for the data.
                List<(string, int)> highscores = new List<(string, int)>();
                // Iterate over the data.
                foreach (var score in ((Dictionary<string, int>)obj))
                {
                    // Add the data to the list.
                    highscores.Add((score.Key, score.Value));
                }
                // Save the list.
                info.AddValue(HIGHSCORE_SAVE_NAME, highscores);
            }

            /// <inheritdoc/>
            public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
            {
                // Make a new dictionary to store the saved data.
                Dictionary<string, int> highscores = new Dictionary<string, int>();
                // Create a new list with the saved data.
                List<(string, int)> savedScores = (List<(string, int)>)info.GetValue(HIGHSCORE_SAVE_NAME, typeof(List<(string, int)>));
                // Iterate over the list.
                foreach ((string key, int value) data in savedScores)
                {
                    highscores[data.key] = data.value;
                }
                // Return the highscores.
                return highscores;
            }
        }

        private class RulesSurrogate : ISerializationSurrogate
        {
            /// <summary>
            /// The name to save the explaned rule as.
            /// </summary>
            private static readonly string RULE_SAVE_NAME = $"{ nameof(UserProfile) }.ExplainedRules";

            /// <inheritdoc/>
            public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
            {
                // Dictionaries aren't serializabel, so we convert it to a list to store its data.
                // Create the list for the data.
                List<string> highscores = new List<string>();
                // Iterate over the data.
                foreach (var score in (HashSet<string>)obj)
                {
                    // Add the data to the list.
                    highscores.Add(score);
                }
                // Save the list.
                info.AddValue(RULE_SAVE_NAME, highscores);
            }

            /// <inheritdoc/>
            public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
            {
                // Make a new dictionary to store the saved data.
                HashSet<string> set = new HashSet<string>();
                // Create a new list with the saved data.
                List<string> list = (List<string>)info.GetValue(RULE_SAVE_NAME, typeof(List<string>));
                // Iterate over the list.
                foreach (string data in list)
                {
                    set.Add(data);
                }
                // Return the rules.
                return set;
            }
        }
    }
}