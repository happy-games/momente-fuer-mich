namespace HappyGames.Data
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "RankColorSet", menuName = "Set/Rank/Color")]
    public class RankColorSet : RankSet<Color> { }
}
