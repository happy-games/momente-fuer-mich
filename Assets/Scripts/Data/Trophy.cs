namespace HappyGames.Data
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [Serializable]
    public abstract class Trophy : IEquatable<Trophy>
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        /// <summary>
        /// The type of the trophy.
        /// </summary>
        public abstract TrophyType TrophyType { get; }

        /// <summary>
        /// The date when the trophy was obtained.
        /// </summary>
        public DateTime Date { get; private set; }

        private long id;
        private static long idCount = long.MinValue;


        // --- | Constructor | ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Creates a new tropy.
        /// </summary>
        public Trophy()
        {
            Date = DateTime.Now;
            id = idCount++;
        }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Compares two resources.
        /// </summary>
        /// <param name="other">The resource to compare to.</param>
        /// <returns>True if the resouces are the same.</returns>
        public bool Equals(Trophy other) => id == other.id;
    }
    
    /// <summary>
    /// The typo of a resource.
    /// </summary>
    public enum TrophyType { Flower, FruitBasket, Baloon }

    /// <summary>
    /// A collection of values for all trophies types.
    /// </summary>
    /// <typeparam name="T">The type of the value.</typeparam>
    public abstract class TrophySet<T> : ScriptableObject, IEnumerable<KeyValuePair<TrophyType, T>>
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The value for the flower type.")]
        private T flower = default;
        /// <summary>
        /// The value for the flower type.
        /// </summary>
        public T Flower => flower;

        [SerializeField, Tooltip("The value for the fruit basket type.")]
        private T fruitBasket = default;
        /// <summary>
        /// The value for the fruit basket type.
        /// </summary>
        public T FruitBasket => fruitBasket;

        [SerializeField, Tooltip("The value for the medal type.")]
        private T medal = default;
        /// <summary>
        /// The value for the medal type.
        /// </summary>
        public T Medal => medal;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns the value for a spesific trophy type.
        /// </summary>
        /// <param name="trophyType">The trophy type whos value to return.</param>
        /// <returns>The value for the trophy type.</returns>
        public T GetValue(TrophyType trophyType)
        {
            switch (trophyType)
            {
                default: return default;
                case TrophyType.Flower: return flower;
                case TrophyType.FruitBasket: return fruitBasket;
                case TrophyType.Baloon: return medal;
            }
        }

        /// <inheritdoc/>
        public IEnumerator<KeyValuePair<TrophyType, T>> GetEnumerator()
        {
            yield return new KeyValuePair<TrophyType, T>(TrophyType.Flower, flower);
            yield return new KeyValuePair<TrophyType, T>(TrophyType.FruitBasket, fruitBasket);
            yield return new KeyValuePair<TrophyType, T>(TrophyType.Baloon, medal);
        }

        /// <inheritdoc/>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    /// <summary>
    /// A collection of values for all trophy types.
    /// </summary>
    /// <typeparam name="T">The type of the value.</typeparam>
    [Serializable]
    public class TrophyCollection<T> : TrophyCollection<T, T, T, T> { }
    /// <summary>
    /// A collection of values for all trophy types.
    /// </summary>
    /// <typeparam name="T">The parent type to use.</typeparam>
    /// <typeparam name="TFlower">The type for the value for the flower trophies.</typeparam>
    /// <typeparam name="TFruitbasket">The type for the value for the fruit basket trophies.</typeparam>
    /// <typeparam name="TMedal">The type for the value for the medal trophies.</typeparam>
    [Serializable]
    public class TrophyCollection<T, TFlower, TFruitbasket, TMedal> : IEnumerable<KeyValuePair<TrophyType, T>> where TFlower : T where TFruitbasket : T where TMedal : T
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The value for the flower type.")]
        private TFlower flower = default;
        /// <summary>
        /// The value for the flower type.
        /// </summary>
        public TFlower Flower => flower;

        [SerializeField, Tooltip("The value for the fruit basket type.")]
        private TFruitbasket fruitBasket;
        /// <summary>
        /// The value for the fruit basket type.
        /// </summary>
        public TFruitbasket Fruitbasket => fruitBasket;

        [SerializeField, Tooltip("The value for the medal type.")]
        private TMedal medal;
        /// <summary>
        /// The value for the medal type.
        /// </summary>
        public TMedal Medal => medal;


        // --- | Constructors | --------------------------------------------------------------------------------------------

        /// <summary>
        /// Creates a new collection for all trophy types.
        /// </summary>
        /// <param name="flower">The value for the flowers.</param>
        /// <param name="fruitBasket">The value for the fruit basket.</param>
        /// <param name="medal">The value for the medal.</param>
        public TrophyCollection(TFlower flower, TFruitbasket fruitBasket, TMedal medal)
        {
            this.flower = flower;
            this.fruitBasket = fruitBasket;
            this.medal = medal;
        }
        /// <summary>
        /// Creates a new collection for all trophy types.
        /// </summary>
        public TrophyCollection() : this(default, default, default) { }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns the value for a spesific trophy type.
        /// </summary>
        /// <param name="trophyType">The trophy type whos value to return.</param>
        /// <returns>The value for the trophy type.</returns>
        public T GetValue(TrophyType trophyType)
        {
            switch (trophyType)
            {
                default: return default;
                case TrophyType.Flower: return flower;
                case TrophyType.FruitBasket: return fruitBasket;
                case TrophyType.Baloon: return medal;
            }
        }

        /// <inheritdoc/>
        public IEnumerator<KeyValuePair<TrophyType, T>> GetEnumerator()
        {
            yield return new KeyValuePair<TrophyType, T>(TrophyType.Flower, flower);
            yield return new KeyValuePair<TrophyType, T>(TrophyType.FruitBasket, fruitBasket);
            yield return new KeyValuePair<TrophyType, T>(TrophyType.Baloon, medal);
        }
        /// <inheritdoc/>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
