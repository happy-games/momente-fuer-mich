namespace HappyGames.Data
{
    using HappyGames.Emotion;
    using UnityEngine;
    using UnityEngine.Events;

    public class UnlockedEmotionCountListener : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called when the number of unlocked emotions changes.")]
        protected UnityEvent<int> onCountUpdate = default;

        [SerializeField, Tooltip("Called at the start of the scene with the current count of unlocked emotions.")]
        protected UnityEvent<int> onStartCount = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            onStartCount?.Invoke(ProfileManager.GetActiveProfile().UnlockedEmottionCount);
        }

        private void OnEnable()
        {
            ProfileManager.GetActiveProfile().OnUnlockedEmotionCountUpdate += onCountUpdate.Invoke;
        }

        private void OnDisable()
        {
            ProfileManager.GetActiveProfile().OnUnlockedEmotionCountUpdate -= onCountUpdate.Invoke;
        }
    }
}
