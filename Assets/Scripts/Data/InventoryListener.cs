namespace HappyGames.Data
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public class InventoryListener : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("Called when a trophy was added to the inventory.")]
        protected UnityEvent<Trophy> onTrophyAdded = default;

        [SerializeField, Tooltip("Called when a trophy was removed to the inventory.")]
        protected UnityEvent<Trophy> onTrophyRemoved = default;

        [Space, SerializeField, Tooltip("Called at the start of the scene with the trophy in the inventory.")]
        protected UnityEvent<IEnumerable<Trophy>> onStartTrophy = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            onStartTrophy?.Invoke(ProfileManager.GetActiveProfile().Inventory.GetTrophies());
            ProfileManager.GetActiveProfile().Inventory.OnTrophyAdded += onTrophyAdded.Invoke;
            ProfileManager.GetActiveProfile().Inventory.OnTrophyRemoved += onTrophyRemoved.Invoke;
        }

        private void OnDisable()
        {
            ProfileManager.GetActiveProfile().Inventory.OnTrophyAdded -= onTrophyAdded.Invoke;
            ProfileManager.GetActiveProfile().Inventory.OnTrophyRemoved -= onTrophyRemoved.Invoke;
        }
    }
}
