using HappyGames.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

namespace HappyGames.Translations
{
    public class TranslationDialog : MonoBehaviour
    {
        public DialogueRunner runner;

        public YarnProgram german;
        public YarnProgram english;


        private void OnEnable()
        {
            ProfileManager.OnProfileActivated += ChangeProfile;
            if (ProfileManager.HasActiveProfile)
            {
                ChangeProfile(null, ProfileManager.GetActiveProfile());
            }
            else
            {
                ChangeText(default);
            }
        }

        private void OnDisable()
        {
            if (ProfileManager.HasActiveProfile)
            {
                ChangeProfile(ProfileManager.GetActiveProfile(), null);
            }
            ProfileManager.OnProfileActivated -= ChangeProfile;
        }


        private void ChangeText(Language l)
        {
            Debug.Log("changing dialog");
            if (l == Language.German)
            {
                runner.yarnScripts[0] = german;
                runner.textLanguage = Language.German.ToString();
            }
            else if (l == Language.English)
            {
                runner.yarnScripts[0] = english;
                runner.textLanguage = Language.English.ToString();
            }
        }

        private void ChangeProfile(UserProfile oldProfile, UserProfile newProfile)
        {
            if (oldProfile != null)
            {
                oldProfile.Settings.OnLanguageChanged -= ChangeText;
            }
            if (newProfile != null)
            {
                newProfile.Settings.OnLanguageChanged += ChangeText;
                ChangeText(newProfile.Settings.Language);
            }
        }
    }
}
