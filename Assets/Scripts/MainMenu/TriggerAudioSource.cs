using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerAudioSource : MonoBehaviour
{
    public AudioSource audioSource;
    public Slider slider;

    public void OnClick()
    {
        audioSource.volume = slider.value;
        audioSource.Play();
    }
}
