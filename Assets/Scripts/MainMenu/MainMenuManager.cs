namespace HappyGames.UI
{
    using HappyGames.Data;
    using UnityEngine;
    using UnityEngine.Events;

    public class MainMenuManager : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The menu to select a profile.")]
        protected MiniGameMenu profileSelect = default;

        [SerializeField, Tooltip("The menu to select the next step.")]
        protected MiniGameMenu mainSelect = default;

        [SerializeField, Tooltip("Called when a profile was loaded.")]
        protected UnityEvent<UserProfile> onProfileLoaded = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Start()
        {
            if (ProfileManager.HasActiveProfile && !string.IsNullOrEmpty(ProfileManager.GetActiveProfile().Name))
            {
                mainSelect.Open();
                onProfileLoaded?.Invoke(ProfileManager.GetActiveProfile());
            }
            else
            {
                profileSelect.Open();
            }
        }

        private void OnEnable()
        {
            ProfileManager.OnProfileActivated += (o, n) => onProfileLoaded.Invoke(n);
        }

        private void OnDisable()
        {
            ProfileManager.OnProfileActivated -= (o, n) => onProfileLoaded.Invoke(n);
        }
    }
}
