namespace HappyGames.Translations
{
    using HappyGames.Data;
    using UnityEngine;
    using UnityEngine.UI;

    public class TranslatableImageSwapper : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The images whos sprite to change.")]
        private Image target = null;

        [SerializeField, Tooltip("The sprites for the different languages.")]
        private TranslatableCollection<Sprite> sprites = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            if (ProfileManager.HasActiveProfile)
            {
                SwitchProfile(null, ProfileManager.GetActiveProfile());
            }
            else
            {
                UpdateSprite(default);
            }
            ProfileManager.OnProfileActivated += SwitchProfile;
        }

        private void OnDisable()
        {
            ProfileManager.OnProfileActivated -= SwitchProfile;
            if (ProfileManager.HasActiveProfile)
            {
                SwitchProfile(ProfileManager.GetActiveProfile(), null);
            }
        }

        /// <summary>
        /// Switches the profile in use.
        /// </summary>
        /// <param name="oldProfile">The previously used profile.</param>
        /// <param name="newProfile">The now used profile.</param>
        private void SwitchProfile(UserProfile oldProfile, UserProfile newProfile)
        {
            if (oldProfile != null)
            {
                oldProfile.Settings.OnLanguageChanged -= UpdateSprite;
            }
            if (newProfile != null)
            {
                newProfile.Settings.OnLanguageChanged -= UpdateSprite;
                UpdateSprite(newProfile.Settings.Language);
            }
        }

        /// <summary>
        /// Updates the sprite to duisplay the correct language.
        /// </summary>
        /// <param name="language">The language to use.</param>
        private void UpdateSprite(Language language) => target.sprite = sprites.GetValue(language);
    }
}
