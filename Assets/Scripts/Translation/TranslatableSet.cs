namespace HappyGames.Translations
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Provides values for the different <see cref="Language"/>s.
    /// </summary>
    /// <typeparam name="T">The type of the values.</typeparam>
    public abstract class TranslatableSet<T> : ScriptableObject, IEnumerable<KeyValuePair<Language, T>>
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The value for the german language.")]
        protected T german = default;
        /// <summary>
        /// The value for the german language.
        /// </summary>
        public T German => german;

        [SerializeField, Tooltip("The value for the english language.")]
        protected T english = default;
        /// <summary>
        /// The value for the english language.
        /// </summary>
        public T English => english;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Clones the set.
        /// </summary>
        /// <param name="german">The value for the german language.</param>
        /// <param name="english">The value for the english language.</param>
        public TranslatableSet<T> Clone(T german, T english)
        {
            TranslatableSet<T> set = Instantiate(this);
            set.german = german;
            set.english = english;
            return set;
        }

        /// <summary>
        /// Returns the value for the given <see cref="Language"/>.
        /// </summary>
        /// <param name="language">The language whos value to return.</param>
        /// <returns>The value for the given <see cref="Language"/>.</returns>
        public T GetValue(Language language)
        {
            switch (language)
            {
                case Language.German: return german;
                case Language.English: return english;
                default: return default;
            }
        }

        /// <inheritdoc/>
        public IEnumerator<KeyValuePair<Language, T>> GetEnumerator()
        {
            yield return new KeyValuePair<Language, T>(Language.German, german);
            yield return new KeyValuePair<Language, T>(Language.English, english);
        }

        /// <inheritdoc/>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    /// <summary>
    /// Creates a new collection.
    /// </summary>
    /// <typeparam name="T">The type of the values in the collection.</typeparam>
    [System.Serializable]
    public class TranslatableCollection<T> : TranslatableCollection<T, T, T> 
    {
        public TranslatableCollection() : base() { }
        public TranslatableCollection(T german, T english) : base(german, english) { }
    }
    /// <summary>
    /// Creates a new collection.
    /// </summary>
    /// <typeparam name="T">The base type of the collections values.</typeparam>
    /// <typeparam name="TGerman">The type for the german value.</typeparam>
    /// <typeparam name="TEnglish">The type for the english value.</typeparam>
    [System.Serializable]
    public class TranslatableCollection<T, TGerman, TEnglish> : IEnumerable<KeyValuePair<Language, T>> 
        where TGerman : T where TEnglish : T
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The value for the german language.")]
        protected TGerman german = default;
        /// <summary>
        /// The value for the german language.
        /// </summary>
        public TGerman German => german;

        [SerializeField, Tooltip("The value for the english language.")]
        protected TEnglish english = default;
        /// <summary>
        /// The value for the english language.
        /// </summary>
        public TEnglish English => english;


        // --- | Constructor | ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Creates a new collection.
        /// </summary>
        public TranslatableCollection() : this(default, default) { }
        /// <summary>
        /// Creates a new collection.
        /// </summary>
        /// <param name="german">The value for the german language.</param>
        /// <param name="english">The value for the english language.</param>
        public TranslatableCollection(TGerman german, TEnglish english)
        {
            this.german = german;
            this.english = english;
        }


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns the value for the given <see cref="Language"/>.
        /// </summary>
        /// <param name="language">The language whos value to return.</param>
        /// <returns>The value for the given <see cref="Language"/>.</returns>
        public T GetValue(Language language)
        {
            switch (language)
            {
                case Language.German: return german;
                case Language.English: return english;
                default: return default;
            }
        }

        /// <inheritdoc/>
        public IEnumerator<KeyValuePair<Language, T>> GetEnumerator()
        {
            yield return new KeyValuePair<Language, T>(Language.German, german);
            yield return new KeyValuePair<Language, T>(Language.English, english);
        }

        /// <inheritdoc/>
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
