namespace HappyGames.Translations
{
    using HappyGames.Data;
    using UnityEngine;

    public class TranslatableAnimationSwapper : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The animator to target.")]
        private Animator target = default;

        [SerializeField, Tooltip("The controller to use for the different languages.")]
        private TranslatableCollection<RuntimeAnimatorController> controller = default;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void OnEnable()
        {
            if (ProfileManager.HasActiveProfile)
            {
                SwtichProfile(null, ProfileManager.GetActiveProfile());
            }
            else
            {
                UpdateController(default);
            }
            ProfileManager.OnProfileActivated += SwtichProfile;
        }

        private void OnDisable()
        {
            ProfileManager.OnProfileActivated -= SwtichProfile;
            if (ProfileManager.HasActiveProfile)
            {
                SwtichProfile(ProfileManager.GetActiveProfile(), null);
            }
        }

        /// <summary>
        /// Updates the used profile.
        /// </summary>
        /// <param name="oldProfile">The previously used profile.</param>
        /// <param name="newProfile">The now used profile.</param>
        private void SwtichProfile(UserProfile oldProfile, UserProfile newProfile)
        {
            if (oldProfile != null)
            {
                oldProfile.Settings.OnLanguageChanged -= UpdateController;
            }
            if (newProfile != null)
            {
                newProfile.Settings.OnLanguageChanged += UpdateController;
                UpdateController(newProfile.Settings.Language);
            }
        }

        /// <summary>
        /// Updates the used controller wor the given language.
        /// </summary>
        /// <param name="language">The language to use.</param>
        private void UpdateController(Language language) => target.runtimeAnimatorController = controller.GetValue(language);
    }
}
