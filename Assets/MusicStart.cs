using HappyGames.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicStart : MonoBehaviour
{
    public AudioSource music, athmo;
    // Start is called before the first frame update
    void Start()
    {
        if(ProfileManager.GetActiveProfile().StoryPart < 1)
        {
            music.Play();
            athmo.Play();
        }
    }

}
