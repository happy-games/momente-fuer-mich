namespace HappyGames.Editor
{
    using UnityEngine;
    using UnityEditor;
    using HappyGames.Utilities;
    using System.Collections.Generic;

    [CustomEditor(typeof(SmoothLine)), CanEditMultipleObjects]
    public class SmoothLineDrawer : Editor
    {
        private List<LineData> lines;
        private float xStart = 0f;
        private float yStart = 0f;
        private float xEnd = 0f;
        private float yEnd = 0f;

        private float startOffset = 0f;
        private float endOffset = 0f;

        private bool useLocalXStart = true;
        private bool useLocalYStart = true;
        private bool useLocalXEnd = true;
        private bool useLocalYEnd = true;

        private bool isHorizontal = true;
        private AnimationCurve curve = new AnimationCurve(new Keyframe(), new Keyframe(1f, 1f));

        private Vector2 position;
        private bool hasChanges = false;


        private void OnEnable()
        {
            lines = new List<LineData>();
            foreach (var target in targets)
            {
                LineData line = new LineData(this, (SmoothLine)target);
                GetValues(line.line);
                CalculateValues(line);
                lines.Add(line);
            }
            SceneView.duringSceneGui += OnSceneGUI;
            Undo.undoRedoPerformed += UpdateAll;
            SceneView.RepaintAll();
        }


        private void OnDisable()
        {
            Undo.undoRedoPerformed -= UpdateAll;
            SceneView.duringSceneGui -= OnSceneGUI;
        }

        public override void OnInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();

            EditorGUILayout.LabelField("Start", EditorStyles.boldLabel);
            useLocalXStart = EditorGUILayout.Toggle("Local X", useLocalXStart);
            useLocalYStart = EditorGUILayout.Toggle("Local Y", useLocalYStart);

            xStart = EditorGUILayout.FloatField(useLocalXStart ? "X Offset" : "X Position", xStart);
            yStart = EditorGUILayout.FloatField(useLocalYStart ? "Y Offset" : "Y Position", yStart);

            startOffset = EditorGUILayout.Slider("Offset", startOffset, 0f, 1f);
            if (startOffset > 1f - endOffset)
            {
                endOffset = 1f - startOffset;
            }

            EditorGUILayout.LabelField("End", EditorStyles.boldLabel);
            useLocalXEnd = EditorGUILayout.Toggle("Local X", useLocalXEnd);
            useLocalYEnd = EditorGUILayout.Toggle("Local Y", useLocalYEnd);

            xEnd = EditorGUILayout.FloatField(useLocalXEnd ? "X Offset" : "X Position", xEnd);
            yEnd = EditorGUILayout.FloatField(useLocalYEnd ? "Y Offset" : "Y Position", yEnd);

            endOffset = EditorGUILayout.Slider("Offset", endOffset, 0f, 1f);
            if (endOffset > 1f - startOffset)
            {
                startOffset = 1f - endOffset;
            }

            EditorGUILayout.LabelField("Curve", EditorStyles.boldLabel);
            isHorizontal = EditorGUILayout.Toggle("Is Horizonzal", isHorizontal);
            curve = EditorGUILayout.CurveField("Curve", curve);

            if (EditorGUI.EndChangeCheck())
            {
                hasChanges = true;
                foreach (LineData line in lines)
                {
                    CalculateValues(line);
                }
                SceneView.RepaintAll();
            }
            EditorGUILayout.Space();
            if (GUILayout.Button(hasChanges ? "Update" : "Reapply") && hasChanges)
            {
                ApplyChanges();
            }
        }

        private void UpdateAll()
        {
            foreach (LineData line in lines)
            {
                GetValues(line.line);
                CalculateValues(line);
            }
        }

        private void GetValues(SmoothLine line)
        {
            xStart = line.Start.x;
            yStart = line.Start.y;
            xEnd = line.End.x;
            yEnd = line.End.y;
            useLocalXStart = line.UseLocalXStart;
            useLocalYStart = line.UseLocalYStart;
            useLocalXEnd = line.UseLocalXEnd;
            useLocalYEnd = line.UseLocalYEnd;
            isHorizontal = line.IsHorizontal;
            startOffset = line.StartOffset;
            endOffset = line.EndOffset;
            curve = line.Curve;
        }

        private void CalculateValues(LineData line)
        {
            line.localStartPoint = SmoothLine.CalculateStartPoint(line);
            line.localEndPoint = SmoothLine.CalculateEndPoint(line);
            line.delta = line.localEndPoint - line.localStartPoint;
            position = line.line.transform.position;
        }

        private void ApplyChanges()
        {
            foreach (LineData line in lines)
            {
                Undo.RecordObjects(new Object[] { line.line, line.line.LineRenderer }, "Curve Update");
                line.line.UpdateAll(new Vector2(xStart, yStart), new Vector2(xEnd, yEnd), useLocalXStart, useLocalYStart, useLocalXEnd, useLocalYEnd, startOffset, endOffset, curve, isHorizontal);
            }
            hasChanges = false;
        }

        private void OnSceneGUI(SceneView sceneView)
        {
            if (Selection.activeTransform?.position != position)
            {
                hasChanges = true;
                foreach (LineData line in lines)
                {
                    CalculateValues(line);
                }
            }
            foreach (LineData line in lines)
            {
                for (float i = 1; i <= line.line.LineRenderer.positionCount; i++)
                {
                    float start = (i - 1f) / line.line.LineRenderer.positionCount;
                    float end = i / line.line.LineRenderer.positionCount;
                    if (startOffset + endOffset != 1f)
                    {
                        if (start <= startOffset && end > startOffset)
                        {
                            Handles.DrawLine(SmoothLine.GetGloablPoint(line, start), SmoothLine.GetGloablPoint(line, startOffset));
                            start = startOffset;
                            Handles.color = Color.red;
                        }
                        if (start <= 1f - endOffset && end > 1f - endOffset)
                        {
                            Handles.DrawLine(SmoothLine.GetGloablPoint(line, start), SmoothLine.GetGloablPoint(line, 1f - endOffset));
                            Handles.color = Color.white;
                            start = 1f - endOffset;
                        }
                    }
                    Handles.DrawLine(SmoothLine.GetGloablPoint(line, start), SmoothLine.GetGloablPoint(line, end));
                }
            }
        }

        private class LineData : ISmoothLine
        {
            public SmoothLine line;
            public SmoothLineDrawer drawer;

            public LineData(SmoothLineDrawer drawer,  SmoothLine line)
            {
                this.line = line;
                this.drawer = drawer;
            }

            /// <inheritdoc/>
            public Vector2 Start => new Vector2(drawer.xStart, drawer.yStart);

            /// <inheritdoc/>
            public Vector2 End => new Vector2(drawer.xEnd, drawer.yEnd);

            /// <inheritdoc/>
            public bool UseLocalXStart => drawer.useLocalXStart;

            /// <inheritdoc/>
            public bool UseLocalYStart => drawer.useLocalYStart;

            /// <inheritdoc/>
            public bool UseLocalXEnd => drawer.useLocalXEnd;

            /// <inheritdoc/>
            public bool UseLocalYEnd => drawer.useLocalYEnd;

            /// <inheritdoc/>
            public bool IsHorizontal => drawer.isHorizontal;

            public Vector2 localStartPoint;
            /// <inheritdoc/>
            public Vector2 LocalStartPoint => localStartPoint;

            public Vector2 localEndPoint;
            /// <inheritdoc/>
            public Vector2 LocalEndPoint => localEndPoint;

            public Vector2 delta;
            /// <inheritdoc/>
            public Vector2 PointDelta => delta;

            /// <inheritdoc/>
            public Transform transform => line.transform;

            /// <inheritdoc/>
            public AnimationCurve Curve => drawer.curve;

            /// <inheritdoc/>
            public float StartOffset => drawer.startOffset;

            /// <inheritdoc/>
            public float EndOffset => drawer.endOffset;
        }
    }
}
