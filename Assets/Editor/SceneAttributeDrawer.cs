﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Draws the properies with the <see cref="SceneAttribute"/>.
/// </summary>
[CustomPropertyDrawer(typeof(SceneAttribute))]
public class SceneAttributeDrawer : PropertyDrawer
{
    // --- | Variables | ---------------------------------------------------------------------------------------------------

    string warningMessage = string.Empty;
    const float WARNING_FIELD_HEIGHT = 30f;
    bool HasWarning => !string.IsNullOrEmpty(warningMessage);

    // Messages -----------------------------------------------------------------------------------

    /// <summary>
    /// The message to display if the attribute is used with an invalid type.
    /// </summary>
    const string INVALID_TYPE_ERRROR_MESSAGE = "Use [Scene] with strings.";

    /// <summary>
    /// The string used to format the warning message if the scene was invalid.<br></br>
    /// 0 | The scene name.
    /// </summary>
    const string INVALID_SCENE_WARNING_MESSAGE_FORMAT = "The scene [0] cannot be used. To use this scene add it to the build settings for the project";


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float height = base.GetPropertyHeight(property, label);
        if (HasWarning)
        {
            height += WARNING_FIELD_HEIGHT + EditorGUIUtility.standardVerticalSpacing;
        }
        return height;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.propertyType == SerializedPropertyType.String)
        {
            var sceneObject = GetSceneObject(property.stringValue);
            var scene = EditorGUI.ObjectField(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight), label, sceneObject, typeof(SceneAsset), true);
            if (scene == null)
            {
                property.stringValue = string.Empty;
            }
            else if (scene.name != property.stringValue)
            {
                var sceneObj = GetSceneObject(scene.name);
                if (sceneObj == null)
                {
                    property.stringValue = string.Empty;
                    warningMessage = string.Format(INVALID_SCENE_WARNING_MESSAGE_FORMAT, scene.name);
                }
                else
                {
                    property.stringValue = scene.name;
                    warningMessage = string.Empty;
                }
            }
            if (HasWarning)
            {
                Rect warningField = position;
                warningField.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
                warningField.height = WARNING_FIELD_HEIGHT;
                EditorGUI.HelpBox(warningField, warningMessage, MessageType.Warning);
            }
        }
        else
        {
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            EditorGUI.HelpBox(position, INVALID_TYPE_ERRROR_MESSAGE, MessageType.Error);
        }
    }

    protected SceneAsset GetSceneObject(string sceneObjectName)
    {
        if (string.IsNullOrEmpty(sceneObjectName))
        {
            return null;
        }

        foreach (var editorScene in EditorBuildSettings.scenes)
        {
            if (editorScene.path.Contains(sceneObjectName))
            {
                return AssetDatabase.LoadAssetAtPath(editorScene.path, typeof(SceneAsset)) as SceneAsset;
            }
        }
        return null;
    }
}
