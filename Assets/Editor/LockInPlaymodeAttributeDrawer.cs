﻿namespace bugiflo.Editor
{
    using UnityEditor;
    using UnityEngine;

    /// <summary>
    /// Draws peroperties with the <see cref="LockInPlaymodeAttribute"/>.
    /// </summary>
    [CustomPropertyDrawer(typeof(LockInPlaymodeAttribute))]
    public class LockInPlaymodeAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (Application.isPlaying && GUI.enabled)
            {
                GUI.enabled = false;
                EditorGUI.PropertyField(position, property, label, true);
                GUI.enabled = true;
            }
            else
            {
                EditorGUI.PropertyField(position, property, label, true);
            }
        }
    }
}