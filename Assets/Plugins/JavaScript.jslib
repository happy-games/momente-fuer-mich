var plugin = {
    VideoStart: function()
    {
        startVideo();
    }
};

mergeInto(LibraryManager.library, plugin);

var stop = {
    VideoStop: function()
    {
        stopVideo();
    }
};

mergeInto(LibraryManager.library, stop);