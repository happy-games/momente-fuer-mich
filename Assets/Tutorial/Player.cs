namespace Tutorial
{
    using UnityEngine;
    using UnityEngine.Events;

    public class Player : MonoBehaviour, IDamageable
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField]
        private MaxHealth maxHealth = default;

        [Space, SerializeField]
        private UnityEvent<int, int> onHealthUpdate;

        public event UnityAction OnDeath
        {
            add => onDeath.AddListener(value);
            remove => onDeath.RemoveListener(value);
        }

        [SerializeField]
        private UnityEvent onDeath;  


        // --- | Varaibles | -----------------------------------------------------------------------------------------------

        private int currentHealth = 0;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Start()
        {
            currentHealth = maxHealth.Value;
        }

        /// <inheritdoc/>
        /// <exception cref="System.ArgumentOutOfRangeException">The amount must be a value above 0.</exception>
        public void TakeDamage(int amount)
        {
            if (amount <= 0)
            {
                throw new System.ArgumentOutOfRangeException(nameof(amount), amount, "The damage to deal must be greather than 0.");
            }
            currentHealth -= amount;
            if (currentHealth <= 0)
            {
                onHealthUpdate?.Invoke(0, maxHealth.Value);
                Death();
            }
            else
            {
                onHealthUpdate?.Invoke(currentHealth, maxHealth.Value);
            }
        }
        
        /// <summary>
        /// Kills the player.
        /// </summary>
        private void Death()
        {
            currentHealth = 0;
            onDeath?.Invoke();
            Destroy(gameObject);
        }
    }
}
