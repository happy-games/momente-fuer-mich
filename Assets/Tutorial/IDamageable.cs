namespace Tutorial
{
    public interface IDamageable
    {
        /// <summary>
        /// Deaals damage to the object.
        /// </summary>
        /// <param name="amount">The amount of damage to deal.</param>
        void TakeDamage(int amount);
    }
}
