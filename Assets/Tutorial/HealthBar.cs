namespace Tutorial
{
    using UnityEngine;

    public class HealthBar : MonoBehaviour
    {
        /// <summary>
        /// Updates the health bars values.
        /// </summary>
        /// <param name="current">The current health value.</param>
        /// <param name="max">The maximum health value.</param>
        public void UpdateBar(int current, int max) => Debug.Log($"{ current } / { max }");
    }
}
