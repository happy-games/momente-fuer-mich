namespace Tutorial
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "SOTest", menuName = "Test/ScriptableObject")]
    public class MaxHealth : ScriptableObject
    {
        [SerializeField, LockInPlaymode, Min(1), Tooltip("The maximum health value.")]
        private int value = 5;
        /// <summary>
        /// The maximum health value.
        /// </summary>
        public int Value => value;
    }
}
