namespace Tutorial
{
    using UnityEngine;

    public class Spike : MonoBehaviour
    {
        // --- | Methods | -------------------------------------------------------------------------------------------------

        [SerializeField, Min(1), Tooltip("The damage the spike deals.")]
        private int damage = 1;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        // The "OnTriggerEnter(Collider2D)" method is called when a trigger 
        // collider on the object is overlapping with another collider.
        private void OnTriggerEnter2D(Collider2D collision)
        {
            // Find the IDamageable component on the colliding object.
            IDamageable damageable = collision.GetComponent<IDamageable>();
            // Check if there was a IDamageable component.
            if (damageable != null)
            {
                // Dealt damage to the object.
                damageable.TakeDamage(damage);
            }
        }
    }
}
