using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class PostproFade : MonoBehaviour
{
    public Volume postpro;
    public float TimeToFade = 2f;
    private float timer = 0, step = 0;
    private bool fade = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (fade)
        {
            postpro.weight += step*Time.deltaTime;

            if(postpro.weight <= 0)
            {
                postpro.weight = 0;
                fade = false;
                postpro.gameObject.SetActive(false);
            } if(postpro.weight >= 1)
            {
                postpro.weight = 1;
                fade = false;
            }
        }
    }

    public void FadeIn()
    {
        postpro.gameObject.SetActive(true);
        postpro.weight = 0;
        fade = true;
        step = 1 / TimeToFade;
    }
    public void FadeOut()
    {
        fade = true;
        postpro.weight = 1;
        step = (1 / TimeToFade)*-1;
    }
}
