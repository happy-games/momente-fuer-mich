const video = document.getElementById('video')
var tracking = false;
const threshold = []
threshold[0] = 1 //NEUTRAL
threshold[1] = 0.001 //ANGRY
threshold[2] = 0.9 //HAPPY
threshold[3] = 0.1 //SAD
threshold[4] = 0.5 //SURPRISED

Promise.all([
  faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
  faceapi.nets.faceLandmark68Net.loadFromUri('/models'),
  faceapi.nets.faceRecognitionNet.loadFromUri('/models'),
  faceapi.nets.faceExpressionNet.loadFromUri('/models')
]).then()

function startVideo() {
  tracking = true;
  navigator.mediaDevices.getUserMedia({ video: {} })
.then(function(stream) {
  video.srcObject = stream;
})
.catch(function(err) {
  err => console.error(err);
});
}

function stopVideo() {
  tracking = false;
  /*var stream = video.srcObject;
  var tracks = stream.getTracks();

  for (var i = 0; i < tracks.length; i++) {
    var track = tracks[i];
    track.stop();
  }

  video.srcObject = null;*/
};

video.addEventListener('play', () => {
  setInterval(async () => {
    if(tracking){
      const detections = await faceapi.detectSingleFace(video, new faceapi.TinyFaceDetectorOptions()).withFaceExpressions()
      if(detections != null){
        console.log("sending Expression")
        myGameInstance.SendMessage('EmotionTracker', 'MaxEmotionDeliveryFunction',maxEmotion(detections["expressions"]));
      }
    }
  }, 500)
})

function maxEmotion(emotions)
{
    var max = 0.0;
    var index = 0;
    console.log("neutral = " +emotions["neutral"])
    console.log("angry = " +emotions["angry"])
    console.log("happy = " +emotions["happy"])
    console.log("sad = " +emotions["sad"])
    console.log("surprised = " +emotions["surprised"])
    var emo = [emotions["neutral"], emotions["angry"], emotions["happy"], emotions["sad"],emotions["surprised"]];
    for (var i = 0; i < emo.length; i++)
    {
        if (emo[i] > max && emo[i] > threshold[i])
        {
            max = emo[i];
            index = i;

        }
    }
    return index;

}