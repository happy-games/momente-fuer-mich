## Implement API with Unity WebGL build

1. copy the two scripts and the models folder to the same dir as the index.html

2. copy under end of script tag in index.html (</script>)

        <script defer src="face-api.min.js"></script>
        <script defer src="script.js"></script>
        <video id="video" width="0" height="0" autoplay muted></video>

3. copy to the beginning of the index.html

        var myGameInstance = null;

4. copy into createUnityInstance().then(*paste here*)

        myGameInstance = unityInstance;